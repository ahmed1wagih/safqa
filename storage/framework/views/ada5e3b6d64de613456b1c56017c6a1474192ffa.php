<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>مدراء الدول</li>
        <?php if(Request::is('super_admin/users/active')): ?>
            <li class="active">فعال</li>
        <?php else: ?>
            <li class="active">موقوف</li>
        <?php endif; ?>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/user/create"><button type="button" class="btn btn-info"> أضف مدير جديد </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الدولة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الهاتف</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($admin->country->ar_name); ?></td>
                                        <td><?php echo e($admin->name); ?></td>
                                        <td><?php echo e($admin->email); ?></td>
                                        <td><?php echo e($admin->phone); ?><br/>
                                        <td>
                                            <a href="/super_admin/user/<?php echo e($admin->id); ?>/edit"><button class="btn btn-condensed btn-warning" title="تعديل"><i class="fa fa-edit"></i></button></a>
                                        <?php if($admin->status == 'active'): ?>
                                                <button class="btn btn-primary btn-condensed mb-control" onclick="modal_suspend(<?php echo e($admin->id); ?>)" data-box="#message-box-primary" title="توقيف"><i class="fa fa-minus-circle"></i></button>
                                            <?php else: ?>
                                                <button class="btn btn-success btn-condensed mb-control" onclick="modal_activate(<?php echo e($admin->id); ?>)" data-box="#message-box-success" title="تفعيل"><i class="fa fa-check-square"></i></button>
                                            <?php endif; ?>
                                            <button class="btn btn-danger btn-condensed mb-control" onclick="modal_destroy(<?php echo e($admin->id); ?>)" data-box="#message-box-danger" title="حذف"><i class="fa fa-trash-o"></i></button>                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <?php echo e($admins->links()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- success with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert" id="message-box-success">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك التفعيل,و سيتمكن المدير من الدخول إلي لوحة التحكم,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/super_admin/user/change_status" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_activate" value="">
                        <input type="hidden" name="status" value="active">
                        <button class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end success with sound -->

    <!-- warning with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert" id="message-box-primary">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك التوقيف,لن يتمكن المدير من الدخول إلي لوحة التحكم,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/super_admin/user/change_status" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_suspend" value="">
                        <input type="hidden" name="status" value="suspended">
                        <button class="btn btn-primary btn-lg pull-right">توقيف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end warning with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert" id="message-box-danger">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك الحذف,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/super_admin/user/delete" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_destroy" value="">
                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>