<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات </li>
        <li class="active">تعديل الإعدادات العامة</li>
    </ul>
    <!-- END BREADCRUMB -->
    <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/settings/settings/update">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('ar_name') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">إسم الموقع بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="ar_name" value="<?php echo e($settings->ar_name); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'ar_name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('en_name') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">إسم الموقع بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="en_name" value="<?php echo e($settings->en_name); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'en_name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('ar_rights') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">حقوق الموقع بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="ar_rights" value="<?php echo e($settings->ar_rights); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'ar_rights'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('en_rights') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">حقوق الموقع بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="en_rights" value="<?php echo e($settings->en_rights); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'en_rights'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('credit_on_register') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">النقاط المجانية عند التسجيل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="number" class="form-control" name="credit_on_register" value="<?php echo e($settings->credit_on_register); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'credit_on_register'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('delegate_fee') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">النسبة المؤية لقيمة بيع الصفقة بالنقاط</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="number" class="form-control" name="delegate_fee" value="<?php echo e($settings->delegate_fee); ?>"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'delegate_fee'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                              تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>