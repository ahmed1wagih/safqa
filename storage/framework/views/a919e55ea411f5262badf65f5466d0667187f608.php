<!DOCTYPE html>
<!--[if lt IE 9 ]> <html lang="ar" dir="rtl" class="no-js ie-old"> <![endif]-->
<!--[if IE 9 ]> <html lang="ar" dir="rtl" class="no-js ie9"> <![endif]-->
<!--[if IE 10 ]> <html lang="ar" dir="rtl" class="no-js ie10"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en" dir="rtl" class="no-js">
<!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- META TAGS                                 -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile specific meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- PAGE TITLE                                -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <?php if(App::getLocale() == 'ar'): ?>
        <title><?php echo e($settings->ar_name); ?></title>
    <?php else: ?>
        <title><?php echo e($settings->en_name); ?></title>
    <?php endif; ?>
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- SEO METAS                                 -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="description" content="Best Deal Market">
    <meta name="keywords" content="insert, keywords, here">
    <meta name="robots" content="index, follow">
    <meta name="author" content="CODASTROID">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- PAGE FAVICON                              -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="apple-touch-icon" href="<?php echo e(asset('web/images/favicon/apple-touch-icon.png')); ?>">
    <link rel="icon" href="<?php echo e(asset('web/images/favicon/favicon.png')); ?>">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- GOOGLE FONTS                              -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=El+Messiri:700" rel="stylesheet">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- Include CSS Filess                        -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->



    <?php if(App::getLocale() == 'ar'): ?>
        <link href="<?php echo e(asset('web/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/bootstrap-rtl.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/base.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/style.css')); ?>" rel="stylesheet">
    <?php else: ?>
        <link href="<?php echo e(asset('web/css/en/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/en/bootstrap-rtl.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/en/base.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('web/css/en/style.css')); ?>" rel="stylesheet">
    <?php endif; ?>

    <!-- Font Awesome -->
    <link href="<?php echo e(asset('web/vendors/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <!-- Linearicons -->
    <link href="<?php echo e(asset('web/vendors/linearicons/css/linearicons.css')); ?>" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo e(asset('web/vendors/owl-carousel/owl.carousel.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('web/vendors/owl-carousel/owl.theme.min.css')); ?>" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="<?php echo e(asset('web/vendors/flexslider/flexslider.css')); ?>" rel="stylesheet">
    <!-- Template Stylesheet -->

    <script type="text/javascript" src="<?php echo e(asset('web/js/jquery-1.12.3.min.js')); ?>"></script>
</head>

    <body id="body" class="wide-layout ">
    <div id="preloader" class="preloader">
        <div class="loader-cube">
            <div class="loader-cube__item1 loader-cube__item"></div>
            <div class="loader-cube__item2 loader-cube__item"></div>
            <div class="loader-cube__item4 loader-cube__item"></div>
            <div class="loader-cube__item3 loader-cube__item"></div>
        </div>
    </div>
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- WRAPPER                                   -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <div id="pageWrapper" class="page-wrapper">
        <!-- –––––––––––––––[ HEADER ]––––––––––––––– -->
        <header id="mainHeader" class="main-header">
            <!-- Top Bar -->
            <div class="top-bar bg-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5 is-hidden-sm-down">
                            <ul class="nav-top nav-top-left list-inline t-right">

                                <li><a href="/terms"><i class="fa fa-question-circle"></i> <?php echo e(trans('trans.terms')); ?> </a> </li>
                                <li><a href="/faqs"><i class="fa fa-support"></i><?php echo e(trans('trans.q&a')); ?></a> </li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <ul class="nav-top nav-top-right list-inline t-xs-center t-md-left">
                                <?php if(!user()): ?>
                                    <li> <a href="#"><img src="/addresses/<?php echo e(country()->image); ?>" style="width: 16px; height: 11px; margin: 2px;"><?php echo e(lang() == 'ar' ? country()->ar_name : country()->en_name); ?>

                                            <i class="fa fa-caret-down"></i></a>
                                        <ul>
                                            <?php $__currentLoopData = $all_countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($country->id != country()->id): ?>
                                                    <li><a href="/change_country/<?php echo e($country->id); ?>"><img src="/addresses/<?php echo e($country->image); ?>" style="width: 16px; height: 11px; margin: 2px;"><?php echo e($country->name); ?></a></li>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                                <li> <a href="#"><i class="fa fa-map-marker"></i> <?php echo e(lang() == 'ar' ? city()->ar_name : city()->en_name); ?></a>
                                    <ul>
                                        <?php $__currentLoopData = country()->active_cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($city->id != city()->id): ?>
                                                <li>
                                                    <a href="/change_city/<?php echo e($city->id); ?>"><i class="fa fa-map-marker"></i>
                                                    <?php echo e(lang() == 'ar' ? $city->ar_name : $city->en_name); ?>

                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-globe"></i>
                                        <?php if(App::getLocale() == 'ar'): ?>
                                            العربية
                                        <?php else: ?>
                                            English
                                        <?php endif; ?>
                                    <i></i>
                                    <ul>
                                        <?php $__currentLoopData = Config::get('languages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lang => $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($lang != App::getLocale()): ?>
                                                <a href="<?php echo e(route('lang.switch', $lang)); ?>"><i class="fa fa-globe fa-2x font_awesome"></i><?php echo e($language); ?></a>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </ul>
                                </li>

                                <?php if(user()): ?>
                                    <li><a href="/logout"><i class="fa fa-sign-out"></i><?php echo e(trans('trans.logout')); ?></a> </li>
                                <?php else: ?>
                                    <li><a href="/login"><i class="fa fa-lock"></i><?php echo e(trans('trans.login')); ?></a> </li>
                                    <li><a href="/register"><i class="fa fa-user"></i><?php echo e(trans('trans.register')); ?></a> </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Bar -->
            <!-- Header Header -->
            <div class="header-header bg-white">
                <div class="container">
                    <div class="row row-rl-0 row-tb-20 row-md-cell">
                        <div class="brand col-md-3 t-xs-center t-md-right valign-middle"> <a href="/" class="logo"> <img src="<?php echo e(asset('web/images/logo_'. lang() .'.jpg')); ?>" alt="" style="height: 90px;width: 185px;"> </a> </div>
                        <div class="header-search col-md-9">
                            <div class="row row-tb-10 ">
                                <div class="col-sm-8">
                                    <form class="search-form" action="/search" method="get">
                                        
                                        <div class="input-group">
                                            <input type="text" name="text" class="form-control input-lg search-input" value="<?php echo e(old('text')); ?>" placeholder="<?php echo e(trans('trans.enter_word_search')); ?>">
                                            <div class="input-group-btn">
                                                <div class="input-group">
                                                    <select class="form-control input-lg search-select" name="cat_id">
                                                        <option selected disabled><?php echo e(trans('trans.pick_cat')); ?></option>
                                                        <?php $__currentLoopData = $all_global_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($cat->id); ?>">
                                                                <?php if(App::getLocale() == 'ar'): ?>
                                                                    <?php echo e($cat->ar_name); ?>

                                                                <?php else: ?>
                                                                    <?php echo e($cat->en_name); ?>

                                                                <?php endif; ?>
                                                            </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-lg btn-search btn-block"> <i class="fa fa-search font-16"></i> </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php if(Auth::user()): ?>
                                    <div class="col-sm-4 t-xs-center t-md-left">
                                        <div class="header-cart"> <a href="/cart"> <span class="icon lnr lnr-cart"></span> </a> </div>
                                        <div class="header-wishlist mr-20"> <a href="/wish_list"> <span class="icon lnr lnr-heart font-30"></span> </a> </div>
                                        <div class="header-wishlist mr-20"> <a href="/profile"> <span class="icon lnr lnr-user font-30"></span> </a> </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Header Header -->
            <!-- Header Menu -->
            <div class="header-menu bg-blue">
                <div class="container">
                    <nav class="nav-bar">
                        <div class="nav-header"> <span class="nav-toggle" data-toggle="#header-navbar"> <i></i> <i></i> <i></i> </span> </div>
                        <div id="header-navbar" class="nav-collapse">
                            <ul class="nav-menu">
                                <?php $__currentLoopData = $global_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="dropdown-mega-menu"> <a href="/category/<?php echo e($cat->id); ?>/all">
                                            <?php if(App::getLocale() == 'ar'): ?>
                                                <?php echo e($cat->ar_name); ?>

                                            <?php else: ?>
                                                <?php echo e($cat->en_name); ?>

                                            <?php endif; ?>
                                        </a>
                                    <div class="mega-menu">
                                        <div class="row row-v-10">
                                            <div class="col-md-3">
                                                <ul>
                                                    <?php $__currentLoopData = $cat->global_sub_cats($cat->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li><a href="/category/<?php echo e($sub->parent->id); ?>/<?php echo e($sub->id); ?>">
                                                                <?php if(App::getLocale() == 'ar'): ?>
                                                                    <?php echo e($sub->ar_name); ?>

                                                                <?php else: ?>
                                                                    <?php echo e($sub->en_name); ?>

                                                                <?php endif; ?>
                                                            </a> </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                            <?php $__currentLoopData = $cat->home_products($cat->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <a href="/product/<?php echo e($product->id); ?>">
                                                    <div class="col-md-3">
                                                    <figure class="deal-thumbnail embed-responsive embed-responsive-4by3" style='height: auto; width: auto; object-fit: contain' data-bg-img="<?php echo e(asset('products/'.$product->cover->image)); ?>">
                                                        <div class="label-discount top-10 left-10"><?php echo e(trans('trans.exclusive')); ?></div>
                                                        <div class="deal-about p-10 pos-a bottom-0 right-0">



                                                            <h6 class="deal-title mb-10"> <a href="/product/<?php echo e($product->id); ?>" class="color-lighter">
                                                                    <?php if(App::getLocale() == 'ar'): ?>
                                                                        <?php echo e($product->ar_title); ?>

                                                                    <?php else: ?>
                                                                        <?php echo e($product->en_title); ?>

                                                                    <?php endif; ?>
                                                                </a> </h6>
                                                        </div>
                                                    </figure>
                                                </div>
                                                </a>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- End Header Menu -->
        </header>
        <!-- –––––––––––––––[ HEADER ]––––––––––––––– -->

        <?php echo $__env->make('web.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

