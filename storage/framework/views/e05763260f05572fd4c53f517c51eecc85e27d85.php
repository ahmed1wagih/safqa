<?php $__env->startSection('content'); ?>
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-10">
            <div class="container">
                <div class="section deals-header-area ptb-30">
                    <div class="row row-tb-20">
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <div class="header-deals-slider owl-slider" data-loop="true" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false" data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1" data-lg-nav="true">
                                <?php $__currentLoopData = $specials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="deal-single panel item">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="<?php echo e(asset('/products/'.$product->cover->image)); ?>">
                                        <div class="deal-about p-20 pos-a bottom-0 right-0">
                                            <div class="rating mb-10"> <span class="rating-stars" data-rating="<?php echo e($product->get_rate($product->id)); ?>"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews color-light"> ( <span class="rating-count"><?php echo e($product->get_rate($product->id)); ?></span> <?php echo e(trans('trans.rates')); ?> ) </span> </div>
                                            <h3 class="deal-title mb-10 "> <a href="/product/<?php echo e($product->id); ?>" class="color-light color-h-lighter">
                                                    <?php if(App::getLocale() == 'ar'): ?>
                                                        <?php echo e($product->ar_title); ?>

                                                    <?php else: ?>
                                                        <?php echo e($product->en_title); ?>

                                                    <?php endif; ?>
                                                </a> </h3>
                                        </div>
                                    </figure>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            <?php $__currentLoopData = $side_specials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $side): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a href="<?php echo e($side->link); ?>" title="<?php echo e($side->link); ?>"><div class="deal-single panel panel2">
                                    <figure class="deal-thumbnail2 deal-thumbnail embed-responsive embed-responsive-16by9"  data-bg-img="<?php echo e(asset('/sides/'.$side->image)); ?>" style="background-image: url('/public/sides/<?php echo e($side->image); ?>');"></figure>
                                </div></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <style>
                            .deal-thumbnail2{
                                height: 239px !important;
                            }
                            .panel2{
                                border-radius: 0px !important;
                            }
                        </style>
                    </div>
                </div>

                <section class="section latest-deals-area ptb-30">
                    <header class="panel ptb-15 prl-20 pos-r mb-30">
                        <h3 class="section-title font-18"><?php echo e(trans('trans.catch_or_not')); ?></h3>
                        <a href="/limited" class="btn btn-o btn-xs pos-a left-10 pos-tb-center"><?php echo e(trans('trans.see_all')); ?></a> </header>
                    <div class="row row-masnory row-tb-20">
                        <?php $__currentLoopData = $limited_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-sm-6 col-lg-4">
                                <div class="deal-single panel">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="<?php echo e(asset('products/'.$product->cover->image)); ?>">
                                        <div class="time-right bottom-15 left-20 font-md-14">
                                            <span>
                                                <h5><?php echo e(trans('trans.exclusive')); ?></h5>
                                            </span>
                                        </div>

                                        <div class="time-right bottom-15 -20 font-md-14"> <span> <i class="ico fa fa-clock-o ml-10"></i> <span class="t-uppercase" data-countdown="<?php echo e($product->expire_at); ?>"></span> </span> </div>
                                    </figure>
                                    <div class="bg-white pt-20 pr-20 pl-15">
                                        <div class="pl-md-10">
                                            <div class="rating mb-10"> <span class="rating-stars" data-rating="<?php echo e($product->get_rate($product->id)); ?>"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews">  </span> </div>
                                            <h3 class="deal-title mb-10">
                                                <?php if(App::getLocale() == 'ar'): ?>
                                                    <a href="/product/<?php echo e($product->id); ?>" title="<?php echo e($product->ar_title); ?>">
                                                        <?php echo e(mb_substr($product->ar_title,0,32)); ?>

                                                    </a>
                                                <?php else: ?>
                                                    <a href="/product/<?php echo e($product->id); ?>" title="<?php echo e($product->en_title); ?>">
                                                        <?php echo e(mb_substr($product->ar_title,0,32)); ?>

                                                    </a>
                                                <?php endif; ?>
                                            </h3>
                                            <ul class="deal-meta list-inline mb-10 color-mid">
                                                <li><i class="ico fa fa-map-marker ml-10"></i>
                                                    <?php if(App::getLocale() == 'ar'): ?>
                                                        <?php echo e($product->city->ar_name); ?>

                                                    <?php else: ?>
                                                        <?php echo e($product->city->en_name); ?>

                                                    <?php endif; ?>
                                                </li>
                                                <li><span style="color: #36c341"> <?php echo e(trans('trans.discount')); ?> </span><?php echo e($product->get_discount($product->id)); ?>%</li>
                                            </ul>
                                            <p class="text-muted mb-20"></p>
                                        </div>
                                        <div class="deal-price pos-r mb-15">
                                            <h3 class="price ptb-5 "><span class="price-sale"> <?php echo e($product->old_price); ?> - <?php echo e($product->city->parent->ar_currency); ?></span> <?php echo e($product->new_price); ?> - <?php echo e($product->city->parent->ar_currency); ?></h3>
                                        </div>
                                        <ul class="deal-meta list-inline mb-10 color-mid">

                                            <?php if($product->expire_at >= \Carbon\Carbon::today()): ?>
                                                <li><a href="/cart/store/<?php echo e($product->id); ?>" class="btn btn-lg btn-rounded ml-10"><?php echo e(trans('trans.buy_deal')); ?></a></li>
                                            <?php else: ?>
                                                <li><a class="btn btn-lg btn-rounded ml-10" disabled><?php echo e(trans('trans.deal_expired')); ?></a></li>
                                            <?php endif; ?>

                                            <li> <h3 class="price ptb-5 " style="color: green;">
                                                    <?php if($product->deal_price == 1): ?>
                                                        <?php echo e(trans('trans.one_point')); ?>

                                                    <?php elseif($product->deal_price == 2): ?>
                                                        <?php echo e(trans('trans.two_points')); ?>

                                                    <?php elseif($product->deal_price > 2 && $product->deal_price <11): ?>
                                                        <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.points')); ?>

                                                    <?php else: ?>
                                                        <?php if(App::getLocale() == 'ar'): ?>
                                                            <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.point')); ?>

                                                        <?php else: ?>
                                                            <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.points')); ?>

                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </h3></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </section>

                <?php $__currentLoopData = $all_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <section class="section latest-deals-area ptb-30">
                        <?php if(1 == 2): ?>
                        <div class="col-md-12 col-sm-12 col-lg-12" style="margin-bottom: 30px">
                            <a href="<?php echo e($cat->banner->link); ?>"><img src="/public/banners/<?php echo e($cat->banner->image); ?>" style="height: <?php echo e($cat->banner->height); ?>px !important;  width : <?php echo e($cat->banner->width); ?>px !important;"> </a>
                        </div>
                        <?php endif; ?>
                            <header class="panel ptb-15 prl-20 pos-r mb-30">
                                <h3 class="section-title font-18">
                                    <?php if(App::getLocale() == 'ar'): ?>
                                        <?php echo e($cat->ar_name); ?>

                                    <?php else: ?>
                                        <?php echo e($cat->en_name); ?>

                                    <?php endif; ?>
                                </h3>
                                <a href="/category/<?php echo e($cat->id); ?>/all" class="btn btn-o btn-xs pos-a left-10 pos-tb-center"><?php echo e(trans('trans.see_all')); ?></a>
                            </header>
                            <div class="row row-masnory row-tb-20">
                                <?php $__currentLoopData = $cat->home_products($cat->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="deal-single panel">
                                            <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="<?php echo e(asset('/products/'.$product->cover->image)); ?>">
                                                <div class="time-right bottom-15 left-20 font-md-14">
                                            <span>
                                                <h5><?php echo e(trans('trans.exclusive')); ?></h5>
                                            </span>
                                                </div>
                                                <div class="time-right bottom-15 left-20 font-md-14"> <span> <i class="ico fa fa-clock-o ml-10"></i> <span class="t-uppercase" data-countdown="<?php echo e($product->expire_at); ?>"></span></span> </div>
                                            </figure>
                                            <div class="bg-white pt-20 pr-20 pl-15">
                                                <div class="pl-md-10">
                                                    <div class="rating mb-10"> <span class="rating-stars" data-rating="<?php echo e($product->get_rate($product->id)); ?>"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews">  </span> </div>
                                                    <h3 class="deal-title mb-10">
                                                        <?php if(App::getLocale() == 'ar'): ?>
                                                            <a href="/product/<?php echo e($product->id); ?>" title="<?php echo e($product->ar_title); ?>">
                                                                <?php echo e(mb_substr($product->ar_title,0,32)); ?>

                                                            </a>
                                                        <?php else: ?>
                                                            <a href="/product/<?php echo e($product->id); ?>" title="<?php echo e($product->en_title); ?>">
                                                                <?php echo e(mb_substr($product->ar_title,0,32)); ?>

                                                            </a>
                                                        <?php endif; ?>
                                                    </h3>
                                                    <ul class="deal-meta list-inline mb-10 color-mid">
                                                        <li><i class="ico fa fa-map-marker ml-10"></i>
                                                            <?php if(App::getLocale() == 'ar'): ?>
                                                                <?php echo e($product->city->ar_name); ?>

                                                            <?php else: ?>
                                                                <?php echo e($product->city->en_name); ?>

                                                            <?php endif; ?></li>
                                                        <li><span style="color: #36c341"> <?php echo e(trans('trans.discount')); ?> </span><?php echo e($product->get_discount($product->id)); ?>%</li>
                                                    </ul>
                                                    <p class="text-muted mb-20"></p>
                                                </div>
                                                <div class="deal-price pos-r mb-15">
                                                        <h3 class="price ptb-5 "><span class="price-sale"> <?php echo e($product->old_price); ?> - <?php echo e($product->city->parent->ar_currency); ?></span> <?php echo e($product->new_price); ?> - <?php echo e($product->city->parent->ar_currency); ?></h3>
                                                </div>
                                                <ul class="deal-meta list-inline mb-10 color-mid">

                                                    <?php if($product->expire_at >= \Carbon\Carbon::today()): ?>
                                                        <li><a href="/cart/store/<?php echo e($product->id); ?>" class="btn btn-lg btn-rounded ml-10"><?php echo e(trans('trans.buy_deal')); ?></a></li>
                                                    <?php else: ?>
                                                        <li><a class="btn btn-lg btn-rounded ml-10" disabled><?php echo e(trans('trans.deal_expired')); ?></a></li>
                                                    <?php endif; ?>

                                                    <li> <h3 class="price ptb-5" style="color: green;">
                                                            <?php if($product->deal_price == 1): ?>
                                                                <?php echo e(trans('trans.one_point')); ?>

                                                            <?php elseif($product->deal_price == 2): ?>
                                                                <?php echo e(trans('trans.two_points')); ?>

                                                            <?php elseif($product->deal_price > 2 && $product->deal_price <11): ?>
                                                                <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.points')); ?>

                                                            <?php else: ?>
                                                                <?php if(App::getLocale() == 'ar'): ?>
                                                                    <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.point')); ?>

                                                                <?php else: ?>
                                                                    <?php echo e($product->deal_price); ?> <?php echo e(trans('trans.points')); ?>

                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </h3>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                    </section>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>