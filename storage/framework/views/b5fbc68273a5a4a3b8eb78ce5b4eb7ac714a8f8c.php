<?php $__env->startSection('content'); ?>
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li class="active">
            الملف الشخصي
        </li>
    </ul>
    <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2 class="switch_order">الملف الشخصي</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-5">

                <form action="#" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><?php echo e(super()->name); ?></h3>
                            <p><span class="label label-info label-form">مدير عام</span></p>
                            <div class="text-center" id="user_image">
                                <img src="/users/default.png" class="img-thumbnail"/>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 col-sm-8 col-xs-7">

                <form action="/super_admin/profile/update" id="jvalidate" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الإسم</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="name" value="<?php echo e(super()->name); ?>" class="form-control"/>
                                </div>
                                <?php echo $__env->make('super_admin.layouts.error',['input' => 'name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">البريد الإلكتروني</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="email" name="email" id="email" value="<?php echo e(super()->email); ?>" class="form-control"/>
                                </div>
                                <?php echo $__env->make('super_admin.layouts.error',['input' => 'email'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الهاتف</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="phone" value="<?php echo e(super()->phone); ?>" class="form-control"/>
                                </div>
                                <?php echo $__env->make('super_admin.layouts.error',['input' => 'phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">كلمة المرور</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="إتركه فارغاً إذا لم يكن هناك تعديل"/>
                                </div>
                                <?php echo $__env->make('super_admin.layouts.error',['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="إتركه فارغاً إذا لم يكن هناك تعديل"/>
                                </div>
                                <?php echo $__env->make('super_admin.layouts.error',['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-5">
                                    <button class="btn btn-primary <?php echo e(lang() == 'ar' ? 'pull-left' : 'pull-right'); ?>">تحديث</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3>بيانات سريعة</h3>
                    </div>
                    <div class="panel-body form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">آخر تحديث</label>
                            <div class="col-md-8 col-xs-7 line-height-30"><?php echo e(\Carbon\Carbon::parse(super()->updated_at)->format('m-d-Y')); ?><br/><?php echo e(\Carbon\Carbon::parse(super()->updated_at)->format('h:s a')); ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">تاريخ التسجيل</label>
                            <div class="col-md-8 col-xs-7 line-height-30"><?php echo e(\Carbon\Carbon::parse(super()->created_at)->format('m-d-Y')); ?><br/><?php echo e(\Carbon\Carbon::parse(super()->created_at)->format('h:s a')); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">
        setTimeout(
            function() {
                $('#email').val('<?php echo e(super()->email); ?>');
                $(':password').val('');
                },
            2000  //1,000 milliseconds = 1 second
        );
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>