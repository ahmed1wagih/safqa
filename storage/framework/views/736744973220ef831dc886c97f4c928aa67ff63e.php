<?php $__env->startSection('content'); ?>
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title"><?php echo e(trans('trans.apply_as_merchant')); ?></h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-12 col-right">
                            <form class="p-40" method="post" action="/merchant/apply">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.ar_name')); ?></label>
                                    <input type="text" name="ar_name" class="form-control input-lg" placeholder="<?php echo e(trans('trans.ar_name')); ?>" value="<?php echo e(old('ar_name')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'ar_name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.en_name')); ?></label>
                                    <input type="text" name="en_name" class="form-control input-lg" placeholder="<?php echo e(trans('trans.en_name')); ?>" value="<?php echo e(old('en_name')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'en_name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.ar_desc')); ?></label>
                                    <textarea name="ar_desc" class="form-control input-lg" placeholder="<?php echo e(trans('trans.ar_desc')); ?>" rows="5"><?php echo e(old('ar_desc')); ?></textarea>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'ar_desc'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.en_desc')); ?></label>
                                    <textarea name="en_desc" class="form-control input-lg" placeholder="<?php echo e(trans('trans.en_desc')); ?>" rows="5"><?php echo e(old('en_desc')); ?></textarea>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'en_desc'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.ar_address')); ?></label>
                                    <textarea name="ar_address" class="form-control input-lg" placeholder="<?php echo e(trans('trans.ar_address')); ?>" rows="5"><?php echo e(old('ar_address')); ?></textarea>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'ar_address'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.en_address')); ?></label>
                                    <textarea name="en_address" class="form-control input-lg" placeholder="<?php echo e(trans('trans.en_address')); ?>" rows="5"><?php echo e(old('en_address')); ?></textarea>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'en_address'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.owner')); ?></label>
                                    <input type="text" name="owner" class="form-control input-lg" placeholder="<?php echo e(trans('trans.owner')); ?>" value="<?php echo e(old('owner')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'owner'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.owner_phone')); ?></label>
                                    <input type="text" name="owner_phone" class="form-control input-lg" placeholder="<?php echo e(trans('trans.owner_phone')); ?>" value="<?php echo e(old('owner_phone')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'owner_phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.in_charge')); ?></label>
                                    <input type="text" name="in_charge" class="form-control input-lg" placeholder="<?php echo e(trans('trans.in_charge')); ?>" value="<?php echo e(old('in_charge')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'in_charge'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.in_charge_phone')); ?></label>
                                    <input type="text" name="in_charge_phone" class="form-control input-lg" placeholder="<?php echo e(trans('trans.in_charge_phone')); ?>" value="<?php echo e(old('in_charge_phone')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'in_charge_phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.email')); ?></label>
                                    <input type="email" name="email" class="form-control input-lg" placeholder="<?php echo e(trans('trans.email')); ?>" value="<?php echo e(old('email')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'email'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>

                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.password')); ?></label>
                                    <input type="password" name="new_password" class="form-control input-lg" placeholder="<?php echo e(trans('trans.password')); ?>" value="<?php echo e(old('new_password')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'new_password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>

                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.password_confirmation')); ?></label>
                                    <input type="password" name="new_password_confirmation" class="form-control input-lg" placeholder="<?php echo e(trans('trans.password_confirmation')); ?>" value="<?php echo e(old('new_password_confirmation')); ?>" required>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'new_password_confirmation'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.type')); ?></label>
                                    <select name="type" id="type_select" class="form-control input-lg" required>
                                        <option selected disabled><?php echo e(trans('trans.choose_from_below')); ?></option>
                                        <option value="local" <?php echo e(old('type') == 'local' ? 'selected' : ''); ?>><?php echo e(trans('trans.local')); ?></option>
                                        <option value="online" <?php echo e(old('type') == 'online' ? 'selected' : ''); ?>><?php echo e(trans('trans.online')); ?></option>
                                    </select>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'type'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group online_div" style="display: none;">
                                    <label class="sr-only"><?php echo e(trans('trans.website_link')); ?></label>
                                    <input type="text" name="link" class="form-control input-lg" placeholder="<?php echo e(trans('trans.website_link')); ?>" value="<?php echo e(old('link')); ?>">
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'website_link'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>

                                <div class="form-group local_div" style="display: none;">
                                    <label class="sr-only"><?php echo e(trans('trans.map_location')); ?></label>
                                    <div >
                                        <div id="map" style=" height: 600px;"></div>
                                    </div>
                                    <input type="hidden" value="" id="mylat" name="lat">
                                    <input type="hidden" value="" id="mylng" name="lng">
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'lat'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>

                                <div class="custom-checkbox mb-20">
                                    <input type="checkbox" name="policy" id="agree_terms" required>
                                    <label class="color-mid" for="agree_terms"><?php echo e(trans('trans.i_agree_on')); ?> <a href="/terms/merchants" class="color-green"><?php echo e(trans('trans.merchant_terms')); ?></a> <?php echo e(trans('trans.i_signed')); ?> <?php echo e(trans('trans.and')); ?> <a href="/privacy" class="color-green"><?php echo e(trans('trans.policy')); ?></a>.</label>
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'policy'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <button type="submit" class="btn btn-block btn-lg"><?php echo e(trans('trans.apply')); ?></button>
                            </form>

                        </div>

                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

    <script>
        $('#type_select').on('change', function()
        {
           if($(this).val() === 'local')
           {
               $('.local_div').show();
               $('.online_div').hide();
               console.log('local',$('.online_div').attr('style'));
           }
           else if($(this).val() === 'online')
           {
               $('.online_div').show();
               $('.local_div').hide();
               console.log('online',$('.local_div').attr('style'));
           }
        });
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7136, lng: 46.6753},
                zoom: 14

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = $('#mylat'),
                        mylng = $('#mylng');

                    var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                    mylat.value = pos.lat;
                    mylng.value = pos.lng;

                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        $('#mylat').val(marker.getPosition().lat());
                        $('#mylng').val(marker.getPosition().lng());
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>