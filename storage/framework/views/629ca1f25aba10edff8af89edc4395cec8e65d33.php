<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>إعدادات التطبيق</li>
        <li class="active">إنشر صفقتك</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-body" style="overflow: auto;">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="rtl_th">النص بالعربية</th>
                                    <th class="rtl_th">النص بالإنجليزية</th>
                                    <th class="rtl_th">الإجراء المتخذ</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php echo $submit->ar_text; ?>

                                        </td>
                                        <td>
                                            <?php echo $submit->en_text; ?>

                                        </td>
                                        <td>
                                            <a href="/super_admin/settings/submit/edit" title="تعديل" class="buttons"><button class="btn btn-info btn-condensed"><i class="fa fa-edit"></i></button></a>
                                        </td>
                                    </tr>
                        </tbody>

                        </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>