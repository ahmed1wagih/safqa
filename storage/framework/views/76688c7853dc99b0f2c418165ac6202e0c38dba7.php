<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>المستخدمين</li>
        <li class="active"><?php echo e(isset($user) ? 'تعديل مستخدم' : 'إنشاء مستخدم'); ?></li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="<?php echo e(isset($user) ? '/admin/user/update' : '/admin/user/store'); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <?php echo e(isset($user) ? 'تعديل مستخدم' : 'إنشاء مستخدم'); ?>

                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group <?php echo e($errors->has('type') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">دور المستخدم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <select class="form-control select" name="type" required>
                                            <option disabled selected>إختر الدور من فضلك</option>
                                            <option value="admin">مدير</option>
                                            <option value="delegate">مندوب</option>
                                            <option value="user">مستخدم عادي</option>
                                        </select>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'type'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" class="form-control" name="name" required <?php if(isset($user)): ?> value="<?php echo e($user->name); ?>" <?php else: ?> <?php echo e(old('name')); ?> <?php endif; ?>/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>


                            <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
                                        <input type="text" class="form-control" name="email" required <?php if(isset($user)): ?> value="<?php echo e($user->email); ?>" <?php else: ?> <?php echo e(old('email')); ?> <?php endif; ?>/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'email'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <div id="field">
                                            <input type="text" class="form-control phone" name="phone" required/>
                                        </div>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>



                            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password" required/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password_confirmation" required/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'password_confirmation'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <?php if(isset($user)): ?>
                                <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
                            <?php endif; ?>
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                <?php echo e(isset($user) ? 'تعديل' : 'إضافة'); ?>

                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>