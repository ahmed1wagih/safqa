<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/banners/index">البانرات الإعلامية</a></li>
        <li class="active"><?php if(isset($banner)): ?> تعديل صورة <?php else: ?> إضافة صورة <?php endif; ?></li>
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" <?php if(isset($banner)): ?> action="/admin/banner/update" <?php else: ?> action="/admin/banner/store" <?php endif; ?> enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                 <?php echo e(isset($banner) ? 'تعديل صورة': 'إضافة صورة'); ?>

                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('link') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">لينك التحويل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="link" value="<?php echo e(isset($banner) ? $banner->link : old('link')); ?>" style="direction: ltr;" required/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'link'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>


                            <div class="form-group <?php echo e($errors->has('height') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">إرتفاع البانر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-arrows-v"></span></span>
                                        <input type="number" step="1" class="form-control" name="height" value="<?php echo e(isset($banner) ? $banner->height : old('height')); ?>" required/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'height'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('width') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">عرض البانر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-arrows-v"></span></span>
                                        <input type="number" step="1" class="form-control" name="width" value="<?php echo e(isset($banner) ? $banner->width : old('width')); ?>" required/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'width'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>


                        <?php if(isset($banner)): ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">الصورة الحالية</label>

                                    <div class="col-md-6">
                                        <img src="<?php echo e(asset('/banners/'.$banner->image)); ?>" class="img-responsive" style="width: 100%"/>
                                    </div>
                                <?php echo $__env->make('admin.layouts.error', ['input' => 'image'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <?php endif; ?>

                            <div class="form-group <?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 control-label">أرفق صورة</label>
                                <div class="col-md-9">
                                    <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="إرفق الصورة"/>
                                </div>
                                <?php echo $__env->make('admin.layouts.error', ['input' => 'image'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>

                            <?php if(isset($banner)): ?>
                                <input type="hidden" name="banner_id" value="<?php echo e($banner->id); ?>">
                            <?php endif; ?>

                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                              <?php echo e(isset($banner) ? 'تعديل': 'إضافة'); ?>

                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>