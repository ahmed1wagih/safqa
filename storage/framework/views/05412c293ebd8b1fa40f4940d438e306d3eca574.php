<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li><a href="/super_admin/addresses/all">الدول</a></li>
        <li> <?php echo e(\App\Models\Address::get_address($parent)); ?></li>
        <li class="active">الحسابات البنكية</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/bank_account/create"><button type="button" class="btn btn-info"> أضف حساب بنكي </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>إسم البنك بالعربية</th>
                                    <th>إسم البنك بالإنجليزية</th>
                                    <th>رقم الحساب</th>
                                    <th>رقم IBAN</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($bank->ar_name); ?></td>
                                        <td><?php echo e($bank->en_name); ?></td>
                                        <td><?php echo e($bank->number); ?></td>
                                        <td><?php echo e($bank->iban); ?></td>
                                        <td>
                                            <a title="تعديل" href="/super_admin/addresses/bank_account/<?php echo e($bank->id); ?>/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-<?php echo e($bank->id); ?>" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-<?php echo e($bank->id); ?>">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف هذا الحساب البنكي و لن تستطيع إستعادتها مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/super_admin/bank_account/delete" class="buttons">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="bank_id" value="<?php echo e($bank->id); ?>">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        <!-- end danger with sound -->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                        </table>
                        <?php echo e($banks->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>