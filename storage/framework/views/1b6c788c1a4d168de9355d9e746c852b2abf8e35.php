<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li><a href="/super_admin/users/active">المدراء</a></li>
        <li class="active"><?php echo e(isset($user) ? 'تعديل مدير عام' : 'إنشاء مدير عام'); ?></li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="<?php echo e(isset($user) ? '/super_admin/user/update' : '/super_admin/user/store'); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <?php echo e(isset($user) ?  'تعديل مدير عام' : 'إنشاء مدير عام'); ?>

                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('country_id') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-super"></span></span>
                                        <select class="form-control select" name="country_id" required>
                                            <option selected disabled>إختر من التالي</option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($country->id); ?>" <?php if(isset($user) && $user->country_id == $country->id): ?> selected <?php elseif(!isset($user) && old('country_id') == $country->id): ?> selected <?php endif; ?>><?php echo e($country->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-super"></span></span>
                                        <input type="text" class="form-control" name="name" required <?php if(isset($user)): ?> value="<?php echo e($user->name); ?>" <?php else: ?>  value="<?php echo e(old('name')); ?>" <?php endif; ?>/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
                                        <input type="text" class="form-control" name="email" required <?php if(isset($user)): ?> value="<?php echo e($user->email); ?>" <?php else: ?>  value="<?php echo e(old('email')); ?>" <?php endif; ?>/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'email'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <div id="field">
                                            <input type="text" class="form-control phone" name="phone" required <?php if(isset($user)): ?> value="<?php echo e($user->phone); ?>" <?php else: ?>  value="<?php echo e(old('phone')); ?>" <?php endif; ?>/>
                                        </div>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الصورة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="Select image"/>
                                    </div>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'image'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password"/>
                                    </div>
                                    <?php if(isset($user)): ?>
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    <?php endif; ?>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password_confirmation"/>
                                    </div>
                                    <?php if(isset($user)): ?>
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    <?php endif; ?>
                                    <?php echo $__env->make('super_admin.layouts.error', ['input' => 'password_confirmation'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <?php if(isset($user)): ?>
                                <input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                                <?php echo e(isset($user) ? 'تعديل' : 'إضافة'); ?>

                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>