<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">إدارة البانرات الإعلانية</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            <?php echo $__env->make('admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/banner/create">
                            <button type="button" class="btn btn-info">أضف بانر إعلاني</button>
                        </a>
                    </div>
                    <div class="panel-body" style="overflow: auto;">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="rtl_th">الصورة</th>
                                    <th class="rtl_th">المقاس</th>
                                    <th class="rtl_th">لينك التحويل</th>
                                    <th class="rtl_th">الإجراء المتخذ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                                <img class="img-responsive" src="/banners/<?php echo e($banner->image); ?>" width="250" height="250"/>
                                        </td>
                                        <td>
                                            <?php echo e($banner->height); ?> إرتفاع
                                            <br/>
                                            1140 عرض
                                        </td>
                                        <td>
                                            <a href="<?php echo e($banner->link); ?>" target="_blank"><?php echo e($banner->link); ?></a>
                                        </td>
                                        <td>
                                            <a href="/admin/banner/<?php echo e($banner->id); ?>/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-<?php echo e($banner->id); ?>" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-<?php echo e($banner->id); ?>">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف البانر الإعلامي و لن تستطيع إسترجاعه مرة أخري,هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                                    <form method="post" action="/admin/banner/delete" class="buttons">
                                                        <?php echo e(csrf_field()); ?>

                                                        <input type="hidden" name="banner_id" value="<?php echo e($banner->id); ?>">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                        <!-- end danger with sound -->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>

                        </table>
                        <?php echo e($banners->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>