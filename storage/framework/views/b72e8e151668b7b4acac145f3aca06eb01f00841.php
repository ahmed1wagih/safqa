<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->

    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">الملف الشخصي</li>
    </ul>
    <!-- END BREADCRUMB -->

    <?php if($errors->has('password') || $errors->has('password_confirmation')): ?>
        <script>
            $(window).load(function() {
                $('#modal_change_password').modal('show');
            });
        </script>
    <?php endif; ?>

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-eye"></span> مشاهدة الملف الشخصي</h2>
    </div>
    <!-- END PAGE TITLE -->
    <?php echo $__env->make('admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-5">

                <form action="#" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-user-secret"></span> <?php echo e(admin()->name); ?> </h3>
                            <p>
                                <span class="label label-success label-form"> مدير دولة </span>
                            </p>
                            <div class="text-center" id="user_image" >
                                <img src="/users/default.png" class="img-thumbnail" style="width: 370px; height: 430px;"/>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الدولة</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"><?php echo e(admin()->country->ar_name); ?></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 col-xs-12">
                                    <a href="#" class="btn btn-warning btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_password">تغيير كلمة المرور</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 col-sm-8 col-xs-7">

                <form action="/delegate/profile/update" class="form-horizontal">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-pencil"></span>بياناتي</h3>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الإسم</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" class="form-control" name="name" value="<?php echo e(admin()->name); ?>"/>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'name'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">البريد الإلكتروني</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="email" class="form-control" name="email" value="<?php echo e(admin()->email); ?>"/>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'email'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الهاتف</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" class="form-control" name="phone" value="<?php echo e(admin()->phone); ?>"/>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'phone'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning">تعديل</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                

                

                

            </div>

            <div class="col-md-3">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3>    معلومات سريعة    <span class="fa fa-info-circle"></span></h3>
                    </div>
                    <div class="panel-body form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">تاريخ التسجيل</label>
                            <div class="col-md-8 col-xs-7 line-height-30"><?php echo e(admin()->created_at); ?></div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div class="modal animated fadeIn" id="modal_change_photo" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>
                    <h4 class="modal-title" id="smallModalHead">تغيير الصورة</h4>
                </div>
                <form  method="post" enctype="multipart/form-data" action="/delegate/change_image">
                    <?php echo e(csrf_field()); ?>

                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 control-label">الصورة الجديدة</label>
                            <div class="col-md-4">
                                <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="حدد الصورة"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">تغيير</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- change password -->
    <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>
                    <h4 class="modal-title" id="smallModalHead">تغيير كلمة المرور</h4>
                </div>
                <form method="post" action="/admin/change_password">
                    <?php echo e(csrf_field()); ?>

                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label class="col-md-3 control-label">كلمة المرور الجديدة</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password" required/>
                                <?php echo $__env->make('admin.layouts.error', ['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                            <label class="col-md-3 control-label">تأكيد كلمة المرور</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password_confirmation" required/>
                                <?php echo $__env->make('admin.layouts.error', ['input' => 'password_confirmation'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning">تغيير</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end change password -->

    <!-- END PAGE CONTENT WRAPPER -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>