<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>صفقة - لوحة التحكم</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php echo e(asset('/web/images/logo.logo.jpg')); ?>" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo e(asset('admin/css/theme-default_rtl.css')); ?>"/>
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo e(asset('admin/css/rtl.css')); ?>"/>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery/jquery.min.js')); ?>"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery/jquery-ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/bootstrap/bootstrap.min.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/morris/raphael.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/morris/morris.min.js')); ?>"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<!-- START PAGE CONTAINER -->
<div class="page-container page-mode-rtl page-content-rtl">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/super_admin/dashboard">صفقة - لوحة التحكم</a>
                <a href="#" class="x-navigation-control"></a>
            </li>

            <li class="xn-profile">
                <div class="profile">
                    <div class="profile-image">
                        <a href="/super_admin/profile" title="الملف الشخصي"><img src="/users/default.png" alt="صفقة" style="width: 150px; height: 150px; border-radius: 360px;"/></a>
                    </div>

                </div>
            </li>

            <li <?php if(Request::is('super_admin/dashboard')): ?> class="active" <?php endif; ?>>
                <a href="/super_admin/dashboard"><span class="xn-text">الرئيسية</span><span class="fa fa-dashboard"></span></a>
            </li>

            <li <?php if(Request::is('super_admin/addresses/*')xor Request::is('super_admin/address/*')): ?> class="active" <?php endif; ?>>
                <a href="/super_admin/addresses/all"><span class="xn-text">الدول و المدن</span><span class="fa fa-flag"></span></a>
            </li>

            <li <?php if(Request::is('super_admin/categories/*') xor Request::is('super_admin/category/*')): ?> class="active" <?php endif; ?>>
                <a href="/super_admin/categories/all"><span class="xn-text">الأقسام</span><span class="fa fa-cubes"></span></a>
            </li>

            <li class="xn-openable <?php if(Request::is('super_admin/users/*') xor Request::is('super_admin/user/*')): ?> active <?php endif; ?>" >
                <a href="#"><span class="xn-text">مدراء الدول</span><span class="fa fa-user-secret"></span></a>
                <ul>
                    <li <?php if(Request::is('super_admin/users/active')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/users/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/users/suspended')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/users/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

            <li class="xn-openable <?php if(Request::is('super_admin/settings/*')): ?> active <?php endif; ?>" >
                <a href="#"><span class="xn-text">الإعدادات</span><span class="fa fa-cogs"></span></a>
                <ul>
                    <li <?php if(Request::is('super_admin/settings/abouts') xor Request::is('super_admin/settings/about/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/abouts"><span class="xn-text">معلومات عنا</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/terms') xor Request::is('super_admin/settings/term/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/terms"><span class="xn-text">شروط الإستخدام</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/partners')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/partners"><span class="xn-text">شركاء النجاح</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/merchant_terms') xor Request::is('super_admin/settings/merchant_terms/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/merchant_terms"><span class="xn-text">شروط الإستخدام للتجار</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/privacy') xor Request::is('super_admin/settings/privacy/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/privacy"><span class="xn-text">سياسة الخصوصية</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/faqs') xor Request::is('super_admin/settings/faq/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/faqs"><span class="xn-text">الأسئلة الشائعة</span><span class="fa fa-cog"></span></a>
                    </li>



                    <li <?php if(Request::is('super_admin/settings/socials') xor Request::is('super_admin/settings/social/*')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/socials"><span class="xn-text">حسابات التواصل الإجتماعي</span><span class="fa fa-cog"></span></a>
                    </li>
                    <li <?php if(Request::is('super_admin/settings/settings/edit')): ?> class="active" <?php endif; ?>>
                        <a href="/super_admin/settings/settings/edit"><span class="xn-text"> الإعدادات العامة للموقع</span><span class="fa fa-cog"></span></a>
                    </li>



                </ul>
            </li>

            <li <?php if(Request::is('super_admin/suggests')): ?> class="active" <?php endif; ?>>
                <a href="/super_admin/suggests"><span class="xn-text"> رسائل الإقتراحات </span><span class="fa fa-mail-forward"></span></a>
            </li>

            <li <?php if(Request::is('super_admin/mail_list/*')): ?> class="active" <?php endif; ?>>
                <a href="/super_admin/mail_list"><span class="xn-text">القائمة البريدية</span><span class="fa fa-send"></span></a>
            </li>

        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-left last">
                <a href="#" class="mb-control" data-box="#mb-signout" title="Logout"><span class="fa fa-power-off"></span></a>
            </li>
            <!-- END POWER OFF -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> تسجيل <strong>الخروج</strong> ؟</div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/super_admin/logout" class="btn btn-success btn-lg">نعم</a>
                            <button class="btn btn-default btn-lg mb-control-close">لا</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

     <?php echo $__env->yieldContent('content'); ?>





<!-- START PRELOADS -->
<audio id="audio-alert" src="<?php echo e(asset('admin/audio/alert.mp3')); ?>" preload="auto"></audio>
<audio id="audio-fail" src="<?php echo e(asset('admin/audio/fail.mp3')); ?>" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src="<?php echo e(asset('admin/js/plugins/icheck/icheck.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')); ?>"></script>

<script type='text/javascript' src='<?php echo e(asset('admin/js/plugins/icheck/icheck.min.js')); ?>'></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/owl/owl.carousel.min.js')); ?>"></script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/actions.js')); ?>"></script>


<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/bootstrap/bootstrap-select.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')); ?>"></script>
<!-- END THIS PAGE PLUGINS -->
<!-- END SCRIPTS -->
</body>

<script>
    function modal_activate(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_activate').val(id);
    }


    function modal_suspend(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_suspend').val(id);
    }


    function modal_destroy(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_destroy').val(id);
    }
</script>
</html>






