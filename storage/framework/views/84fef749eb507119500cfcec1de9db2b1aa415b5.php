<?php $__env->startSection('content'); ?>
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title"><?php echo e(trans('trans.login')); ?><small> <?php echo e(trans('trans.or')); ?> </small><a href="/register" class="color-green"><?php echo e(trans('trans.register')); ?></a></h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/login" method="post">
                                  <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.email_or_phone')); ?></label>
                                    <input type="text" name="login" class="form-control input-lg" placeholder="<?php echo e(trans('trans.email_or_phone')); ?>">
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'login'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only"><?php echo e(trans('trans.password')); ?></label>
                                    <input type="password" name="password" class="form-control input-lg" placeholder="<?php echo e(trans('trans.password')); ?>">
                                    <?php echo $__env->make('web.layouts.error', ['input' => 'password'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <a href="/password_reset" class="forgot-pass-link color-green"><?php echo e(trans('trans.forgot_password')); ?></a>
                                </div>

                                <button type="submit" class="btn btn-block btn-lg"><?php echo e(trans('trans.login')); ?></button>
                            </form>

                        </div>
                        <div class="col-sm-6 col-md-5 col-left">
                            <div >
                                <label style="    margin-top: 30%; margin-right: 20%" class="color-mid" for="remember_social"><a href="/register"><?php echo e(trans('trans.register_and_enjoy')); ?></a></label>
                            </div>
                        </div>
                    </div>
                </section>

















            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('web.layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>