<?php $__env->startSection('content'); ?>
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="margin-top: 10px;">
        <!-- START WIDGETS -->
        <div class="row">

            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-binoculars"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($countries->count()); ?></div>
                        <div class="widget-title">الدول</div>
                        <div class="widget-subtitle">موقوف <?php echo e($countries->where('status','suspended')->count()); ?></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($users); ?></div>
                        <div class="widget-title">عدد المستخدمين</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-shopping-basket"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($merchants->count()); ?></div>
                        <div class="widget-title">عدد التجار</div>
                        
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-success widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-cube"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($deals->count()); ?></div>
                        <div class="widget-title">كل الصفقات</div>
                        <div class="widget-subtitle"><?php echo e($deals->where('status','suspended')->count()); ?> موقوف </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget widget-success widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-barcode"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($all_codes->count()); ?></div>
                        <div class="widget-title">كل أكواد الصفقات</div>
                        <div class="widget-subtitle"><?php echo e($all_codes->where('user_id','!=',NULL)->count()); ?> تم شراؤها </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3">
                <div class="widget widget-success widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-exchange"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count"><?php echo e($transfers->count()); ?></div>
                        <div class="widget-title">عدد الإشتراكات السنوية</div>




                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>الأقسام</h3>
                        <span>عدد الصفقات لكل قسم</span>
                    </div>
                </div>
                <div class="panel-body padding-0">
                    <div class="chart-holder" id="category-deals-donut" style="height: 330px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>التجار</h3>
                        <span>عدد الصفقات لكل تاجر</span>
                    </div>
                </div>
                <div class="panel-body padding-0">
                    <div class="chart-holder" id="merchant-deals-donut" style="height: 330px;"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>عمليات الشراء</h3>
                        <span>عدد عمليات الشراء الشهرية</span>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="month-trans-count" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->


    <script>
        var morrisCharts = function() {

            var days_data;

            $.ajax(
                {
                    async : false,
                    url : '/super_admin/month_deals_graph',
                    method : 'get',
                    dataType : 'json',
                    success : function(data)
                    {
                        days_data = data;
                    },
                    error : function()
                    {
                        console.log('month graphs ajax error')
                    },
                }
            );

            Morris.Line({
                element: 'month-trans-count',
                data: days_data,
                xkey: 'x',
                ykeys: ['y'],
                labels: ['عمليات الشراء'],
                resize: false,
                lineColors: ['#33414E']
            });
        }();


        var merchat_deals = [];
        var donut_colors = ["#0074D9","#B70004","#33414E","#FF4136","#2ECC40","#840003","#FF851B","#358E33","#7FDBFF","#B10DC9","#FFDC00","#001f3f","#39CCCC","#01FF70","#85144b","#F012BE","#3D9970","#111111","#921880"];
        var merchant_deals_donut = [];
        var category_deals_donut = [];

        $.ajax(
            {
                async : false,
                url : '/super_admin/merchant_deals',
                method : 'get',
                dataType : 'json',
                success : function(data)
                {
                    $.each(data, function (i,merchant)
                    {
                        merchant_deals_donut.push({label: merchant.ar_name, value: merchant.deals});
                    });
                },
                error : function()
                {
                    console.log('merchant deals donut ajax error')
                },
            }
        );

        $.ajax(
            {
                async : false,
                url : '/super_admin/category_deals',
                method : 'get',
                dataType : 'json',
                success : function(data)
                {
                    $.each(data, function (i,category)
                    {
                        category_deals_donut.push({label: category.ar_name, value: category.deals});
                    });
                },
                error : function()
                {
                    console.log('category deals donut ajax error')
                },
            }
        );

        console.log(merchant_deals_donut,category_deals_donut);

        Morris.Donut({
            element: 'merchant-deals-donut',
            data: merchant_deals_donut,
            colors: donut_colors,
            resize: false
        });


        Morris.Donut({
            element: 'category-deals-donut',
            data: category_deals_donut,
            colors: donut_colors,
            resize: false
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>