<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">إرسال بريد إلكتروني</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/mail/store">
                    <?php echo e(csrf_field()); ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <li class="active">إرسال بريد إلكتروني</li>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group <?php echo e($errors->has('country_ids') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">الدول</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-paper-plane"></span></span>
                                        <select class="form-control select" name="country_ids[]" multiple>
                                            <option disabled>إختر دولة أو أكثر</option>
                                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($country->id); ?>"><?php echo e($country->ar_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'country_ids'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">العنوان</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title" value="<?php echo e(old('title')); ?>"/>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'title'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('text') ? ' has-error' : ''); ?>">
                                <label class="col-md-3 col-xs-12 control-label">المحتوي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-paper-plane"></span></span>
                                        <textarea class="form-control summernote" name="text" rows="5"><?php echo e(old('text')); ?></textarea>
                                    </div>
                                    <?php echo $__env->make('admin.layouts.error', ['input' => 'text'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button type="submit" class="btn btn-primary pull-right">
                                إرسال
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src='<?php echo e(asset("admin/js/plugins/icheck/icheck.min.js")); ?>'></script>
    <script type="text/javascript" src="<?php echo e(asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/summernote/summernote.js')); ?>"></script>
    <!-- END THIS PAGE PLUGINS-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>