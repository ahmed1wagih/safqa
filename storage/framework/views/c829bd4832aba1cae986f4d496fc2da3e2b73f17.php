<div>

    <?php if(Session::has('success')): ?>
        <div class="alert alert-success" style="text-align: right;" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>
            <span style="margin-right: 10px;"> <?php echo e(Session::get('success')); ?> </span>
        </div>
    <?php endif; ?>


    <?php if(Session::has('error')): ?>
            <div class="alert alert-danger" style="text-align: right;" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>
             <span style="margin-right: 10px;"> <?php echo e(Session::get('error')); ?> </span>
        </div>
    <?php endif; ?>


</div>
