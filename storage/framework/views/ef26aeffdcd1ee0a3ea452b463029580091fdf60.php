<?php $__env->startSection('content'); ?>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <?php if(Request::is('super_admin/addresses/all')): ?>
            <li class="active">الدول</li>
        <?php else: ?>
            <li><a href="/super_admin/addresses/all">الدول</a></li>
            <li> <?php echo e(\App\Models\Address::get_address($parent)); ?></li>
            <li class="active">المدن</li>
        <?php endif; ?>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            <?php echo $__env->make('super_admin.layouts.message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/address/city/create"><button type="button" class="btn btn-info"> أضف مدينة </button></a>
                        <a href="/super_admin/address/country/create"><button type="button" class="btn btn-info"> أضف دولة </button></a>
                        <a href="/super_admin/bank_account/create"><button type="button" class="btn btn-info"> أضف حساب بنكي </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم بالعربية</th>
                                    <th>الإسم بالإنجليزية</th>
                                    <?php if(Request::is('super_admin/addresses/all')): ?>
                                        <th>المدن</th>
                                        <th>الحسابات البنكية</th>
                                    <?php endif; ?>
                                    <th>الحالة</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($address->ar_name); ?></td>
                                            <td><?php echo e($address->en_name); ?></td>
                                            <?php if(Request::is('super_admin/addresses/all')): ?>
                                                <td><?php echo e($address->cities->count()); ?></td>
                                                <td><?php echo e($address->bank_accounts->count()); ?></td>
                                            <?php endif; ?>
                                            <td><span class="label label-form <?php echo e($address->status == 'active' ? 'label-success' : 'label-default'); ?>"><?php echo e($address->status == 'active' ? 'مفعل' : 'موقوف'); ?></span></td>
                                            <td>
                                                <?php if($address->parent_id == NULL && $address->cities->count() != 0): ?>
                                                    <a title="مشاهدة الحسابات البنكية" href="/super_admin/addresses/<?php echo e($address->id); ?>/bank_accounts"><button class="btn btn-success btn-condensed"><i class="fa fa-bank"></i></button></a>
                                                    <a title="مشاهدة المدن" href="/super_admin/addresses/<?php echo e($address->id); ?>"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                                <?php endif; ?>
                                                <a title="Edit" href="/super_admin/address/<?php echo e($address->id); ?>/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                                <?php if($address->status == 'active'): ?>
                                                    <button class="btn btn-primary btn-condensed mb-control" onclick="modal_suspend(<?php echo e($address->id); ?>)" data-box="#message-box-primary" title="توقيف"><i class="fa fa-minus-circle"></i></button>
                                                <?php else: ?>
                                                    <button class="btn btn-success btn-condensed mb-control" onclick="modal_activate(<?php echo e($address->id); ?>)" data-box="#message-box-success" title="تفعيل"><i class="fa fa-check-square"></i></button>
                                                <?php endif; ?>
                                                <button class="btn btn-danger btn-condensed mb-control" onclick="modal_destroy(<?php echo e($address->id); ?>)" data-box="#message-box-danger" title="حذف"><i class="fa fa-trash-o"></i></button>                                        </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        <?php echo e($addresses->links()); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- danger with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-success">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    <?php if($parent != 'all'): ?>
                        <p>أنت علي وشك أن تفعل هذه المدينة,و جميع من يتبع من  تجار و صفقات,هل أنت متأكد ؟</p>
                    <?php else: ?>
                        <p>أنت علي وشك أن تفعل هذه الدولة,و جميع من يتبع من مدراء و مدن و تجار و صفقات و مستخدمين يستطيعون مباشرة جميع أعمالهم,هل أنت متأكد ؟</p>
                    <?php endif; ?>
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/change_status" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_activate" value="">
                        <input type="hidden" name="status" value="active">
                        <button class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-primary">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    <?php if($parent != 'all'): ?>
                        <p>أنت علي وشك أن توقف عمل هذه المدينة و كل من يتبعها من تجار و صفقات,هل أنت متأكد ؟</p>
                    <?php else: ?>
                        <p>أنت علي وشك أن توقف عمل هذه الدولة و كل من يتبعها من مدراء مدن و تجار و صفقات و مستخدمين و لن يستطيعوا مباشرة جيع أعمالهم من الآن,هل أنت متأكد ؟</p>
                    <?php endif; ?>
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/change_status" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_suspend" value="">
                        <input type="hidden" name="status" value="suspended">
                        <button class="btn btn-primary btn-lg pull-right">توقيف</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    <?php if($parent != 'all'): ?>
                        <p>أنت علي وشك أن تحذف هذه المدينة و ما يتبعها من تجار و صفقات و لن تستطيع إسترجاع بياناتهم مره أخري,هل أنت متأكد ؟</p>
                    <?php else: ?>
                        <p>أنت علي وشك أن تحذف هذه الدولة و ما يتبعها من مدراء و مدن و تجار و صفقات و مستخدمين و لن تستطيع إسترجاع بياناتهم مره أخري,هل أنت متأكد ؟</p>
                    <?php endif; ?>
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/delete" class="buttons">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" id="id_destroy" value="">
                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->
    <script>
        function modal_activate($id)
        {
            $('#id_activate').val($id);
        }


        function modal_suspend($id)
        {
            $('#id_suspend').val($id);
        }


        function modal_destroy($id)
        {
            $('#id_destroy').val($id);
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('super_admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>