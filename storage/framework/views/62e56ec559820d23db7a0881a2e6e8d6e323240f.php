<?php if($errors->has($input)): ?>
<span class="help-block">
        <strong style="color: red;"><?php echo e(trans('trans.'.$errors->first($input))); ?></strong>
</span>
<?php endif; ?>
