<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>صفقة - لوحة التحكم</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php echo e(asset('/admin/img/favicon.ico')); ?>" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?php echo e(asset('/admin/css/theme-default.css')); ?>"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">
    <div class="login-box animated fadeInDown">
        <div class="login-body" style="text-align: center;">
            <img src="<?php echo e(asset('web/images/logo_only.png')); ?>" width="150px" height="150px">
            <h1 style="color: white; text-align: center;">دخول المدير</h1>
            <?php if(Session::has('error')): ?>
                <div class="alert alert-danger" role="alert" style="text-align: center;">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <span style="size: 15px;">
                                <?php echo e(Session::get('error')); ?>

                    </span>
                </div>
            <?php endif; ?>
            <form action="/admin/login" class="form-horizontal" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="email" placeholder="البريد الإلكتروني"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password" placeholder="كلمة المرر"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-info btn-block">تسجيل الدخول</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

</body>
</html>

