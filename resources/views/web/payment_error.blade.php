@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-12 col-md-12">
                        <!-- Checkout Area -->

                        <div id="circle">
                            <h3 class="circle-content" style="margin-top: 30px; display: block; text-align: center;">
                                @if(user()->credit == 1)
                                    {{trans('trans.one_point')}}
                                @elseif(user()->credit == 2)
                                    {{trans('trans.two_points')}}
                                @elseif(user()->credit > 2 && user()->credit <11)
                                    {{user()->credit}} <br/> {{trans('trans.points')}}
                                @else
                                    @if(lang() == 'ar')
                                        {{user()->credit}} <br/> {{trans('trans.point')}}
                                    @else
                                        {{user()->credit}} <br/> {{trans('trans.points')}}
                                    @endif
                                @endif
                            </h3>
                        </div>
                        <!-- End Checkout Area -->
                    </div>
                    <section class="section checkout-area panel prl-30 pt-20 pb-40">
                        <div style="display: block; margin: 0 auto; text-align: center" >
                            <i class="fa fa-times fa-5x" style="color: red;"></i>
                            <h3 >
                                {{trans('trans.payment_error')}}
                            </h3>
                        </div>


                    </section>

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
