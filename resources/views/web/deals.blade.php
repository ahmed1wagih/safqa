@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container">
            <div class="container">
                <section class="cart-area ptb-60">
                    <div class="container">
                        <div class="cart-wrapper">
                            <h3 class="h-title mb-30 t-uppercase">{{trans('trans.deals_list')}}</h3>
                            <table id="cart_list" class="cart-list mb-30">
                                <thead class="panel t-uppercase">
                                <tr>
                                    <th>{{trans('trans.deal_name')}}</th>
                                    <th>{{trans('trans.deal_price')}}</th>
                                    <th>{{trans('trans.date')}}</th>
                                    <th>{{trans('trans.details')}}</th>

                                </tr>
                                </thead>
                                @foreach($deals as $deal)
                                    <tbody>
                                    <tr class="panel alert">
                                        <td>
                                            <div class="media-right is-hidden-sm-down">
                                                <figure class="product-thumb">
                                                    <img src="{{asset('/products/'.$deal->product->cover->image)}}" alt="
                                                        @if(lang() == 'ar')
                                                            {{$deal->product->ar_title}}
                                                        @else
                                                            {{$deal->product->en_title}}
                                                        @endif
                                                        ">
                                                </figure>
                                            </div>
                                            <div class="media-body valign-middle">
                                                <h6 class="title mb-15 t-uppercase"><a href="/product/{{$deal->product_id}}">
                                                        @if(lang() == 'ar')
                                                            {{$deal->product->ar_title}}
                                                        @else
                                                            {{$deal->product->en_title}}
                                                        @endif
                                                    </a></h6>
                                                <div class="type font-12"><a href="/category/{{$deal->product->category->parent->id}}/{{$deal->product->category->id}}"><span class="t-uppercase">{{trans('trans.category')}} :</span>
                                                        @if(lang() == 'ar')
                                                            {{$deal->product->category->ar_name}}
                                                        @else
                                                            {{$deal->product->category->en_name}}
                                                        @endif
                                                    </a></div>
                                            </div>
                                        </td>
                                        <td>
                                            @if($deal->product->deal_price == 1)
                                                {{trans('trans.one_point')}}
                                            @elseif($deal->product->deal_price == 2)
                                                {{trans('trans.two_points')}}
                                            @elseif($deal->product->deal_price > 2 && $deal->product->deal_price <11)
                                                {{$deal->product->deal_price}} {{trans('trans.points')}}
                                            @else
                                                @if(lang() == 'ar')
                                                    {{$deal->product->deal_price}} {{trans('trans.point')}}
                                                @else
                                                    {{$deal->product->deal_price}} {{trans('trans.points')}}
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::parse($deal->purchased_at)->toDateString()}}
                                            <br/>
                                            {{\Carbon\Carbon::parse($deal->purchased_at)->toTimeString()}}
                                        </td>
                                        <td>
                                            <a href="/deal/{{$deal->id}}/info"><div class="sub-total">{{trans('trans.show_details')}}</div></a>
                                        </td>

                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            <div class="cart-price">
                                <h5 class="t-uppercase mb-20">{{trans('trans.statistics')}}</h5>
                                <ul class="panel mb-20">
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.total_purchases')}}
                                        </div>
                                        <div class="price">
                                            @if($user->purchased($user->id) == 0)
                                                {{trans('trans.0_points')}}
                                            @elseif($user->purchased($user->id) == 1)
                                                {{trans('trans.one_point')}}
                                            @elseif($user->purchased($user->id) == 2)
                                                {{trans('trans.two_points')}}
                                            @elseif($user->purchased($user->id) > 2 && $user->purchased($user->id) <11)
                                                {{$user->purchased($user->id)}} {{trans('trans.points')}}
                                            @else
                                                @if(lang() == 'ar')
                                                    {{$user->purchased($user->id)}} {{trans('trans.point')}}
                                                @else
                                                    {{$user->purchased($user->id)}} {{trans('trans.points')}}
                                                @endif
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.total_saving')}}
                                        </div>
                                        <div class="price">
                                            {{$user->money_saved($user->id)}} {{lang() == 'ar' ? $user->country->ar_currency : $user->country->en_currency}}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.deals_count')}}
                                        </div>
                                        <div class="price">
                                            @if($deals->count() == 0)
                                                {{trans('trans.0_deals')}}
                                            @elseif($deals->count() == 1)
                                                {{trans('trans.one_deal')}}
                                            @elseif($deals->count() == 2)
                                                {{trans('trans.two_deals')}}
                                            @elseif($deals->count() > 2 && $deals->count() <11)
                                                {{$deals->count()}}  {{trans('trans.deals')}}
                                            @else
                                                @if(lang() == 'ar')
                                                    {{$deals->count()}} {{trans('trans.deal')}}
                                                @else
                                                    {{$deals->count()}} {{trans('trans.deals')}}
                                                @endif
                                            @endif
                                        </div>
                                    </li>

                                </ul>
                                <div class="t-left">
                                    <a href="/profile" class="btn btn-rounded btn-lg">{{trans('trans.back_to_profile')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
