@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <!-- Contact Us Area -->
                <section class="terms-area panel">
                    <div class="ptb-30 prl-30">
                        <h3 class="t-uppercase h-title mb-40">{{trans('trans.merchant_terms')}}</h3>
                        {!! $terms->text !!}
                    </div>
                </section>
                <!-- End Contact Us Area -->
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
