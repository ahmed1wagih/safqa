@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.login')}}<small> {{trans('trans.or')}} </small><a href="/register" class="color-green">{{trans('trans.register')}}</a></h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/reset_password" method="post">
                                  {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email')}}</label>
                                    <input type="text" name="login" class="form-control input-lg" placeholder="{{trans('trans.email')}}">
                                    @include('web.layouts.error', ['input' => 'email'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="number" name="code" class="form-control input-lg" placeholder="{{trans('trans.code')}}">
                                    @include('web.layouts.error', ['input' => 'password'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.check_code')}}</button>
                            </form>

                        </div>
                    </div>
                </section>
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.reset_password')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/send_reset_code" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.send_reset_code')}}</label>
                                    <input type="number" name="email_send" class="form-control input-lg" placeholder="{{trans('trans.send')}}">
                                    @include('web.layouts.error', ['input' => 'email_send'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.send')}}</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
