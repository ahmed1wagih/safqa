<section class="footer-top-area pt-70 pb-30 pos-r bg-blue">
    <div class="container">
        <div class="row row-tb-20">
            <div class="col-sm-12 col-md-6">
                <div class="row row-tb-20">
                    <div class="footer-col col-sm-6">
                        <div class="footer-top-instagram instagram-widget">
                            <h2>{{trans('trans.contact')}}</h2>
                            <div class="row row-tb-5 row-rl-5">
                                <ul class="twitter-list">
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-map-marker"></i>
                                            @if(lang() == 'ar')
                                                {!! $abouts->ar_address !!}
                                            @else
                                                {!! $abouts->en_address !!}
                                            @endif
                                        </p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-phone"></i> {!! $abouts->phone !!} </p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-envelope"></i>{!! $abouts->email !!} </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-col col-sm-6">
                        <div class="footer-top-twitter">
                            <h2 class="color-lighter">{{trans('trans.about_us')}}</h2>
                            <div class="row row-tb-5 row-rl-5">
                                <ul class="twitter-list">
                                    {{--<li class="single-twitter">--}}
                                        {{--<p class="color-light"><i class="ico fa fa-plus icon-footer"></i>  <a href="#" class="footer-link">  فكرة بيست ديل   </a></p>--}}
                                    {{--</li>--}}
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i> <a href="/contact_us" class="footer-link">   {{trans('trans.contact_us')}}   </a></p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i> <a href="/terms" class="footer-link">   {{trans('trans.terms')}}   </a></p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i> <a href="/partners" class="footer-link">   {{trans('trans.partners')}}   </a></p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i><a href="/privacy" class="footer-link">    {{trans('trans.policy')}}   </a></p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i><a href="/faqs" class="footer-link">    {{trans('trans.q&a')}}  </a> </p>
                                    </li>
                                    <li class="single-twitter">
                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i><a href="/merchant/apply" class="footer-link">    {{trans('trans.apply_as_merchant')}}   </a></p>
                                    </li>
{{--                                    <li class="single-twitter">--}}
{{--                                        <p class="color-light"><i class="ico fa fa-plus icon-footer"></i><a href="/submit_deal" class="footer-link">    {{trans('trans.publish_deal')}}   </a></p>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="row row-tb-20">
                    <div class="footer-col col-sm-6">
                        <div class="footer-links">
                            <h2 class="color-lighter">{{trans('trans.statistics')}}</h2>
                            <p class="color-light2">
                                @if(\App\Models\FooterStates::where('country_id',Session::get('country')->id)->first())
                                {{\App\Models\FooterStates::where('country_id',Session::get('country')->id)->first()->total_sold}}
                                @else
                                0
                                @endif
                            </p>
                            <small class="static">{{trans('trans.deals_sold')}} </small>
                            <p class="color-light2">
                                @if(\App\Models\FooterStates::where('country_id',Session::get('country')->id)->first())
                                    {{\App\Models\FooterStates::where('country_id',Session::get('country')->id)->first()->total_saved}}
                                    @else
                                0
                                @endif
                            </p>
                            <small class="static">{{trans('trans.money_saved')}} </small> </div>
                    </div>
                    <div class="footer-col col-sm-6">
                        <div class="footer-about"><a href="https://maroof.sa/131005" target="_blank"><img class="mb-40" src="/web/images/logo_bottom.png" width="200" alt=""></a>
{{--                            <p class="color-light">--}}
{{--                                @if(lang() == 'ar')--}}
{{--                                    {!! $abouts->ar_text !!}--}}
{{--                                @else--}}
{{--                                    {!! $abouts->en_text !!}--}}
{{--                                @endif--}}
{{--                            </p>--}}
{{--                            <section class="section subscribe-area ptb-40 t-center" >--}}

{{--                            </section>--}}
                        </div>
                        <div>
                            <ul style="display: block; margin: 0 auto;">
                                <a href="https://play.google.com/store/apps/details?id=bestdeal.shop" target="_blank"><img src="{{asset('images/playstore.png')}}" style="width: 100px; height: 100px;"></a>
                                <a href="#"><img src="{{asset('images/istore.png')}}" style="width: 100px; height: 100px;"></a>
                            </ul>


                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="payment-methods t-center">
                    {{--<span><img src="/web/images/icons/payment/paypal.jpg" alt="Paypal" style="width: 64px; height: 38px;"></span>--}}
                    <span><img src="/web/images/icons/payment/paytabs.png" alt="PayTabs" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/visa.jpg" alt="Visa" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/mastercard.jpg" alt="Master Card" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/ahly.png" alt="NCB" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/raghy.png" alt="Raghy Bank" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/sadad.png" alt="Sadad" style="width: 64px; height: 38px;"></span>
                    <span><img src="/web/images/icons/payment/mada.png" alt="Mada" style="width: 64px; height: 38px;"></span>
                </div>
            </div>
            <div class="col-xs-12">
                <ul class="social-icons list-inline" style="text-align: center">
                    <li class="social-icons__item"> <a href="{{$socials->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                    <li class="social-icons__item"> <a href="{{$socials->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a> </li>
                    <li class="social-icons__item"> <a href="{{$socials->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a> </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- –––––––––––––––[ FOOTER ]––––––––––––––– -->
<footer id="mainFooter" class="main-footer">
    <div class="container">
        <div class="row">
            @if(lang() == 'ar')
                <p>{{$settings->ar_rights}}</p>
            @else
                <p>{{$settings->en_rights}}</p>
            @endif
        </div>
    </div>
</footer>
<!-- –––––––––––––––[ END FOOTER ]––––––––––––––– -->
</div>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- END WRAPPER                               -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->

<!-- ========== BUY THEME ========== -->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '40b0d5aa-2cd2-444f-a9be-199a73980c59', f: true }); done = true; } }; })();</script>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- BACK TO TOP                               -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<div id="backTop" class="back-top is-hidden-sm-down"> <i class="fa fa-angle-up" aria-hidden="true"></i> </div>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- SCRIPTS                                   -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- (!) Placed at the end of the document so the pages load faster -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- Initialize jQuery library                 -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<script type="text/javascript" src="{{asset('web/js/jquery-1.12.3.min.js')}}"></script>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- Latest compiled and minified Bootstrap    -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<script type="text/javascript" src="{{asset('web/js/bootstrap.min.js')}}"></script>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- JavaScript Plugins                        -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- (!) Include all compiled plugins (below), or include individual files as needed -->
<!-- Modernizer JS -->
<script src="{{asset('web/vendors/modernizr/modernizr-2.6.2.min.js')}}"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="{{asset('web/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- FlexSlider -->
<script type="text/javascript" src="{{asset('web/vendors/flexslider/jquery.flexslider-min.js')}}"></script>
<!-- Coutdown -->
<script type="text/javascript" src="{{asset('web/vendors/countdown/jquery.countdown.js')}}"></script>
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- Custom Template JavaScript                -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
    @if(lang() == 'ar')
        <script type="text/javascript" src="{{asset('web/js/main.js')}}"></script>
    @else
        <script type="text/javascript" src="{{asset('web/js/en_main.js')}}"></script>
    @endif
</body>
</html>
