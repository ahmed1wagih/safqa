@if ($errors->has($input))
<span class="help-block">
        <strong style="color: red;">{{ trans('trans.'.$errors->first($input)) }}</strong>
</span>
@endif
