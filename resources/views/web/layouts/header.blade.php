<!DOCTYPE html>
<!--[if lt IE 9 ]> <html lang="ar" dir="rtl" class="no-js ie-old"> <![endif]-->
<!--[if IE 9 ]> <html lang="ar" dir="rtl" class="no-js ie9"> <![endif]-->
<!--[if IE 10 ]> <html lang="ar" dir="rtl" class="no-js ie10"> <![endif]-->
<!--[if (gt IE 10)|!(IE)]><!-->
<html lang="en" dir="rtl" class="no-js">
<!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- META TAGS                                 -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile specific meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- PAGE TITLE                                -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    @if(lang() == 'ar')
        <title>{{$settings->ar_name}}</title>
    @else
        <title>{{$settings->en_name}}</title>
    @endif
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- SEO METAS                                 -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="description" content="Best Deal Market">
    <meta name="keywords" content="insert, keywords, here">
    <meta name="robots" content="index, follow">
    <meta name="author" content="CODASTROID">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- PAGE FAVICON                              -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="apple-touch-icon" href="{{asset('web/images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" href="{{asset('web/images/favicon/favicon.png')}}">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- GOOGLE FONTS                              -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=El+Messiri:700" rel="stylesheet">
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- Include CSS Filess                        -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->



    @if(lang() == 'ar')
        <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/bootstrap-rtl.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/base.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/style.css')}}" rel="stylesheet">
    @else
        <link href="{{asset('web/css/en/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/en/bootstrap-rtl.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/en/base.css')}}" rel="stylesheet">
        <link href="{{asset('web/css/en/style.css')}}" rel="stylesheet">
    @endif

    <!-- Font Awesome -->
    <link href="{{asset('web/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Linearicons -->
    <link href="{{asset('web/vendors/linearicons/css/linearicons.css')}}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{asset('web/vendors/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/vendors/owl-carousel/owl.theme.min.css')}}" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="{{asset('web/vendors/flexslider/flexslider.css')}}" rel="stylesheet">
    <!-- Template Stylesheet -->

    <script type="text/javascript" src="{{asset('web/js/jquery-1.12.3.min.js')}}"></script>
</head>

    <body id="body" class="wide-layout ">
    <div id="preloader" class="preloader">
        <div class="loader-cube">
            <div class="loader-cube__item1 loader-cube__item"></div>
            <div class="loader-cube__item2 loader-cube__item"></div>
            <div class="loader-cube__item4 loader-cube__item"></div>
            <div class="loader-cube__item3 loader-cube__item"></div>
        </div>
    </div>
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <!-- WRAPPER                                   -->
    <!-- ––––––––––––––––––––––––––––––––––––––––– -->
    <div id="pageWrapper" class="page-wrapper">
        <!-- –––––––––––––––[ HEADER ]––––––––––––––– -->
        <header id="mainHeader" class="main-header">
            <!-- Top Bar -->
            <div class="top-bar bg-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-5 is-hidden-sm-down">
                            <ul class="nav-top nav-top-left list-inline t-right">

                                <li><a href="/terms"><i class="fa fa-question-circle"></i> {{trans('trans.terms')}} </a> </li>
                                <li><a href="/faqs"><i class="fa fa-support"></i>{{trans('trans.q&a')}}</a> </li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <ul class="nav-top nav-top-right list-inline t-xs-center t-md-left">
                                @if(!user())
                                    <li> <a href="#"><img src="/addresses/{{country()->image}}" style="width: 16px; height: 11px; margin: 2px;">{{lang() == 'ar' ? country()->ar_name : country()->en_name}}
                                            <i class="fa fa-caret-down"></i></a>
                                        <ul>
                                            @foreach($all_countries as $country)
                                                @if($country->id != country()->id)
                                                    <li><a href="/change_country/{{$country->id}}"><img src="/addresses/{{$country->image}}" style="width: 16px; height: 11px; margin: 2px;">{{$country->name}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a><img src="/addresses/{{country()->image}}" style="width: 16px; height: 11px; margin: 2px;">{{lang() == 'ar' ? country()->ar_name : country()->en_name}}</a>
                                    </li>
                                @endif
                                <li> <a href="#"><i class="fa fa-map-marker"></i> {{lang() == 'ar' ? city()->ar_name : city()->en_name}}</a>
                                    <ul>
                                        @foreach(country()->active_cities as $city)
                                            @if($city->id != city()->id)
                                                <li>
                                                    <a href="/change_city/{{$city->id}}"><i class="fa fa-map-marker"></i>
                                                    {{lang() == 'ar' ? $city->ar_name : $city->en_name}}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-globe"></i>
                                        @if(lang() == 'ar')
                                            العربية
                                        @else
                                            English
                                        @endif
                                    <i></i>
                                    <ul>
                                        @foreach (Config::get('languages') as $lang => $language)
                                            @if ($lang != lang())
                                                <a href="{{ route('lang.switch', $lang) }}"><i class="fa fa-globe fa-2x font_awesome"></i>{{$language}}</a>
                                            @endif
                                        @endforeach

                                    </ul>
                                </li>

                                @if(user())
                                    <li><a href="/logout"><i class="fa fa-sign-out"></i>{{trans('trans.logout')}}</a> </li>
                                @else
                                    <li><a href="/login"><i class="fa fa-lock"></i>{{trans('trans.login')}}</a> </li>
                                    <li><a href="/register"><i class="fa fa-user"></i>{{trans('trans.register')}}</a> </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Bar -->
            <!-- Header Header -->
            <div class="header-header bg-white">
                <div class="container">
                    <div class="row row-rl-0 row-tb-20 row-md-cell">
                        <div class="brand col-md-3 t-xs-center t-md-right valign-middle"> <a href="/" class="logo"> <img src="{{asset('web/images/logo_'. lang() .'.jpg')}}" alt="" style="height: 90px;width: 185px;"> </a> </div>
                        <div class="header-search col-md-9">
                            <div class="row row-tb-10 ">
                                <div class="col-sm-8">
                                    <form class="search-form" action="/search" method="get">
                                        {{--{{csrf_field()}}--}}
                                        <div class="input-group">
                                            <input type="text" name="text" class="form-control input-lg search-input" value="{{old('text')}}" placeholder="{{trans('trans.enter_word_search')}}">
                                            <div class="input-group-btn">
                                                <div class="input-group">
                                                    <select class="form-control input-lg search-select" name="cat_id">
                                                        <option selected disabled>{{trans('trans.pick_cat')}}</option>
                                                        @foreach($all_global_cats as $cat)
                                                            <option value="{{$cat->id}}">
                                                                @if(lang() == 'ar')
                                                                    {{$cat->ar_name}}
                                                                @else
                                                                    {{$cat->en_name}}
                                                                @endif
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="input-group-btn">
                                                        <button type="submit" class="btn btn-lg btn-search btn-block"> <i class="fa fa-search font-16"></i> </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                @if(user())
                                    <div class="col-sm-4 t-xs-center t-md-left">
                                        <div class="header-cart"> <a href="/cart"> <span class="icon lnr lnr-cart"></span> </a> </div>
                                        <div class="header-wishlist mr-20"> <a href="/wish_list"> <span class="icon lnr lnr-heart font-30"></span> </a> </div>
                                        <div class="header-wishlist mr-20"> <a href="/profile"> <span class="icon lnr lnr-user font-30"></span> </a> </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Header Header -->
            <!-- Header Menu -->
            <div class="header-menu bg-blue">
                <div class="container">
                    <nav class="nav-bar">
                        <div class="nav-header"> <span class="nav-toggle" data-toggle="#header-navbar"> <i></i> <i></i> <i></i> </span> </div>
                        <div id="header-navbar" class="nav-collapse">
                            <ul class="nav-menu">
                                @foreach($global_cats as $cat)
                                    <li class="dropdown-mega-menu"> <a href="/category/{{$cat->id}}/all">
                                            @if(lang() == 'ar')
                                                {{$cat->ar_name}}
                                            @else
                                                {{$cat->en_name}}
                                            @endif
                                        </a>
                                    <div class="mega-menu">
                                        <div class="row row-v-10">
                                            <div class="col-md-3">
                                                <ul>
                                                    @foreach($cat->global_sub_cats($cat->id) as $sub)
                                                        <li><a href="/category/{{$sub->parent->id}}/{{$sub->id}}">
                                                                @if(lang() == 'ar')
                                                                    {{$sub->ar_name}}
                                                                @else
                                                                    {{$sub->en_name}}
                                                                @endif
                                                            </a> </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @foreach($cat->home_products($cat->id) as $product)
                                                <a href="/product/{{$product->id}}">
                                                    <div class="col-md-3">
                                                    <figure class="deal-thumbnail embed-responsive embed-responsive-4by3" style='height: auto; width: auto; object-fit: contain' data-bg-img="{{asset('products/'.$product->cover->image)}}">
                                                        <div class="label-discount top-10 left-10">{{trans('trans.exclusive')}}</div>
                                                        <div class="deal-about p-10 pos-a bottom-0 right-0">

{{--                                                            <div class="rating mb-10"> <span class="rating-stars" data-rating="{{$product->get_rate($product->if)}}"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> </div>--}}

                                                            <h6 class="deal-title mb-10"> <a href="/product/{{$product->id}}" class="color-lighter">
                                                                    @if(lang() == 'ar')
                                                                        {{$product->ar_title}}
                                                                    @else
                                                                        {{$product->en_title}}
                                                                    @endif
                                                                </a> </h6>
                                                        </div>
                                                    </figure>
                                                </div>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- End Header Menu -->
        </header>
        <!-- –––––––––––––––[ HEADER ]––––––––––––––– -->

        @include('web.layouts.message')

