@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-12 col-md-12">
                        <!-- Checkout Area -->

                        <div id="circle">
                            <h3 class="circle-content" style="margin-top: 30px;">
                                @if(user()->credit == 1)
                                    <span style="display: block; margin: 0 auto; text-align: center;">
                                        {{trans('trans.one_point')}}
                                    </span>
                                @elseif(user()->credit == 2)
                                    <span style="display: block; margin: 0 auto; text-align: center;">
                                        {{trans('trans.two_points')}}
                                    </span>
                                @elseif(user()->credit > 2 && user()->credit <11)
                                    {{user()->credit}} <br/> <span style="display: block; margin: 0 auto; text-align: center;"> {{trans('trans.points')}}</span>
                                @else
                                    @if(lang() == 'ar')
                                        {{user()->credit}} <br/> <span style="display: block; margin: 0 auto; text-align: center;"> {{trans('trans.point')}}</span>
                                    @else
                                        {{user()->credit}} <br/> <span style="display: block; margin: 0 auto; text-align: center;"> {{trans('trans.points')}}</span>
                                    @endif
                                @endif
                            </h3>
                        </div>
                        <!-- End Checkout Area -->
                        <h3 style="text-align: center;">
                            @if(user()->expire_at >= \Carbon\Carbon::today()->toDateString())
                                <span class="label label-success label-form" style="background-color: green;">{{trans('trans.expire_at')}} {{user()->expire_at}}</span>
                            @else
                                <span class="label label-danger label-form" style="background-color: #B22026;">{{trans('trans.expired')}}</span>
                            @endif
                        </h3>

                    </div>
                    <section class="section checkout-area panel prl-30 pt-20 pb-40">
                        <h3 class="mb-20 h-title">
                            {{trans('trans.subscribe')}} - {{trans('trans.bank_trans')}}
                        </h3>
                        <form class="mb-30" method="post" action="/charge">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.bank')}}
                                        </label>
                                        <select class="form-control" name="bank_id">
                                            <option selected disabled>{{trans('trans.select_bank')}}</option>
                                            @foreach($banks as $bank)
                                                <option value="{{$bank->id}}">{{$bank->name}} - {{trans('trans.number')}} {{$bank->number}} - {{trans('trans.iban')}} {{$bank->iban}}</option>
                                            @endforeach
                                        </select>
                                        @include('web.layouts.error',['input' => 'bank_id'])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.full_name')}}
                                        </label>
                                        <input type="text" name="name" class="form-control" placeholder="{{trans('trans.full_name')}}" required>
                                        @include('web.layouts.error',['input' => 'name'])
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.trans_no')}}
                                        </label>
                                        <input type="text" name="trans_no" class="form-control" placeholder="{{trans('trans.trans_no')}}" required>
                                        @include('web.layouts.error',['input' => 'trans_no'])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.pack')}}
                                        </label>
                                        <select class="form-control" name="pack_id" id="packs">
                                            <option selected disabled>{{trans('trans.pick_pack')}}</option>
                                            @foreach($packs as $pack)
                                                    <option value="{{$pack->id}}">{{$pack->name}} - {{$pack->points}} {{trans('trans.point')}} - {{$pack->price}} {{lang() == 'ar' ? user()->country->ar_currency : user()->country->en_currency}}</option>
                                            @endforeach
                                        </select>
                                        @include('web.layouts.error',['input' => 'pack_id'])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.trans_date')}}
                                        </label>
                                        <input type="date" name="date" class="form-control" placeholder="{{trans('trans.trans_date')}}" required>
                                        @include('web.layouts.error',['input' => 'date'])
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="charge">
                            </div>
                            <button class="btn btn-info btn-lg btn-rounded ml-10">{{trans('trans.confirm')}}</button>
                        </form>
                    </section>


                    <section class="section checkout-area panel prl-30 pt-20 pb-40">
                        <h3 class="mb-20 h-title">
                            {{trans('trans.charge_paytabs')}} <span>
                                <img src="/web/images/icons/payment/mada.png" alt="PayTabs" style="width: 64px; height: 38px;"></span>
                                <span><img src="/web/images/icons/payment/visa.jpg" alt="Visa" style="width: 64px; height: 38px;"></span>
                                <span><img src="/web/images/icons/payment/mastercard.jpg" alt="Master Card" style="width: 64px; height: 38px;"></span>
                        </h3>
                        <form class="mb-30" method="post" action="/paytabs/pay">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.pack')}}
                                        </label>
                                        <select class="form-control" name="pack_id" id="packs">
                                            <option selected disabled>{{trans('trans.pick_pack')}}</option>
                                            @foreach($packs as $pack)
                                                <option value="{{$pack->id}}">{{$pack->name}} - {{$pack->points}} {{trans('trans.point')}} - {{$pack->price}} {{lang() == 'ar' ? user()->country->ar_currency : user()->country->en_currency}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.first_name')}}
                                        </label>
                                        <input type="text" name="first_name" class="form-control" placeholder="{{trans('trans.first_name')}}" value="{{old('first_name')}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.last_name')}}
                                        </label>
                                        <input type="text" name="last_name"  class="form-control" placeholder="{{trans('trans.last_name')}}" value="{{old('last_name')}}" required>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-info btn-lg btn-rounded ml-10">{{trans('trans.pay')}}</button>
                        </form>
                    </section>

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
