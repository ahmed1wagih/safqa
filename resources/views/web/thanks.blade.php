@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-12 col-md-12">
                        <!-- Checkout Area -->
                        <img src="{{asset('assets/images/thanks.png')}}" style="max-width: 20%;display: table;
        margin: 0 auto;">
                        <h4 class="mb-20" style="display: table;
        margin: 0 auto; padding-top: 20px">
                            {{trans('trans.thanks')}}
                        </h4>
                        <!-- End Checkout Area -->
                    </div>

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
