@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container pt-40 pt-10">
            <div class="container">
                <section class="section faq-area pb-60">
                    <h3 class="h-title mb-30 t-uppercase">{{trans('trans.q&a')}}</h3>
                    <div id="accordion" class="panel-group">
                        @foreach($faqs as $faq)
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" @if($faq->id != $faqs[0]->id) class="collapsed" @endif href="#faq_payment{{$faq->id}}">{!! $faq->question !!}</a>
                                </h5>
                            </div>
                            <div id="faq_payment{{$faq->id}}" class="panel-collapse collapse @if($faq->id == $faqs[0]->id) in @endif">
                                <div class="panel-body">
                                    <p class='mb-30'>{!! $faq->answer !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
