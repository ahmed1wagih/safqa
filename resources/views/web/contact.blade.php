@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content" >
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-7 col-md-12">
                        <!-- Contact Us Area -->
                        <section class="contact-area contact-area-v2 panel ptb-30 prl-20">
                            <div class="row row-tb-30">
                                <div class="col-xs-12">
                                    <div class="contact-area-col contact-info">
                                        <div class="contact-info">
                                            <h3 class="t-uppercase h-title mb-20">{{trans('trans.contact')}}</h3>
                                                <p>{!! $about->text !!}</p>
                                            <ul class="contact-list mb-40">
                                                <li>
                                                    <span class="icon lnr lnr-map-marker"></span>
                                                    <h5>{{trans('trans.address')}}</h5>
                                                    <p class="color-mid">{!! $about->address !!}</p>
                                                </li>
                                                <li>
                                                    <span class="icon lnr lnr-envelope"></span>
                                                    <h5>{{trans('trans.email')}}</h5>
                                                    <p class="color-mid">{!! $about->email !!}</p>
                                                </li>
                                                <li>
                                                    <span class="icon lnr lnr-phone-handset"></span>
                                                    <h5>{{trans('trans.phone')}}</h5>
                                                    <p class="color-mid">{!! $about->phone !!}</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="contact-area-col contact-form">
                                        <h3 class="t-uppercase h-title mb-20">{{trans('trans.got_suggestion')}}</h3>
                                        <form action="/suggest" method="post">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label>{{trans('trans.name')}}</label>
                                                <input type="text" class="form-control" name="name" value="@if(user()) {{user()->name}} @else {{old('name')}} @endif" required>
                                            </div>
                                            <div class="form-group">
                                                <label>{{trans('trans.email')}}</label>
                                                <input type="text" class="form-control" name="email" value="@if(user()) {{user()->email}} @else {{old('email')}} @endif" required>
                                            </div>
                                            <div class="form-group">
                                                <label>{{trans('trans.message')}}</label>
                                                <textarea rows="5" class="form-control" name="message" required>{{old('message')}}</textarea>
                                            </div>
                                            <button class="btn" type="submit">{{trans('trans.send')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- End Contact Us Area -->
                    </div>

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

    <script>
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: parseFloat(mylat.value), lng: parseFloat(mylng.value)},
                zoom: 13

            });


            var pos = {
                lat: parseFloat(mylat.value),
                lng: parseFloat(mylng.value)
            };

            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable: false,
                animation: google.maps.Animation.DROP,
                title: '{{trans('trans.here')}}'

            });


        }


    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>
@endsection
