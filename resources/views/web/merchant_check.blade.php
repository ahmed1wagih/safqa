@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <section class="section checkout-area panel prl-30 pt-20 pb-40">
                        <h3 class="mb-20 h-title">
                           {{trans('trans.code_check')}}
                        </h3>
                        <div class="row">
                            <form class="mb-30" method="get" action="/merchant/deal/check">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                الكود
                                            </label>
                                            <input type="text" name="code" class="form-control" placeholder="أكتب الكود" required>
                                        </div>
                                    <button class="btn btn-info btn-lg btn-rounded ml-10">تحقق</button>
                                    </div>
                            </form>
                        </div>
                    </section>

                    @if(isset($code))
                        <section class="section checkout-area panel prl-30 pt-20 pb-40">
                            <h3 class="mb-20 h-title">
                               {{$code->serial_no}}
                            </h3>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                {{trans('trans.validity')}} :
                                            </label>
                                            <label>
                                               @if($code->status == 'valid')
                                                    <span style="color: green;"> {{trans('trans.valid')}}</span>
                                                @else
                                                    <span style="color: red;"> {{trans('trans.not_valid')}}</span>
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                @if($code->user_id != NULL)
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                {{trans('trans.code_owner')}} :
                                            </label>
                                            <label>
                                                    <span>{{$code->user->name}}</span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                {{trans('trans.old_price')}} :
                                            </label>
                                            <label>
                                                    <span style="color: grey;">{{$code->product->old_price}}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{trans('trans.new_price')}} :
                                        </label>
                                        <label>
                                            <span style="color: green;">{{$code->product->new_price}}</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <form class="mb-30" method="post" action="/merchant/report">
                                    {{csrf_field()}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="serial_no" class="form-control" value="{{$code->serial_no}}">
                                        </div>
                                        <button class="btn btn-primary btn-lg btn-rounded ml-10"> {{trans('trans.report_usage')}}</button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    @endif

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
