@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container">
            <div class="container">
                <section class="cart-area ptb-60">
                    <div class="container">
                        <div class="cart-wrapper">
                            <h3 class="h-title mb-30 t-uppercase">{{trans('trans.cart')}} ( {{$carts->count()}} )</h3>
                            <table id="cart_list" class="cart-list mb-30">
                                <thead class="panel t-uppercase">
                                <tr>
                                    <th>{{trans('trans.deal_name')}}</th>
                                    <th>{{trans('trans.price')}}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($carts as $cart)
                                    <tr class="panel alert">
                                    <td>
                                        <div class="media-right is-hidden-sm-down">
                                            <figure class="product-thumb">
                                                <img src="{{asset('/products/'.$cart->product->cover->image)}}" alt="{{$cart->product->title}}" alt="{{$cart->product->title}}">
                                            </figure>
                                        </div>
                                        <div class="media-body valign-middle">
                                            <h6 class="title mb-15 t-uppercase"><a href="/product/{{$cart->product_id}}">
                                                @if(lang() == 'ar')
                                                    {{$cart->ar_title}}
                                                @else
                                                        {{$cart->en_title}}
                                                @endif
                                            </a></h6>
                                            <div class="type font-12"><span class="t-uppercase">{{trans('trans.category')}}:</span>
                                                @if(lang() == 'ar')
                                                    {{$cart->product->category->parent->ar_name}} - {{$cart->product->category->ar_name}}
                                                @else
                                                    {{$cart->product->category->parent->en_name}} - {{$cart->product->category->en_name}}
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if($cart->product->deal_price == 1)
                                            {{trans('trans.one_point')}}
                                        @elseif($cart->product->deal_price == 2)
                                            {{trans('trans.two_points')}}
                                        @elseif($cart->product->deal_price > 2 && $cart->product->deal_price <11)
                                            {{$cart->product->deal_price}} {{trans('trans.points')}}
                                        @else
                                            @if(lang() == 'ar')
                                                {{$cart->product->deal_price}} {{trans('trans.point')}}
                                            @else
                                                {{$cart->product->deal_price}} {{trans('trans.points')}}
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        <a href="/cart/delete/{{$cart->id}}" title="{{trans('trans.remove')}}">
                                            <button type="button" class="close">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if($carts->count() > 0)
                                <div class="cart-price">
                                    <h5 class="t-uppercase mb-20">{{trans('trans.cart_total')}}</h5>
                                    <ul class="panel mb-20">
                                        <li>
                                            <div class="item-name">
                                                <strong class="t-uppercase">{{trans('trans.total_required')}}</strong>
                                            </div>
                                            <div class="price">

                                                <span>@if($total == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($total == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($total > 2 && $total <11)
                                                        {{$total}} {{trans('trans.points')}}
                                                    @else
                                                        @if(lang() == 'ar')
                                                            {{$total}} {{trans('trans.point')}}
                                                        @else
                                                            {{$total}} {{trans('trans.points')}}
                                                        @endif
                                                    @endif</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="t-left">
                                        <a href="/checkout/payment" class="btn btn-rounded btn-lg">{{trans('trans.checkout')}}</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
