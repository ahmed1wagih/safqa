@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.apply_as_merchant')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-12 col-right">
                            <form class="p-40" method="post" action="/merchant/apply">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.city')}}</label>
                                    <select name="city_id"  class="form-control input-lg" title="{{trans('trans.city')}}" required>
                                        <option selected disabled>{{trans('trans.choose_from_below')}}</option>
                                        @foreach($countries as $country)
                                            <optgroup label="{{$country->name}}">
                                                @foreach($country->cities as $city)
                                                    <option value="{{$city->id}}">{{lang() == 'ar' ? $city->ar_name : $city->en_name}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    @include('web.layouts.error', ['input' => 'type'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.ar_name')}}</label>
                                    <input type="text" name="ar_name" class="form-control input-lg" placeholder="{{trans('trans.ar_name')}}" value="{{old('ar_name')}}" required>
                                    @include('web.layouts.error', ['input' => 'ar_name'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.en_name')}}</label>
                                    <input type="text" name="en_name" class="form-control input-lg" placeholder="{{trans('trans.en_name')}}" value="{{old('en_name')}}" required>
                                    @include('web.layouts.error', ['input' => 'en_name'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.ar_desc')}}</label>
                                    <textarea name="ar_desc" class="form-control input-lg" placeholder="{{trans('trans.ar_desc')}}" rows="5">{{old('ar_desc')}}</textarea>
                                    @include('web.layouts.error', ['input' => 'ar_desc'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.en_desc')}}</label>
                                    <textarea name="en_desc" class="form-control input-lg" placeholder="{{trans('trans.en_desc')}}" rows="5">{{old('en_desc')}}</textarea>
                                    @include('web.layouts.error', ['input' => 'en_desc'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.ar_address')}}</label>
                                    <textarea name="ar_address" class="form-control input-lg" placeholder="{{trans('trans.ar_address')}}" rows="5">{{old('ar_address')}}</textarea>
                                    @include('web.layouts.error', ['input' => 'ar_address'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.en_address')}}</label>
                                    <textarea name="en_address" class="form-control input-lg" placeholder="{{trans('trans.en_address')}}" rows="5">{{old('en_address')}}</textarea>
                                    @include('web.layouts.error', ['input' => 'en_address'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.owner')}}</label>
                                    <input type="text" name="owner" class="form-control input-lg" placeholder="{{trans('trans.owner')}}" value="{{old('owner')}}" required>
                                    @include('web.layouts.error', ['input' => 'owner'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.owner_phone')}}</label>
                                    <input type="text" name="owner_phone" class="form-control input-lg" placeholder="{{trans('trans.owner_phone')}}" value="{{old('owner_phone')}}" required>
                                    @include('web.layouts.error', ['input' => 'owner_phone'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.in_charge')}}</label>
                                    <input type="text" name="in_charge" class="form-control input-lg" placeholder="{{trans('trans.in_charge')}}" value="{{old('in_charge')}}" required>
                                    @include('web.layouts.error', ['input' => 'in_charge'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.in_charge_phone')}}</label>
                                    <input type="text" name="in_charge_phone" class="form-control input-lg" placeholder="{{trans('trans.in_charge_phone')}}" value="{{old('in_charge_phone')}}" required>
                                    @include('web.layouts.error', ['input' => 'in_charge_phone'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email')}}</label>
                                    <input type="email" name="email" class="form-control input-lg" placeholder="{{trans('trans.email')}}" value="{{old('email')}}" required>
                                    @include('web.layouts.error', ['input' => 'email'])
                                </div>

                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="password" name="new_password" class="form-control input-lg" placeholder="{{trans('trans.password')}}" value="{{old('new_password')}}" required>
                                    @include('web.layouts.error', ['input' => 'new_password'])
                                </div>

                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password_confirmation')}}</label>
                                    <input type="password" name="new_password_confirmation" class="form-control input-lg" placeholder="{{trans('trans.password_confirmation')}}" value="{{old('new_password_confirmation')}}" required>
                                    @include('web.layouts.error', ['input' => 'new_password_confirmation'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.type')}}</label>
                                    <select name="type" id="type_select" class="form-control input-lg" required>
                                        <option selected disabled>{{trans('trans.choose_from_below')}}</option>
                                        <option value="local" {{old('type') == 'local' ? 'selected' : ''}}>{{trans('trans.local')}}</option>
                                        <option value="online" {{old('type') == 'online' ? 'selected' : ''}}>{{trans('trans.online')}}</option>
                                    </select>
                                    @include('web.layouts.error', ['input' => 'type'])
                                </div>
                                <div class="form-group online_div" style="display: none;">
                                    <label class="sr-only">{{trans('trans.website_link')}}</label>
                                    <input type="text" name="link" class="form-control input-lg" placeholder="{{trans('trans.website_link')}}" value="{{old('link')}}">
                                    @include('web.layouts.error', ['input' => 'website_link'])
                                </div>

                                <div class="form-group local_div" style="display: none;">
                                    <label class="sr-only">{{trans('trans.map_location')}}</label>
                                    <div >
                                        <div id="map" style=" height: 600px;"></div>
                                    </div>
                                    <input type="hidden" value="" id="mylat" name="lat">
                                    <input type="hidden" value="" id="mylng" name="lng">
                                    @include('web.layouts.error', ['input' => 'lat'])
                                </div>

                                <div class="custom-checkbox mb-20">
                                    <input type="checkbox" name="policy" id="agree_terms" required>
                                    <label class="color-mid" for="agree_terms">{{trans('trans.i_agree_on')}} <a href="/terms/merchants" class="color-green">{{trans('trans.merchant_terms')}}</a> {{trans('trans.i_signed')}} {{trans('trans.and')}} <a href="/privacy" class="color-green">{{trans('trans.policy')}}</a>.</label>
                                    @include('web.layouts.error', ['input' => 'policy'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.apply')}}</button>
                            </form>

                        </div>

                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

    <script>
        $('#type_select').on('change', function()
        {
           if($(this).val() === 'local')
           {
               $('.local_div').show();
               $('.online_div').hide();
           }
           else if($(this).val() === 'online')
           {
               $('.online_div').show();
               $('.local_div').hide();
           }
        });
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7136, lng: 46.6753},
                zoom: 14

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = $('#mylat'),
                        mylng = $('#mylng');

                    var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };

                    mylat.value = pos.lat;
                    mylng.value = pos.lng;

                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        $('#mylat').val(marker.getPosition().lat());
                        $('#mylng').val(marker.getPosition().lng());
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>

@endsection
