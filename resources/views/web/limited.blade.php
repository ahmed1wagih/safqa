@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-10">
            <div class="container">
                <section class="section deals-area ptb-30">
                    <!-- Page Control -->
                {{--<header class="page-control panel ptb-15 prl-20 pos-r mb-30">--}}
                {{--<!-- List Control View -->--}}
                {{--<ul class="list-control-view list-inline">--}}
                {{--<li><a href="deals_list.html"><i class=""></i></a></li>--}}
                {{--<li><a href="deals_grid.html"><i class=""></i></a></li>--}}
                {{--</ul>--}}
                {{--<!-- End List Control View -->--}}
                {{--<div class="left-10 pos-tb-center">--}}
                {{--<select class="form-control input-sm">--}}
                {{--<option>ترتيب حسب</option>--}}
                {{--<option>أحدث العناصر</option>--}}
                {{--<option>أفضل البائعين</option>--}}
                {{--<option>أفضل تصنيف</option>--}}
                {{--<option>السعر من الارخص للاعلى</option>--}}
                {{--<option>السعر: من الأعلى إلى الأقل</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--</header>--}}
                <!-- End Page Control -->
                    <div class="row row-masnory row-tb-20">
                        @foreach($products as $product)
                            <div class="col-sm-6 col-lg-4">
                                <div class="deal-single panel">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{asset('/products/'.$product->cover->image)}}">
                                        <div class="label-discount left-20 top-15">-{{$product->get_discount($product->id)}}%</div>
                                        <ul class="deal-actions top-10 right-10">
                                            @if(user())
                                                <li class="like-deal">
                                                    <span style="width: 32px; height: 26px;">
                                                        @if($product->is_like($product->id) == true)
                                                            <a href="/wish_delete/{{$product->id}}"><i class="fa fa-heart fa-2x" style="color: red !important;"></i></a>
                                                        @else
                                                            <a href="/wish_store/{{$product->id}}"><i class="fa fa-heart-o fa-2x" style="color: red !important;"></i></a>
                                                        @endif
                                                    </span>
                                                </li>
                                            @endif
                                        </ul>
                                        <div class="time-right bottom-15 left-20 font-md-14">
                                            <span>
                                                <h5>{{trans('trans.exclusive')}}</h5>
                                            </span>
                                        </div>
                                        <div class="time-right bottom-15 left-20 font-md-14"> <span> <i class="ico fa fa-clock-o ml-10"></i> <span class="t-uppercase" data-countdown="{{$product->expire_at}}"></span> </span> </div>
                                    </figure>
                                    <div class="bg-white pt-20 pr-20 pl-15">
                                        <div class="pl-md-10">
                                            <div class="rating mb-10"> <span class="rating-stars rate-allow" data-rating="{{$product->get_rate($product->id)}}"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> </div>
                                            <h3 class="deal-title mb-10"> <a href="/product/{{$product->id}}">
                                                    @if(lang() == 'ar')
                                                        {{$product->ar_title}}
                                                    @else
                                                        {{$product->en_title}}
                                                    @endif
                                                </a> </h3>
                                            <ul class="deal-meta list-inline mb-10 color-mid">
                                                <li><i class="ico fa fa-map-marker ml-10"></i>
                                                    @if(lang() == 'ar')
                                                        {{$product->city->ar_name}}
                                                    @else
                                                        {{$product->city->en_name}}
                                                    @endif
                                                </li>
                                                <li><span style="color: #36c341"> {{trans('trans.discount')}} </span>{{$product->get_discount($product->id)}}%</li>
                                            </ul>
                                            <p class="text-muted mb-20"></p>
                                        </div>
                                        <div class="deal-price pos-r mb-15">
                                            <h3 class="price ptb-5 "><span class="price-sale">{{$product->old_price}}</span>{{$product->new_price}} {{$product->city->parent->ar_currency}}</h3>
                                        </div>
                                        <ul class="deal-meta list-inline mb-10 color-mid">

                                            @if($product->expire_at >= \Carbon\Carbon::today()->toDateString())
                                                <li><a href="/cart/store/{{$product->id}}" class="btn btn-lg btn-rounded ml-10">{{trans('trans.buy_deal')}}</a></li>
                                            @else
                                                <li><a class="btn btn-lg btn-rounded ml-10" disabled>{{trans('trans.deal_expired')}}</a></li>
                                            @endif

                                            <li> <h3 class="price ptb-5 ">
                                                    @if($product->deal_price == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($product->deal_price == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                        {{$product->deal_price}} {{trans('trans.points')}}
                                                    @else
                                                        @if(lang() == 'ar')
                                                            {{$product->deal_price}} {{trans('trans.point')}}
                                                        @else
                                                            {{$product->deal_price}} {{trans('trans.points')}}
                                                        @endif
                                                    @endif
                                                </h3></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <!-- Page Pagination -->
                    <div class="page-pagination text-center mt-30 p-10 panel">
                        <nav>
                            <!-- Page Pagination -->
                            <ul class="page-pagination">
                                @if($products->previousPageUrl() != NULL) <li><a href="{{$products->previousPageUrl()}}" class="page-numbers previous">{{trans('trans.previous')}}</a></li> @endif
                                    @for($i = 1; $i <= $products->lastPage(); $i++)
                                        <li><a href="?page={{$i}}" class="page-numbers @if($i == $products->currentPage()) current @endif">{{$i}}</a></li>
                                    @endfor
                                @if($products->nextPageUrl() != NULL) <li><a href="{{$products->nextPageUrl()}}" class="page-numbers next">{{trans('trans.next')}}</a></li> @endif
                            </ul>
                            <!-- End Page Pagination -->
                        </nav>
                    </div>
                    <!-- End Page Pagination -->
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
