@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.reset_password')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/password_reset/check" method="post">
                                  {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email')}}</label>
                                    <input type="text" name="email" class="form-control input-lg" placeholder="{{trans('trans.email')}}" required>
                                    @include('web.layouts.error', ['input' => 'email'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="number" name="code" class="form-control input-lg" placeholder="{{trans('trans.code')}}" required>
                                    @include('web.layouts.error', ['input' => 'password'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.check_code')}}</button>
                            </form>

                        </div>
                    </div>
                </section>
                <section class="sign-area panel p-40">
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/password_reset/send" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email')}}</label>
                                    <input type="email" name="email_send" class="form-control input-lg" placeholder="{{trans('trans.email')}}" required>
                                    @include('web.layouts.error', ['input' => 'email_send'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.re_send')}}</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
