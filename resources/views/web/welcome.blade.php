@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-10">
            <div class="container">
                <div class="section deals-header-area ptb-30">
                    <div class="row row-tb-20">
                        <div class="col-xs-12 col-md-8 col-lg-9">
                            <div class="header-deals-slider owl-slider" data-loop="true" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false" data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1" data-lg-nav="true">
                                @foreach($specials as $product)
                                    <div class="deal-single panel item">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{asset('/products/'.$product->cover->image)}}">
                                        <div class="deal-about p-20 pos-a bottom-0 right-0">
                                            <div class="rating mb-10"> <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews color-light"> ( <span class="rating-count">{{$product->get_rate($product->id)}}</span> {{trans('trans.rates')}} ) </span> </div>
                                            <h3 class="deal-title mb-10 "> <a href="/product/{{$product->id}}" class="color-light color-h-lighter">
                                                    @if(lang() == 'ar')
                                                        {{$product->ar_title}}
                                                    @else
                                                        {{$product->en_title}}
                                                    @endif
                                                </a> </h3>
                                        </div>
                                    </figure>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-3">
                            @foreach($side_specials as $side)
                                <a href="{{$side->link}}" title="{{$side->link}}"><div class="deal-single panel panel2">
                                    <figure class="deal-thumbnail2 deal-thumbnail embed-responsive embed-responsive-16by9"  data-bg-img="{{asset('/sides/'.$side->image)}}" style="background-image: url('/public/sides/{{$side->image}}');"></figure>
                                </div></a>
                            @endforeach
                        </div>
                        <style>
                            .deal-thumbnail2{
                                height: 239px !important;
                            }
                            .panel2{
                                border-radius: 0px !important;
                            }
                        </style>
                    </div>
                </div>

                <section class="section latest-deals-area ptb-30">
                    <header class="panel ptb-15 prl-20 pos-r mb-30">
                        <h3 class="section-title font-18">{{trans('trans.catch_or_not')}}</h3>
                        <a href="/limited" class="btn btn-o btn-xs pos-a left-10 pos-tb-center">{{trans('trans.see_all')}}</a> </header>
                    <div class="row row-masnory row-tb-20">
                        @foreach($limited_products as $product)
                            <div class="col-sm-6 col-lg-4">
                                <div class="deal-single panel">
                                    <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{asset('products/'.$product->cover->image)}}">
                                        <div class="time-right bottom-15 left-20 font-md-14">
                                            <span>
                                                <h5>{{trans('trans.exclusive')}}</h5>
                                            </span>
                                        </div>

                                        <div class="time-right bottom-15 -20 font-md-14"> <span> <i class="ico fa fa-clock-o ml-10"></i> <span class="t-uppercase" data-countdown="{{$product->expire_at}}"></span> </span> </div>
                                    </figure>
                                    <div class="bg-white pt-20 pr-20 pl-15">
                                        <div class="pl-md-10">
                                            <div class="rating mb-10"> <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews">  </span> </div>
                                            <h3 class="deal-title mb-10">
                                                @if(lang() == 'ar')
                                                    <a href="/product/{{$product->id}}" title="{{$product->ar_title}}">
                                                        {{ mb_substr($product->ar_title,0,32)}}
                                                    </a>
                                                @else
                                                    <a href="/product/{{$product->id}}" title="{{$product->en_title}}">
                                                        {{ mb_substr($product->ar_title,0,32)}}
                                                    </a>
                                                @endif
                                            </h3>
                                            <ul class="deal-meta list-inline mb-10 color-mid">
                                                <li><i class="ico fa fa-map-marker ml-10"></i>
                                                    @if(lang() == 'ar')
                                                        {{$product->city->ar_name}}
                                                    @else
                                                        {{$product->city->en_name}}
                                                    @endif
                                                </li>
                                                <li><span style="color: #36c341"> {{trans('trans.discount')}} </span>{{$product->get_discount($product->id)}}%</li>
                                            </ul>
                                            <p class="text-muted mb-20"></p>
                                        </div>
                                        <div class="deal-price pos-r mb-15">
                                            <h3 class="price ptb-5 "><span class="price-sale"> {{$product->old_price}}  {{$product->city->parent->ar_currency}}</span> {{$product->new_price}}  {{$product->city->parent->ar_currency}}</h3>
                                        </div>
                                        <ul class="deal-meta list-inline mb-10 color-mid">

                                            @if($product->expire_at >= \Carbon\Carbon::today()->toDateString())
                                                <li><a href="/cart/store/{{$product->id}}" class="btn btn-lg btn-rounded ml-10">{{trans('trans.buy_deal')}}</a></li>
                                            @else
                                                <li><a class="btn btn-lg btn-rounded ml-10" disabled>{{trans('trans.deal_expired')}}</a></li>
                                            @endif

                                            <li> <h3 class="price ptb-5 " style="color: green;">
                                                    @if($product->deal_price == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($product->deal_price == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                        {{$product->deal_price}} {{trans('trans.points')}}
                                                    @else
                                                        @if(lang() == 'ar')
                                                            {{$product->deal_price}} {{trans('trans.point')}}
                                                        @else
                                                            {{$product->deal_price}} {{trans('trans.points')}}
                                                        @endif
                                                    @endif
                                                </h3></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>

                @foreach($all_cats as $cat)
                    <section class="section latest-deals-area ptb-30">
                        @php
                            $banner = \App\Models\Category::getBanner();
                        @endphp
                        @if($banner)
                            <div class="col-md-12 col-sm-12 col-lg-12" style="margin-bottom: 30px">
                                <a href="{{$banner->link}}" class="banner-link" data-id="{{$banner->id}}" target="_blank"><img src="/public/banners/{{$banner->image}}" style="height: {{$banner->height}}px !important;  width : {{$banner->width}}px !important;"> </a>
                            </div>
                        @endif

                            <header class="panel ptb-15 prl-20 pos-r mb-30">
                                <h3 class="section-title font-18">
                                    @if(lang() == 'ar')
                                        {{$cat->ar_name}}
                                    @else
                                        {{$cat->en_name}}
                                    @endif
                                </h3>
                                <a href="/category/{{$cat->id}}/all" class="btn btn-o btn-xs pos-a left-10 pos-tb-center">{{trans('trans.see_all')}}</a>
                            </header>
                            <div class="row row-masnory row-tb-20">
                                @foreach($cat->home_products($cat->id) as $product)
                                    <div class="col-sm-6 col-lg-4">
                                        <div class="deal-single panel">
                                            <figure class="deal-thumbnail embed-responsive embed-responsive-16by9" data-bg-img="{{asset('/products/'.$product->cover->image)}}">
                                                <div class="time-right bottom-15 left-20 font-md-14">
                                            <span>
                                                <h5>{{trans('trans.exclusive')}}</h5>
                                            </span>
                                                </div>
                                                <div class="time-right bottom-15 left-20 font-md-14"> <span> <i class="ico fa fa-clock-o ml-10"></i> <span class="t-uppercase" data-countdown="{{$product->expire_at}}"></span></span> </div>
                                            </figure>
                                            <div class="bg-white pt-20 pr-20 pl-15">
                                                <div class="pl-md-10">
                                                    <div class="rating mb-10"> <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> <span class="rating-reviews">  </span> </div>
                                                    <h3 class="deal-title mb-10">
                                                        @if(lang() == 'ar')
                                                            <a href="/product/{{$product->id}}" title="{{$product->ar_title}}">
                                                                {{ mb_substr($product->ar_title,0,32)}}
                                                            </a>
                                                        @else
                                                            <a href="/product/{{$product->id}}" title="{{$product->en_title}}">
                                                                {{ mb_substr($product->ar_title,0,32)}}
                                                            </a>
                                                        @endif
                                                    </h3>
                                                    <ul class="deal-meta list-inline mb-10 color-mid">
                                                        <li><i class="ico fa fa-map-marker ml-10"></i>
                                                            @if(lang() == 'ar')
                                                                {{$product->city->ar_name}}
                                                            @else
                                                                {{$product->city->en_name}}
                                                            @endif</li>
                                                        <li><span style="color: #36c341"> {{trans('trans.discount')}} </span>{{$product->get_discount($product->id)}}%</li>
                                                    </ul>
                                                    <p class="text-muted mb-20"></p>
                                                </div>
                                                <div class="deal-price pos-r mb-15">
                                                        <h3 class="price ptb-5 "><span class="price-sale"> {{$product->old_price}} {{$product->city->parent->ar_currency}}</span> {{$product->new_price}} {{$product->city->parent->ar_currency}}</h3>
                                                </div>
                                                <ul class="deal-meta list-inline mb-10 color-mid">

                                                    @if($product->expire_at >= \Carbon\Carbon::today()->toDateString())
                                                        <li><a href="/cart/store/{{$product->id}}" class="btn btn-lg btn-rounded ml-10">{{trans('trans.buy_deal')}}</a></li>
                                                    @else
                                                        <li><a class="btn btn-lg btn-rounded ml-10" disabled>{{trans('trans.deal_expired')}}</a></li>
                                                    @endif

                                                    <li> <h3 class="price ptb-5" style="color: green;">
                                                            @if($product->deal_price == 1)
                                                                {{trans('trans.one_point')}}
                                                            @elseif($product->deal_price == 2)
                                                                {{trans('trans.two_points')}}
                                                            @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                                {{$product->deal_price}} {{trans('trans.points')}}
                                                            @else
                                                                @if(lang() == 'ar')
                                                                    {{$product->deal_price}} {{trans('trans.point')}}
                                                                @else
                                                                    {{$product->deal_price}} {{trans('trans.points')}}
                                                                @endif
                                                            @endif
                                                        </h3>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                    </section>
                @endforeach
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
    <script>
        $('.banner-link').on('click', function () {
            var banner_id = $(this).data('id');
            console.log(banner_id);
            $.ajax
            (
                {
                    url: '/banner/click',
                    method: 'POST',
                    data: { banner_id: banner_id, _token: '{{csrf_token()}}' },
                    dataType: 'json',
                    success: function (data)
                    {
                        console.log('banner clicked')
                    },
                    error : function(data)
                    {
                        console.log('banner ajax error',data)
                    }
                }
            );
        });
    </script>
    </script>
@endsection
