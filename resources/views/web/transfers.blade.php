@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container">
            <div class="container">
                <section class="cart-area ptb-60">
                    <div class="container">
                        <td class="cart-wrapper">
                            <h3 class="h-title mb-30 t-uppercase">{{trans('trans.bank_transfers')}}</h3>
                            <table id="cart_list" class="cart-list mb-30">
                                <thead class="panel t-uppercase">
                                <tr>
                                    <th>{{trans('trans.bank')}}</th>
                                    <th>{{trans('trans.trans_no')}}</th>
                                    <th>{{trans('trans.trans_name')}}</th>
                                    <th>{{trans('trans.date')}}</th>
                                    <th>{{trans('trans.pack')}}</th>
                                    <th>{{trans('trans.price')}}</th>
                                    <th>{{trans('trans.status')}}</th>

                                </tr>
                                </thead>
                                @foreach($transfers as $transfer)
                                    <tbody>
                                    <tr class="panel alert">
                                        <td>
                                            {{lang() == 'ar' ? $transfer->bank->ar_name : $transfer->bank->en_name}}
                                        </td>
                                        <td>
                                            {{$transfer->trans_no}}
                                        </td>
                                        <td>
                                            {{$transfer->name}}
                                        </td>
                                        <td>
                                            {{$transfer->date}}
                                        </td>
                                        <td>
                                            {{lang() == 'ar' ? $transfer->pack->ar_name : $transfer->pack->en_name}}
                                        </td>
                                        <td>
                                            {{$transfer->price}} {{lang() == 'ar' ? user()->country->ar_currency : user()->country->en_currency}}
                                        </td>
                                        <td>
                                            <span style="background-color : @if($transfer->status == 'awaiting') grey; @elseif($transfer->status == 'approved') green; @else red; @endif" class="label">{{trans('trans.'.$transfer->status)}}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>

                            <h3 class="h-title mb-30 t-uppercase">{{trans('trans.online_transfers')}}</h3>
                            <table id="cart_list" class="cart-list mb-30">
                                <thead class="panel t-uppercase">
                                <tr>
                                    <th>{{trans('trans.platform')}}</th>
                                    <th>{{trans('trans.trans_no')}}</th>
                                    <th>{{trans('trans.date')}}</th>
                                    <th>{{trans('trans.pack')}}</th>
                                    <th>{{trans('trans.price')}}</th>
                                    <th>{{trans('trans.status')}}</th>

                                </tr>
                                </thead>
                                @foreach($onlines as $online)
                                    <tbody>
                                    <tr class="panel alert">
                                        <td>
                                            {{trans('trans.'.$online->type)}}
                                        </td>
                                        <td>
                                            {{$online->trans_id}}
                                        </td>
                                        <td>
                                            {{$online->created_at->toDateString()}}
                                        </td>
                                        <td>
                                            {{lang() == 'ar' ? $online->pack->ar_name : $transfer->pack->en_name}}
                                        </td>
                                        <td>
                                            {{$transfer->price}} {{lang() == 'ar' ? 'دولار' : 'usd'}}
                                        </td>
                                        <td>
                                            <span style="background-color : @if($transfer->status == 'awaiting') grey; @elseif($transfer->status == 'approved') green; @else red; @endif" class="label">{{trans('trans.'.$transfer->status)}}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>

                            <div class="cart-price">
                                <h5 class="t-uppercase mb-20">{{trans('trans.total')}}</h5>
                                <ul class="panel mb-20">
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.total_confirmed_transfers')}}
                                        </div>
                                        <div class="price">
                                            {{$transfers->where('status','approved')->sum('price') + $onlines->where('status','approved')->sum('amount')}} {{lang() == 'ar' ? user()->country->ar_currency : user()->country->en_currency}}
                                        </div>
                                    </li>
                                </ul>
                                <div class="t-left">
                                    <a href="/profile" class="btn btn-rounded btn-lg">{{trans('trans.back_to_profile')}}</a>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
