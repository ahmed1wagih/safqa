@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container">
            <div class="container">
                <section class="cart-area ptb-60">
                    <div class="container">
                        <div class="cart-wrapper">


                            <div class="cart-price">
                                <h5 class="t-uppercase mb-20">{{trans('trans.profile')}} </h5>
                                <ul class="panel mb-20">
                                    <li>
                                        <div class="item-name">
                                            <i class="fa fa-user"> </i>
                                            {{trans('trans.my_info')}}
                                        </div>
                                        <div class="price">
                                            <a href="/profile/edit">{{trans('trans.update')}}</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            <i class="fa fa-shopping-cart"> </i>
                                            {{trans('trans.my_deals')}}

                                        </div>
                                        <div class="price">
                                            <a href="/my_deals">{{trans('trans.view')}}</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            <i class="fa fa-money"> </i>
                                            {{trans('trans.my_transfers')}}

                                        </div>
                                        <div class="price">
                                            <a href="/my_transfers">{{trans('trans.view')}}</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            <i class="fa fa-heart"> </i>
                                            {{trans('trans.favorites')}}
                                        </div>
                                        <div class="price">
                                            <a href="/wish_list">{{trans('trans.view')}}</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.credit')}}
                                        </div>
                                        <div class="price">
                                            <a href="/charge_credit">{{user()->credit}}
                                                @if(user()->credit == 0)
                                                    {{trans('trans.0_points')}}
                                                @elseif(user()->credit == 1)
                                                    {{trans('trans.one_point')}}
                                                @elseif(user()->credit == 2)
                                                    {{trans('trans.two_points')}}
                                                @elseif(user()->credit > 2 && user()->credit < 11)
                                                    {{trans('trans.points')}}
                                                @else
                                                    @if(lang() == 'ar')
                                                        {{trans('trans.point')}}
                                                    @else
                                                        {{trans('trans.points')}}
                                                    @endif
                                                @endif
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            <i class="fa fa-comment"> </i>
                                            {{trans('trans.contact_admin')}}
                                        </div>
                                        <div class="price">
                                            <a href="/contact_us">{{trans('trans.contact_now')}}</a>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
