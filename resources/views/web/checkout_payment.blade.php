@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-8 col-md-9">
                        <!-- Checkout Area -->
                        {{--<section class="section checkout-area panel prl-30 pt-20 pb-40">--}}
                            {{--<h3 class="mb-20 h-title">--}}
                              {{--إشحن رصيدك--}}
                                {{--(--}}
                                {{--البطاقات الإئتمانية--}}
                                {{-----}}
                                {{--تحويل بنكي--}}
                                {{-----}}
                                {{--<a href="https://www.paytabz.com/bestdeal" target="_blank">باي تابز</a>--}}
                                {{--)--}}
                            {{--</h3>--}}
                            {{--<!-- Checkout Area -->--}}
                            {{--<section class="section checkout-area panel prl-30 pt-20 pb-40">--}}
                                {{--<h3 class="mb-20 h-title">--}}
                                    {{--تحويل بنكي--}}
                                {{--</h3>--}}
                                {{--<form class="mb-30" method="post" action="/charge">--}}
                                    {{--{{csrf_field()}}--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>--}}
                                                    {{--البنك المحول اليه--}}
                                                {{--</label>--}}
                                                {{--<select class="form-control" name="bank_id">--}}
                                                    {{--<option selected disabled>إختر بنك من الآتي</option>--}}
                                                    {{--@foreach($banks as $bank)--}}
                                                        {{--<option value="{{$bank->id}}">{{$bank->ar_name}} - {{$bank->number}} - {{$bank->iban}} - {{$bank->country->ar_name}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>--}}
                                                    {{--الاسم الكامل--}}
                                                {{--</label>--}}
                                                {{--<input type="text" name="name" class="form-control" placeholder="أدخل  الاسم بالكامل" required>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>--}}
                                                    {{--رقم عملية التحويل--}}
                                                {{--</label>--}}
                                                {{--<input type="text" name="trans_no" class="form-control" placeholder="أدخل رقم العملية" required>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>--}}
                                                    {{--المبلغ--}}
                                                {{--</label>--}}
                                                {{--<input type="number" name="amount" min="0" class="form-control" placeholder="أدخل المبلغ" required>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>--}}
                                                    {{--تاريخ التحويل--}}
                                                {{--</label>--}}
                                                {{--<input type="date" name="date" class="form-control" placeholder="أدخل تاريخ التحويل" required>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<input type="hidden" name="type" value="transfer">--}}
                                    {{--</div>--}}
                                    {{--<button type="submit" class="btn btn-info btn-lg btn-rounded ml-10">تأكيد عملية التحويل</button>--}}
                                {{--</form>--}}
                            {{--</section>--}}
                            <section class="section checkout-area panel prl-30 pt-20 pb-40">
                                <h3 class="mb-20 h-title">
                                    {{trans('trans.pay_with_credit')}}
                                </h3>
                                <form class="mb-30" dir="rtl" method="post" action="/credit_checkout">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>
                                                    {{trans('trans.current_credit')}}
                                                </label>
                                                <label class="form-control" style="color: green;">
                                                    @if(user()->credit == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif(user()->credit == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif(user()->credit > 2 && user()->credit <11)
                                                        {{user()->credit}} {{trans('trans.points')}}
                                                    @else
                                                        {{user()->credit}} {{trans('trans.point')}}
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>
                                                    {{trans('trans.cost')}}
                                                </label>
                                                <label class="form-control" style="color: red;">
                                                    @if($total == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($total == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($total > 2 && $total <11)
                                                        {{$total}} {{trans('trans.points')}}
                                                    @else
                                                        {{$total}} {{trans('trans.point')}}
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>
                                                    {{trans('trans.remaining_credit')}}
                                                </label>
                                                <label class="form-control" @if(user()->credit >= $total) style="color: green;" @else style="color: red;" @endif>
                                                    @if(user()->credit - $total == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif(user()->credit - $total == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif(user()->credit - $total > 2 && user()->credit - $total <11)
                                                        {{user()->credit - $total}} {{trans('trans.points')}}
                                                    @else
                                                        {{user()->credit - $total}} {{trans('trans.point')}}
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @if(user()->credit >= $total)
                                            <button type="submit" class="btn btn-info btn-lg btn-rounded ml-10">{{trans('trans.pay_with_credit')}}</button>
                                        @else
                                            <a href="/charge_credit"><label class="btn btn-lg btn-rounded ml-10">{{trans('trans.subscribe')}}</label></a>
                                        @endif
                                    </div>
                                </form>
                            </section>

                            <!-- End Checkout Area -->
                        </section>
                    </div>
                    <div class="page-sidebar col-xs-12 col-sm-4 col-md-3">
                        <!-- Blog Sidebar -->
                        <aside class="sidebar blog-sidebar">
                            <div class="row row-tb-10">
                                <div class="col-xs-12">
                                    <!-- Recent Posts -->
                                    <div class="widget checkout-widget panel p-20">
                                        <div class="widget-body">
                                            <table class="table mb-15">
                                                <tbody>
                                                <tr>
                                                    <td class="color-mid">{{trans('trans.total_deals')}}</td>
                                                    <td>
                                                    @if($total == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($total == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($total > 2 && $total <11)
                                                        {{$total}} {{trans('trans.points')}}
                                                    @else
                                                        {{$total}} {{trans('trans.point')}}
                                                    @endif
                                                   </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <a href="/cart" class="btn btn-info btn-block btn-sm">{{trans('trans.cart_edit')}}</a>
                                        </div>
                                    </div>
                                    <!-- End Recent Posts -->
                                </div>
                            </div>
                        </aside>
                        <!-- End Blog Sidebar -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
