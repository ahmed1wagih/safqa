@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.activate')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/activate" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.code')}}</label>
                                    <input type="number" name="code" class="form-control input-lg" min="1000" max="9999" placeholder="{{trans('trans.code')}}">
                                    @include('web.layouts.error', ['input' => 'code'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.activate_now')}}</button>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
