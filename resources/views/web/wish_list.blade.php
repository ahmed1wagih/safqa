@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-20">
            <div class="container">
                <section class="wishlist-area ptb-30">
                    <div class="container">
                        <div class="wishlist-wrapper">
                            <h3 class="h-title mb-40 t-uppercase">{{trans('trans.favorites')}}</h3>
                            <table id="cart_list" class="wishlist">
                                <tbody>
                                @foreach($wishes as $product)
                                    <tr class="panel alert">
                                        <td class="col-sm-8 col-md-9">
                                            <div class="media-right is-hidden-sm-down">
                                                <figure class="product-thumb">
                                                    <a href="/product/{{$product->id}}">
                                                    <img src="{{asset('/products/'.$product->cover->image)}}" alt="{{$product->title}}" style="width: 120px; height: 90px;">
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="media-body valign-middle">
                                                <h5 class="title mb-5 t-uppercase"><a href="/product/{{$product->id}}">{{$product->title}}</a></h5>
                                                <div class="rating mb-10">
                                                    <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}">
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                    </span>
                                                    <span class="rating-reviews">
                                                    ( <span class="rating-count">{{$product->get_discount($product->id)}}%</span> {{trans('trans.discount')}} )
                                                            </span>
                                                </div>
                                                <h4 class="price color-green">
                                                    @if($product->deal_price == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($product->deal_price == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                        {{$product->deal_price}} {{trans('trans.points')}}
                                                    @else
                                                        @if(lang() == 'ar')
                                                            {{$product->deal_price}} {{trans('trans.point')}}
                                                        @else
                                                            {{$product->deal_price}} {{trans('trans.points')}}
                                                        @endif
                                                    @endif
                                                </h4>
                                                <a href="/cart/store/{{$product->id}}"><button class="btn btn-rounded btn-sm mt-15 is-hidden-sm-up">{{trans('trans.add_to_cart')}}</button></a>
                                            </div>
                                        </td>
                                        <td class="col-sm-3 col-md-2 is-hidden-xs-down">
                                            @if($product->expire_at >= \Carbon\Carbon::today()->toDateString())
                                                <a href="/cart/store/{{$product->id}}" class="btn btn-lg btn-rounded ml-10">{{trans('trans.add_to_cart')}}</a>
                                            @else
                                                <a class="btn btn-lg btn-rounded ml-10" disabled>{{trans('trans.deal_expired')}}</a>
                                            @endif
                                        </td>
                                        <td class="col-sm-1">
                                            <a href="/wish_delete/{{$product->id}}">
                                                <button type="button" class="close pr-xs-0 pl-sm-10">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
