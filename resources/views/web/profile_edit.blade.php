@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.profile')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-12 col-right">
                            <form class="p-40" action="/profile/update" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.full_name')}}</label>
                                    <input type="text" class="form-control input-lg" name="name" value="{{$user->name}}">
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only">{{trans('trans.email')}}</label>--}}
{{--                                    <input type="email" class="form-control input-lg" name="email" value="{{$user->email}}">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only">{{trans('trans.phone')}}</label>--}}
{{--                                    <input type="number" class="form-control input-lg" name="phone" value="{{$user->phone}}">--}}
{{--                                </div>--}}

                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="password" class="form-control input-lg" name="password" placeholder="{{trans('trans.password')}} | {{trans('trans.leave_blank')}}">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password_confirmation')}}</label>
                                    <input type="password" class="form-control input-lg" name="password_confirmation" placeholder="{{trans('trans.password_confirmation')}}">
                                </div>


                                <button type="submit" class="btn btn-block btn-lg">
                                    {{trans('trans.update')}}
                                </button>
                            </form>

                        </div>

                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

    {{--<script>--}}
        {{--$('#country').on('change', function (e) {--}}
            {{--var parent_id = e.target.value;--}}

            {{--if (parent_id) {--}}
                {{--$.ajax({--}}
                    {{--url: '/get_cities/' + parent_id,--}}
                    {{--type: "GET",--}}
                    {{--dataType: "json",--}}

                    {{--success: function (data) {--}}
                        {{--$('#cities').empty();--}}
                        {{--$('#cities').append('<option selected disabled> إختر المدينة فضلك </option>');--}}
                        {{--$.each(data, function (i, city) {--}}
                            {{--$('#city').append('<option value="' + city.id + '">' + city.ar_name + '</option>');--}}
                        {{--});--}}
                    {{--}--}}
                {{--});--}}
            {{--}});--}}
    {{--</script>--}}
@endsection
