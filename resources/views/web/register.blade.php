@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.register')}}<small> {{trans('trans.or')}} </small><a href="/login" class="color-green">{{trans('trans.login')}}</a></h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-12 col-right">
                            <form class="p-40" method="post" action="/register">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.full_name')}}</label>
                                    <input type="text" name="name" class="form-control input-lg" placeholder="{{trans('trans.full_name')}}" value="{{old('name')}}" required>
                                    @include('web.layouts.error', ['input' => 'name'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email')}}</label>
                                    <input type="email" name="email" class="form-control input-lg" placeholder="{{trans('trans.email')}}" value="{{old('email')}}" required>
                                    @include('web.layouts.error', ['input' => 'email'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.phone')}}</label>
                                    <input type="text" name="phone" class="form-control input-lg" placeholder="{{trans('trans.phone')}}" value="{{old('phone')}}" required>
                                    @include('web.layouts.error', ['input' => 'phone'])
                                </div>

                               <div class="form-group">
                                    <label class="sr-only">الدولة</label>
                                    <select name="country_id" class="form-control input-lg">
                                        <option selected disabled>الرجاء إختيار دولة</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if(old('country_id') && $country->id == old('country_id')) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @include('web.layouts.error', ['input' => 'country_id'])
                              </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="password" name="password" class="form-control input-lg" placeholder="{{trans('trans.password')}}" required>
                                    @include('web.layouts.error', ['input' => 'password'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password_confirmation')}}</label>
                                    <input type="password" name="password_confirmation" class="form-control input-lg" placeholder="{{trans('trans.password_confirmation')}}" required>
                                    @include('web.layouts.error', ['input' => 'password_confirmation'])
                                </div>

                                <div class="custom-checkbox mb-20">
                                    <input type="checkbox" name="policy" id="agree_terms" required>
                                    <label class="color-mid" for="agree_terms">{{trans('trans.i_agree_on')}} <a href="/terms" class="color-green">{{trans('trans.terms')}}</a> و <a href="/privacy" class="color-green">{{trans('trans.policy')}}</a>.</label>
                                    @include('web.layouts.error', ['input' => 'policy'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.register')}}</button>
                            </form>

                        </div>

                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->

    <script>
        setTimeout(function (){
            $(':password').val('');
        },2000);
    </script>
{{--    <script type="text/javascript" src="{{asset('web/js/jquery-1.12.3.min.js')}}"></script>--}}

    <!--<script>-->
    <!--    $('#countries').on('change', function (e) {-->
    <!--        var country_id = e.target.value;-->
    <!--        if (country_id) {console.log(country_id);-->
    <!--            $.ajax({-->
    <!--                url: '/get_cities/' + country_id,-->
    <!--                type: "GET",-->

    <!--                dataType: "json",-->

    <!--                success: function (data) {-->
    <!--                    $('#cities').empty();-->
    <!--                    $('#cities').append('<option selected disabled> إختر المدينة </option>');-->
    <!--                    $.each(data, function (i, city) {-->
    <!--                        $('#cities').append('<option value="' + city.id + '">' + city.ar_name + '</option>');-->
    <!--                    });-->
    <!--                }-->

    <!--            });-->
    <!--        }});-->
    <!--</script>-->
@endsection
