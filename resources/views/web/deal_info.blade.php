@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container">
            <div class="container">
                <section class="cart-area ptb-60">
                    <div class="container">
                        <div class="cart-wrapper">
                            <h3 class="h-title mb-30 t-uppercase">{{trans('trans.deal_info')}}</h3>
                            <table id="cart_list" class="cart-list mb-30">
                                <thead class="panel t-uppercase">
                                <tr>
                                    <th>{{trans('trans.deal_name')}}</th>
                                    <th>{{trans('trans.deal_amount')}}</th>
                                    <th>{{trans('trans.date')}}</th>
                                    <th>{{trans('trans.validity')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <tr class="panel alert">
                                        <td>
                                            <div class="media-right is-hidden-sm-down">
                                                <figure class="product-thumb">
                                                    <img src="{{asset('/products/'.$deal->product->cover->image)}}" alt="{{$deal->product->ar_title}}">
                                                </figure>
                                            </div>
                                            <div class="media-body valign-middle">
                                                <h6 class="title mb-15 t-uppercase"><a href="/product/{{$deal->product_id}}">
                                                        @if(lang() == 'ar')
                                                            {{$deal->product->ar_title}}
                                                        @else
                                                            {{$deal->product->en_title}}
                                                        @endif
                                                    </a></h6>
                                                <div class="type font-12"><a href="/category/{{$deal->product->category->parent->id}}/{{$deal->product->category->id}}"><span class="t-uppercase">{{trans('trans.category')}} :</span>
                                                        @if(lang() == 'ar')
                                                            {{$deal->product->category->ar_name}}
                                                        @else
                                                            {{$deal->product->category->en_name}}
                                                        @endif
                                                    </a></div>
                                            </div>
                                        </td>
                                        <td>
                                            @if($deal->product->deal_price == 1)
                                                {{trans('trans.one_point')}}
                                            @elseif($deal->product->deal_price == 2)
                                                {{trans('trans.two_points')}}
                                            @elseif($deal->product->deal_price > 2 && $deal->product->deal_price <11)
                                                {{$deal->product->deal_price}} {{trans('trans.points')}}
                                            @else
                                                @if(lang() == 'ar')
                                                    {{$deal->product->deal_price}} {{trans('trans.point')}}
                                                @else
                                                    {{$deal->product->deal_price}} {{trans('trans.points')}}
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::parse($deal->purchased_at)->toDateString()}}
                                            <br/>
                                            {{\Carbon\Carbon::parse($deal->purchased_at)->toTimeString()}}
                                        </td>
                                        <td>
                                            @if($deal->status == 'invalid')
                                                <div class="sub-total">{{trans('trans.not_valid')}}</div>
                                            @else
                                                <div class="sub-total" style="color: green; !important;">{{trans('trans.valid')}}</div>
                                            @endif
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <div class="cart-price">
                                <h5 class="t-uppercase mb-20">{{trans('trans.codes')}}</h5>
                                <ul class="panel mb-20">
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.num_code')}}
                                        </div>
                                        <div class="price">
                                           {{$deal->serial_no}}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-name">
                                            {{trans('trans.bar_code')}}
                                        </div>
                                        <div class="price">

                                          @php
                                              echo \Milon\Barcode\DNS1D::getBarcodeSVG($deal->serial_no, "C39",3,130, null,true);
                                          @endphp
                                        </div>
                                    </li>

                                </ul>
                                <div class="t-left">
                                    <a href="/my_deals" class="btn btn-rounded btn-lg">{{trans('trans.back_to_my_deals')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
