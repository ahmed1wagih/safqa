@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.change_password')}}</h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/password_reset/change" method="post">
                                  {{csrf_field()}}
                                <input type="hidden" name="code" value="{{$code}}">
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="password" name="new_password" class="form-control input-lg" placeholder="{{trans('trans.password')}}" required>
                                    @include('web.layouts.error', ['input' => 'new_password'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password_confirmation')}}</label>
                                    <input type="password" name="new_password_confirmation" class="form-control input-lg" placeholder="{{trans('trans.password_confirmation')}}" required>
                                    @include('web.layouts.error', ['input' => 'new_password_confirmation'])
                                </div>
                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.change')}}</button>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
