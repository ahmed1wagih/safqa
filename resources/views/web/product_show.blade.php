@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->

    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-7 col-md-8">
                        <div class="row row-tb-20">
                            <div class="col-xs-12">
                                <div class="deal-deatails panel">
                                    <div class="deal-slider">
                                        <div id="product_slider" class="flexslider">
                                            <ul class="deal-actions top-10 right-10">
                                                @if(user())
                                                    <li class="like-deal">
                                                        <span style="width: 45px; height: 38px;">
                                                            @if($product->is_like($product->id) == true)
                                                                <a href="/wish_delete/{{$product->id}}"><i class="fa fa-heart fa-3x" style="color: red !important;"></i></a>
                                                            @else
                                                                <a href="/wish_store/{{$product->id}}"><i class="fa fa-heart-o fa-3x" style="color: red !important;"></i></a>
                                                            @endif
                                                        </span>
                                                    </li>
                                                @endif
                                            </ul>
                                            <ul class="slides">
                                                @foreach($product->images as $image)
                                                <li>
                                                    <img alt="" src="{{asset('/products/'.$image->image)}}">
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="deal-body p-20">

                                        <div class="rating mb-10" style="text-align: center">
                                            <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}">
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </span>
                                        </div>
                                            {{trans('trans.deal_info')}}
                                        <p class="mb-15">
                                            @if(lang() == 'ar')
                                                {!! $product->ar_desc !!}
                                            @else
                                                {!! $product->en_desc !!}
                                            @endif
                                        </p>
                                            {{trans('trans.deal_terms')}}
                                        <p class="mb-15">
                                            @if(lang() == 'ar')
                                                {!! $product->ar_conds !!}
                                            @else
                                                {!! $product->en_conds !!}
                                            @endif
                                        </p>
                                        @if($product->merchant->type == 'local')
                                            <a href="https://www.google.com/maps/?q={{$product->merchant->lat}},{{$product->merchant->lng}}" target="_blank" class="btn btn-block btn-lg">
                                                <i class="fa fa-map font-16 ml-10"></i> {{trans('trans.deal_location')}}
                                            </a>
                                        @else
                                            <a href="{{$product->merchant->link}}" target="_blank" class="btn btn-block btn-lg">
                                                <i class="fa fa-globe font-16 ml-10"></i> {{trans('trans.website_link')}}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="posted-review panel p-30">
                                    <h3 class="h-title">{{$product->get_rate_count($product->id)}} {{trans('trans.ratings')}}</h3>
                                    @foreach($product->rates as $rate)
                                        <div class="review-single pt-30">
                                        <div class="media">
                                            <div class="media-right">
                                                <img class="media-object ml-10 radius-4" src="{{asset('/users/default.png')}}" width="90" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="review-wrapper clearfix">
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span class="review-holder-name h5">{{$rate->user->name}}</span>
                                                        </li>
                                                        <li>
                                                            <div class="rating">
                                                                    <span class="rating-stars" data-rating="{{$rate->rate}}">
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <p class="review-date mb-5">{{\Carbon\Carbon::parse($rate->create_at)->toDateString()}}</p>
                                                    <p class="copy">{{$rate->text}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @if(user())
                            <div class="col-xs-12">
                                <div class="post-review panel p-20">
                                    <h3 class="h-title">{{trans('trans.rate_now')}}</h3>
                                    @include('admin.layouts.error', ['input' => 'rate'])
                                    @include('admin.layouts.error', ['input' => 'text'])
                                    <form class="horizontal-form pt-30" action="/rate" method="post">
                                        {{csrf_field()}}
                                        <div class="row row-v-10">
                                            <div class="col-xs-12">
                                                <ul class="select-rate list-inline ptb-20">
                                                    <li><span>{{trans('trans.your_rate')}} : </span></li>
                                                    <li>
                                                    <input type="radio" value="1" name="rate">
                                                    <span class="rating" role="button">
                                                    <i class="fa fa-star"></i>
                                                    </span>
                                                    </li>

                                                    <li>
                                                    <input type="radio" value="2" name="rate">
                                                    <span class="rating" role="button">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    </span>

                                                    </li>
                                                    <li>
                                                    <input type="radio" value="3" name="rate">
                                                    <span class="rating" role="button">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    </span>
                                                    </li>

                                                    <li>
                                                    <input type="radio" value="4" name="rate">
                                                    <span class="rating" role="button">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    </span>
                                                    </li>

                                                    <li>
                                                    <input type="radio" value="5" name="rate">
                                                    <span class="rating" role="button">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    </span>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="col-xs-12">
                                                <textarea class="form-control" placeholder="{{trans('trans.your_rate')}}" name="text" rows="6"></textarea>
                                            </div>
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <div class="col-xs-12 text-left">
                                                <button type="submit" class="btn mt-20">{{trans('trans.publish_rate')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="page-sidebar col-md-4 col-sm-5 col-xs-12">
                        <!-- Blog Sidebar -->
                        <aside class="sidebar blog-sidebar">
                            <div class="row row-tb-10">
                                <div class="col-xs-12">
                                    <div class="widget single-deal-widget panel ptb-30 prl-20">
                                        <div class="widget-body text-center">
                                            <h2 class="mb-20 h3">
                                                @if(lang() == 'ar')
                                                    {{$product->ar_title}}
                                                @else
                                                    {{$product->en_title}}
                                                @endif

                                            </h2>
                                            <ul class="deal-meta list-inline mb-10 color-mid">
                                                <li><i class="ico fa fa-map-marker ml-10"></i>
                                                    @if(lang() == 'ar')
                                                        {{$product->city->parent->ar_name}} - {{$product->city->ar_name}}
                                                    @else
                                                        {{$product->city->parent->en_name}} - {{$product->city->en_name}}
                                                    @endif
                                                </li>
                                                <li> <span style="color: #36c341"> {{trans('trans.discount')}} </span> {{$product->get_discount($product->id)}}%</li>
                                            </ul>

                                            <div class="price mb-20">
                                                <h3 class="price ptb-5 "><span class="price-sale"> {{$product->old_price}} - {{$product->city->parent->ar_currency}}</span> {{$product->new_price}} - {{$product->city->parent->ar_currency}}</h3>
                                            </div>
                                            <div class="price mb-20">
                                                <h2 class="price" style="color: green;">
                                                    @if($product->deal_price == 1)
                                                        {{trans('trans.one_point')}}
                                                    @elseif($product->deal_price == 2)
                                                        {{trans('trans.two_points')}}
                                                    @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                        {{$product->deal_price}} {{trans('trans.points')}}
                                                    @else
                                                        @if(lang() == 'ar')
                                                            {{$product->deal_price}} {{trans('trans.point')}}
                                                        @else
                                                            {{$product->deal_price}} {{trans('trans.points')}}
                                                        @endif
                                                    @endif
                                                 </h2>
                                            </div>
                                            <div class="buy-now mb-40">
                                                @if($product->expire_at >= \Carbon\Carbon::today()->toDateString())
                                                    @if(user() && $product->limit <= \App\Models\ProductInfo::where('product_id',$product->id)->where('user_id',user()->id)->count())
                                                        <a class="btn btn-block btn-lg" disabled>
                                                            <i class="fa fa-shopping-cart font-16 ml-10"></i>{{trans('trans.limit_used')}}
                                                        </a>
                                                    @else
                                                        <a href="/cart/store/{{$product->id}}" class="btn btn-block btn-lg">
                                                            <i class="fa fa-shopping-cart font-16 ml-10"></i>{{trans('trans.buy_deal')}}
                                                        </a>
                                                    @endif
                                                @else
                                                    <a class="btn btn-block btn-lg" disabled>
                                                        <i class="fa fa-shopping-cart font-16 ml-10"></i>{{trans('trans.expired_deal')}}
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="time-right mb-30">
                                                <p class="t-uppercase color-muted">
                                                    {{trans('trans.catch_or_not')}}
                                                </p>
                                                <h5 class="color-mid font-14 font-lg-16" style="color: #3644c3 !important">
                                                    <i class="ico fa fa-clock-o ml-10"></i>
                                                    <span data-countdown="{{$product->expire_at}}"></span>
                                                </h5>
                                            </div>
                                            <ul class="list-inline social-icons social-icons--colored t-center">
                                                <li class="social-icons__item">
                                                    <a href="{{$socials->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li class="social-icons__item">
                                                    <a href="{{$socials->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li class="social-icons__item">
                                                    <a href="{{$socials->instagram}}" target="_blank"><i class="fa fa-instagram" style="color: white; background-color: red;"></i></a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <!-- Recent Posts -->
                                    <div class="widget about-seller-widget panel ptb-30 prl-20" style="text-align: center;">
                                        <h3 class="widget-title h-title">{{trans('trans.merchant_info')}}</h3>
                                        <div class="widget-body t-center">
                                            {{--<figure class="mtb-10">--}}
                                                {{--<img src="{{asset('merchants/')}}assets/images/brands/store_logo.jpg" alt="">--}}
                                            {{--</figure>--}}
                                            <br/>
                                            <div class="store-about mb-20">
                                                <h3 class="mb-10">
                                                    @if(lang() == 'ar')
                                                        {{$product->merchant->ar_name}}
                                                    @else
                                                        {{$product->merchant->en_name}}
                                                    @endif
                                                </h3>
                                                <div class="rating mb-10">
{{--                                                        <span class="rating-stars rate-allow" data-rating="{{$product->merchant->get_rate($product->id)}}">--}}
                                                        <span class="rating-stars" data-rating="{{$product->merchant->get_rate($product->merchant_id)}}">
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o star-active"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                        </span>
                                                    <span class="rating-reviews">
                                                        ( <span class="rating-count">{{$product->merchant->get_rate_count($product->merchant_id)}}</span> {{trans('trans.ratings')}} )
                                                    </span>
                                                </div>
                                                @if(lang() == 'ar')
                                                    <p class="mb-15">{!! $product->merchant->ar_desc !!}</p>
                                                    <p class="mb-15">{!! $product->merchant->ar_address !!}</p>
                                                @else
                                                    <p class="mb-15">{!! $product->merchant->en_desc !!}</p>
                                                    <p class="mb-15">{!! $product->merchant->en_address !!}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Recent Posts -->
                                </div>
                                    <div class="col-xs-12">
                                        <!-- Latest Deals Widegt -->
                                        <div class="widget latest-deals-widget panel prl-20">
                                            <div class="widget-body ptb-20">
                                                <div class="owl-slider" data-loop="true" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false" data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1" data-lg-nav="true">
                                                    @foreach($latest as $product)
                                                        <div class="latest-deals__item item">
                                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3" data-bg-img="{{asset('/products/'.$product->cover->image)}}">
                                                        </figure>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Latest Deals Widegt -->
                                    </div>

                                {{--<div class="col-xs-12">--}}
                                    {{--<!-- Best Rated Deals -->--}}
                                    {{--<div class="widget best-rated-deals panel pt-20 prl-20">--}}
                                        {{--<h3 class="widget-title h-title">أفضل العروض تقييما</h3>--}}
                                        {{--<div class="widget-body ptb-30">--}}
                                            {{--@foreach($related as $product)--}}
                                            {{--<div class="media">--}}
                                                {{--<div class="media-right">--}}
                                                    {{--<a href="#">--}}
                                                        {{--<img class="media-object" src="{{asset('/products/'.$product->cover->image)}}" alt="Thumb" width="80">--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                                {{--<div class="media-body">--}}
                                                    {{--<h6 class="mb-5">--}}
                                                        {{--<a href="/product/{{$product->id}}">{{$product->ar_title}}</a>--}}
                                                    {{--</h6>--}}
                                                    {{--<div class="mb-5">--}}
                                                            {{--<span class="rating">--}}
                                                                {{--<span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}">--}}
                                                                    {{--<i class="fa fa-star-o"></i>--}}
                                                                    {{--<i class="fa fa-star-o"></i>--}}
                                                                    {{--<i class="fa fa-star-o"></i>--}}
                                                                    {{--<i class="fa fa-star-o"></i>--}}
                                                                    {{--<i class="fa fa-star-o"></i>--}}
                                                                {{--</span>--}}
                                                            {{--</span>--}}
                                                    {{--</div>--}}
                                                    {{--<h4 class="price font-16">{{$product->new_price}} {{$product->city->parent->ar_currency}} <span class="price-sale color-muted">{{$product->old_price}}</span></h4>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--@endforeach--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- Best Rated Deals -->--}}
                                {{--</div>--}}
                                <div class="col-xs-12">
                                    <!-- Best Rated Deals -->
                                    <div class="widget best-rated-deals panel pt-20 prl-20">
                                        <h3 class="widget-title h-title">{{trans('trans.similar_deals')}}</h3>
                                        <div class="widget-body ptb-30">
                                            @foreach($related as $product)
                                                <div class="media">
                                                <div class="media-right">
                                                    <a href="/product/{{$product->id}}">
                                                        <img class="media-object" src="{{asset('/products/'.$product->cover->image)}}" alt="Thumb" width="80">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mb-5">
                                                        <a href="/product/{{$product->id}}">
                                                            @if(lang() == 'ar')
                                                                {{$product->ar_title}}
                                                            @else
                                                                {{$product->en_title}}
                                                            @endif
                                                        </a>
                                                    </h6>
                                                    <div class="mb-5">
                                                            <span class="rating">
                                                                <span class="rating-stars" data-rating="{{$product->get_rate($product->id)}}">
                                                                    <i class="fa fa-star-o"></i>
                                                                    <i class="fa fa-star-o"></i>
                                                                    <i class="fa fa-star-o"></i>
                                                                    <i class="fa fa-star-o"></i>
                                                                    <i class="fa fa-star-o"></i>
                                                                </span>
                                                            </span>
                                                    </div>
                                                    <h4 class="price font-16">{{$product->new_price}} {{$product->city->parent->ar_currency}} <span class="price-sale color-muted">{{$product->old_price}}</span></h4>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- Best Rated Deals -->
                                </div>
                                <div class="col-xs-12">
                                    <!-- Contact Us Widget -->
                                    <div class="widget contact-us-widget panel pt-20 prl-20">
                                        <h3 class="widget-title h-title">{{trans('trans.got_questions')}}</h3>
                                        <div class="widget-body ptb-30">
                                            <p class="mb-20 color-mid">
                                                {{trans('trans.if_you_face_problem')}}
                                            </p>
                                            <a href="/contact_us" class="btn btn-block"><i class="ml-10 font-15 fa fa-envelope-o"></i>{{trans('trans.contact_us')}}</a>
                                        </div>
                                    </div>
                                    <!-- End Contact Us Widget -->
                                </div>

                            </div>
                        </aside>
                        <!-- End Blog Sidebar -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
