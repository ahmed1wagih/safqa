@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <!-- Page Container -->
        <div class="page-container ptb-60">
            <div class="container">
                <div class="row row-rl-10 row-tb-20">
                    <div class="page-content col-xs-12 col-sm-7 col-md-12">
                        <!-- Contact Us Area -->
                        <section class="contact-area contact-area-v2 panel ptb-30 prl-20">
                            <div class="row row-tb-30">
                                <div class="col-xs-12">
                                    <div class="contact-area-col contact-form">
                                        <h3 class="t-uppercase h-title mb-20"> مراسلة الإدارة </h3>
                                        <form action="/suggest/store" method="post">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label>الاسم</label>
                                                <input type="text" name="name" class="form-control" value="{{user()->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label>البريد الإلكتروني</label>
                                                <input type="email" name="email" class="form-control" value="{{user()->email}}">
                                            </div>
                                            <div class="form-group">
                                                <label>الرسالة</label>
                                                <textarea rows="5" name="text" class="form-control"></textarea>
                                            </div>
                                            <button class="btn">إرسال </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- End Contact Us Area -->
                    </div>

                </div>
            </div>
        </div>
        <!-- End Page Container -->
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
