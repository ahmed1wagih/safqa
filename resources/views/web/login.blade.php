@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container ptb-60">
            <div class="container">
                <section class="sign-area panel p-40">
                    <h3 class="sign-title">{{trans('trans.login')}}<small> {{trans('trans.or')}} </small><a href="/register" class="color-green">{{trans('trans.register')}}</a></h3>
                    <div class="row row-rl-0">
                        <div class="col-sm-6 col-md-7 col-right">
                            <form class="p-40" action="/login" method="post">
                                  {{csrf_field()}}
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.email_or_phone')}}</label>
                                    <input type="text" name="login" class="form-control input-lg" placeholder="{{trans('trans.email_or_phone')}}">
                                    @include('web.layouts.error', ['input' => 'login'])
                                </div>
                                <div class="form-group">
                                    <label class="sr-only">{{trans('trans.password')}}</label>
                                    <input type="password" name="password" class="form-control input-lg" placeholder="{{trans('trans.password')}}">
                                    @include('web.layouts.error', ['input' => 'password'])
                                </div>
                                <div class="form-group">
                                    <a href="/password_reset" class="forgot-pass-link color-green">{{trans('trans.forgot_password')}}</a>
                                </div>
                                <div class="form-group">
                                    <a href="/activate" class="forgot-pass-link color-green">{{trans('trans.activate')}}</a>
                                </div>

                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.login')}}</button>
                            </form>

                        </div>
                        <div class="col-sm-6 col-md-5 col-left">
                            <div >
                                <label style="    margin-top: 30%; margin-right: 20%" class="color-mid" for="remember_social"><a href="/register">{{trans('trans.register_and_enjoy')}}</a></label>
                            </div>
                        </div>
                    </div>
                </section>
{{--                <section class="sign-area panel p-40">--}}
{{--                    <h3 class="sign-title">{{trans('trans.activate')}}</h3>--}}
{{--                    <div class="row row-rl-0">--}}
{{--                        <div class="col-sm-6 col-md-7 col-right">--}}
{{--                            <form class="p-40" action="/activate" method="post">--}}
{{--                                {{csrf_field()}}--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only">{{trans('trans.code')}}</label>--}}
{{--                                    <input type="number" name="code" class="form-control input-lg" min="1000" max="9999" placeholder="{{trans('trans.code')}}">--}}
{{--                                    @include('web.layouts.error', ['input' => 'code'])--}}
{{--                                </div>--}}
{{--                                <button type="submit" class="btn btn-block btn-lg">{{trans('trans.activate_now')}}</button>--}}
{{--                            </form>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </section>--}}
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
