@extends('web.layouts.layout')
@section('content')
    <!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
    <main id="mainContent" class="main-content">
        <div class="page-container pt-40 pb-60">
            <div class="container">
                <section class="error-page-area">
                    <div class="container">
                        <div class="error-page-wrapper t-center">
                            <div class="error-page-header">
                                <span class="color-blue">4</span>
                                <span class="color-green">0</span>
                                <span class="color-blue">4</span>
                            </div>
                            <div class="error-page-footer">
                                <h2 class="t-uppercase m-10 color-green">معذرة</h2>
                                <p class="color-muted mb-30 font-15">
                                    الصفحة التي تبحث عنها لا يمكن العثور عليه!
                                </p>
                            </div>
                            <a href="/" class="btn btn-rounded">الرجوع إلى الصفحة الرئيسية</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <!-- –––––––––––––––[ END PAGE CONTENT ]––––––––––––––– -->
@endsection
