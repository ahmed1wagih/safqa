@extends('merchant.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/merchant/dashboard">الرئيسية</a></li>
        <li>الصفقات</li>
        @if(Request::is('merchant/products/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('merchant.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/merchant/product/create"><button type="button" class="btn btn-info"> أضف صفقة جديدة </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>العنوان بالعربية</th>
                                    <th>المدينة</th>
                                    <th>الكمية</th>
                                    <th>حد الشراء</th>
                                    <th>السعر قبل</th>
                                    <th>السعر بعد</th>
                                    <th>القيمة بالنقاط</th>
                                    <th>ينتهي في</th>
                                    <th>مميز</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->ar_title}}</td>
                                        <td>{{$product->city->ar_name}}</td>
                                        <td>{{$product->count}}</td>
                                        <td>{{$product->limit}}</td>
                                        <td>
                                            {{$product->old_price}} {{$produc->country->ar_currency}}
                                        </td>
                                        <td>
                                            {{$product->new_price}} {{$product->country->ar_currency}}
                                        </td>
                                        <td>
                                            {{$product->deal_price}}
                                        </td>
                                        <td>{{$product->expire_at}}<br/>
                                        <td>
                                            @if($product->special == 1)
                                                <span style="color: green;">نعم</span>
                                            @else
                                                <span style="color: grey;">لا</span>
                                            @endif
                                        <br/>
                                        <td>
                                            @if($product->by == 'merchant')
                                                <a title="تعديل" href="/merchant/product/{{$product->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            @endif
                                            <a title="تنزيل ملف الصفقات" href="/merchant/product/{{$product->id}}/deals_export"><button class="btn btn-success btn-condensed"><i class="fa fa-file-excel-o"></i></button></a>
{{--                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$product->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                            {{$products->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
