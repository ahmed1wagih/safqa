@extends('merchant.layouts.app')
@section('content')
    <style>
        @media (max-width: 992px){
            .widget-custom .col-md-1, .widget-custom .col-md-10, .widget-custom .col-md-11, .widget-custom .col-md-12, .widget-custom .col-md-2, .widget-custom .col-md-3, .widget-custom .col-md-4, .widget-custom .col-md-5, .widget-custom .col-md-6, .widget-custom .col-md-7, .widget-custom .col-md-8, .widget-custom .col-md-9{
                float: none!important;
            }
        }

        @media (min-width: 992px)
        {
            .index-widget
            {
                display: flex !important;
                justify-content: center !important;
            }
        }
    </style>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/merchant/dashboard">الرئيسية</a></li>
        <li>أكواد الخصم المنتهية</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row widget-custom" style="direction:rtl;">
            <form action="" method="get">
                <div class="col-md-12 index-widget">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="serial_no" class="form-control" placeholder="الرقم التسلسلي" value="{{isset($_GET['serial_no']) ? $_GET['serial_no'] : ''}}">
                        </div>
                    </div>
                </div>
                    <br/>
                    <br/>
                @if(isset($_GET['serial_no']))
                    <div class="col-md-12 index-widget">
                    <div class="col-md-6 table-responsive" style="margin: 0 auto;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>الصفقة</th>
                                <th>الرقم التسلسلي</th>
                                <th>إسم الوكيل</th>
                                <th>إسم العميل</th>
                                <th>التاريخ</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($check_deal))
                                        <td>{{$check_deal->product->ar_title}}</td>
                                        <td>{{$check_deal->serial_no}}</td>
                                        <td>{{$check_deal->rep->name}}</td>
                                        <td>{{$check_deal->user->name}}</td>
                                        <td>
                                            {{$check_deal->updated_at->toTimeString()}}<br/>
                                            {{$check_deal->updated_at->toDateString()}}
                                        </td>
                                    @else
                                        <td colspan="5" style="text-align: center;">عفواً,الرقم التسلسلي غير صحيح</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                @endif
                <div class="col-md-12 index-widget">
                    <div class="row index-widget mt-2">
                        <div class="form-group" style="direction: rtl;">
                            <a><button type="submit" class="btn btn-info">تحقق</button></a>
                            <a href="?" class="btn btn-danger">إلغاء</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="page-content-wrap" style="padding-top: 20px;">
        <div class="row">
            <div class="col-md-12">
            @include('merchant.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الصفقة</th>
                                    <th>الرقم التسلسلي</th>
                                    <th>إسم الوكيل</th>
                                    <th>إسم العميل</th>
                                    <th>التاريخ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($codes as $code)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$code->product->ar_title}}</td>
                                        <td>{{$code->serial_no}}</td>
                                        <td>{{$code->rep->name}}</td>
                                        <td>{{$code->user->name}}</td>
                                        <td>
                                            {{$code->updated_at->toTimeString()}}<br/>
                                            {{$code->updated_at->toDateString()}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$codes->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
