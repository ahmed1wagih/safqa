@extends('merchant.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/merchant/dashboard">الرئيسية</a></li>
        <li>وكلاء الفروع</li>
        @if(Request::is('merchant/reps/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->


    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('merchant.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/merchant/rep/create"><button type="button" class="btn btn-info"> أضف وكيل جديد </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الهاتف</th>
                                    <th>عدد الأكواد</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reps as $rep)
                                    <tr>
                                        <td>{{$rep->name}}</td>
                                        <td>{{$rep->email}}</td>
                                        <td>{{$rep->phone}}<br/>
                                        <td>{{$rep->codes->count()}}<br/>
                                        <td>
                                            <a title="تعديل" href="/merchant/rep/{{$rep->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            @if($rep->status == 'active')
                                                <button class="btn btn-primary btn-condensed mb-control" data-box="#message-box-suspend-{{$rep->id}}" title="توقيف"><i class="fa fa-minus-square"></i></button>
                                            @elseif($rep->status == 'suspended')
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-activate-{{$rep->id}}" title="تفعييل"><i class="fa fa-check-square"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$rep->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$rep->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تفعل وكيل,سيتمكن من تسجيل الدخول من الآن و مباشرة جميع مهامه .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/merchant/rep/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="rep_id" value="{{$rep->id}}">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- suspend with sound -->
                                    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$rep->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن توقف وكيل,لن يستطيع تسجل الدخول من الآن و لن يتمكن من مباشرة جميع مهامه .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/merchant/rep/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="rep_id" value="{{$rep->id}}">
                                                        <button type="submit" class="btn btn-primary btn-lg pull-right">توقيف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end suspend with sound -->

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$rep->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف وكيل,لن تستطيع أن تستعيد بياناته مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/merchant/rep/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="rep_id" value="{{$rep->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$reps->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
