@extends('merchant.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/merchant/dashboard">الرئيسية</a></li>
        <li><a>الوكلاء</a></li>
        <li class="active">{{isset($rep) ? 'تعديل وكيل' : 'أضافة وكيل'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    {{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($rep) ? '/merchant/rep/update' : '/merchant/rep/store'}}">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($rep) ? 'تعديل وكيل' : 'أضافة وكيل'}}
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم </label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="name" @if(isset($rep)) value="{{$rep->name}}" @else {{old('name')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'name'])
                                </div>
                            </div>


                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-at"></span></span>
                                    <input type="email" class="form-control" name="email"  @if(isset($rep)) value="{{$rep->email}}" @else {{old('email')}} @endif/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'email'])
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">رقم الهاتف</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                    <input type="text" class="form-control" name="phone"  @if(isset($rep)) value="{{$rep->phone}}" @else {{old('phone')}} @endif/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'phone'])
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">كلمة المرورالجديدة</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                    <input type="password" class="form-control" name="password"/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'password'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                    <input type="password" class="form-control" name="password_confirmation"/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'password_confirmation'])
                            </div>
                        </div>

                        @if(isset($rep))
                                <input type="hidden" name="rep_id" value="{{$rep->id}}">
                        @endif
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                {{isset($rep) ? 'تعديل' : 'إنشاء'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='{{asset("merchant/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("merchant/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('merchant/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->


    <script>
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7136, lng: 46.6753},
                zoom: 14

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = $('#mylat'),
                        mylng = $('#mylng');

                    @if(isset($rep))
                        var pos = {
                                lat: parseFloat($('#mylat').val()),
                                lng: parseFloat($('#mylng').val())
                            };
                    @else
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                    @endif

                        mylat.value = pos.lat;
                    mylng.value = pos.lng;

                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        $('#mylat').val(marker.getPosition().lat());
                        $('#mylng').val(marker.getPosition().lng());
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>
@endsection
