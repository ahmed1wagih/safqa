@extends('merchant.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/merchant/dashboard">الرئيسية</a></li>
        <li><a>بياناتي</a></li>
        <li class="active">تعديل</li>
    </ul>
    <!-- END BREADCRUMB -->
    @include('merchant.layouts.message')
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/merchant/profile/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                               تعديل البيانات الشخصية
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="ar_name" value="{{$merchant->ar_name}}"/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="en_name" value="{{$merchant->en_name}}"/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_desc">{!! $merchant->ar_desc !!}</textarea>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'ar_desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_desc">{!! $merchant->en_desc !!}</textarea>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'en_desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_address">{!! $merchant->ar_address !!}</textarea>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'ar_address'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_address">{!! $merchant->en_address !!}</textarea>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'en_address'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">لينك الموقع الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="link"  value="{{$merchant->link}}"/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('lat') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الموقع علي الخريطة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div id="map" style=" height: 600px;"></div>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'lat'])
                            </div>

                            <input type="hidden" value="{{$merchant->lat}}" id="mylat" name="lat">
                            <input type="hidden" value="{{$merchant->lng}}" id="mylng" name="lng">
                        </div>

                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-at"></span></span>
                                    <input type="email" class="form-control" name="email"  value="{{$merchant->email}}"/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'email'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">كلمة المرورالجديدة</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                    <input type="password" class="form-control" name="new_password"/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'new_password'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                    <input type="password" class="form-control" name="new_password_confirmation"/>
                                </div>
                                @include('merchant.layouts.error', ['input' => 'new_password_confirmation'])
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                              تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->

    <script>
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7136, lng: 46.6753},
                zoom: 14

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = $('#mylat'),
                        mylng = $('#mylng');

                    @if(isset($merchant))
                        var pos = {
                                lat: parseFloat($('#mylat').val()),
                                lng: parseFloat($('#mylng').val())
                            };
                    @else
                        var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                    @endif

                    mylat.value = pos.lat;
                    mylng.value = pos.lng;

                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        $('#mylat').val(marker.getPosition().lat());
                        $('#mylng').val(marker.getPosition().lng());
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>
@endsection
