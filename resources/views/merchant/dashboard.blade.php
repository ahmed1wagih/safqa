@extends('merchant.layouts.app')
@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="margin-top: 10px;">
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-6">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-cube"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$deals->count()}}</div>
                        <div class="widget-title">كل الصفقات</div>
                        <div class="widget-subtitle">{{$deals->where('status','active')->count()}}  مفعلة </div>
                        <div class="widget-subtitle">{{$deals->where('status','suspended')->count()}} موقوف </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-barcode"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$all_codes->count()}}</div>
                        <div class="widget-title">كل أكواد الصفقات</div>
                        <div class="widget-subtitle">{{$all_codes->where('user_id','!=',NULL)->count()}} تم شراؤها </div>
                        <div class="widget-subtitle">{{$all_codes->where('user_id','!=',NULL)->where('status','invalid')->count()}} تم إستخدامها </div>

                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>الصفقات</h3>
                            <span>عدد الخصومات المباعة</span>
                        </div>
                    </div>
                    <div class="panel-body padding-0">
                        <div class="chart-holder" id="merchant-deals-donut" style="height: 330px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>الصفقات</h3>
                            <span>تقييمات الصفقات</span>
                        </div>
                    </div>
                    <div class="panel-body padding-0">
                        <div class="chart-holder" id="deals-rates-donut" style="height: 330px;"></div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>عمليات الشراء</h3>
                            <span>عدد عمليات الشراء الشهرية</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="month-trans-count" style="height: 300px;"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->


    <script>
        var morrisCharts = function() {

            var days_data;

            $.ajax(
                {
                    async : false,
                    url : '/merchant/month_deals_graph',
                    method : 'get',
                    dataType : 'json',
                    success : function(data)
                    {
                        days_data = data;
                    },
                    error : function()
                    {
                        console.log('month graphs ajax error')
                    },
                }
            );

            Morris.Line({
                element: 'month-trans-count',
                data: days_data,
                xkey: 'x',
                ykeys: ['y'],
                labels: ['عمليات الشراء'],
                resize: false,
                lineColors: ['#33414E']
            });
        }();


        var merchant_deals = [];
        var donut_colors = ["#0074D9","#B70004","#33414E","#FF4136","#2ECC40","#840003","#FF851B","#358E33","#7FDBFF","#B10DC9","#FFDC00","#001f3f","#39CCCC","#01FF70","#85144b","#F012BE","#3D9970","#111111","#921880"];
        var merchant_deals_donut = [];
        var deals_rates_donut = [];

        $.ajax(
            {
                async : false,
                url : '/merchant/merchant_deals',
                method : 'get',
                dataType : 'json',
                success : function(data)
                {
                    $.each(data, function (i,deal)
                    {
                        merchant_deals_donut.push({label: deal.ar_title, value: deal.sold});
                        deals_rates_donut.push({label: deal.ar_title, value: deal.rate});
                    });
                },
                error : function(data)
                {
                    console.log('merchant deals donut ajax error')
                },
            }
        );


        Morris.Donut({
            element: 'merchant-deals-donut',
            data: merchant_deals_donut,
            colors: donut_colors,
            resize: false
        });


        Morris.Donut({
            element: 'deals-rates-donut',
            data: deals_rates_donut,
            colors: donut_colors,
            resize: false
        });

    </script>
@endsection
