<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>صفقة - لوحة تحكم المندوب</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('/web/images/logo.logo.jpg')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/theme-default_rtl.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/rtl.css')}}"/>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container page-mode-rtl page-content-rtl">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/delegate/dashboard">صفقة - لوحة تحكم المندوب</a>
                <a href="#" class="x-navigation-control"></a>
            </li>

            <li class="xn-profile">
                <div class="profile">
                    <div class="profile-image">
                        <a href="/delegate/profile" title="الملف الشخصي"><img src="/users/default.png" alt="صفقة" style="width: 150px; height: 150px; border-radius: 360px;"/></a>
                    </div>

                </div>
            </li>

            <li @if(Request::is('delegate/dashboard')) class="active" @endif>
                <a href="/delegate/dashboard"><span class="xn-text">الرئيسية</span><span class="fa fa-dashboard"></span></a>
            </li>

            <li class="xn-openable @if(Request::is('delegate/products/*') xor Request::is('delegate/product/*')) active @endif" >
                <a href="#"><span class="xn-text">الصفقات</span><span class="fa fa-product-hunt"></span></a>
                <ul>
                    <li @if(Request::is('delegate/products/active')) class="active" @endif>
                        <a href="/delegate/products/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('delegate/products/suspended')) class="active" @endif>
                        <a href="/delegate/products/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>


            <li @if(Request::is('delegate/withdraws')) class="active" @endif>
                <a href="/delegate/withdraws"><span class="xn-text">طلبات السحب</span><span class="fa fa-money"></span></a>
            </li>

        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-left last">
                <a href="#" class="mb-control" data-box="#mb-signout" title="Logout"><span class="fa fa-power-off"></span></a>
            </li>
            <!-- END POWER OFF -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> تسجيل <strong>الخروج</strong> ؟</div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/logout" class="btn btn-success btn-lg">نعم</a>
                            <button class="btn btn-default btn-lg mb-control-close">لا</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

    @yield('content')





    <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{asset('admin/audio/alert.mp3')}}" preload="auto"></audio>
        <audio id="audio-fail" src="{{asset('admin/audio/fail.mp3')}}" preload="auto"></audio>
        <!-- END PRELOADS -->


        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

        <script type='text/javascript' src='{{asset('admin/js/plugins/icheck/icheck.min.js')}}'></script>
        <script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('admin/js/plugins/owl/owl.carousel.min.js')}}"></script>
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{asset('admin/js/plugins.js')}}"></script>
        <script type="text/javascript" src="{{asset('admin/js/actions.js')}}"></script>


        <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
        <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
        <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
        <!-- END THIS PAGE PLUGINS -->
        <!-- END SCRIPTS -->
</body>
</html>






