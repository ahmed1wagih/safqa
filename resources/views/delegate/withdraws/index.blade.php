@extends('delegate.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/delegate/dashboard">الرئيسية</a></li>
        <li>طلبات سحب الأرباح</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('delegate.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="modal" data-target="#modal_create">
                            <button type="button" class="btn btn-info">تقديم طلب جديد</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>المبلغ</th>
                                    <th>التاريخ</th>
                                    <th>الحالة</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($withdraws as $withdraw)
                                    <tr>
                                        <td>{{$withdraw->amount}}</td>
                                        <td>{{$withdraw->created_at}}<br/>
                                        <td>@if($withdraw->status == 'approved') مقبول @elseif($withdraw->status == 'declined') مرفوض @else قيد الإنتظار @endif<br/>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$withdraws->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal animated fadeIn" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">إغلاق</span></button>
                    <h4 class="modal-title" id="smallModalHead">إضافة قسم</h4>
                </div>
                <form method="post" action="/delegate/withdraw/store">
                    {{csrf_field()}}
                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-3 control-label">المبلغ</label>
                            <div class="col-md-9">
                                <input type="number" step="1" class="form-control" name="amount"/>
                                @include('delegate.layouts.error', ['input' => 'amount'])
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">تقديم طلب</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
