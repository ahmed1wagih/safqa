<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>صفقة - لوحة تحكم المندوب</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('/admin/img/favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/theme-default.css')}}"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>
{{--{{dd($errors)}}--}}
<div class="login-container">
    <div class="login-box animated fadeInDown">
        <div class="login-body" style="direction: ltr;">
            <img src="{{asset('web/images/logo.png')}}" width="350px" height="100px">
            <br/>
            <br/>
            <h1 style="color: white; text-align: center;">دخول المندوب</h1>
            @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert" style="text-align: center;">
                        <button type="button" class="close" data-dismiss="alert"></button>
                            <span style="size: 15px;">
                                {{ Session::get('error') }}
                            </span>
                    </div>
            @endif
            <form action="/delegate/login" class="form-horizontal" method="post" style="direction: rtl;">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="email" placeholder="البريد الإلكتروني"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password" placeholder="كلمة المرر"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6" style="float: right !important;">
                        <button class="btn btn-info btn-block">تسجيل الدخول</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

</body>
</html>

