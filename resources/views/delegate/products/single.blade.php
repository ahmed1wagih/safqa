@extends('delegate.layouts.app')
@section('content')

    <style>
        .img
        {
            min-width: 312px;
            max-width: 312px;
            min-height: 312px;
            max-height: 312px;
        }

        .gallery .gallery-item.active .image
        {
            opacity: 0.2 !important;

        }
    </style>


    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/delegate/dashboard">الرئيسية</a></li>
        <li>الصفقةات</li>
        <li class="active">{{isset($product) ? 'تعديل صفقة' : 'إنشاء صفقة'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($product) ? '/delegate/product/update' : '/delegate/product/store'}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($product) ? 'تعديل صفقة' : 'إنشاء صفقة'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('address_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">المدينة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <select class="form-control select" name="address_id" required>
                                            <option disabled selected>إختر مدينة من فضلك من فضلك</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}" @if(isset($product) && $product->address_id == $city->id) selected @endif>{{$city->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'address_id'])

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">القسم الرئيسي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cubes"></span></span>
                                        <select class="form-control select" id="cats" required>
                                            <option disabled selected>إختر قسم من فضلك</option>
                                            @foreach($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if(isset($product))
                                        <label class="form-control alert-danger" style="margin-top: 5px;">إتركه فارغاً إذا لم ترد التعديل</label>
                                    @endif
                                    @include('delegate.layouts.error', ['input' => 'cat_id'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('cat_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">القسم الفرعي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cubes"></span></span>
                                        <select class="form-control" id="sub_cats" name="cat_id" required>
                                            <option disabled selected>إختر قسم رئيسي أولاً</option>
                                        </select>
                                    </div>
                                    @if(isset($product))
                                        <label class="form-control alert-danger" style="margin-top: 5px;">إتركه فارغاً إذا لم ترد التعديل</label>
                                    @endif
                                    @include('delegate.layouts.error', ['input' => 'cat_id'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('merchant_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">التاجر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user-circle"></span></span>
                                        <select class="form-control select" name="merchant_id" required>
                                            <option disabled selected>إختر تاجر من فضلك</option>
                                            @foreach($merchants as $merchant)
                                                <option value="{{$merchant->id}}" @if(isset($product) && $product->merchant_id == $merchant->id) selected @endif>{{$merchant->ar_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'merchant_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_title') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="ar_title" required @if(isset($product)) value="{{$product->ar_title}}" @else {{old('ar_title')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'ar_title'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_title') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="en_title" required @if(isset($product)) value="{{$product->en_title}}" @else {{old('en_title')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'en_title'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_desc">@if(isset($product)){!! $product->ar_desc !!}@else{{old('ar_desc')}}@endif</textarea>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'ar_desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_desc">@if(isset($product)){!! $product->en_desc !!}@else{{old('en_desc')}}@endif</textarea>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'en_desc'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('ar_conds') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الشروط بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_conds">@if(isset($product)){!! $product->ar_conds !!}@else{{old('ar_conds')}}@endif</textarea>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'ar_conds'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_conds') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الشروط بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_conds">@if(isset($product)){!! $product->en_conds !!}@else{{old('en_conds')}}@endif</textarea>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'en_conds'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('count') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">كمية أكواد الخصم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cubes"></span></span>
                                        <input type="number" min="1" step="1" class="form-control" name="count" required @if(isset($product)) value="{{$product->count}}" @else {{old('count')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'count'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('limit') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">حد أكواد الخصم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-refresh"></span></span>
                                        <input type="number" min="1" step="1" class="form-control" name="limit" required @if(isset($product)) value="{{$product->limit}}" @else {{old('limit')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'limit'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('old_price') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر القديم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" min="1" step="0.5" class="form-control" name="old_price" required @if(isset($product)) value="{{$product->old_price}}" @else {{old('old_price')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'old_price'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('new_price') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر لجديد</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" min="1" step="0.5" class="form-control" name="new_price" required @if(isset($product)) value="{{$product->new_price}}" @else {{old('new_price')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'new_price'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('deal_price') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">قيمة الصفقة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" min="1" step="0.5" class="form-control" name="deal_price" required @if(isset($product)) value="{{$product->deal_price}}" @else {{old('deal_price')}} @endif/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'deal_price'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('expire_at') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">ينتهي في</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar-check-o"></span></span>
                                        <input type="date" class="form-control" name="expire_at" required @if(isset($product)) value="{{$product->expire_at}}" @else {{old('expire_at')}} @endif style="height: 40px;" required/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'expire_at'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الصور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <input type="file" class="fileinput btn-info" name="images[]" multiple id="cp_photo" data-filename-placement="inside" title="{{isset($product) ? 'أضف مزيد من الصور' : 'أضف صور'}}" required/>
                                    </div>
                                    @include('delegate.layouts.error', ['input' => 'images'])
                                </div>

                                @if(isset($product->images))
                                    <div class="gallery" id="links">
                                        @foreach($product->images as $image)
                                            <a href="/products/{{$image->image}}" title="{{$image->image}}" class="gallery-item" data-gallery>
                                                <div class="image">
                                                    <img class="img" src="/products/{{$image->image}}" alt="{{$image->image}}"/>
                                                </div>
                                                @if($product->images->count() > 1)
                                                    <label class="check" title="حذف الصورة"><input type="checkbox" name="deleted_ids[]" value="{{$image->id}}" class="icheckbox" style="border: black solid 1px !important;"/></label>
                                                @endif                                            </a>
                                        @endforeach
                                    </div>
                                @endif

                            </div>


                            @if(isset($product))
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                {{isset($product) ? 'تعديل' : 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->


    @if(isset($item))
        <script>
            document.getElementById('links').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {index: link, event: event,onclosed: function(){
                            setTimeout(function(){
                                $("body").css("overflow","");
                            },200);
                        }},
                    links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };
        </script>
    @endif



    <!-- THIS PAGE PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->

    <script>
        $('#cats').on('change', function (e) {
            var parent_id = e.target.value;

            if (parent_id) {
                $.ajax({
                    url: '/delegate/get_sub_cats/' + parent_id,
                    type: "GET",
                    dataType: "json",

                    success: function (data) {
                        $('#sub_cats').empty();
                        $('#sub_cats').append('<option selected disabled> إختر القسم الفرعي من فضلك </option>');
                        $.each(data, function (i, cat) {
                            $('#sub_cats').append('<option value="' + cat.id + '">' + cat.ar_name + '</option>');
                        });
                    }
                });
            }});
    </script>
@endsection
