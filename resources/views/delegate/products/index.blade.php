@extends('delegate.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/delegate/dashboard">الرئيسية</a></li>
        <li>المنتجات</li>
        @if(Request::is('delegate/products/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('delegate.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/delegate/product/create"><button type="button" class="btn btn-info"> أضف صفقة جديدة </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>العنوان بالعربية</th>
                                    <th>المدينة</th>
                                    <th>التاجر</th>
                                    <th>المندوب</th>
                                    <th>الكمية</th>
                                    <th>السعر</th>
                                    <th>سعر الصفقة</th>
                                    <th>ينتهي في</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->ar_title}}</td>
                                        <td>{{$product->city->ar_name}}</td>
                                        <td>{{$product->merchant->ar_name}}</td>
                                        <td>{{$product->delegate->name}}</td>
                                        <td>{{$product->count}}</td>
                                        <td>
                                            {{$product->old_price}} -> {{$product->new_price}}
                                            <br/>
                                            {{$product->city->parent->ar_currency}}
                                        </td>
                                        <td>
                                            @if($product->deal_price == 1)
                                                نقطة واحدة
                                            @elseif($product->deal_price == 2)
                                                نقطتان
                                            @elseif($product->deal_price > 2 && $product->deal_price <11)
                                                {{$product->deal_price}} نقاط
                                            @else
                                                {{$product->deal_price}} نقطة
                                            @endif
                                        </td>
                                        <td>{{$product->expire_at}}<br/>
                                        <td>
                                            <a title="تعديل" href="/delegate/product/{{$product->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            {{--<button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$product->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$products->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
