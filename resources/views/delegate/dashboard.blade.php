@extends('delegate.layouts.app')
@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="margin-top: 10px;">
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='/delegate/products/active';">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-cube"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$deals->count()}}</div>
                        <div class="widget-title">كل الصفقات</div>
                        <div class="widget-subtitle">الخاصة بك في الموقع</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->

            </div>
            <div class="col-md-3">

                <!-- START WIDGET REGISTRED -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-barcode"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$all_codes->count()}}</div>
                        <div class="widget-title">كل أكواد الصفقات</div>
                        <div class="widget-subtitle">{{$all_codes->where('user_id','!=',NULL)->count()}} تم شراؤها </div>

                    </div>

                </div>
                <!-- END WIDGET REGISTRED -->
            </div>

            <div class="col-md-3">
            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left" style="margin-left: 10px;">
                    <span class="fa fa-money"></span>
                </div>
                <div class="widget-data" style="margin-right: 10px;">
                    <div class="widget-int num-count">{{$delegate->credit}}</div>
                    <div class="widget-title">رصيدك في الأرباح</div>
                </div>

            </div>
            <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET CLOCK -->
                <div class="widget widget-danger widget-padding-sm">
                    <div class="widget-big-int plugin-clock">{{\Carbon\Carbon::now()}}</div>
                    <div class="widget-subtitle plugin-date">Loading...</div>
                </div>
                <!-- END WIDGET CLOCK -->

            </div>
        </div>
        <!-- END WIDGETS -->

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection
