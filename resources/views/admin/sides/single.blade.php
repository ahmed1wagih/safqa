@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>إعدادت التطبيق</li>
        <li><a href="/admin/sides/index">البانرات الإعلامية الجانبية</a></li>
        <li class="active">@if(isset($side)) تعديل صورة @else إضافة صورة @endif</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($side)) action="/admin/side/update" @else action="/admin/side/store" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                 {{isset($side) ? 'تعديل صورة': 'إضافة صورة'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">لينك التحويل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="link" value="{{isset($side) ? $side->link : old('link')}}" style="direction: ltr;" required/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'link'])
                                </div>
                            </div>

                            @if(isset($side))
                            <div class="form-group">
                                <label class="col-md-3 control-label">الصورة الحالية</label>

                                    <div class="col-md-6">
                                        <img src="{{asset('/sides/'.$side->image)}}" class="img-responsive" style="width: 100%"/>
                                    </div>
                                @include('admin.layouts.error', ['input' => 'image'])
                            </div>
                            @endif

                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">أرفق صورة</label>
                                <div class="col-md-9">
                                    <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="إرفق الصورة"/>
                                </div>
                                @include('admin.layouts.error', ['input' => 'image'])
                            </div>

                            @if(isset($side))
                                <input type="hidden" name="side_id" value="{{$side->id}}">
                            @endif

                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                              {{isset($side) ? 'تعديل': 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
