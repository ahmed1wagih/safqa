@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">إدارة البانرات الإعلانية الجانبية</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/side/create">
                            <button type="button" class="btn btn-info">أضف بانر إعلاني جانبي</button>
                        </a>
                    </div>
                    <div class="panel-body" style="overflow: auto;">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="rtl_th">الصورة</th>
                                    <th class="rtl_th">لينك التحويل</th>
                                    <th class="rtl_th">الإجراء المتخذ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sides as $side)
                                    <tr>
                                        <td>
                                                <img class="img-responsive" src="/sides/{{$side->image}}" width="250" height="250"/>
                                        </td>
                                        <td>
                                            <a href="{{$side->link}}" target="_blank">{{$side->link}}</a>
                                        </td>
                                        <td>
                                            <a href="/admin/side/{{$side->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$side->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$side->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف البانر الإعلامي الجانبيو لن تستطيع إسترجاعه مرة أخري,هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                                    <form method="post" action="/admin/side/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="side_id" value="{{$side->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                        <!-- end danger with sound -->
                        @endforeach
                        </tbody>

                        </table>
                        {{$sides->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
