@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الصفقات</li>
        @if(Request::is('admin/products/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>العنوان بالعربية</th>
                                    <th>المدينة</th>
                                    <th>التاجر</th>
                                    <th>الكمية</th>
                                    <th>حد الشراء</th>
                                    <th>السعر</th>
                                    <th>السعر بالنقاط</th>
                                    <th>ينتهي في</th>
                                    <th>النوع</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->ar_title}}</td>
                                        <td>{{$product->city->ar_name}}</td>
                                        <td>{{$product->merchant->ar_name}}</td>
                                        <td>{{$product->count}}</td>
                                        <td>{{$product->limit}}</td>
                                        <td>
                                            {{$product->old_price}} -> {{$product->new_price}}
                                            <br/>
                                            {{$product->city->parent->ar_currency}}
                                        </td>
                                        <td>
                                            {{$product->deal_price}}
                                        </td>
                                        <td>{{$product->expire_at}}<br/>
                                        <td>
                                            @if($product->special == 1)
                                                <span style="color: green;">مميز</span>
                                            @else
                                                <span style="color: grey;">عادي</span>
                                            @endif
                                        <br/>
                                        <td>
                                            <a title="مشاهدة" href="/admin/product/{{$product->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                            <a title="تمييز" href="/admin/product/{{$product->id}}/specialize"><button class="btn btn-default btn-condensed"><i class="fa fa-star"></i></button></a>
                                            @if($product->status == 'active')
                                                <button class="btn btn-primary btn-condensed mb-control" data-box="#message-box-suspend-{{$product->id}}" title="تعطيل"><i class="fa fa-thumbs-o-down"></i></button>
                                            @else
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-activate-{{$product->id}}" title="تفعيل"><i class="fa fa-thumbs-up"></i></button>
                                            @endif
                                            <a title="تنزيل ملف الصفقات" href="/admin/product/{{$product->id}}/deals_export"><button class="btn btn-success btn-condensed"><i class="fa fa-file-excel-o"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$product->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$product->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تفعل منتج و تفعيل كل الباركود الخاص به,و سيظهر للمستخدمين من الآن .</p>                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/product/change_state" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- suspend with sound -->
                                    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$product->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تعطل منتج و تعطيل جميع الباركود الخاص به,و لن يظهر للمستخدمين من الآن .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/product/change_state" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <button type="submit" class="btn btn-primary btn-lg pull-right">تعطيل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end suspend with sound -->

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$product->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف منتج,لن تستطيع أن تستعيد بياناته مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/product/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$products->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
