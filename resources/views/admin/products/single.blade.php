@extends('admin.layouts.app')
@section('content')

    <style>
        .img
        {
            min-width: 312px;
            max-width: 312px;
            min-height: 312px;
            max-height: 312px;
        }

        .gallery .gallery-item.active .image
        {
            opacity: 0.2 !important;

        }
    </style>


    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الصفقات</li>
        <li class="active">مشاهدة صفقة</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                مشاهدة صفقة
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">المدينة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <label class="form-control">{{$product->city->ar_name}}</label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">القسم الرئيسي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cubes"></span></span>
                                        <label class="form-control">{{$product->category->parent->ar_name}}</label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">القسم الفرعي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cubes"></span></span>
                                        <label class="form-control">{{$product->category->ar_name}}</label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">التاجر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user-circle"></span></span>
                                        <label class="form-control">{{$product->merchant->ar_name}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->ar_title}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->en_title}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="form-control summernote" disabled rows="5">{!! $product->ar_desc !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="form-control summernote" disabled rows="5">{!! $product->en_desc !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الشروط بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="form-control summernote" disabled rows="5">{!! $product->ar_conds !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الشروط بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="form-control summernote" disabled rows="5">{!! $product->en_conds !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">السعر القديم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->old_price }} - {{$product->city->parent->ar_currency}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">السعر الجديد</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->new_price }} - {{$product->city->parent->ar_currency}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">حد الشراء</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->limit }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">قيمة الصفقة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->deal_price }} - {{$product->city->parent->ar_currency}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">تاريخ الإنتهاء</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$product->expire_at }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="form-group">
                                    <div class="gallery" id="links">
                                        @foreach($product->images as $image)
                                            <a href="/products/{{$image->image}}" title="{{$image->image}}" class="gallery-item" data-gallery style="width: 375px; height: 210px;">
                                                <div class="image" style="width: 375px; height: 210px;">
                                                    <img class="img" src="/products/{{$image->image}}" alt="{{$image->image}}"/>
                                                </div>
                                            </a>
{{--                                            <label class="check" title="حذف الصورة"><input type="checkbox" name="deleted_ids[]" value="{{$image->id}}" class="icheckbox" style="border: black solid 1px !important;"/></label>--}}
                                        @endforeach
                                    </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">العودة</button> &nbsp;
                            @if($product->status == 'active')
                                <button class="btn btn-primary btn-condensed mb-control" data-box="#message-box-suspend-{{$product->id}}" title="Suspend"><i class="fa fa-thumbs-o-down"></i> تعطيل </button>
                            @else
                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-activate-{{$product->id}}" title="Activate"><i class="fa fa-thumbs-up"></i> تفعيل </button>
                            @endif
                        </div>
                    </div>


            </div>
        </div>

    <!-- activate with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$product->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن تفعل صفقة و تفعيل كل الباركود الخاص به,و سيظهر للمستخدمين من الآن .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/product/change_state" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end activate with sound -->

    <!-- suspend with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$product->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن تعطل صفقة و تعطيل جميع الباركود الخاص به,و لن يظهر للمستخدمين من الآن .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/product/change_state" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <button type="submit" class="btn btn-primary btn-lg pull-right">تعطيل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end suspend with sound -->

    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->


    @if(isset($item))
        <script>
            document.getElementById('links').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {index: link, event: event,onclosed: function(){
                            setTimeout(function(){
                                $("body").css("overflow","");
                            },200);
                        }},
                    links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };
        </script>
    @endif



    <!-- THIS PAGE PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->

    <script>
        $('#cats').on('change', function (e) {
            var parent_id = e.target.value;

            if (parent_id) {
                $.ajax({
                    url: '/delegate/get_sub_cats/' + parent_id,
                    type: "GET",
                    dataType: "json",

                    success: function (data) {
                        $('#sub_cats').empty();
                        $('#sub_cats').append('<option selected disabled> إختر القسم الفرعي من فضلك </option>');
                        $.each(data, function (i, cat) {
                            $('#sub_cats').append('<option value="' + cat.id + '">' + cat.ar_name + '</option>');
                        });
                    }
                });
            }});
    </script>
@endsection
