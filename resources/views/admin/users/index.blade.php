@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>العملاء</li>
        @if(Request::is('admin/users/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/user/create"><button type="button" class="btn btn-info"> أضف عميل جديد </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>المدينة</th>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الهاتف</th>
                                    <th>عدد الصفقات</th>
                                    <th>الرصيد</th>
                                    <th>تاريخ التسجيل</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->city->ar_name}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->purchased($user->id)}}</td>
                                        <td>
                                            {{$user->credit}} {{$user->country->ar_currency}}<br/>
                                            @if($user->expire_at)
                                                حتي {{$user->expire_at}}
                                            @else
                                                لم يتم الإشتراك بعد
                                            @endif
                                        </td>
                                        <td>
                                            {{$user->created_at->toTimeString()}}<br/>
                                            {{$user->created_at->toDateString()}}
                                        </td>
                                        <td>
                                            <a href="/admin/user/{{$user->id}}/edit"><button class="btn btn-condensed btn-warning" title="تعديل"><i class="fa fa-edit"></i></button></a>
                                            @if($user->status == 'active')
                                                <button class="btn btn-primary btn-condensed mb-control" onclick="modal_suspend({{$user->id}})" data-box="#message-box-primary" title="توقيف"><i class="fa fa-minus-circle"></i></button>
                                            @else
                                                <button class="btn btn-success btn-condensed mb-control" onclick="modal_activate({{$user->id}})" data-box="#message-box-success" title="تفعيل"><i class="fa fa-check-square"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-condensed mb-control" onclick="modal_destroy({{$user->id}})" data-box="#message-box-danger" title="حذف"><i class="fa fa-trash-o"></i></button>                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$users->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- success with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert" id="message-box-success">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك التفعيل,و سيتمكن العميل من الدخول إلي الموقع أو التطبيق,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_activate" value="">
                        <input type="hidden" name="status" value="active">
                        <button class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end success with sound -->

    <!-- warning with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert" id="message-box-primary">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك تجميد الحساب,لن يتمكن العميل من الدخول إلي الموقع أو التطبيق,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_suspend" value="">
                        <input type="hidden" name="status" value="suspended">
                        <button class="btn btn-primary btn-lg pull-right">توقيف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end warning with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert" id="message-box-danger">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-check-square"></span>تحذير</div>
                <div class="mb-content">
                    <p>أنت علي وشك الحذف,هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/delete" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_destroy" value="">
                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->
@endsection
