@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/admin/active">الموظفين</a></li>
        <li class="active">{{isset($admin) ? 'تعديل موظف' : 'إنشاء موظف'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($admin) ? '/admin/admin/update' : '/admin/admin/store'}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($admin) ? 'تعديل موظف' : 'إنشاء موظف'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-super"></span></span>
                                        <input type="text" class="form-control" name="name" required @if(isset($admin)) value="{{$admin->name}}" @else  value="{{old('name')}}" @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'name'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
                                        <input type="text" class="form-control" name="email" required @if(isset($admin)) value="{{$admin->email}}" @else  value="{{old('email')}}" @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'email'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <div id="field">
                                            <input type="text" class="form-control phone" name="phone" required @if(isset($admin)) value="{{$admin->phone}}" @else  value="{{old('phone')}}" @endif/>
                                        </div>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'phone'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الصورة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="Select image"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'image'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password"/>
                                    </div>
                                    @if(isset($admin))
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    @endif
                                    @include('super_admin.layouts.error', ['input' => 'password'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password_confirmation"/>
                                    </div>
                                    @if(isset($admin))
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    @endif
                                    @include('super_admin.layouts.error', ['input' => 'password_confirmation'])
                                </div>
                            </div>

                            @if(isset($admin))
                                <input type="hidden" name="id" value="{{$admin->id}}">
                            @endif
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                                {{isset($admin) ? 'تعديل' : 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
