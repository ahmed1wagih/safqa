@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الفواتير و المدفوعات</li>
        @if(Request::is('admin/transfers/awaiting'))
            <li class="active">طلبات تأكيد التحويل</li>
        @elseif(Request::is('admin/transfers/approved'))
            <li class="active">الفواتير  المدفوعة</li>
        @else
            <li class="active">الفواتير الغير مدفوعة</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>صاحب التحويل</th>
                                    <th>الإسم</th>
                                    <th>البنك</th>
                                    <th>رقم التحويل</th>
                                    <th>المبلغ</th>
                                    <th>التاريخ</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transfers as $transfer)
                                    <tr>
                                        <td>{{$transfer->id}}</td>
                                        <td><a href="/admin/member/{{$transfer->user_id}}/view">{{$transfer->user->name}}</a></td>
                                        <td>{{$transfer->name}}</td>
                                        <td>{{$transfer->bank->ar_name}}</td>
                                        <td>{{$transfer->trans_no}}</td>
                                        <td>
                                            {{$transfer->pack->price}} {{admin()->country->ar_currency}}<br/>
                                            {{$transfer->pack->ar_name}}
                                        </td>
                                        <td>
                                            {{$transfer->date}}
                                        </td>
                                        <td>
                                            <a title="مشاهدة التحويل" href="/admin/transfer/{{$transfer->id}}/view"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                            @if($transfer->status == 'awaiting')
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-approve-{{$transfer->id}}" title="الموافقة و قبول التحويل"><i class="fa fa-check-circle"></i></button>
                                                <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-decline-{{$transfer->id}}" title="رفض التحويل"><i class="fa fa-minus-circle"></i></button>
                                            @endif
                                        </td>
                                    </tr>

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-approve-{{$transfer->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    @if($transfer->type == 'order')
                                                        <p>أنت علي وشك أن تقبل تحويل بنكي و تفعيل الباركود الخاص بالصفقات,و سيظهر المستخدم من الآن .</p>
                                                    @else
                                                        <p>أنت علي وشك أن تقبل تحويل بنكي و إضافة الرصيد للمستخدم .</p>
                                                    @endif
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/transfer/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="transfer_id" value="{{$transfer->id}}">
                                                        <input type="hidden" name="status" value="approved">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">قبول التحويل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- suspend with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-decline-{{$transfer->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    @if($transfer->type == 'order')
                                                        <p>أنت علي وشك أن ترفض تحويل بنكي لشراء صفقات .</p>
                                                    @else
                                                        <p>أنت علي وشك أن ترفض تحويل بنكي لشحن رصيد .</p>
                                                    @endif
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/transfer/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="transfer_id" value="{{$transfer->id}}">
                                                        <input type="hidden" name="status" value="declined">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">رفض التحويل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end suspend with sound -->

                                @endforeach
                                </tbody>
                            </table>
                            {{$transfers->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
