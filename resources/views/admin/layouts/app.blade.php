<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>صفقة - لوحة التحكم</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('/web/images/logo.jpg')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/theme-default_rtl.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/rtl.css')}}"/>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('admin/js/plugins/morris/raphael.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/morris/morris.min.js')}}"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container page-mode-rtl page-content-rtl">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/admin/dashboard">صفقة - لوحة التحكم</a>
                <a href="#" class="x-navigation-control"></a>
            </li>

            <li class="xn-profile">
                <div class="profile">
                    <div class="profile-image">
                        <a href="/admin/profile" title="الملف الشخصي"><img src="/users/default.png" alt="صفقة" style="width: 150px; height: 150px; border-radius: 360px;"/></a>
                    </div>
                </div>
            </li>

            <li @if(Request::is('admin/dashboard')) class="active" @endif>
                <a href="/admin/dashboard"><span class="xn-text">الرئيسية</span><span class="fa fa-dashboard"></span></a>
            </li>

            <li class="xn-openable @if(Request::is('admin/admins/*') xor Request::is('admin/admin/*')) active @endif" >
                <a href="#"><span class="xn-text">الموظفين</span><span class="fa fa-user-secret"></span></a>
                <ul>
                    <li @if(Request::is('admin/admins/active')) class="active" @endif>
                        <a href="/admin/admins/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/admins/suspended')) class="active" @endif>
                        <a href="/admin/admins/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

{{--            <li class="xn-openable @if(Request::is('admin/delegates/*') xor Request::is('admin/delegate/*')) active @endif" >--}}
{{--                <a href="#"><span class="xn-text">المناديب</span><span class="fa fa-address-card"></span></a>--}}
{{--                <ul>--}}
{{--                    <li @if(Request::is('admin/delegates/active')) class="active" @endif>--}}
{{--                        <a href="/admin/delegates/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>--}}
{{--                    </li>--}}
{{--                    <li @if(Request::is('admin/delegates/suspended')) class="active" @endif>--}}
{{--                        <a href="/admin/delegates/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

            <li class="xn-openable @if(Request::is('admin/users/*') xor Request::is('admin/user/*')) active @endif" >
                <a href="#"><span class="xn-text">العملاء</span><span class="fa fa-users"></span></a>
                <ul>
                    <li @if(Request::is('admin/users/active')) class="active" @endif>
                        <a href="/admin/users/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/users/suspended')) class="active" @endif>
                        <a href="/admin/users/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

            <li class="xn-openable @if(Request::is('admin/merchants/*') xor Request::is('admin/merchant/*')) active @endif" >
                <a href="#"><span class="xn-text">معلومات التجار</span><span class="fa fa-shopping-cart"></span></a>
                <ul>
                    <li @if(Request::is('admin/merchants/active')) class="active" @endif>
                        <a href="/admin/merchants/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/merchants/suspended')) class="active" @endif>
                        <a href="/admin/merchants/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>


            <li class="xn-openable @if(Request::is('admin/products/*') xor Request::is('admin/product/*')) active @endif" >
                <a href="#"><span class="xn-text">الصفقات</span><span class="fa fa-cart-arrow-down"></span></a>
                <ul>
                    <li @if(Request::is('admin/products/active')) class="active" @endif>
                        <a href="/admin/products/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/products/suspended')) class="active" @endif>
                        <a href="/admin/products/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

            <li class="xn-openable @if(Request::is('admin/packs/*') xor Request::is('admin/pack/*')) active @endif" >
                <a href="#"><span class="xn-text">الباقات السنوية</span><span class="fa fa-calendar"></span></a>
                <ul>
                    <li @if(Request::is('admin/packs/active')) class="active" @endif>
                        <a href="/admin/packs/active"><span class="xn-text">فعال</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/packs/suspended')) class="active" @endif>
                        <a href="/admin/packs/suspended"><span class="xn-text">موقوف</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

            <li class="xn-openable @if(Request::is('admin/transfers/*') xor Request::is('admin/transfer/*')) active @endif" >
                <a href="#"><span class="xn-text">الفواتير و المدفوعات</span><span class="fa fa-get-pocket"></span></a>
                <ul>
                    <li @if(Request::is('admin/transfers/approved')) class="active" @endif>
                        <a href="/admin/transfers/approved"><span class="xn-text">الفواتير المدفوعة</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/transfers/declined')) class="active" @endif>
                        <a href="/admin/transfers/declined"><span class="xn-text">الفواتير الغير مدفوعة</span><span class="fa fa-times-rectangle-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/transfers/awaiting')) class="active" @endif>
                        <a href="/admin/transfers/awaiting"><span class="xn-text">طلبات تأكيد التحويل</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>
            <li class="xn-openable @if(Request::is('admin/online_transfers/*') xor Request::is('admin/online_transfer/*')) active @endif" >
                <a href="#"><span class="xn-text">المدفوعات الإلكترونية</span><span class="fa fa-credit-card"></span></a>
                <ul>
                    <li @if(Request::is('admin/online_transfers/approved')) class="active" @endif>
                        <a href="/admin/online_transfers/approved"><span class="xn-text">مدفوع</span><span class="fa fa-check-square"></span></a>
                    </li>
                    <li @if(Request::is('admin/online_transfers/declined')) class="active" @endif>
                        <a href="/admin/online_transfers/declined"><span class="xn-text">ملغي</span><span class="fa fa-times-rectangle-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/online_transfers/awaiting')) class="active" @endif>
                        <a href="/admin/online_transfers/awaiting"><span class="xn-text">قيد التأكيد</span><span class="fa fa-minus-square"></span></a>
                    </li>
                </ul>
            </li>

{{--            <li class="xn-openable @if(Request::is('admin/withdraws/*')) active @endif">--}}
{{--                <a href="#"><span class="xn-text">طلبات سحب الأرباح</span><span class="fa fa-money"></span></a>--}}
{{--                <ul>--}}
{{--                    <li @if(Request::is('admin/withdraws/awaiting')) class="active" @endif>--}}
{{--                        <a href="/admin/withdraws/awaiting"><span class="xn-text">قيد الإنتظار</span><span class="fa fa-minus-square"></span></a>--}}
{{--                    </li>--}}
{{--                    <li @if(Request::is('admin/withdraws/approved')) class="active" @endif>--}}
{{--                        <a href="/admin/withdraws/approved"><span class="xn-text">موافق عليها</span><span class="fa fa-check-square"></span></a>--}}
{{--                    </li>--}}
{{--                    <li @if(Request::is('admin/withdraws/declined')) class="active" @endif>--}}
{{--                        <a href="/admin/withdraws/declined"><span class="xn-text">مرفوضة</span><span class="fa fa-times-rectangle-o"></span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

            <li @if(Request::is('admin/banners/*') || Request::is('admin/banner/*')) class="active" @endif>
                <a href="/admin/banners/index"><span class="xn-text">البانرات الإعلامية</span><span class="fa fa-image"></span></a>
            </li>

            <li @if(Request::is('admin/sides/*') || Request::is('admin/side/*')) class="active" @endif>
                <a href="/admin/sides/index"><span class="xn-text">البانرات الإعلامية الجانبية</span><span class="fa fa-image"></span></a>
            </li>

        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-left last">
                <a href="#" class="mb-control" data-box="#mb-signout" title="Logout"><span class="fa fa-power-off"></span></a>
            </li>
            <!-- END POWER OFF -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> تسجيل <strong>الخروج</strong> ؟</div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/admin/logout" class="btn btn-success btn-lg">نعم</a>
                            <button class="btn btn-default btn-lg mb-control-close">لا</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

     @yield('content')





<!-- START PRELOADS -->
<audio id="audio-alert" src="{{asset('admin/audio/alert.mp3')}}" preload="auto"></audio>
<audio id="audio-fail" src="{{asset('admin/audio/fail.mp3')}}" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type='text/javascript' src='{{asset('admin/js/plugins/icheck/icheck.min.js')}}'></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/owl/owl.carousel.min.js')}}"></script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="{{asset('admin/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/actions.js')}}"></script>


<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
<!-- END THIS PAGE PLUGINS -->
<!-- END SCRIPTS -->
</body>

<script>
    function modal_activate(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_activate').val(id);
    }


    function modal_suspend(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_suspend').val(id);
    }


    function modal_destroy(id,type = null)
    {
        if(type === 'main')
        {
            $('.main-text').show();
            $('.sub-text').hide();
        }
        else if(type === 'sub')
        {
            $('.sub-text').show();
            $('.main-text').hide();
        }

        $('#id_destroy').val(id);
    }
</script>
</html>






