@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>الباقات السنوية</a></li>
        <li class="active">{{isset($pack) ? 'تعديل باقة' : 'أضافة باقة'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($pack) ? '/admin/pack/update' : '/admin/pack/store'}}">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($pack) ? 'تعديل باقة' : 'أضافة باقة'}}
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($pack)) value="{{$pack->ar_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($pack)) value="{{$pack->en_name}}" @else {{old('en_name')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('points') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عدد النقاط</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="number" class="form-control" name="points" step="1" min="0" @if(isset($pack)) value="{{$pack->points}}" @else {{old('points')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'points'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('months_count') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عدد الشهور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="number" class="form-control" name="months_count" step="1" min="0" @if(isset($pack)) value="{{$pack->months_count}}" @else {{old('months_count')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'months_count'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="number" class="form-control" name="price" step="1" min="0" @if(isset($pack)) value="{{$pack->price}}" @else {{old('price')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'price'])
                                </div>
                            </div>

                            @if(isset($pack))
                                    <input type="hidden" name="pack_id" value="{{$pack->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                                {{isset($pack) ? 'تعديل' : 'إنشاء'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
