@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الباقات السنوية</li>
        @if(Request::is('admin/packs/active'))
            <li class="active">فعال</li>
        @else
            <li class="active">موقوف</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->


    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/pack/create"><button type="button" class="btn btn-info"> أضف باقة جديدة </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم بالعربية</th>
                                    <th>الإسم بالإنجليزية</th>
                                    <th>عدد النقاط</th>
                                    <th>عدد الشهور</th>
                                    <th>السعر</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($packs as $pack)
                                    <tr>
                                        <td>{{$pack->ar_name}}</td>
                                        <td>{{$pack->en_name}}</td>
                                        <td>{{$pack->points}}</td>
                                        <td>{{$pack->months_count}}</td>
                                        <td>{{$pack->price}} {{admin()->country->ar_currency }}</td>
                                        <td>
                                            <a title="تعديل" href="/admin/pack/{{$pack->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            @if($pack->status == 'active')
                                                <button class="btn btn-primary btn-condensed mb-control" data-box="#message-box-suspend-{{$pack->id}}" title="توقيف"><i class="fa fa-minus-square"></i></button>
                                            @elseif($pack->status == 'suspended')
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-activate-{{$pack->id}}" title="تفعييل"><i class="fa fa-check-square"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$pack->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$pack->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تفعل باقة سنوية,ستكون متاحة للطلب من قبل المستخدمين.</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/pack/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="pack_id" value="{{$pack->id}}">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- suspend with sound -->
                                    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$pack->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن توقف باقة سنوية,و لن تكون متاحة للطلب من قبل المستخدمين.</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/pack/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="pack_id" value="{{$pack->id}}">
                                                        <button type="submit" class="btn btn-primary btn-lg pull-right">توقيف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end suspend with sound -->

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$pack->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف باقة سنوية,لن تستطيع أن تستعيد بياناتها مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/pack/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="pack_id" value="{{$pack->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$packs->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
