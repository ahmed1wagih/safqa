@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>طلبات سحب الأرباح</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('delegate.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>المندوب</th>
                                    <th>المبلغ</th>
                                    <th>قيمة المبلغ بالنقاط</th>
                                    <th>الرصيد الحالي بالنقاط</th>
                                    <th>التاريخ</th>
                                    <th>الحالة</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($withdraws as $withdraw)
                                    <tr>
                                        <td><a href="/admin/member/{{$withdraw->delegate_id}}/view" title="مشاهدة المندوب">{{$withdraw->delegate->name}}</a></td>
                                        <td>{{$withdraw->amount}}</td>
                                        <td>{{round($withdraw->amount_to_points($withdraw->amount),0)}}</td>
                                        <td>{{$withdraw->delegate->credit}}</td>
                                        <td>{{$withdraw->created_at}}<br/>
                                        <td>@if($withdraw->status == 'approved') مقبول @elseif($withdraw->status == 'declined') مرفوض @else قيد الإنتظار @endif<br/>
                                        <td>
                                            @if($withdraw->status == 'awaiting')
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-approved-{{$withdraw->id}}" title="قبول"><i class="fa fa-check-square"></i></button>
                                                <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-declined-{{$withdraw->id}}" title="رفض"><i class="fa fa-minus-square"></i></button>
                                            @endif
                                            {{--<button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$user->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>--}}
                                        </td>
                                    </tr>

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-approved-{{$withdraw->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تقبل عملية التحويل,سيتم خصم قيمة المبلغ بالنقاط من رصيد المندوب</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/withdraw/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="withdraw_id" value="{{$withdraw->id}}">
                                                        <input type="hidden" name="status" value="approved">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">قبول</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-declined-{{$withdraw->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن ترفض عملية التحويل</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/withdraw/change_status" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="withdraw_id" value="{{$withdraw->id}}">
                                                        <input type="hidden" name="status" value="declined">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">قبول</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                @endforeach
                                </tbody>
                            </table>
                            {{$withdraws->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
