@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">إدارة البانرات الإعلانية</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/banner/create">
                            <button type="button" class="btn btn-info">أضف بانر إعلاني</button>
                        </a>
                    </div>
                    <div class="panel-body" style="overflow: auto;">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="rtl_th">الصورة</th>
                                    <th class="rtl_th">المقاس</th>
                                    <th class="rtl_th">لينك التحويل</th>
                                    <th class="rtl_th">عدد النقرات</th>
                                    <th class="rtl_th">الإجراء المتخذ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>
                                                <img class="img-responsive" src="/banners/{{$banner->image}}" width="250" height="250"/>
                                        </td>
                                        <td>
                                            {{$banner->height}} إرتفاع
                                            <br/>
                                            1140 عرض
                                        </td>
                                        <td>
                                            <a href="{{$banner->link}}" target="_blank">{{$banner->link}}</a>
                                        </td>
                                        <td>
                                            {{$banner->clicks}}
                                        </td>
                                        <td>
                                            <a href="/admin/banner/{{$banner->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$banner->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$banner->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف البانر الإعلامي و لن تستطيع إسترجاعه مرة أخري,هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                                    <form method="post" action="/admin/banner/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="banner_id" value="{{$banner->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                        <!-- end danger with sound -->
                        @endforeach
                        </tbody>

                        </table>
                        {{$banners->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
