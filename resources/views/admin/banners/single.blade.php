@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/banners/index">البانرات الإعلامية</a></li>
        <li class="active">@if(isset($banner)) تعديل صورة @else إضافة صورة @endif</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($banner)) action="/admin/banner/update" @else action="/admin/banner/store" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                 {{isset($banner) ? 'تعديل صورة': 'إضافة صورة'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">لينك التحويل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="link" value="{{isset($banner) ? $banner->link : old('link')}}" style="direction: ltr;" required/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'link'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('height') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">إرتفاع البانر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-arrows-v"></span></span>
                                        <input type="number" step="1" class="form-control" name="height" value="{{isset($banner) ? $banner->height : old('height')}}" required/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'height'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('width') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عرض البانر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-arrows-v"></span></span>
                                        <input type="number" step="1" class="form-control" name="width" value="{{isset($banner) ? $banner->width : old('width')}}" required/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'width'])
                                </div>
                            </div>


                        @if(isset($banner))
                            <div class="form-group">
                                <label class="col-md-3 control-label">الصورة الحالية</label>

                                    <div class="col-md-6">
                                        <img src="{{asset('/banners/'.$banner->image)}}" class="img-responsive" style="width: 100%"/>
                                    </div>
                                @include('admin.layouts.error', ['input' => 'image'])
                            </div>
                            @endif

                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">أرفق صورة</label>
                                <div class="col-md-9">
                                    <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="إرفق الصورة"/>
                                </div>
                                @include('admin.layouts.error', ['input' => 'image'])
                            </div>

                            @if(isset($banner))
                                <input type="hidden" name="banner_id" value="{{$banner->id}}">
                            @endif

                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                              {{isset($banner) ? 'تعديل': 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
