@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>المدفوعات الإلكترونية</li>
        @if(Request::is('admin/online_transfers/awaiting'))
            <li class="active">قيد التأكيد</li>
        @elseif(Request::is('admin/online_transfers/approved'))
            <li class="active">مدفوع</li>
        @else
            <li class="active">ملغي</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>المنصة</th>
                                    <th>رقم العملية</th>
                                    <th>صاحب الحساب</th>
                                    <th>التاريخ</th>
                                    <th>الباقة</th>
                                    <th>السعر</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($online_transfers as $transfer)
                                    <tr>
                                        <td>{{$transfer->id}}</td>
                                        <td>{{trans('trans.'.$transfer->type)}}</td>
                                        <td>
                                            {{$transfer->trans_id}}
                                        </td>
                                        <td><a href="/admin/member/{{$transfer->user_id}}/view">{{$transfer->user->name}}</a></td>
                                        <td>
                                            {{$transfer->created_at->toTimeString()}}<br/>
                                            {{$transfer->created_at->toDateString()}}<br/>
                                        </td>
                                        <td>
                                            {{$transfer->pack->ar_name}}
                                        </td>
                                        <td>{{$transfer->amount}} {{ $transfer->currency }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$online_transfers->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
