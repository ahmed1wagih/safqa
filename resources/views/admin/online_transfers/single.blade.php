@extends('admin.layouts.app')
@section('content')

    <style>
        .img
        {
            min-width: 312px;
            max-width: 312px;
            min-height: 312px;
            max-height: 312px;
        }

        .gallery .gallery-item.active .image
        {
            opacity: 0.2 !important;

        }
    </style>


    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>التحويلات</li>
        <li class="active">مشاهدة تحويل</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                مشاهدة تحويل
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">النوع</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <label class="form-control">
                                            @if($transfer->type == 'order')
                                                طلب منتجات
                                            @else
                                                شحن رصيد
                                            @endif</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الحالة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        @if($transfer->status == 'approved')
                                            <label class="form-control" style="color: green;">
                                                مقبول
                                            </label>
                                            @elseif($transfer->status == 'awaiting')
                                            <label class="form-control" style="color: grey;">
                                                قيد الإنتظار
                                            </label>
                                            @else
                                            <label class="form-control" style="color: red;">
                                                مرفوض
                                            </label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">صاحب التحويل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <label class="form-control"><a href="/admin/member/{{$transfer->user_id}}/view">{{$transfer->user->name}}</a></label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$transfer->name}}</label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">البنك</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-bank"></span></span>
                                        <label class="form-control">{{$transfer->bank->ar_name}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">رقم التحويل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <label class="form-control">{{$transfer->trans_no}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">الباقة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <label class="form-control">{{$transfer->pack->ar_name}} - {{$transfer->pack->price . ' '  .admin()->country->ar_currency}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">التاريخ</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <label class="form-control">{{$transfer->date}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-footer">
                            @if($transfer->status == 'awaiting')
                                <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-declined-{{$transfer->id}}" title="رفض"><i class="fa fa-thumbs-up"></i> رفض التحويل </button>
                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-approved-{{$transfer->id}}" title="قبول"><i class="fa fa-thumbs-up"></i> قبول التحويل </button>
                            @endif
                        </div>
                    </div>


            </div>
        </div>

    <!-- activate with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-approved-{{$transfer->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    @if($transfer->type == 'order')
                        <p>أنت علي وشك أن تقبل تحويل بنكي و تفعيل الباركود الخاص بالصفقات,و سيظهر المستخدم من الآن .</p>
                    @else
                        <p>أنت علي وشك أن تقبل تحويل بنكي و إضافة الرصيد للمستخدم .</p>
                    @endif
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/transfer/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="transfer_id" value="{{$transfer->id}}">
                        <input type="hidden" name="status" value="approved">
                        <button type="submit" class="btn btn-success btn-lg pull-right">قبول التحويل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end activate with sound -->

    <!-- suspend with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-declined-{{$transfer->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن ترفض تحويل بنكي لشحن رصيد .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/transfer/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="transfer_id" value="{{$transfer->id}}">
                        <input type="hidden" name="status" value="decline">
                        <button type="submit" class="btn btn-danger btn-lg pull-right">رفض التحويل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end suspend with sound -->

    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->


    @if(isset($item))
        <script>
            document.getElementById('links').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {index: link, event: event,onclosed: function(){
                            setTimeout(function(){
                                $("body").css("overflow","");
                            },200);
                        }},
                    links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };
        </script>
    @endif



    <!-- THIS PAGE PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->
@endsection
