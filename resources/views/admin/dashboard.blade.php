@extends('admin.layouts.app')
@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="margin-top: 10px;">
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-building"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$merchants->count()}}</div>
                        <div class="widget-title">عدد التجار</div>
                        <div class="widget-subtitle">{{$merchants->where('status','suspended')->count()}} موقوف </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-cube"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$deals->count()}}</div>
                        <div class="widget-title">كل الصفقات</div>
                        <div class="widget-subtitle">{{$deals->where('status','suspended')->count()}} موقوف </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-barcode"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$all_codes->count()}}</div>
                        <div class="widget-title">كل أكواد الصفقات</div>
                        <div class="widget-subtitle">{{$all_codes->where('user_id','!=',NULL)->count()}} تم شراؤها </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$users->count()}}</div>
                        <div class="widget-title">عدد المستخدمين</div>
                        <div class="widget-subtitle">{{$users->where('status','suspended')->count()}} موقوف </div>

                    </div>
                </div>
            </div>


            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-calendar"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$packs->where('status','active')->count()}}</div>
                        <div class="widget-title">عدد الباقات المفعلة</div>
                        <div class="widget-subtitle">{{$packs->sum('sold')}} عملية بيع </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-money"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$bank_transfers->count()}}</div>
                        <div class="widget-title">تحويلات بنكية مقبولة</div>
                        <div class="widget-title">
                            {{$bank_transfers->sum('price')}}
                            إجمالي مبالغ التحويلات
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget widget-info widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-money"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$online_transfers->count()}}</div>
                        <div class="widget-title">تحويلات إلكترونية مقبولة</div>
                        <div class="widget-title">
                            {{$online_transfers->sum('amount')}}
                            إجمالي مبالغ التحويلات
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>الأقسام</h3>
                            <span>عدد الصفقات لكل قسم</span>
                        </div>
                    </div>
                    <div class="panel-body padding-0">
                        <div class="chart-holder" id="category-deals-donut" style="height: 330px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>التجار</h3>
                            <span>عدد الصفقات لكل تاجر</span>
                        </div>
                    </div>
                    <div class="panel-body padding-0">
                        <div class="chart-holder" id="merchant-deals-donut" style="height: 330px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>عمليات الشراء</h3>
                            <span>عدد عمليات الشراء الشهرية</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="month-trans-count" style="height: 300px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END WIDGETS -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    <script>
        var morrisCharts = function() {

            var days_data;

            $.ajax(
                {
                    async : false,
                    url : '/admin/month_deals_graph',
                    method : 'get',
                    dataType : 'json',
                    success : function(data)
                    {
                        days_data = data;
                    },
                    error : function()
                    {
                        console.log('month graphs ajax error')
                    },
                }
            );

            Morris.Line({
                element: 'month-trans-count',
                data: days_data,
                xkey: 'x',
                ykeys: ['y'],
                labels: ['عمليات الشراء'],
                resize: false,
                lineColors: ['#33414E']
            });
        }();


        var merchat_deals = [];
        var donut_colors = ["#0074D9","#B70004","#33414E","#FF4136","#2ECC40","#840003","#FF851B","#358E33","#7FDBFF","#B10DC9","#FFDC00","#001f3f","#39CCCC","#01FF70","#85144b","#F012BE","#3D9970","#111111","#921880"];
        var merchant_deals_donut = [];
        var category_deals_donut = [];

        $.ajax(
            {
                async : false,
                url : '/admin/merchant_deals',
                method : 'get',
                dataType : 'json',
                success : function(data)
                {
                    $.each(data, function (i,merchant)
                    {
                        merchant_deals_donut.push({label: merchant.ar_name, value: merchant.deals});
                    });
                },
                error : function()
                {
                    console.log('merchant deals donut ajax error')
                },
            }
        );

        $.ajax(
            {
                async : false,
                url : '/admin/category_deals',
                method : 'get',
                dataType : 'json',
                success : function(data)
                {
                    $.each(data, function (i,category)
                    {
                        category_deals_donut.push({label: category.ar_name, value: category.deals});
                    });
                },
                error : function()
                {
                    console.log('category deals donut ajax error')
                },
            }
        );

        Morris.Donut({
            element: 'merchant-deals-donut',
            data: merchant_deals_donut,
            colors: donut_colors,
            resize: false
        });


        Morris.Donut({
            element: 'category-deals-donut',
            data: category_deals_donut,
            colors: donut_colors,
            resize: false
        });

    </script>
@endsection
