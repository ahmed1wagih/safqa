@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>التجار</li>
        <li class="active">{{$status}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/admin/merchant/create"><button type="button" class="btn btn-info"> أضف تاجر جديد </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم بالعربية</th>
                                    <th>الإسم بالإنجليزية</th>
                                    <th>النوع</th>
                                    <th>عدد الصفقات</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($merchants as $merchant)
                                    <tr>
                                        <td>{{$merchant->ar_name}}</td>
                                        <td>{{$merchant->en_name}}</td>
                                        <td>{{$merchant->type == 'online' ? 'متجر إلكتروني' : 'متجر محلي'}}</td>
                                        <td>{{$merchant->products->count()}}</td>
                                        <td>
                                            <a title="تعديل" href="/admin/merchant/{{$merchant->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            @if($merchant->status == 'active')
                                                <button class="btn btn-primary btn-condensed mb-control" data-box="#message-box-suspend-{{$merchant->id}}" title="إيقاف"><i class="fa fa-minus-square"></i></button>
                                            @else
                                                <button class="btn btn-success btn-condensed mb-control" data-box="#message-box-activate-{{$merchant->id}}" title="تفعيل"><i class="fa fa-check-square"></i></button>
                                            @endif
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$merchant->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-warning animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$merchant->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف تاجر,و سيتم حذف جميع البيانات المندرجة أسفله,مثل المنتجات و العروض,و لن تستطيع إستعادتها مرة أخري .</p>
                                                    <br/>
                                                    <p>هل انت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/merchant/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="merchant_id" value="{{$merchant->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->

                                    <!-- activate with sound -->
                                    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$merchant->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تفعل تاجر,ستظهر جميع منتجاته من الآن .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/merchant/change_state" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="merchant_id" value="{{$merchant->id}}">
                                                        <input type="hidden" name="state" value="1">
                                                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end activate with sound -->

                                    <!-- suspend with sound -->
                                    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$merchant->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن توقف مستخدم,لن تظهر جميع منتجاته من الآن .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/admin/merchant/change_state" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="merchant_id" value="{{$merchant->id}}">
                                                        <input type="hidden" name="state" value="0">
                                                        <button type="submit" class="btn btn-primary btn-lg pull-right">توقيف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end suspend with sound -->

                                @endforeach
                                </tbody>
                            </table>
                            {{$merchants->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
