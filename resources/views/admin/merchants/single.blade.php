@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>التجار</a></li>
        <li class="active">{{isset($merchant) ? 'تعديل تاجر' : 'أضافة تاجر'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    {{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($merchant) ? '/admin/merchant/update' : '/admin/merchant/store'}}">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($merchant) ? 'تعديل تاجر' : 'أضافة تاجر'}}
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($merchant)) value="{{$merchant->ar_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($merchant)) value="{{$merchant->en_name}}" @else {{old('en_name')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_desc">@if(isset($merchant)){!! $merchant->ar_desc !!}@else{{old('ar_desc')}}@endif</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_desc">@if(isset($merchant)){!! $merchant->en_desc !!}@else{{old('en_desc')}}@endif</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="ar_address">@if(isset($merchant)){!! $merchant->ar_address !!}@else{{old('ar_address')}}@endif</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_address'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea rows="5" class="form-control summernote" name="en_address">@if(isset($merchant)){!! $merchant->en_address !!}@else{{old('en_address')}}@endif</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_address'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">نوع المتجر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <select class="form-control select" name="type" id="type_select">
                                            <option selected disabled>إختر من التالي</option>
                                            <option value="local" {{isset($merchant) && $merchant->type == 'local' ? 'select' : ''}}>متجر محلي</option>
                                            <option value="online" {{isset($merchant) && $merchant->type == 'online' ? 'select' : ''}}>متجر إلكتروني</option>
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'type'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }} online_div" style="display: none;">
                                <label class="col-md-3 col-xs-12 control-label">لينك الموقع الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="link" @if(isset($merchant)) value="{{$merchant->link}}" @else {{old('link')}} @endif/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('lat') ? ' has-error' : '' }} local_div" style="display: none;">
                                <label class="col-md-3 col-xs-12 control-label">الموقع علي الخريطة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div id="map" style=" height: 600px;"></div>
                                </div>
                                <input type="hidden" @if(isset($merchant))value="{{$merchant->lat}}" @else value="" @endif id="mylat" name="lat">
                                <input type="hidden" @if(isset($merchant))value="{{$merchant->lng}}" @else value="" @endif id="mylng" name="lng">
                                @include('merchant.layouts.error', ['input' => 'lat'])
                            </div>

                            <div class="form-group {{ $errors->has('owner') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">المالك</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" class="form-control" name="owner"  @if(isset($merchant)) value="{{$merchant->owner}}" @else {{old('owner')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'owner'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('owner_phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">هاتف المالك</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
                                        <input type="text" class="form-control" name="owner_phone"  @if(isset($merchant)) value="{{$merchant->owner_phone}}" @else {{old('owner_phone')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'owner_phone'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('in_charge') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">المسئول </label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                        <input type="text" class="form-control" name="in_charge"  @if(isset($merchant)) value="{{$merchant->in_charge}}" @else {{old('in_charge')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'in_charge'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('in_charge_phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">هاتف المسئول</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
                                        <input type="text" class="form-control" name="in_charge_phone"  @if(isset($merchant)) value="{{$merchant->in_charge_phone}}" @else {{old('in_charge_phone')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'in_charge_phone'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-at"></span></span>
                                        <input type="email" class="form-control" name="email"  @if(isset($merchant)) value="{{$merchant->email}}" @else {{old('email')}} @endif/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'email'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرورالجديدة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="new_password"/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'new_password'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="new_password_confirmation"/>
                                    </div>
                                    @include('merchant.layouts.error', ['input' => 'new_password_confirmation'])
                                </div>
                            </div>

                            @if(isset($merchant))
                                    <input type="hidden" name="merchant_id" value="{{$merchant->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                                {{isset($merchant) ? 'تعديل' : 'إنشاء'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->


    <script>

        $('#type_select').on('change', function()
        {
            if($(this).val() === 'local')
            {
                $('.local_div').show();
                $('.online_div').hide();
            }
            else if($(this).val() === 'online')
            {
                $('.online_div').show();
                $('.local_div').hide();
            }
        });

        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7136, lng: 46.6753},
                zoom: 14

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = $('#mylat'),
                        mylng = $('#mylng');

                    @if(isset($merchant))
                        var pos = {
                                lat: parseFloat($('#mylat').val()),
                                lng: parseFloat($('#mylng').val())
                            };
                    @else
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                    @endif

                        mylat.value = pos.lat;
                    mylng.value = pos.lng;

                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        $('#mylat').val(marker.getPosition().lat());
                        $('#mylng').val(marker.getPosition().lng());
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>
@endsection
