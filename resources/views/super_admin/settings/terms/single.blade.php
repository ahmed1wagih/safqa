@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>إعدادت التطبيق</li>
        <li class="active">تعديل نص القواعد و الشروط</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/settings/terms/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل نص القواعد و الشروط
                            </h3>
                        </div>

                        <div class="form-group {{ $errors->has('ar_text') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">النص بالعربية</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                    <textarea rows="5" class="summernote" name="ar_text">{!! $terms->ar_text !!}</textarea>
                                </div>
                                @include('delegate.layouts.error', ['input' => 'ar_text'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('en_text') ? ' has-error' : '' }}">
                            <label class="col-md-3 col-xs-12 control-label">النص بالإنجليزية</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                    <textarea rows="5" class="summernote" name="en_text">{!! $terms->en_text !!}</textarea>
                                </div>
                                @include('delegate.layouts.error', ['input' => 'en_text'])
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                             تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->

@endsection
