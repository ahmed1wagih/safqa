@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>إعدادت التطبيق</li>
        <li><a href="/super_admin/settings/socials">حسابات التواصل الإجتامي</a></li>
        <li class="active">تعديل</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/settings/socials/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('facebook') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">Facebook</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="facebook" value="{{$socials->facebook}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'facebook'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('twitter') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">Twitter</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="twitter" value="{{$socials->twitter}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'twitter'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('instagram') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">Instagram</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-link"></span></span>
                                        <input type="text" class="form-control" name="instagram" value="{{$socials->instagram}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'instagram'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                              تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
