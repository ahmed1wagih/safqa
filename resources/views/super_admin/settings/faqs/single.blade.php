@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>إعدادت التطبيق</li>
        <li><a href="/admin/settings/faq">الأسئلة الشائعة</a></li>
        <li class="active">
            @if(isset($faq))
                تعديل سؤال
            @else
                إضافة سؤال
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                @if(isset($faq))
                                    تعديل سؤال
                                @else
                                    إضافة سؤال
                                @endif
                            </h3>
                        </div>
                        <form class="form-horizontal" method="post" @if(isset($faq)) action="/super_admin/settings/faq/update" @else action="/super_admin/settings/faq/store" @endif>
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('ar_question') ? ' has-error' : '' }}">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">السؤال بالعربية</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-question-circle"></span></span>
                                                <textarea class="summernote" type="text" name="ar_question"> @if(isset($faq)) {!! $faq->ar_question !!} @else {!! old('ar_question') !!}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_question'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_question') ? ' has-error' : '' }}">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">السؤال بالإنجليزية</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-question-circle"></span></span>
                                                <textarea class="summernote" type="text" name="en_question"> @if(isset($faq)) {!! $faq->en_question !!} @else {!! old('en_question') !!}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'en_question'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_question') ? ' has-error' : '' }}">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">الإجابة بالعربية</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                                <textarea class="summernote" rows="5" name="ar_answer">@if(isset($faq)) {!! $faq->ar_answer !!} @else {!! old('ar_answer') !!}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_question'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('ar_question') ? ' has-error' : '' }}">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">الإجابة بالإنجليزية</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                                <textarea class="summernote" rows="5" name="en_answer">@if(isset($faq)) {!! $faq->en_answer !!} @else {!! old('en_answer') !!}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'ar_question'])
                                </div>

                                @if(isset($faq))
                                    <input type="hidden" name="faq_id" value="{{$faq->id}}">
                                @endif

                            </div>
                            <div class="panel-footer">
                                <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                                <button class="btn btn-primary pull-right">
                                    @if(isset($faq))
                                        تعديل
                                    @else
                                        إضافة
                                    @endif
                                </button>
                            </div>
                        </form>


                    </div>

            </div>
        </div>
    </div>

    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->
@endsection
