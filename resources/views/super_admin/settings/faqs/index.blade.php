@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/admin/dashboard">الرئيسية</a></li>
        <li>إعدادات التطبيق</li>
        <li class="active">الأسئلة الشائعة</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/settings/faq/create">
                            <button type="button" class="btn btn-info">أضف سؤال</button>
                        </a>
                    </div>
                    <div class="panel-body" style="overflow: auto;">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="rtl_th">السؤال بالعربية</th>
                                    <th class="rtl_th">الإجابة بالعربية</th>
                                    <th class="rtl_th">الإجراء المتخذ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($faqs as $faq)
                                    <tr>
                                        <td> {!! $faq->ar_question !!} </td>
                                        <td> {!! $faq->ar_answer !!} </td>
                                        <td>
                                            <a href="/super_admin/settings/faq/{{$faq->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$faq->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$faq->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف سؤال .</p>
                                                    <br/>
                                                    <p>هل انت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/super_admin/settings/faq/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="faq_id" value="{{$faq->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                        </tbody>
                        </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
