@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات </li>
        <li class="active">تعديل الإعدادات العامة</li>
    </ul>
    <!-- END BREADCRUMB -->
    @include('super_admin.layouts.message')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/settings/settings/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">إسم الموقع بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="ar_name" value="{{$settings->ar_name}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">إسم الموقع بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="en_name" value="{{$settings->en_name}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('ar_rights') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">حقوق الموقع بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="ar_rights" value="{{$settings->ar_rights}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_rights'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('en_rights') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">حقوق الموقع بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="text" class="form-control" name="en_rights" value="{{$settings->en_rights}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_rights'])
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('credit_on_register') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">النقاط المجانية عند التسجيل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="number" class="form-control" name="credit_on_register" value="{{$settings->credit_on_register}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'credit_on_register'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('delegate_fee') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">النسبة المئوية لقيمة بيع الصفقة بالنقاط</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                        <input type="number" class="form-control" name="delegate_fee" value="{{$settings->delegate_fee}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'delegate_fee'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                              تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
