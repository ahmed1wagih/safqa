@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>إعدادت التطبيق</li>
        <li class="active">تعديل نص معلومات عنا</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" action="/super_admin/settings/abouts/update" method="post">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل نص معلومات عنا
                            </h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('ar_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                        <textarea class="summernote" name="ar_address" rows="5">{!! $abouts->ar_address !!}</textarea>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_address'])
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('en_address') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                        <textarea class="summernote" name="en_address" rows="5">{!! $abouts->en_address !!}</textarea>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_address'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('ar_text') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">النص بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                        <textarea class="summernote" name="ar_text" rows="5">{!! $abouts->ar_text !!}</textarea>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_text'])
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('en_text') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">النص بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-file-text"></span></span>
                                        <textarea class="summernote" name="en_text" rows="5">{!! $abouts->en_text !!}</textarea>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_text'])
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="email" class="form-control" name="email" value="{{$abouts->email}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'email'])
                                </div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="phone" value="{{$abouts->phone}}"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'phone'])
                                </div>
                            </div>
                        </div>


                        <!--<div class="panel-body">-->
                        <!--    <div class="form-group {{ $errors->has('lat') ? ' has-error' : '' }}">-->
                        <!--        <label class="col-md-3 col-xs-12 control-label">الخريطة</label>-->
                        <!--        <div class="col-md-6 col-xs-12">-->
                        <!--            <div class="input-group">-->
                        <!--                <span class="input-group-addon"><span class="fa fa-map"></span></span>-->
                        <!--                <div id="map" style="width: 100%; height: 500px; float: right;"></div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->

                        <input type="hidden" value="{{$abouts->lat}}" id="mylat" name="lat">
                        <input type="hidden" value="{{$abouts->lng}}" id="mylng" name="lng">

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                             تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src='{{asset("admin/js/plugins/icheck/icheck.min.js")}}'></script>
    <script type="text/javascript" src="{{asset("admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->


    <script>
        var marker;

        var map, infoWindow;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 18

            });


            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var mylat = document.getElementById('mylat'),
                        mylng = document.getElementById('mylng');

                    @if(isset($abouts))
                        var pos = {
                                lat: parseFloat(mylat.value),
                                lng: parseFloat(mylng.value)
                            };
                    @else
                        var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                    @endif

                        mylat.value = pos.lat;
                    mylng.value = pos.lng;
                    infoWindow.setPosition(pos);
                    // infoWindow.setContent('    تم تحديد المكان !');
                    // infoWindow.open(map);
                    map.setCenter(pos);

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        title: 'هنا !'

                    });

                    marker.addListener('click', toggleBounce);
                    google.maps.event.addListener(marker, 'dragend', function (ev) {
                        mylat.value = marker.getPosition().lat();
                        mylng.value = marker.getPosition().lng();

                        // console.log(mylat.value, mylng.value);
                    });

                    function toggleBounce() {
                        if (marker.getAnimation() !== null) {
                            marker.setAnimation(null);
                        } else {
                            marker.setAnimation(google.maps.Animation.BOUNCE);
                        }
                    }


                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());


                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());

            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhnmMC23noePz6DA8iEvO9_yNDGGlEaeM&callback=initMap">
    </script>

@endsection
