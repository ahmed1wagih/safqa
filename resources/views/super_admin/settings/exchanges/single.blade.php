@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>الإعدادات </li>
        <li class="active">تعديل قيم التحويل للنقاط</li>
    </ul>
    <!-- END BREADCRUMB -->
    @include('super_admin.layouts.message')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/super_admin/settings/exchanges/update">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                تعديل
                            </h3>
                        </div>
                        @foreach($countries as $country)
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">{{$country->ar_name}}</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                            <input type="number" min="0" class="form-control" name="{{$country->id}}" value="{{$country->exchange->point_per_currency}}" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                              تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
