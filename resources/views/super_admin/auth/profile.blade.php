@extends('super_admin.layouts.app')
@section('content')
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li class="active">
            الملف الشخصي
        </li>
    </ul>
    @include('super_admin.layouts.message')
    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2 class="switch_order">الملف الشخصي</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-5">

                <form action="#" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>{{super()->name}}</h3>
                            <p><span class="label label-info label-form">مدير عام</span></p>
                            <div class="text-center" id="user_image">
                                <img src="/users/default.png" class="img-thumbnail"/>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 col-sm-8 col-xs-7">

                <form action="/super_admin/profile/update" id="jvalidate" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الإسم</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="name" value="{{super()->name}}" class="form-control"/>
                                </div>
                                @include('super_admin.layouts.error',['input' => 'name'])
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">البريد الإلكتروني</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="email" name="email" id="email" value="{{super()->email}}" class="form-control"/>
                                </div>
                                @include('super_admin.layouts.error',['input' => 'email'])
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الهاتف</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="text" name="phone" value="{{super()->phone}}" class="form-control"/>
                                </div>
                                @include('super_admin.layouts.error',['input' => 'phone'])
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">كلمة المرور</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="إتركه فارغاً إذا لم يكن هناك تعديل"/>
                                </div>
                                @include('super_admin.layouts.error',['input' => 'password'])
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-9 col-xs-7">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="إتركه فارغاً إذا لم يكن هناك تعديل"/>
                                </div>
                                @include('super_admin.layouts.error',['input' => 'password'])
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-5">
                                    <button class="btn btn-primary {{lang() == 'ar' ? 'pull-left' : 'pull-right'}}">تحديث</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3>بيانات سريعة</h3>
                    </div>
                    <div class="panel-body form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">آخر تحديث</label>
                            <div class="col-md-8 col-xs-7 line-height-30">{{\Carbon\Carbon::parse(super()->updated_at)->format('m-d-Y')}}<br/>{{\Carbon\Carbon::parse(super()->updated_at)->format('h:s a')}}</div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">تاريخ التسجيل</label>
                            <div class="col-md-8 col-xs-7 line-height-30">{{\Carbon\Carbon::parse(super()->created_at)->format('m-d-Y')}}<br/>{{\Carbon\Carbon::parse(super()->created_at)->format('h:s a')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">
        setTimeout(
            function() {
                $('#email').val('{{super()->email}}');
                $(':password').val('');
                },
            2000  //1,000 milliseconds = 1 second
        );
    </script>
@endsection
