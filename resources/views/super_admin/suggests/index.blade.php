@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>رسائل إتصل بنا</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    {{--<div class="panel-heading">--}}
                        {{--<a href="/super_admin/mail/0/create"><button type="button" class="btn btn-info"> أرسل بريد إلكتروني للجميع </button></a>--}}
                    {{--</div>--}}
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم</th>
                                    <th>البريد الإلكتروني</th>
                                    <th>الرسالة</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($suggests as $suggest)
                                    <tr>
                                        <td>{{$suggest->name}}</td>
                                        <td>{{$suggest->email}}</td>
                                        <td>{{$suggest->message}}</td>
                                        <td>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$suggest->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$suggest->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف هذه الرسالة,و لن تستطيع أن تستعيد بياناتها مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/super_admin/suggest/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="suggest_id" value="{{$suggest->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$suggests->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
