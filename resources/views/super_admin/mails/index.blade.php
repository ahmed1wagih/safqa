@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>القائمة البريدية</li>
    </ul>
    <!-- END BREADCRUMB -->

    <style>
        .logo
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }
    </style>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('super_admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/mail/create"><button type="button" class="btn btn-info"> أرسل بريد إلكتروني </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الدول</th>
                                    <th>العنوان</th>
                                    <th>عدد المستخدمين</th>
                                    <th>التاريخ</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($mails as $mail)
                                    <tr>
                                        <td>{{implode(',',$mail->countries)}}</td>
                                        <td>{{$mail->title}}</td>
                                        <td>{{$mail->users_count}}</td>
                                        <td>
                                            {{$mail->created_at->toTimeString()}}<br/>
                                            {{$mail->created_at->toDateString()}}
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-danger-{{$mail->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>

                                    <!-- danger with sound -->
                                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger-{{$mail->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف بريد من القائمة البريدية,لن تستطيع أن تستعيد بياناته مرة أخري .</p>
                                                    <br/>
                                                    <p>هل أنت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/super_admin/mail/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="mail_id" value="{{$mail->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$mails->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
