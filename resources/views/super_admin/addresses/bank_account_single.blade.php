@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li>الحسابات البنكية</li>
        <li class="active">{{isset($bank) ? 'تعديل حساب بنكي' : 'أضافة حساب بنكي'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($bank) ? '/super_admin/bank_account/update' : '/super_admin/bank_account/store'}}">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($bank) ? 'تعديل حساب بنكي' : 'أضافة حساب بنكي'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            @if(isset($countries))
                                <div class="form-group {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">الدولة</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                            <select class="form-control select" name="country_id" required>
                                                <option disabled selected>إختر دولة</option>
                                                @forelse($countries as $country)
                                                    <option value="{{$country->id}}" @if(isset($bank) && $bank->country_id == $country->id) selected @endif>{{$country->ar_name}}</option>
                                                @empty
                                                    <option selected disabled>أضف دولة أولاً</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'country_id'])
                                    </div>
                                </div>
                            @endif

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($bank)) value="{{$bank->ar_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($bank)) value="{{$bank->en_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('number') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">رقم الحساب</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="number" @if(isset($bank)) value="{{$bank->number}}" @else {{old('number')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'number'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('iban') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">رقم IBAN</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="iban" @if(isset($bank)) value="{{$bank->iban}}" @else {{old('iban')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'iban'])
                                </div>
                            </div>

                            @if(isset($bank))
                            <input type="hidden" name="bank_id" value="{{$bank->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                {{isset($bank) ? 'تعديل' : 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
