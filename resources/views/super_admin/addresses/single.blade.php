@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li> <a href="/super_admin/addresses/all">الدول و المدن</a></li>
        <li class="active">{{isset($address) ? 'تعديل' : 'أضافة'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    {{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($address) ? '/super_admin/address/update' : '/super_admin/address/store'}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($address) ? 'تعديل عنوان' : 'إضافة عنوان'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            @if(isset($countries))
                                <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">الدولة</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                            <select class="form-control select" name="parent_id" required>
                                                <option disabled selected>إختر دولة,إذا لم تحدد دولة فستعتبر ك دولة و ليست مدينة</option>
                                                @forelse($countries as $country)
                                                    <option value="{{$country->id}}" @if(isset($address) && $address->parent_id == $country->id) selected @endif>{{$country->ar_name}}</option>
                                                @empty
                                                    <option selected disabled>أضف دولة أولاً</option>
                                                @endforelse
                                            </select>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'parent_id'])
                                    </div>
                                </div>
                            @endif

                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($address)) value="{{$address->ar_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-flag"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($address)) value="{{$address->en_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>

                            @if(isset($address) && $address->code != NULL || Request::is('super_admin/address/country/create'))
                                <div class="form-group {{ $errors->has('ar_currency') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">العملة بالعربية</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-money"></span></span>
                                            <input type="text" class="form-control" name="ar_currency" @if(isset($address)) value="{{$address->ar_currency}}" @else {{old('ar_currency')}} @endif/>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'ar_currency'])
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('en_currency') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">العملة بالإنجليزية</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-money"></span></span>
                                            <input type="text" class="form-control" name="en_currency" @if(isset($address)) value="{{$address->en_currency}}" @else {{old('en_currency')}} @endif/>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'en_currency'])
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">العلم</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                            <input type="file" class="fileinput btn-info" name="image" multiple id="cp_photo" data-filename-placement="inside" title="{{isset($address) ? 'تعديل العلم' : 'أضف علم'}}"/>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'image'])
                                    </div>
                                </div>
                            @endif

                            @if(isset($address))
                                <input type="hidden" name="address_id" value="{{$address->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                {{isset($address) ? 'تعديل' : 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
