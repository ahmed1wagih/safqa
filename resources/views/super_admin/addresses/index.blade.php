@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        @if(Request::is('super_admin/addresses/all'))
            <li class="active">الدول</li>
        @else
            <li><a href="/super_admin/addresses/all">الدول</a></li>
            <li> {{\App\Models\Address::get_address($parent)}}</li>
            <li class="active">المدن</li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('super_admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/super_admin/address/city/create"><button type="button" class="btn btn-info"> أضف مدينة </button></a>
                        <a href="/super_admin/address/country/create"><button type="button" class="btn btn-info"> أضف دولة </button></a>
                        <a href="/super_admin/bank_account/create"><button type="button" class="btn btn-info"> أضف حساب بنكي </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم بالعربية</th>
                                    <th>الإسم بالإنجليزية</th>
                                    @if(Request::is('super_admin/addresses/all'))
                                        <th>المدن</th>
                                        <th>الحسابات البنكية</th>
                                    @endif
                                    <th>الحالة</th>
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($addresses as $address)
                                        <tr>
                                            <td>{{$address->ar_name}}</td>
                                            <td>{{$address->en_name}}</td>
                                            @if(Request::is('super_admin/addresses/all'))
                                                <td>{{$address->cities->count()}}</td>
                                                <td>{{$address->bank_accounts->count()}}</td>
                                            @endif
                                            <td><span class="label label-form {{$address->status == 'active' ? 'label-success' : 'label-default'}}">{{$address->status == 'active' ? 'مفعل' : 'موقوف'}}</span></td>
                                            <td>
                                                @if($address->parent_id == NULL && $address->cities->count() != 0)
                                                    <a title="مشاهدة الحسابات البنكية" href="/super_admin/addresses/{{$address->id}}/bank_accounts"><button class="btn btn-success btn-condensed"><i class="fa fa-bank"></i></button></a>
                                                    <a title="مشاهدة المدن" href="/super_admin/addresses/{{$address->id}}"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                                                @endif
                                                <a title="Edit" href="/super_admin/address/{{$address->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                                @if($address->status == 'active')
                                                    <button class="btn btn-primary btn-condensed mb-control" onclick="modal_suspend({{$address->id}})" data-box="#message-box-primary" title="توقيف"><i class="fa fa-minus-circle"></i></button>
                                                @else
                                                    <button class="btn btn-success btn-condensed mb-control" onclick="modal_activate({{$address->id}})" data-box="#message-box-success" title="تفعيل"><i class="fa fa-check-square"></i></button>
                                                @endif
                                                <button class="btn btn-danger btn-condensed mb-control" onclick="modal_destroy({{$address->id}})" data-box="#message-box-danger" title="حذف"><i class="fa fa-trash-o"></i></button>                                        </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        {{$addresses->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- danger with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-success">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    @if($parent != 'all')
                        <p>أنت علي وشك أن تفعل هذه المدينة,و جميع من يتبع من  تجار و صفقات,هل أنت متأكد ؟</p>
                    @else
                        <p>أنت علي وشك أن تفعل هذه الدولة,و جميع من يتبع من مدراء و مدن و تجار و صفقات و مستخدمين يستطيعون مباشرة جميع أعمالهم,هل أنت متأكد ؟</p>
                    @endif
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_activate" value="">
                        <input type="hidden" name="status" value="active">
                        <button class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-primary">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    @if($parent != 'all')
                        <p>أنت علي وشك أن توقف عمل هذه المدينة و كل من يتبعها من تجار و صفقات,هل أنت متأكد ؟</p>
                    @else
                        <p>أنت علي وشك أن توقف عمل هذه الدولة و كل من يتبعها من مدراء مدن و تجار و صفقات و مستخدمين و لن يستطيعوا مباشرة جيع أعمالهم من الآن,هل أنت متأكد ؟</p>
                    @endif
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/change_status" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_suspend" value="">
                        <input type="hidden" name="status" value="suspended">
                        <button class="btn btn-primary btn-lg pull-right">توقيف</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-danger">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                <div class="mb-content">
                    @if($parent != 'all')
                        <p>أنت علي وشك أن تحذف هذه المدينة و ما يتبعها من تجار و صفقات و لن تستطيع إسترجاع بياناتهم مره أخري,هل أنت متأكد ؟</p>
                    @else
                        <p>أنت علي وشك أن تحذف هذه الدولة و ما يتبعها من مدراء و مدن و تجار و صفقات و مستخدمين و لن تستطيع إسترجاع بياناتهم مره أخري,هل أنت متأكد ؟</p>
                    @endif
                </div>
                <div class="mb-footer buttons">
                    <form method="post" action="/super_admin/address/delete" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id_destroy" value="">
                        <button class="btn btn-danger btn-lg pull-right">حذف</button>
                    </form>
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->
    <script>
        function modal_activate($id)
        {
            $('#id_activate').val($id);
        }


        function modal_suspend($id)
        {
            $('#id_suspend').val($id);
        }


        function modal_destroy($id)
        {
            $('#id_destroy').val($id);
        }
    </script>
@endsection
