@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li> <a href="/super_admin/categories/all">الأقسام</a></li>
        <li class="active">{{isset($category) ? 'تعديل قسم' : 'أضافة قسم'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    {{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($category) ? '/super_admin/category/update' : '/super_admin/category/store'}}">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($category) ? 'تعديل قسم' : 'أضافة قسم'}}
                            </h3>
                        </div>
                        <div class="panel-body">

                            @if(isset($parents))
                                <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">القسم الرئيسي</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-cube"></span></span>
                                            <select name="parent_id" class="form-control select" required>
                                                <option selected disabled>الرجاء إختيار قسم رئيسي</option>
                                                @foreach($parents as $parent)
                                                    <option value="{{$parent->id}}" @if(isset($category) && $category->parent_id == $parent->id) selected @else {{old('parent_id')}} @endif>{{$parent->ar_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @include('super_admin.layouts.error', ['input' => 'parent_id'])
                                    </div>
                                </div>
                            @endif
                            <div class="form-group {{ $errors->has('ar_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالعربية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cube"></span></span>
                                        <input type="text" class="form-control" name="ar_name" @if(isset($category)) value="{{$category->ar_name}}" @else {{old('ar_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'ar_name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم بالإنجليزية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-cube"></span></span>
                                        <input type="text" class="form-control" name="en_name" @if(isset($category)) value="{{$category->en_name}}" @else {{old('en_name')}} @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'en_name'])
                                </div>
                            </div>


                        @if(isset($category))
                                <input type="hidden" name="category_id" value="{{$category->id}}">
                            @endif
                        </div>

                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                {{isset($category) ? 'تعديل' : 'إنشاء'}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
