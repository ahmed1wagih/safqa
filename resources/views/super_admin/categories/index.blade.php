@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>

        @if(Request::is('super_admin/categories/all'))
            <li class="active">كل الأقسام</li>
        @else
            <li class="active"><a href="/super_admin/categories/all">كل الأقسام</a></li>
            <li class="active"><a href="/super_admin/categories/{{$parent}}">{{\App\Models\Category::get_cat($parent)->ar_name}}</a></li>
        @endif
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            @include('super_admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading" style="direction: rtl;">
                        <a href="/super_admin/category/main/create"><button type="button" class="btn btn-info"> أضف قسم رئيسي جديد </button></a>
                        <a href="/super_admin/category/sub/create"><button type="button" class="btn btn-info"> أضف قسم فرعي جديد </button></a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>الإسم بالعربية</th>
                                    <th>الإسم بالإنجليزية</th>
                                    @if(Request::is('super_admin/categories/all'))
                                    <th>عدد الأقسام الفرعية</th>
                                    @endif
                                    <th>المزيد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->ar_name}}</td>
                                        <td>{{$category->en_name}}</td>
                                        @if(Request::is('super_admin/categories/all'))
                                            <td>
                                                {{$category->sub_cats->count()}}
                                            </td>
                                        @endif
                                        <td>
                                        @if($category->parent_id == NULL)
                                            <a title="مشاهدة الأقسام الفرعية" href="/super_admin/categories/{{$category->id}}"><button class="btn btn-success btn-condensed"><i class="fa fa-eye"></i></button></a>
                                        @endif
                                            <a title="Edit" href="/super_admin/category/{{$category->id}}/edit"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$category->id}}" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <!-- danger with sound -->
                                    <div class="message-box message-box-warning animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$category->id}}">
                                        <div class="mb-container">
                                            <div class="mb-middle warning-msg alert-msg">
                                                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                                                <div class="mb-content">
                                                    <p>أنت علي وشك أن تحذف قسم,و سيتم حذف جميع البيانات المندرجة أسفله,مثل المنتجات و العروض,و لن تستطيع إستعادتها مرة أخري .</p>
                                                    <br/>
                                                    <p>هل انت متأكد ؟</p>
                                                </div>
                                                <div class="mb-footer buttons">
                                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                                                    <form method="post" action="/super_admin/category/delete" class="buttons">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="address_id" value="{{$category->id}}">
                                                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end danger with sound -->
                                @endforeach
                                </tbody>
                            </table>
                            {{$categories->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
