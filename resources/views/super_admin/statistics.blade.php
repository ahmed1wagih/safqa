@extends('admin.layouts.app')
@section('content')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap" style="margin-top: 10px;">
        <!-- START WIDGETS -->
        <div class="row">
            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-cube"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$deals->count()}}</div>
                        <div class="widget-title">كل الصفقات</div>
                        <div class="widget-subtitle">الخاصة بدولتك في الموقع</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->

            </div>
            <div class="col-md-3">

                <!-- START WIDGET REGISTRED -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-barcode"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$all_codes->count()}}</div>
                        <div class="widget-title">كل أكواد الصفقات</div>
                        <div class="widget-subtitle">{{$all_codes->where('user_id','!=',NULL)->count()}} تم شراؤها </div>

                    </div>

                </div>
                <!-- END WIDGET REGISTRED -->
            </div>


            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-user-secret"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$users->where('type','admin')->count()}}</div>
                        <div class="widget-title">عدد المدراء</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$users->where('type','delegate')->count()}}</div>
                        <div class="widget-title">عدد المناديب</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-users"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$users->where('type','user')->count()}}</div>
                        <div class="widget-title">عدد المستخدمين</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-building"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$merchants}}</div>
                        <div class="widget-title">عدد التجار</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-money"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$transfers->count()}}</div>
                        <div class="widget-title">عدد التحويلات المقبولة</div>
                        <div class="widget-title">
                            {{$transfers->where('status','awaiting')->count()}}
                            غير مقبولة بعد
                        </div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>

            <div class="col-md-3">
                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left" style="margin-left: 10px;">
                        <span class="fa fa-money"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 10px;">
                        <div class="widget-int num-count">{{$transfers->where('status','approved')->pluck('amount')->sum()}}</div>
                        <div class="widget-title">مجموع مبالغ التحويلات المقبولة</div>
                    </div>

                </div>
                <!-- END WIDGET MESSAGES -->
            </div>
        </div>
        <!-- END WIDGETS -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
@endsection
