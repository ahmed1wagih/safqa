@extends('super_admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/super_admin/dashboard">الرئيسية</a></li>
        <li><a href="/super_admin/users/active">المدراء</a></li>
        <li class="active">{{isset($user) ? 'تعديل مدير عام' : 'إنشاء مدير عام'}}</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="{{isset($user) ? '/super_admin/user/update' : '/super_admin/user/store'}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                {{isset($user) ?  'تعديل مدير عام' : 'إنشاء مدير عام'}}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-super"></span></span>
                                        <select class="form-control select" name="country_id" required>
                                            <option selected disabled>إختر من التالي</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}" @if(isset($user) && $user->country_id == $country->id) selected @elseif(!isset($user) && old('country_id') == $country->id) selected @endif>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'name'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-super"></span></span>
                                        <input type="text" class="form-control" name="name" required @if(isset($user)) value="{{$user->name}}" @else  value="{{old('name')}}" @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'name'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope-o"></span></span>
                                        <input type="text" class="form-control" name="email" required @if(isset($user)) value="{{$user->email}}" @else  value="{{old('email')}}" @endif/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'email'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <div id="field">
                                            <input type="text" class="form-control phone" name="phone" required @if(isset($user)) value="{{$user->phone}}" @else  value="{{old('phone')}}" @endif/>
                                        </div>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'phone'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الصورة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <input type="file" class="fileinput btn-info" name="image" id="cp_photo" data-filename-placement="inside" title="Select image"/>
                                    </div>
                                    @include('super_admin.layouts.error', ['input' => 'image'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password"/>
                                    </div>
                                    @if(isset($user))
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    @endif
                                    @include('super_admin.layouts.error', ['input' => 'password'])
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">تأكيد كلمة المرور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                        <input type="password" class="form-control" name="password_confirmation"/>
                                    </div>
                                    @if(isset($user))
                                        <span class="label label-warning" style="padding: 2px;"> إتركه فارغاً إذا لم يكن هناك تعديل </span>
                                    @endif
                                    @include('super_admin.layouts.error', ['input' => 'password_confirmation'])
                                </div>
                            </div>

                            @if(isset($user))
                                <input type="hidden" name="id" value="{{$user->id}}">
                            @endif
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary pull-right">
                                {{isset($user) ? 'تعديل' : 'إضافة'}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
