@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    @php
        if($user->active == 1)
        {
            $state = 'active';
        }
        elseif($user->active == 0)
        {
            $state = 'suspended';
        }
    @endphp

    @if($errors->has('password') || $errors->has('password_confirmation'))
        <script>
            $(window).load(function() {
                $('#modal_change_password').modal('show');
            });
        </script>
    @endif
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li> <a href="/admin/users/{{$state}}"> المستخدمين </a></li>
        <li class="active">مشاهدة</li>
    </ul>
    <!-- END BREADCRUMB -->
    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-eye"></span> مشاهدة البيانات </h2>
    </div>
    <!-- END PAGE TITLE -->
    @include('admin.layouts.message')
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-5">

                <form action="#" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>
                            <span
                            @if($user->type == 'admin') class="fa fa-user-secret" @elseif($user->type == 'delegate') class="fa fa-id-card" @else class="fa fa-user" @endif
                            >
                                </span> {{$user->name}} </h3>
                            <p>
                                @if($user->active == 1)
                                    <span class="label label-success label-form">
                                        @if($user->type == 'admin') مدير @elseif($user->type == 'delegate') مندوب @else مستخدم @endif
                                        فعال
                                    </span>
                                @elseif($user->active == 0)
                                    <span class="label label-primary label-form">
                                         @if($user->type == 'admin') مدير @elseif($user->type == 'delegate') مندوب @else مستخدم @endif
                                        موقوف </span>
                                @endif
                            </p>
                            <div class="text-center" id="user_image">
                                <img src="/users/{{$user->image}}" class="img-thumbnail"/>
                            </div>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">#ID</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"> {{$user->id}} </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">البريد الإلكتروني</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"> {{$user->email}} </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">رقم الهاتف</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"> {{$user->phone}} </span><br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-6 col-sm-8 col-xs-7">

                <form action="#" class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-pencil"></span> البيانات </h3>
                        </div>
                        <div class="panel-body form-group-separated">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الإسم</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"> {{$user->name}} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">المدينة</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control"> {{$user->address->ar_name}} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-5 control-label">الجنس</label>
                                <div class="col-md-9 col-xs-7">
                                    <span class="form-control">@if($user->gender == 1) ذكر @else أنثي @endif</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="panel panel-default tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">إرسال بريد إلكتروني</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane panel-body active" id="tab1">
                            <div class="form-group">
                                <label>العنوان</label>
                                <input type="email" class="form-control" placeholder="العنوان">
                            </div>
                            <div class="form-group">
                                <label>المحتوي</label>
                                <textarea class="form-control" placeholder="المحتوي" rows="3"></textarea>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-md-3">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <h3><span class="fa fa-info-circle"></span> معلومات سريعة </h3>
                    </div>
                    <div class="panel-body form-group-separated">
                        <div class="form-group">
                            <label class="col-md-4 col-xs-5 control-label">تاريخ التسجيل</label>
                            <div class="col-md-8 col-xs-7 line-height-30">{{$user->created_at}}</div>
                        </div>
                        @if($user->type == 'user')
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">عمليات الشراء</label>
                                <div class="col-md-8 col-xs-7 line-height-30">{{ $user->orders->count() }}</div>
                            </div>
                        @else
                            <div class="form-group">
                                <label class="col-md-4 col-xs-5 control-label">المنتجات</label>
                                <div class="col-md-8 col-xs-7 line-height-30">{{ $user->products->count() }}</div>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3><span class="fa fa-cog"></span> الإعدادات </h3>
                    </div>
                    <div class="panel-body form-horizontal form-group-separated">

                        @if($user->active == 1)
                            <div class="form-group">
                                <label class="col-md-6 col-xs-6 control-label">توقيف</label>
                                <div class="col-md-6 col-xs-6">
                                    <button class="btn btn-primary btn mb-control" data-box="#message-box-suspend-{{$user->id}}" title="توقف"><i class="fa fa-minus-square"></i></button>
                                </div>
                            </div>

                        @elseif($user->active == 0)
                            <div class="form-group">
                                <label class="col-md-6 col-xs-6 control-label">تفعيل</label>
                                <div class="col-md-6 col-xs-6">
                                    <button class="btn btn-success btn mb-control" data-box="#message-box-activate-{{$user->id}}" title="تفعيل"><i class="fa fa-plus-square"></i></button>
                                </div>
                            </div>
                        @endif
                            <div class="form-group">
                                <label class="col-md-6 col-xs-6 control-label">حذف</label>
                                <div class="col-md-6 col-xs-6">
                                    <button class="btn btn-danger btn mb-control" data-box="#message-box-warning-{{$user->id}}" title="حذف"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- activate with sound -->
    <div class="message-box message-box-success animated fadeIn" data-sound="alert/fail" id="message-box-activate-{{$user->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن تفعل مستخدم,سيتمكن من تسجيل الدخول من الآن .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/change_state" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <input type="hidden" name="state" value="1">
                        <button type="submit" class="btn btn-success btn-lg pull-right">تفعيل</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end activate with sound -->

    <!-- suspend with sound -->
    <div class="message-box message-box-primary animated fadeIn" data-sound="alert/fail" id="message-box-suspend-{{$user->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن توقف مستخدم,لن يستطيع تسجل الدخول من الآن .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/change_state" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <input type="hidden" name="state" value="0">
                        <button type="submit" class="btn btn-primary btn-lg pull-right">توقيف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end suspend with sound -->

    <!-- danger with sound -->
    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$user->id}}">
        <div class="mb-container">
            <div class="mb-middle warning-msg alert-msg">
                <div class="mb-title"><span class="fa fa-times"></span>تحذير !</div>
                <div class="mb-content">
                    <p>أنت علي وشك أن تحذف مستخدم,لن تستطيع أن تستعيد بياناته مرة أخري .</p>
                    <br/>
                    <p>هل أنت متأكد ؟</p>
                </div>
                <div class="mb-footer buttons">
                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-left: 5px;">إغلاق</button>
                    <form method="post" action="/admin/user/delete" class="buttons">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <button type="submit" class="btn btn-danger btn-lg pull-right">حذف</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end danger with sound -->

    <!-- END PAGE CONTENT WRAPPER -->
@endsection
