<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Middleware\IsSuper;
use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsMerchant;
use App\Http\Middleware\IsUser;


Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'Web\LanguageController@switchLang']);

Route::get('/register', 'Web\AuthController@register_view');
Route::post('/register', 'Web\AuthController@register');
Route::get('/get_cities/{parent_id}', 'Web\AuthController@get_cities');

Route::get('/login', 'Web\AuthController@login_view')->name('login');
Route::post('/login', 'Web\AuthController@login');
Route::get('/activate', 'Web\AuthController@activate_view');
Route::post('/activate', 'Web\AuthController@activate');

Route::get('/password_reset', 'Web\AuthController@password_reset');
Route::post('/password_reset/send', 'Web\AuthController@password_reset_send');
Route::post('/password_reset/check', 'Web\AuthController@password_reset_check');
Route::post('/password_reset/change', 'Web\AuthController@password_reset_change');

Route::get('/', 'Web\HomeController@index');
Route::get('/change_country/{id}', 'Web\HomeController@change_country');
Route::get('/change_city/{id}', 'Web\HomeController@change_city');
Route::get('/latest', 'Web\HomeController@get_latest_products');
Route::get('/limited', 'Web\HomeController@get_limited_products');
Route::get('/category/{parent_id}/{filter}', 'Web\HomeController@get_cat_products');
Route::get('/product/{id}', 'Web\ProductController@show');
Route::get('/search', 'Web\HomeController@search');
Route::post('/subscribe', 'Web\HomeController@subscribe');

Route::get('/terms', 'Web\HomeController@terms');
Route::get('/partners', 'Web\HomeController@partners');
Route::get('/terms/merchants', 'Web\HomeController@terms_merchants');
Route::get('/privacy', 'Web\HomeController@privacy');
Route::get('/faqs', 'Web\HomeController@faqs');
Route::get('/submit_deal', 'Web\HomeController@submit_deal');
Route::get('/contact_us', 'Web\HomeController@contact_us');
Route::post('/suggest', 'Web\HomeController@suggest');

//Route::get('/merchant/check', 'Web\MerchantController@merchant_check');
//Route::post('/merchant/report', 'Web\MerchantController@merchant_report');
Route::get('/merchant/apply', 'Web\MerchantController@apply_view');
Route::post('/merchant/apply', 'Web\MerchantController@apply');

Route::get('/payment_methods', 'Web\HomeController@payment_view');

Route::get('browse/{country_code}/{city}', 'Web\HomeController@browse');

Route::post('/payment/result', 'Web\PayTabsController@fallback_result');

Route::group(['middleware' => IsUser::class], function ()
{
    Route::get('/profile','Web\UserController@profile');
    Route::get('/profile/edit','Web\UserController@edit');
    Route::get('/my_deals','Web\UserController@my_deals');
    Route::get('/my_transfers','Web\UserController@my_transfers');
    Route::get('/deal/{id}/info','Web\UserController@deal_info');
    Route::post('/profile/update', 'Web\UserController@update');
    Route::get('/wish_list', 'Web\UserController@wish_list');
    Route::get('/wish_store/{id}', 'Web\UserController@wish_store');
    Route::get('/wish_delete/{id}', 'Web\UserController@wish_delete');

    Route::get('/charge_credit', 'Web\UserController@charge_credit');
    Route::post('/charge', 'Web\PaymentController@charge');

    Route::post('/rate', 'Web\UserController@rate');

    Route::get('/cart', 'Web\CartController@index');
    Route::get('/cart/store/{id}', 'Web\CartController@store');
    Route::get('/cart/delete/{id}', 'Web\CartController@destroy');

    Route::get('/checkout/payment', 'Web\PaymentController@payment');
    Route::post('/credit_checkout', 'Web\PaymentController@credit_checkout');

    Route::post('/paytabs/pay', 'Web\PayTabsController@pay');
    Route::get('/currencies_values/{currency}', 'Web\PayTabsController@get_values');
//    Route::get('/payment/result/{trans_id}', 'Web\PayTabsController@fallback');
});

Route::post('/banner/click','Web\HomeController@banner_click');


Route::get('/logout', 'Web\HomeController@logout');


Route::group(['prefix' => '/super_admin'], function() {

    Route::get('/login', 'SuperAdmin\AuthController@login_view');
    Route::post('/login', 'SuperAdmin\AuthController@login');

    Route::group(['middleware' => ['web', IsSuper::class]], function () {

        Route::get('/dashboard', 'SuperAdmin\HomeController@dashboard');
        Route::get('/profile', 'SuperAdmin\AuthController@show');
        Route::post('/profile/update', 'SuperAdmin\AuthController@update');
        Route::get('/logout', 'SuperAdmin\AuthController@logout');
//        Route::get('/statistics/{country_id}', 'SuperAdmin\HomeController@statistics');

        Route::get('/month_deals_graph','SuperAdmin\HomeController@month_deals_graph');
        Route::get('/merchant_deals','SuperAdmin\HomeController@merchant_deals');
        Route::get('/category_deals','SuperAdmin\HomeController@category_deals');

        Route::get('/addresses/{parent}', 'SuperAdmin\AddressController@index');
        Route::get('/address/country/create', 'SuperAdmin\AddressController@country_create');
        Route::get('/address/city/create', 'SuperAdmin\AddressController@city_create');
        Route::post('/address/store', 'SuperAdmin\AddressController@store');
        Route::get('/address/{id}/edit', 'SuperAdmin\AddressController@edit');
        Route::post('/address/update', 'SuperAdmin\AddressController@update');
        Route::post('/address/change_status', 'SuperAdmin\AddressController@change_status');
        Route::post('/address/delete', 'SuperAdmin\AddressController@destroy');
        Route::get('/addresses/{parent}/bank_accounts', 'SuperAdmin\AddressController@bank_accounts_index');
        Route::get('/bank_account/create', 'SuperAdmin\AddressController@bank_account_create');
        Route::post('/bank_account/store', 'SuperAdmin\AddressController@bank_account_store');
        Route::get('/addresses/bank_account/{id}/edit', 'SuperAdmin\AddressController@bank_account_edit');
        Route::post('/bank_account/update', 'SuperAdmin\AddressController@bank_account_update');
        Route::post('/bank_account/delete', 'SuperAdmin\AddressController@bank_account_destroy');

        Route::get('/get_cities/{parent}', 'SuperAdmin\AddressController@get_cities');

        Route::get('/categories/{parent}', 'SuperAdmin\CategoryController@index');
        Route::get('/category/main/create', 'SuperAdmin\CategoryController@main_create');
        Route::get('/category/sub/create', 'SuperAdmin\CategoryController@sub_create');
        Route::post('/category/store', 'SuperAdmin\CategoryController@store');
        Route::get('/category/{id}/edit', 'SuperAdmin\CategoryController@edit');
        Route::post('/category/update', 'SuperAdmin\CategoryController@update');
        Route::post('/category/delete', 'SuperAdmin\CategoryController@destroy');

        Route::get('/users/{status}', 'SuperAdmin\UserController@index');
        Route::get('/user/create', 'SuperAdmin\UserController@create');
        Route::post('/user/store', 'SuperAdmin\UserController@store');
        Route::get('/user/{id}/edit', 'SuperAdmin\UserController@edit');
        Route::post('/user/update', 'SuperAdmin\UserController@update');
        Route::post('/user/change_status', 'SuperAdmin\UserController@change_status');
        Route::post('/user/delete', 'SuperAdmin\UserController@destroy');


        Route::group(['prefix' => '/settings'], function(){

            Route::get('/abouts', 'SuperAdmin\AboutController@index');
            Route::get('/abouts/edit', 'SuperAdmin\AboutController@edit');
            Route::post('/abouts/update', 'SuperAdmin\AboutController@update');

            Route::get('/terms', 'SuperAdmin\TermsController@index');
            Route::get('/terms/edit', 'SuperAdmin\TermsController@edit');
            Route::post('/terms/update', 'SuperAdmin\TermsController@update');

            Route::get('/partners', 'SuperAdmin\PartnerController@index');
            Route::get('/partners/edit', 'SuperAdmin\PartnerController@edit');
            Route::post('/partners/update', 'SuperAdmin\PartnerController@update');

            Route::get('/merchant_terms', 'SuperAdmin\MerchantTermController@index');
            Route::get('/merchant_terms/edit', 'SuperAdmin\MerchantTermController@edit');
            Route::post('/merchant_terms/update', 'SuperAdmin\MerchantTermController@update');

            Route::get('/submit', 'SuperAdmin\SubmitController@index');
            Route::get('/submit/edit', 'SuperAdmin\SubmitController@edit');
            Route::post('/submit/update', 'SuperAdmin\SubmitController@update');

            Route::get('/socials', 'SuperAdmin\SocialController@index');
            Route::get('/socials/edit', 'SuperAdmin\SocialController@edit');
            Route::post('/socials/update', 'SuperAdmin\SocialController@update');

            Route::get('/privacy', 'SuperAdmin\PrivacyController@index');
            Route::get('/privacy/edit', 'SuperAdmin\PrivacyController@edit');
            Route::post('/privacy/update', 'SuperAdmin\PrivacyController@update');

            Route::get('/faqs', 'SuperAdmin\FaqController@index');
            Route::get('/faq/create', 'SuperAdmin\FaqController@create');
            Route::post('/faq/store', 'SuperAdmin\FaqController@store');
            Route::get('/faq/{id}/edit', 'SuperAdmin\FaqController@edit');
            Route::post('/faq/update', 'SuperAdmin\FaqController@update');
            Route::post('/faq/delete', 'SuperAdmin\FaqController@destroy');

            Route::get('/settings/edit', 'SuperAdmin\SettingController@edit');
            Route::post('/settings/update', 'SuperAdmin\SettingController@update');
//
//            Route::get('/exchanges/edit', 'SuperAdmin\CurrencyExchangeController@edit');
//            Route::post('/exchanges/update', 'SuperAdmin\CurrencyExchangeController@update');
        });

        Route::get('/suggests', 'SuperAdmin\SuggestController@index');
        Route::post('/suggest/delete', 'SuperAdmin\SuggestController@destroy');

        Route::get('/mail_list', 'SuperAdmin\MailController@index');
        Route::get('/mail/create', 'SuperAdmin\MailController@create');
        Route::post('/mail/store', 'SuperAdmin\MailController@store');
        Route::post('/mail/delete', 'SuperAdmin\MailController@destroy');
    });
});


Route::group(['prefix' => '/admin'], function(){

    Route::get('/login', 'Admin\AuthController@login_view');
    Route::post('/login', 'Admin\AuthController@login');

//    Route::get('/activate', 'Admin\AuthController@activate_view');
//    Route::post('/activate', 'Admin\AuthController@activate');


    Route::group(['middleware' => IsAdmin::class], function () {

        Route::get('/dashboard', 'Admin\HomeController@dashboard');
        Route::get('/profile', 'Admin\AuthController@show');
        Route::post('/profile/update', 'Admin\AuthController@update');
        Route::get('/logout', 'Admin\AuthController@logout');

        Route::get('/month_deals_graph','Admin\HomeController@month_deals_graph');
        Route::get('/merchant_deals','Admin\HomeController@merchant_deals');
        Route::get('/category_deals','Admin\HomeController@category_deals');

        Route::get('/products/{cat_id}/{state}', 'Admin\ProductController@index');
        Route::post('/product/change_status', 'Admin\ProductController@change_status');

        Route::get('/admins/{status}', 'Admin\AdminController@index');
        Route::get('/admin/create', 'Admin\AdminController@create');
        Route::post('/admin/store', 'Admin\AdminController@store');
        Route::get('/admin/{id}/edit', 'Admin\AdminController@edit');
        Route::post('/admin/update', 'Admin\AdminController@update');
        Route::post('/admin/change_status', 'Admin\AdminController@change_status');
        Route::post('/admin/delete', 'Admin\AdminController@destroy');

        Route::get('/users/{status}', 'Admin\UserController@index');
        Route::get('/user/create', 'Admin\UserController@create');
        Route::post('/user/store', 'Admin\UserController@store');
        Route::get('/user/{id}/edit', 'Admin\UserController@edit');
        Route::post('/user/update', 'Admin\UserController@update');
        Route::post('/user/change_status', 'Admin\UserController@change_status');
        Route::post('/user/charge', 'Admin\UserController@charge');
        Route::post('/user/delete', 'Admin\UserController@destroy');

        Route::get('/merchants/{status}', 'Admin\MerchantController@index');
        Route::get('/merchant/create', 'Admin\MerchantController@create');
        Route::post('/merchant/store', 'Admin\MerchantController@store');
        Route::get('/merchant/{id}/edit', 'Admin\MerchantController@edit');
        Route::post('/merchant/update', 'Admin\MerchantController@update');
        Route::post('/merchant/change_state', 'Admin\MerchantController@change_state');
        Route::post('/merchant/delete', 'Admin\MerchantController@destroy');

        Route::get('/products/{status}', 'Admin\ProductController@index');
        Route::get('/product/{id}/view', 'Admin\ProductController@show');
        Route::get('/product/{id}/deals_export', 'Admin\ProductController@deals_export');
        Route::get('/product/{id}/specialize', 'Admin\ProductController@specialize');
        Route::post('/product/change_state', 'Admin\ProductController@change_state');
        Route::post('/product/delete', 'Admin\ProductController@destroy');

        Route::get('/packs/{status}', 'Admin\PackController@index');
        Route::get('/pack/create', 'Admin\PackController@create');
        Route::post('/pack/store', 'Admin\PackController@store');
        Route::get('/pack/{id}/edit', 'Admin\PackController@edit');
        Route::post('/pack/update', 'Admin\PackController@update');
        Route::post('/pack/change_status', 'Admin\PackController@change_status');
        Route::post('/pack/delete', 'Admin\PackController@destroy');

        Route::get('/transfers/{status}', 'Admin\TransferController@index');
        Route::get('/transfer/{id}/view', 'Admin\TransferController@show');
        Route::post('/transfer/change_status', 'Admin\TransferController@change_status');


        Route::get('/online_transfers/{status}', 'Admin\OnlineTransferController@index');
        Route::get('/online_transfer/{id}/view', 'Admin\OnlineTransferController@show');

        Route::get('/withdraws/{status}', 'Admin\WithdrawController@index');
        Route::post('/withdraw/change_status', 'Admin\WithdrawController@change_status');

        Route::get('/banners/index', 'Admin\BannerController@index');
        Route::get('/banner/create', 'Admin\BannerController@create');
        Route::post('/banner/store', 'Admin\BannerController@store');
        Route::get('/banner/{id}/edit', 'Admin\BannerController@edit');
        Route::post('/banner/update', 'Admin\BannerController@update');
        Route::post('/banner/delete', 'Admin\BannerController@destroy');

        Route::get('/sides/index', 'Admin\SideController@index');
        Route::get('/side/create', 'Admin\SideController@create');
        Route::post('/side/store', 'Admin\SideController@store');
        Route::get('/side/{id}/edit', 'Admin\SideController@edit');
        Route::post('/side/update', 'Admin\SideController@update');
        Route::post('/side/delete', 'Admin\SideController@destroy');

    });
});


Route::group(['prefix' => '/merchant'], function(){

    Route::get('/login', 'Merchant\AuthController@login_view');
    Route::post('/login', 'Merchant\AuthController@login');

    Route::group(['middleware' => IsMerchant::class], function () {

        Route::get('/dashboard', 'Merchant\HomeController@dashboard');
        Route::get('/profile', 'Merchant\AuthController@profile');
        Route::post('/profile/update', 'Merchant\AuthController@update');

        Route::get('/merchant_deals','Merchant\HomeController@merchant_deals');
        Route::get('/month_deals_graph','Merchant\HomeController@month_deals_graph');

        Route::get('/products/{status}', 'Merchant\ProductController@index');
        Route::get('/product/create', 'Merchant\ProductController@create');
        Route::post('/product/store', 'Merchant\ProductController@store');
        Route::get('/product/{id}/edit', 'Merchant\ProductController@edit');
        Route::post('/product/update', 'Merchant\ProductController@update');
        Route::get('/product/{id}/deals_export', 'Merchant\ProductController@deals_export');
        Route::Get('/get_sub_cats/{parent_id}', 'Merchant\HomeController@get_sub_cats');

        Route::get('/reps/{status}', 'Merchant\RepController@index');
        Route::get('/rep/create', 'Merchant\RepController@create');
        Route::post('/rep/store', 'Merchant\RepController@store');
        Route::get('/rep/{id}/edit', 'Merchant\RepController@edit');
        Route::post('/rep/update', 'Merchant\RepController@update');
        Route::post('/rep/change_status', 'Merchant\RepController@change_status');
        Route::post('/rep/delete', 'Merchant\RepController@destroy');

        Route::get('/deals/index','Merchant\DealController@index');
        Route::get('/deal/check','Merchant\DealController@check');

        Route::get('/logout', 'Merchant\AuthController@logout');
    });
});

Route::get('/crone/codes_delete', 'Web\CroneController@codes_delete');
