<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/get_addresses/{lang}', 'Api\HomeController@get_addresses');
Route::get('/index/{lang}/{address_id}/{user_id}', 'Api\HomeController@index');
Route::get('/category/{id}/products/{lang}/{address_id}/{user_id}', 'Api\HomeController@get_category_products');
Route::get('/latest/{lang}/{address_id}/{user_id}', 'Api\HomeController@get_latest_products');
Route::get('/search/{lang}/{address_id}/{user_id}', 'Api\HomeController@search');
Route::get('/product/{id}/show/{lang}/{user_id}', 'Api\ProductController@show');
Route::post('/product/rate', 'Api\ProductController@add_rate');
Route::get('/favorites/{lang}/{user_id}', 'Api\ProductController@favorites');
Route::post('/product/favorite', 'Api\ProductController@add_favorite');

Route::post('/cart/show/{lang}', 'Api\CartController@show');
Route::post('/cart/store', 'Api\CartController@store');
Route::post('/cart/remove', 'Api\CartController@remove');
Route::post('/cart/checkout', 'Api\CartController@checkout');

Route::get('/bank_accounts/{lang}/{country_id}', 'Api\TransferController@get_banks');
Route::post('/my_transfers/{lang}', 'Api\TransferController@my_transfers');
Route::post('/transfer', 'Api\TransferController@store');

Route::get('/packs/get', 'Api\PayTabsController@get_packs');
Route::get('/paytabs/get_currencies/{lang}', 'Api\PayTabsController@get_currencies');
Route::post('/paytabs/pay', 'Api\PayTabsController@pay');
Route::post('/paytaps/set_reference', 'Api\PayTabsController@set_reference');

Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
Route::post('/code/send', 'Api\AuthController@code_send');
Route::post('/code/check', 'Api\AuthController@code_check');
Route::post('/password/reset', 'Api\AuthController@reset_password');
Route::post('/profile/view/{lang}', 'Api\AuthController@profile_show');
Route::post('/profile/update', 'Api\AuthController@profile_update');

Route::post('/profile/my_deals/{lang}', 'Api\AuthController@my_deals');
Route::post('/profile/deal/view/{lang}', 'Api\AuthController@deal_view');
Route::post('/profile/my_credit', 'Api\AuthController@my_credit');
Route::post('/profile/contact', 'Api\AuthController@contact');

Route::get('/merchants/{lang}', 'Api\HomeController@merchants');
Route::get('/merchant/deals/{lang}/{merchant_id}/{user_id}', 'Api\HomeController@merchant_deals');

Route::get('/about_us/{lang}', 'Api\HomeController@get_about');
Route::get('/terms/{lang}', 'Api\HomeController@get_terms');
Route::get('/faqs/{lang}', 'Api\HomeController@get_faqs');
Route::get('/privacy/{lang}', 'Api\HomeController@get_privacy');
Route::get('/policy', 'Api\HomeController@get_policy');


Route::prefix('/cashier')->group( function ()
{
    Route::post('/login', 'Api\RepController@login');
    Route::post('/code_check/{lang}', 'Api\RepController@code_check');
    Route::post('/code_checkout', 'Api\RepController@code_checkout');
    Route::post('/previous_scans/{lang}', 'Api\RepController@get_previous_scans');
});


Route::get('/msg/test', 'Api\AuthController@msg');



