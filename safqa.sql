-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 28, 2018 at 10:18 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `safqa`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_uses`
--

DROP TABLE IF EXISTS `about_uses`;
CREATE TABLE IF NOT EXISTS `about_uses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_address` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `en_address` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `ar_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `parent_id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, NULL, 'السعودية', 'Saudi Arabia', '2018-10-23 20:35:57', '2018-10-23 20:35:57'),
(2, 1, 'الرباض', 'Riyadh', '2018-10-23 20:38:16', '2018-10-23 20:38:16'),
(5, 1, 'جدة', 'Jeddah', '2018-10-23 20:39:12', '2018-10-23 20:39:12'),
(6, 1, 'الدمام', 'Dammam', '2018-10-23 20:39:55', '2018-10-23 20:39:55'),
(7, NULL, 'مصر', 'Egypt', '2018-10-28 18:26:53', '2018-10-28 18:26:53'),
(8, 7, 'القاهرة', 'Cairo', '2018-10-28 18:28:09', '2018-10-28 18:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `ad_images`
--

DROP TABLE IF EXISTS `ad_images`;
CREATE TABLE IF NOT EXISTS `ad_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `expire_at` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 'أجهزة كهربائية', 'Electric Devices', '2018-10-23 20:51:41', '2018-10-23 20:51:41'),
(2, 'هواتف نقالة', 'Mobile Phones', '2018-10-23 20:52:16', '2018-10-23 20:52:16'),
(3, 'صحة و جمال', 'Health & Beauty', '2018-10-23 20:53:36', '2018-10-23 20:53:36'),
(4, 'هدايا و إكسسوارات', 'Gifts & Accesories', '2018-10-23 20:54:00', '2018-10-23 20:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_uses`
--

DROP TABLE IF EXISTS `contact_uses`;
CREATE TABLE IF NOT EXISTS `contact_uses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `replied` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_question` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `en_question` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `ar_answer` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `en_answer` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mail_lists`
--

DROP TABLE IF EXISTS `mail_lists`;
CREATE TABLE IF NOT EXISTS `mail_lists` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

DROP TABLE IF EXISTS `merchants`;
CREATE TABLE IF NOT EXISTS `merchants` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `ar_desc` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_desc` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `ar_name`, `en_name`, `ar_desc`, `en_desc`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'محل ألف صنف و صنف للهدايا', '100 item & item for gifts', 'وصف وصف وصف وصف', 'desc desc desc desc', 30.1232726, 31.5827073, '2018-10-27 22:00:00', '2018-10-27 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_10_21_201335_create_users_table', 1),
(2, '2018_10_21_201446_create_addresses_table', 1),
(3, '2018_10_21_201846_create_categories_table', 1),
(4, '2018_10_21_201858_create_merchants_table', 1),
(5, '2018_10_21_201916_create_products_table', 1),
(6, '2018_10_21_201927_create_product_infos_table', 1),
(7, '2018_10_21_201935_create_product_images_table', 1),
(8, '2018_10_23_220230_create_product_rates_table', 1),
(9, '2018_10_23_220329_create_product_favorites_table', 1),
(10, '2018_10_23_220355_create_carts_table', 1),
(11, '2018_10_23_220407_create_orders_table', 1),
(12, '2018_10_23_220420_create_mail_lists_table', 1),
(13, '2018_10_23_220429_create_supports_table', 1),
(14, '2018_10_23_220441_create_contact_uses_table', 1),
(15, '2018_10_23_220450_create_faqs_table', 1),
(16, '2018_10_23_220459_create_about_uses_table', 1),
(17, '2018_10_23_220508_create_socials_table', 1),
(18, '2018_10_23_220534_create_ad_images_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `delegate_id` int(11) NOT NULL,
  `ar_title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `ar_desc` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `en_desc` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `old_price` double NOT NULL,
  `new_price` double NOT NULL,
  `expire_at` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_favorites`
--

DROP TABLE IF EXISTS `product_favorites`;
CREATE TABLE IF NOT EXISTS `product_favorites` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_infos`
--

DROP TABLE IF EXISTS `product_infos`;
CREATE TABLE IF NOT EXISTS `product_infos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `serial_no` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `barcode` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_rates`
--

DROP TABLE IF EXISTS `product_rates`;
CREATE TABLE IF NOT EXISTS `product_rates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `text` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

DROP TABLE IF EXISTS `socials`;
CREATE TABLE IF NOT EXISTS `socials` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `facebook` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

DROP TABLE IF EXISTS `supports`;
CREATE TABLE IF NOT EXISTS `supports` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `sender_seen` tinyint(4) NOT NULL DEFAULT '0',
  `target_seen` tinyint(4) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8_unicode_ci,
  `image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('admin','delegate','user') COLLATE utf8_unicode_ci NOT NULL,
  `address_id` int(11) NOT NULL,
  `credit` double NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `active`, `type`, `address_id`, `credit`, `name`, `email`, `phone`, `password`, `image`, `gender`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 2, 99999, 'Admin', 'admin@admin.com', '0111111111', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', 'default.png', 'male', '2018-10-23 21:38:39', '2018-10-23 22:16:51'),
(2, 1, 'user', 2, 0, 'Ahmed Wagih', 'ahmed1wagih@gmail.caom', '0123456789', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15403379195bcfb0ff3274e-enhanced-19898-1425418851-9.jpg', 'male', '2018-10-23 21:38:39', '2018-10-23 22:16:51'),
(3, 1, 'delegate', 5, 0, 'المندوب محمد أحمد', 'mandood_moh@mandoob.com', '0132165468468', '$2y$10$8aYFdEc1yjHVo0a/bYr8ZeWT4DClMua9MkyRGp9D4gHC.yLP6/c6a', '15403450345bcfccca354c8-Amr-Diab-2.jpg', 'male', '2018-10-23 23:37:14', '2018-10-23 23:47:45'),
(4, 1, 'delegate', 6, 0, 'المندوبة شيرين عيد', 'sheren@sheren.com', '013146854684', '$2y$10$uddyF.OgeQlhu2D4H8E55.ONV8lsu/8ImspB/c96uJ7dQn9/Wiv/W', '15403450975bcfcd09bb36d-9998728238.jpg', 'female', '2018-10-23 23:38:17', '2018-10-23 23:47:48');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
