<?php

namespace App\Providers;

use App\Models\AboutUs;
use App\Models\Address;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\Social;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(! country())
        {
            $country = Address::where('status','active')->has('active_cities')->first();
            Session::put('country', $country);
            Session::put('city', Address::where('parent_id',country()->id)->where('status','active')->first());
        }

        if(! city())
        {
            $city = Address::where('parent_id',country()->id)->where('status','active')->first();
            Session::put('city', $city);
        }


        $all_countries = Address::where('parent_id',NULL)->where('status','active')->has('active_cities')->select('id',lang().'_name as name','image')->get();
        $all_cities = Address::where('parent_id',country()->id)->has('active_products')->select('id',lang().'_name as name')->get();

        $cats = Category::where('parent_id', NULL)->take(8)->get();
        $all_cats = Category::where('parent_id', NULL)->select('id','ar_name','en_name')->get();
        $abouts = AboutUs::first();
        $settings = Setting::first();
        $socials = Social::first();
        $deals = ProductInfo::where('user_id','!=',NULL)->pluck('product_id');

        View::share
        (
            [
                'all_countries' => $all_countries,
                'all_cities' => $all_cities,
                'global_cats' => $cats,
                'all_global_cats' => $all_cats,
                'abouts' => $abouts,
                'settings' => $settings,
                'socials' => $socials,
                'deals_count' => $deals->count()
            ]
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
