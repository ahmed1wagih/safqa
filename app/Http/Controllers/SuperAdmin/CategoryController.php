<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index($parent)
    {
        if($parent == 'all')
        {
            $categories = Category::where('parent_id', NULL)->paginate(50);
        }
        else
        {
            $categories = Category::where('parent_id', $parent)->paginate(50);
        }

        return view('super_admin.categories.index', compact('categories','parent'));
    }


    public function main_create()
    {
        return view('super_admin.categories.single');
    }


    public function sub_create()
    {
        $parents = Category::where('parent_id', NULL)->get();
        return view('super_admin.categories.single', compact('parents'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'parent_id' => 'sometimes|exists:categories,id',
                'ar_name' => 'required|unique:categories',
                'en_name' => 'required|unique:categories',
            ],
            [
                'parent_id.exists' => 'الرجاء إختيار القسم الرئيسي',
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالإنجليزية موجود من قبل',
            ]
        );

        $category = Category::create($request->all());

        if($request->parent_id)
        {
            return redirect('/super_admin/categories/'.$request->parent_id)->with('success', 'تمت الإضافة بنجاح');
        }
        else
        {
            return redirect('/super_admin/categories/all')->with('success', 'تمت الإضافة بنجاح');
        }


    }


    public function edit($id)
    {
        $category = Category::find($id);
        return view('super_admin.categories.single', compact('category'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'category_id' => 'required|exists:categories,id',
                'ar_name' => 'required|unique:categories,ar_name,'.$request->category_id,
                'en_name' => 'required|unique:categories,en_name,'.$request->category_id,
            ],
            [
                'ar_name.required' => 'Please enter an arabic name .',
                'ar_name.unique' => 'Arabic name already exists,please choose another one .',
                'en_name.required' => 'Please enter an english name .',
                'en_name.unique' => 'English name already exists,please choose another one .',
            ]
        );

        $category = Category::find($request->category_id);
            $category->en_name = $request->en_name;
            $category->ar_name = $request->ar_name;
        $category->save();

        return redirect('/super_admin/categories')->with('success', 'تم التعديل بنجاح');

    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'category_id' => 'required|exists:categories,id',
            ]
        );

        Category::where('id', $request->address_id)->delete();

        return back()->with('success','تم الحذف بنجاح');
    }
}
