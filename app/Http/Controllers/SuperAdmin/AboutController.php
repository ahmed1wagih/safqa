<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\AboutUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        $abouts = AboutUs::first();
        return view('super_admin.settings.abouts.index', compact('abouts'));
    }


    public function edit()
    {
        $abouts = AboutUs::first();
        return view('super_admin.settings.abouts.single', compact('abouts'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_address' => 'required',
                'en_address' => 'required',
                'ar_text' => 'required',
                'en_text' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'lat' => 'required',
                'lng' => 'required',
            ],
            [
                'ar_address.required' => 'العنوان بالعربية مطلوب',
                'en_address.required' => 'العنوان بالإنجليزية مطلوب',
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'phone.required' => 'الهاتف مطلوب',
            ]
        );

        $abouts = AboutUs::first();
            $abouts->ar_address = $request->ar_address;
            $abouts->en_address = $request->en_address;
            $abouts->ar_text = $request->ar_text;
            $abouts->en_text = $request->en_text;
            $abouts->email = $request->email;
            $abouts->phone = $request->phone;
            $abouts->lat = $request->lat;
            $abouts->lng = $request->lng;
        $abouts->save();

        return redirect('/super_admin/settings/abouts')->with('success', 'تم التعديل بنجاح');
    }
}
