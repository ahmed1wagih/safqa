<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\MerchantTerm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MerchantTermController extends Controller
{
    public function index()
    {
        $terms = MerchantTerm::first();
        return view('super_admin.settings.merchant_terms.index', compact('terms'));
    }


    public function edit()
    {
        $terms = MerchantTerm::first();
        return view('super_admin.settings.merchant_terms.single', compact('terms'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_text' => 'required',
                'en_text' => 'required',
            ],
            [
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
            ]
        );

        $terms = MerchantTerm::first();
            $terms->ar_text = $request->ar_text;
            $terms->en_text = $request->en_text;
        $terms->save();

        return redirect('/super_admin/settings/merchant_terms')->with('success', 'تم التعديل بنجاح');
    }
}
