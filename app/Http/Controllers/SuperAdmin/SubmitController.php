<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\SubmitDeal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmitController extends Controller
{
    public function index()
    {
        $submit = SubmitDeal::first();
        return view('super_admin.settings.submit_deals.index', compact('submit'));
    }


    public function edit()
    {
        $submit = SubmitDeal::first();
        return view('super_admin.settings.submit_deals.single', compact('submit'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_text' => 'required',
                'en_text' => 'required',
            ],
            [
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
            ]
        );

        $submit = SubmitDeal::first();
            $submit->ar_text = $request->ar_text;
            $submit->en_text = $request->en_text;
        $submit->save();

        return redirect('/super_admin/settings/submit')->with('success', 'تم التعديل بنجاح');
    }

}
