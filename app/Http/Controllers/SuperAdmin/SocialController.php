<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialController extends Controller
{
    public function index()
    {
        $socials = Social::first();
        return view('super_admin.settings.socials.index', compact('socials'));
    }


    public function edit()
    {
        $socials = Social::first();
        return view('super_admin.settings.socials.single', compact('socials'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'facebook' => 'required',
                'twitter' => 'required',
                'instagram' => 'required'
            ],
            [
                'facebook.required' => 'عفواً,حساب الفيسبوك مطلوب',
                'twitter.required' => 'عفواً,حساب التويتر مطلوب',
                'instagram.required' => 'عفواً,حساب الإنستا مطلوب',
            ]
        );

        $socials = Social::first();
                $socials->facebook = $request->facebook;
                $socials->twitter = $request->twitter;
                $socials->instagram = $request->instagram;
        $socials->save();


        return redirect('/super_admin/settings/socials')->with('success', 'تم التعديل بنجاح');
    }
}
