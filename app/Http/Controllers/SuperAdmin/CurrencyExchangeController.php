<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Address;
use App\Models\CurrencyExchange;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyExchangeController extends Controller
{
    public function edit()
    {
        $countries = Address::where('parent_id', NULL)->get();
        return view('super_admin.settings.exchanges.single', compact('countries'));
    }


    public function update(Request $request)
    {
        $countries = Address::where('parent_id', NULL)->pluck('id');

        foreach($countries as $country_id)
        {
            $exchange = CurrencyExchange::where('country_id', $country_id)->first();
                $exchange->point_per_currency = $request->$country_id;
            $exchange->save();
        }

        return back()->with('success', 'تم التعديل بنجاح');
    }
}
