<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function edit()
    {
        $settings = Setting::first();
        return view('super_admin.settings.settings.single', compact('settings'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required',
                'en_name' => 'required',
                'ar_rights' => 'required',
                'en_rights' => 'required',
                'credit_on_register' => 'required|integer',
                'delegate_fee' => 'required|integer'
            ],
            [
                'ar_name.required' => 'عفواً,إسم الموقع بالعربية مطلوب',
                'en_name.required' => 'عفواً,إسم الموقع بالعربية مطلوب',
                'ar_rights.required' => 'عفواً,حقوق الموقع بالعربية مطلوبة',
                'en_rights.required' => 'عفواً,حقوق الموقع بالإنجليزية مطلوبة',
                'credit_on_register.required' => 'عفواً,إسم الموقع بالعربية مطلوب',
                'delegate_fee.required' => 'عفواً,نسبة المندوب المئوية مطلوبة',
            ]
        );

        $settings = Setting::first();
            $settings->ar_name = $request->ar_name;
            $settings->en_name = $request->en_name;
            $settings->ar_rights = $request->ar_rights;
            $settings->en_rights = $request->en_rights;
            $settings->credit_on_register = $request->credit_on_register;
            $settings->delegate_fee = $request->delegate_fee;
        $settings->save();

        return back()->with('success', 'تم التعديل بنجاح');
    }
}
