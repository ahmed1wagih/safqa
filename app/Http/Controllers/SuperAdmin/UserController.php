<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Address;
use App\Models\Admin;
use App\Models\Super;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index($status)
    {
        $admins = Admin::where('status' , $status)->paginate(20);
        return view('super_admin.users.index', compact('admins'));
    }


    public function create()
    {
        $countries = Address::where('type','main')->select('id',lang().'_name as name')->get();
        return view('super_admin.users.single', compact('countries'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'country_id' => 'required|exists:addresses,id,type,main',
                'name' => 'required',
                'email' => 'required|email|unique:super_admins,email',
                'phone' => 'required|unique:super_admins,phone',
                'image' => 'sometimes|image',
                'password' => 'required|confirmed|min:6',
            ],
            [
                'country_id.required' => 'الدولة مطلوبة مطلوب',
                'country_id.exists' => 'الدولة غير صحيحة',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $admin = new Admin();
            $admin->country_id = $request->country_id;
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            $admin->password = Hash::make($request->password);
            if($request->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $admin->image = $image;
            }
        $admin->save();

        return redirect('/super_admin/users/active')->with('success', 'تم إضافة حساب المدير بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['admin_id' => $id]);
        $this->validate($request,
        [
            'admin_id' => 'required|exists:country_admins,id'
        ]);

        $countries = Address::where('type','main')->select('id',lang().'_name as name')->get();
        $user = Admin::find($request->admin_id);

        return view('super_admin.users.single', compact('user','countries'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id',
                'country_id' => 'required|exists:addresses,id,type,main',
                'name' => 'required',
                'email' => 'required|email|unique:country_admins,email,'.$request->id,
                'phone' => 'required|unique:country_admins,phone,'.$request->id,
                'image' => 'sometimes|image',
                'password' => 'sometimes|nullable|confirmed|min:6',
            ],
            [
                'country_id.required' => 'الدولة مطلوبة',
                'country_id.exists' => 'الدولةغير صحيحة',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $admin = Admin::find($request->id);
            $admin->country_id = $request->country_id;
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            if($request->password) $admin->password = Hash::make($request->password);
            if($request->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $admin->image = $image;
            }
        $admin->save();

        return redirect('/super_admin/users/'.$admin->status)->with('success', 'تم تعديل الحساب بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id',
                'status' => 'required|in:active,suspended'
            ]
        );

        $admin = Admin::find($request->id);
            $admin->status = $request->status;
        $admin->save();

        return back()->with('success', 'تم تغيير الحالة بنجاح');

    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id'
            ]
        );

        Admin::where('id', $request->id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
