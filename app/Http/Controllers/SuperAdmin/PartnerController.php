<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = Partner::first();
        return view('super_admin.settings.partners.index', compact('partners'));
    }


    public function edit()
    {
        $partners = Partner::first();
        return view('super_admin.settings.partners.single', compact('partners'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_text' => 'required',
                'en_text' => 'required',

            ],
            [
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
            ]
        );

        $partners = Partner::first();
            $partners->ar_text = $request->ar_text;
            $partners->en_text = $request->en_text;
        $partners->save();

        return redirect('/super_admin/settings/partners')->with('success', 'تم التعديل بنجاح');
    }
}
