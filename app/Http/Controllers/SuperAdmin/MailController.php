<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Address;
use App\Models\MailList;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index()
    {
        $mails = MailList::paginate(20);
        foreach ($mails as $mail) $mail['countries'] = Address::where('type','main')->whereIn('id',$mail->country_ids)->pluck('ar_name')->toArray();

        return view('super_admin.mails.index', compact('mails'));
    }


    public function create()
    {
        $countries = Address::where('type','main')->select('id','ar_name')->get();
        return view('super_admin.mails.single', compact('countries'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'country_ids' => 'required|array|min:1',
                'country_ids.*' => 'exists:addresses,id,type,main',
                'title' => 'required',
                'text' => 'required',
            ],
            [
                'country_ids.required' => 'الرجاء إختيار الدول',
                'country_ids.min' => 'الرجاء إختيار الدول',
                'country_ids.exists' => 'عفواً,الدول غير صحيحة',
                'title.required' => 'عفواً,العنوان مطلوب',
                'text.required' => 'عفواً,محتوي البريد الإلكتروني مطلوب',
            ]
        );


        $users = User::where('country_id',$request->country_ids)->select('name','email')->get();

        MailList::create
        (
            [
                'country_ids' => json_encode($request->country_ids),
                'title' => $request->title,
                'users_count' => $users->count(),
            ]
        );

        foreach($users as $user)
        {
            $data =
                [
                    'name' => $user->name,
                    'subject' => $request->title,
                    'content' => $request->text,
                    'code' => NULL,
                    'more' => NULL
                ];


             Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
            $message->from('info@bestdeal.market','Info@BestDeal')
                ->to($user->email)
                ->subject('بيست ديل | Best Deal');
            });
        }


        return redirect('/super_admin/mail/create')->with('success', 'تم إرسال البريد بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'mail_id' => 'required|exists:mail_lists,id',
            ]
        );

        MailList::where('id', $request->mail_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
