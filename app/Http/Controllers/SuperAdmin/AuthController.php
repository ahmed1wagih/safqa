<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Admin;
use App\Models\Super;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AuthController extends Controller
{
    public function login_view()
    {
        if(super()) return redirect('/super_admin/dashboard');
        return view('super_admin.login.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );


        if(Auth::guard('super')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('/super_admin/dashboard');
        }
        else
        {
            return back()->with('error','بيانات خاطئة');
        }
    }


    public function show()
    {
        return view('super_admin.auth.profile');
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:super_admins,email,'.super()->id,
                'phone' => 'required|numeric|unique:super_admins,phone,'.super()->id,
                'password' => 'nullable|confirmed|min:6'
            ],
            [
                'name.required' => 'name_required',
                'email.required' => 'email_required',
                'email.email' => 'email_email',
                'email.unique' => 'email_unique',
                'phone.required' => 'phone_required',
                'phone.numeric' => 'phone_numeric',
                'phone.unique' => 'phone_required',
                'password.min' => 'password_min',
                'password.confirmed' => 'password_confirmed',
            ]
        );

        $super = Super::find(super()->id);
            $super->name = $request->name;
            $super->email = $request->email;
            $super->phone = $request->phone;
                if($request->password) $super->password = Hash::make($request->password);
        $super->save();

        return back()->with('success', 'تم تحديث البيانات بنجاح');
    }


    public function logout()
    {
        Auth::guard('super')->logout();
        return redirect('/super_admin/login');
    }

}
