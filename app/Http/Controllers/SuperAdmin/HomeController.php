<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Address;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Company;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Provider;
use App\Models\Super;
use App\Models\Technician;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function dashboard()
    {
        $countries = Address::where('parent_id', NULL)->select('status')->get();
        $deals = Product::select('status')->get();
        $all_codes = ProductInfo::get();
        $supers = Super::select('status')->get();
        $admins = Admin::select('status')->get();
        $users = User::count();
        $transfers = Transfer::get();
        $merchants = Merchant::select('status')->get();

        return view('super_admin.dashboard', compact('countries','deals','all_codes','supers','admins','users','transfers','merchants'));
    }


    public function merchant_deals()
    {
        $merchants = Merchant::select('id','ar_name')->get();
        foreach($merchants as $merchant) $merchant['deals'] = Product::where('merchant_id',$merchant->id)->count();

        return response()->json($merchants);
    }


    public function category_deals()
    {
        $categories = Category::select('id','ar_name')->get();
        foreach($categories as $category) $category['deals'] = Product::where('cat_id',$category->id)->count();

        return response()->json($categories);
    }


    public function month_deals_graph()
    {
        $date = date('Y-m');
        $count = Carbon::now()->daysInMonth;
        $days = [];

        for($i = 1; $i < $count; $i++)
        {
            $days[] = $i;
        }

        $data = new Collection();

        foreach($days as $day)
        {
            if($day < 10) $this_day = '0'.$day;
            else $this_day = $day;

            $trans_count = ProductInfo::whereDate('purchased_at',$date.'-'.$this_day)->count();
            $data[] = collect(['x' => $date.'-'.$this_day, 'y' => $trans_count]);
        }

        return response()->json($data);
    }


    public function statistics($country_id)
    {
        $deals = Product::where('country_id', $country_id)->get();
        $all_codes = ProductInfo::get();
        $users = User::count();
        $transfers = Transfer::get();
        $merchants = Merchant::count();

        return view('super_admin.statistics', compact('deals','all_codes','users','transfers','merchants'));
    }
}
