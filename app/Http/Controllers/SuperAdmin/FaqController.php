<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::paginate(50);
        return view('super_admin.settings.faqs.index', compact('faqs'));
    }


    public function create()
    {
        return view('super_admin.settings.faqs.single');
    }


    public function store(Request $request)
    {

        $this->validate($request,
            [
                'ar_question' => 'required',
                'en_question' => 'required',
                'ar_answer' => 'required',
                'en_answer' => 'required',
            ],
            [
                'ar_question.required' => 'السؤال بالعربية مطلوب',
                'en_question.required' => 'السؤال بالإنجليزية مطلوب',
                'ar_answer.required' => 'اإلإجابة بالعربية مطلوبة',
                'en_answer.required' => 'الإجابة بالإنجليزية مطلوبة',
            ]
        );

        Faq::create
        (
            [
                'ar_question' => $request->ar_question,
                'en_question' => $request->en_question,
                'ar_answer' => $request->ar_answer,
                'en_answer' => $request->en_answer,
            ]
        );

        return redirect('/super_admin/settings/faqs')->with('success', 'تمت الإضافة بنجاح');
    }


    public function edit($id)
    {
        $faq = Faq::find($id);
        return view('super_admin.settings.faqs.single', compact('faq'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'faq_id' => 'required|exists:faqs,id',
                'ar_question' => 'required',
                'en_question' => 'required',
                'ar_answer' => 'required',
                'en_answer' => 'required',
            ],
            [
                'ar_question.required' => 'السؤال بالعربية مطلوب',
                'en_question.required' => 'السؤال بالإنجليزية مطلوب',
                'ar_answer.required' => 'اإلإجابة بالعربية مطلوبة',
                'en_answer.required' => 'الإجابة بالإنجليزية مطلوبة',
            ]
        );

        $faq = Faq::find($request->faq_id);
            $faq->ar_question = $request->ar_question;
            $faq->en_question = $request->en_question;
            $faq->ar_answer = $request->ar_answer;
            $faq->en_answer = $request->en_answer;
        $faq->save();

        return redirect('/super_admin/settings/faqs')->with('success', 'تم التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'faq_id' => 'required|exists:faqs,id',
            ]
         );

        Faq::where('id', $request->faq_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
