<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Address;
use App\Models\Admin;
use App\Models\BankAccount;
use App\Models\FooterStates;
use App\Models\Merchant;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function index($parent)
    {
        if($parent == 'all')
        {
            $addresses = Address::where('parent_id', NULL)->paginate(50);
        }
        else
        {
            $addresses = Address::where('parent_id', $parent)->paginate(50);
        }

        return view('super_admin.addresses.index', compact('addresses','parent'));
    }


    public function country_create()
    {
        return view('super_admin.addresses.single');
    }


    public function city_create()
    {
        $countries = Address::where('parent_id', NULL)->get();
        return view('super_admin.addresses.single', compact('countries'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'parent_id' => 'sometimes|exists:addresses,id,type,main',
                'ar_name' => 'required|unique:addresses',
                'en_name' => 'required|unique:addresses',
                'ar_currency' => 'sometimes',
                'en_currency' => 'sometimes'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالإنجليزية موجود من قبل',
            ]
        );

        $address = new Address();
            if($request->parent_id) $address->parent_id = $request->parent_id;
            else
            {
                $address->type = 'main';
                $address->ar_currency = $request->ar_currency;
                $address->en_currency = $request->en_currency;
            }
            $address->ar_name = $request->ar_name;
            $address->en_name = $request->en_name;


            if($request->image)
            {
                $name = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/addresses/', $name);
                $address->image = $name;
            }
        $address->save();

        if($address->parent_id == NULL)
        {
            FooterStates::create
            (
                [
                    'country_id' => $address->id,
                    'total_sold' => 0,
                    'total_saved' => 0
                ]
            );
        }


        if($address->parent_id == NULL)
        {
            return redirect('/super_admin/addresses/all')->with('success', 'تمت الإضافة بنجاح');
        }
        else
        {
            return redirect('/super_admin/addresses/'.$address->parent_id ? $address->parent_id : 'all')->with('success', 'تمت الإضافة بنجاح');
        }

    }


    public function edit($id)
    {
        $address = Address::find($id);
        return view('super_admin.addresses.single', compact('address'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'address_id' => 'required|exists:addresses,id',
                'ar_name' => 'required|unique:addresses,ar_name,'.$request->address_id,
                'en_name' => 'required|unique:addresses,en_name,'.$request->address_id,
                'ar_currency' => 'sometimes',
                'en_currency' => 'sometimes',
                'image' => 'sometimes|image'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالعربية موجود من قبل',
                'code.unique' => 'الكود موجود من قبل',
                'image.image' => 'عفواً,صورة غير صحيحة'
            ]
        );

        $address = Address::find($request->address_id);
            $address->en_name = $request->en_name;
            $address->ar_name = $request->ar_name;
            if($request->ar_currency) $address->ar_currency = $request->ar_currency;
            if($request->en_currency) $address->en_currency = $request->en_currency;
            if($request->image)
            {
                $name = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/addresses/', $name);
                $address->image = $name;
            }

        $address->save();

        return redirect('/super_admin/addresses/'.$address->parent_id ? $address->parent_id : 'all')->with('success', 'تم تعديل المدينة بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:addresses',
                'status' => 'required|in:active,suspended',
            ]
        );


        Address::where('id', $request->id)->orWhere('parent_id', $request->id)->update(['status' => $request->status]);
        Admin::where('country_id', $request->id)->update(['status' => $request->status]);
        Merchant::where('country_id', $request->id)->update(['status' => $request->status]);
        User::where('country_id', $request->id)->update(['status' => $request->status]);

        return back()->with('success', 'تم تغيير الحالة بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'address_id' => 'required|exists:addresses,id',
            ]
        );

        Address::where('id', $request->address_id)->delete();

        return back()->with('success','تم الحذف بنجاح');
    }


    public function bank_accounts_index($parent)
    {
        $banks = BankAccount::where('country_id', $parent)->paginate(50);
        return view('super_admin.addresses.bank_accounts', compact('parent','banks'));
    }


    public function bank_account_create()
    {
        $countries = Address::where('parent_id', NULL)->get();
        return view('super_admin.addresses.bank_account_single', compact('countries'));
    }


    public function bank_account_store(Request $request)
    {
        $this->validate($request,
            [
                'country_id' => 'required|exists:addresses,id,type,main',
                'ar_name' => 'required',
                'en_name' => 'required',
                'number' => 'required',
                'iban' => 'required'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'number.required' => 'رقم الحساب مطلوب',
                'iban.required' => 'رقم IBAN مطلوب',
            ]
        );

        BankAccount::create
        (
            [
                'country_id' => $request->country_id,
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
                'number' => $request->number,
                'iban' => $request->iban,
            ]
        );

        return redirect('/super_admin/addresses/'.$request->country_id.'/bank_accounts')->with('success', 'تمت الإضافة بنجاح');
    }


    public function bank_account_edit($id)
    {
        $countries = Address::where('parent_id', NULL)->get();
        $bank = BankAccount::find($id);
        return view('super_admin.addresses.bank_account_single', compact('countries','bank'));
    }


    public function bank_account_update(Request $request)
    {
        $this->validate($request,
            [
                'bank_id' => 'required|exists:bank_accounts,id',
                'country_id' => 'required|exists:addresses,id,type,main',
                'ar_name' => 'required',
                'en_name' => 'required',
                'number' => 'required',
                'iban' => 'required'
            ],
            [
                'country_id.required' => 'الدولة مطلوبة',
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'number.required' => 'رقم الحساب مطلوب',
                'iban.required' => 'رقم IBAN مطلوب'
            ]
        );

        BankAccount::where('id', $request->bank_id)->update
        (
            [
                'country_id' => $request->country_id,
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
                'number' => $request->number,
                'iban' => $request->iban,
            ]
        );

        return redirect('/super_admin/addresses/'.$request->country_id.'/bank_accounts')->with('success', 'تم التعديل بنجاح');
    }


    public function bank_account_destroy(Request $request)
    {
        $this->validate($request,
            [
                'bank_id' => 'required|exists:bank_accounts,id'
            ]
        );

        BankAccount::where('id', $request->bank_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
