<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Privacy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivacyController extends Controller
{
    public function index()
    {
        $privacy = Privacy::first();
        return view('super_admin.settings.privacy.index', compact('privacy'));
    }


    public function edit()
    {
        $privacy = Privacy::first();
        return view('super_admin.settings.privacy.single', compact('privacy'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_text' => 'required',
                'en_text' => 'required',
            ],
            [
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
            ]
        );

        $privacy = Privacy::first();
            $privacy->ar_text = $request->ar_text;
            $privacy->en_text = $request->en_text;
        $privacy->save();

        return redirect('/super_admin/settings/privacy')->with('success', 'تم التعديل بنجاح');
    }

}
