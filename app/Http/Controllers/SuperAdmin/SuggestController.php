<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Suggest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestController extends Controller
{
    public function index()
    {
        $suggests = Suggest::paginate(50);
        return view('super_admin.suggests.index', compact('suggests'));
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'suggest_id' => 'required|exists:suggests,id',
            ]
        );

        Suggest::where('id', $request->suggest_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
