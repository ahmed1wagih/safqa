<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Models\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermsController extends Controller
{
    public function index()
    {
        $terms = Term::first();
        return view('super_admin.settings.terms.index', compact('terms'));
    }


    public function edit()
    {
        $terms = Term::first();
        return view('super_admin.settings.terms.single', compact('terms'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'ar_text' => 'required',
                'en_text' => 'required',
            ],
            [
                'ar_text.required' => 'النص بالعربية مطلوب',
                'en_text.required' => 'النص بالإنجليزية مطلوب',
            ]
        );

        $terms = Term::first();
            $terms->ar_text = $request->ar_text;
            $terms->en_text = $request->en_text;
        $terms->save();

        return redirect('/super_admin/settings/terms')->with('success', 'تم التعديل بنجاح');
    }

}
