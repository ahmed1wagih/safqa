<?php

namespace App\Http\Controllers\Web;

use App\Models\Suggest;
use App\Models\Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestController extends Controller
{
    public function index()
    {
        $lang = lang();
        $about = AboutUs::select('id',$lang.'_address as address', $lang.'_text as text','email','phone','lat','lng')->first();

        return view('web.contact', compact('about'));    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required',
                'message' => 'required'
            ],
            [
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'message.required' => 'النص مطلوب',
            ]
        );

        Suggest::create
        (
            [
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message,
            ]
        );

        return back()->with('success', 'تم الإرسال بنجاح');
    }
}
