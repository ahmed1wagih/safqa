<?php

namespace App\Http\Controllers\Web;

use App\Models\BankAccount;
use App\Models\Cart;
use App\Models\CurrencyExchange;
use App\Models\FooterStates;
use App\Models\Pack;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\Transfer;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    public function payment()
    {
        $total = Cart::get_cart_total();
        $banks = BankAccount::get();

        return view('web.checkout_payment', compact('total','banks'));
    }


    public function charge_view()
    {
        $total = Cart::get_cart_total();
        $banks = BankAccount::where('country_id', Session::get('country')->id)->get();

        return view('web.checkout_payment', compact('banks','total'));
    }


    public function charge(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'bank_id' => 'required|exists:bank_accounts,id',
                'trans_no' => 'required|numeric',
                'pack_id' => 'required|exists:packs,id',
                'date' => 'required|date',
            ],
            [
                'name.required' => 'name_required',
                'bank_id.required' => 'bank_required',
                'bank_id.exists' => 'bank_invalid',
                'trans_no.required' => 'trans_no_required',
                'trans_no.numeric' => 'trans_no_invalid',
                'pack.required' => 'pack_required',
                'pack.exists' => 'pack_invalid',
                'date.required' => 'date_required',
                'date.date' => 'date_date',
            ]
        );

        $pack = Pack::where('id',$request->pack_id)->select('price')->first();

        Transfer::create
        (
            [
                'user_id' => user()->id,
                'name' => $request->name,
                'country_id' => BankAccount::where('id', $request->bank_id)->first()->id,
                'bank_id' => $request->bank_id,
                'trans_no' => $request->trans_no,
                'pack_id' => $request->pack_id,
                'price' => $pack->price,
                'date' => $request->date,
            ]
        );

        return redirect('/my_deals')->with('success', 'request_sent_wait_to_confirm');
    }


    public function credit_checkout()
    {
        $user = user();
        $carts = Cart::where('user_id', $user->id)->pluck('product_id');

        $total = Product::whereIn('id', $carts)->pluck('deal_price')->sum();

        if($user->credit >= $total)
        {
            if($user->expire_at < Carbon::today()->toDateString()) return back()->with('error', 'expired_subscription');


            foreach($carts as $product_id)
            {
                $product = Product::where('id',$product_id)->select('id','deal_price','ar_title','expire_at','country_id','old_price','new_price','limit')->first();

                if($product->expire_at < Carbon::today()->toDateString())
                {
                    Cart::where('user_id', $user->id)->where('product_id', $product->id)->delete();
                    return back()->with('error','expired_deal_deleted');
                }

                $count = ProductInfo::where('product_id',$product_id)->where('user_id', $user->id)->count();

                if($count >= $product->limit)
                {
                    Cart::where('user_id', $user->id)->where('product_id', $product->id)->delete();
                    return back()->with('error','deal_exceed_limits');
                }

                $barcode = ProductInfo::where('status', 'valid')->where('product_id', $product_id)->where('user_id', NULL)->first();
                    if(!$barcode) return back()->with('error','no_barcodes_left');
                    $barcode->user_id = $user->id;
                    $barcode->purchased_at = Carbon::now()->toDateTimeString();
                $barcode->save();

                $user->credit -= $product->deal_price;
                $user->save();

                $footer_states = FooterStates::where('country_id', $product->country_id)->first();
                    $footer_states->total_sold = $footer_states->total_sold + 1;
                    $footer_states->total_saved = $product->old_price - $product->new_price;
                $footer_states->save();

                $data = [
                    'name' => $user->name,
                    'subject' => 'شكرا لشراء الصفقة',
                    'code' => 'الكود الخاص بك' . ' : ' . $barcode->serial_no,
                    'content' => 'يمكنك تقييم الصفقة عبر الموقع او تطبيق الجوال لتحسين جودة الخدمة ',
                    'more' => 'Best Deal www.best-deal.app'
                ];


                Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
                    $message->from('info@bestdeal.market','Sales@BestDeal')
                        ->to($user->email)
                        ->subject('بيانات صفقتك | Best Deal');
                });
            }

            Cart::where('user_id', $user->id)->delete();

            return redirect('/my_deals')->with('success', 'purchase_success');
        }
        else
        {
            return back()->with('error', 'no_enough_credit');
        }


    }
}
