<?php

namespace App\Http\Controllers\Web;

use App\Models\AboutUs;
use App\Models\Ad;
use App\Models\Address;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Faq;
use App\Models\MailList;
use App\Models\MerchantTerm;
use App\Models\Partner;
use App\Models\Privacy;
use App\Models\Product;
use App\Models\Side;
use App\Models\SubmitDeal;
use App\Models\Suggest;
use App\Models\Term;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        $specials = Product::where('address_id', city()->id)->where('special', 1)->where('status', 'active')->where('expire_at','>',Carbon::today()->toDateString())->inRandomOrder()->take(5)->get();
        $side_specials = Side::where('country_id', country()->id)->inRandomOrder()->take(2)->select('link','image')->get();
        $limited_products = Product::where('address_id', city()->id)->where('status', 'active')->take(6)->get();
        $latest = Product::where('address_id', city()->id)->where('status', 'active')->latest()->take(3)->get();
        $all_cats = Category::where('parent_id', NULL)->get();

        return view('web.welcome', compact('specials','side_specials','limited_products','latest', 'all_cats'));
    }


    public function change_country($id)
    {
        $country = Address::where('id', $id)->first();
        Session::put('country', $country);
        Session::put('city', Address::where('parent_id',$country->id)->where('status','active')->first());

        return redirect('/');
    }


    public function change_city($id)
    {
        $city = Address::where('id', $id)->first();
        Session::put('country', $city->parent);
        Session::put('city', $city);

        return redirect('/');
    }


    public function browse($country_code,$city_name)
    {
        $country = Address::where('code',$country_code)->first();

        if($city_name != 'all')
        {
            $city = Address::where('en_name',$city_name)->first();

            $products = Product::where('address_id', $city->id)->where('status', 'active')->where('expire_at','>=',Carbon::today()->toDateString())->paginate(30);
        }
        else
        {
            $cities = Address::where('parent_id', $country->id)->pluck('id');
            $products = Product::whereIn('address_id', $cities)->where('status', 'active')->where('expire_at','>=',Carbon::today()->toDateString())->paginate(30);
        }

        return view('web.latest', compact('products'));
    }


    public function search(Request $request)
    {
        $city = Address::where('id', Session::get('city')->id)->first();

        $cats = Category::where('parent_id', $request->cat_id)->pluck('id');
        $products = Product::where('address_id', $city->id)->whereIn('cat_id', $cats)->where('status','active')->where('count','>',0)->where('ar_title','like','%'.$request->text.'%')->orWhere('en_title','like','%'.$request->text.'%')->where('expire_at', '>=', Carbon::today()->toDateString())->paginate(30);

        return view('web.latest', compact('products'));
    }


    public function get_cat_products($parent_id,$filter)
    {
        $ids = Category::where('parent_id', $parent_id)->pluck('id');
        $city = Address::where('id', Session::get('city')->id)->first();

        if($filter == 'all')
        {
            $products = Product::where('address_id', $city->id)->where('expire_at', '>=', Carbon::today()->toDateString())->whereIn('cat_id', $ids)->where('status', 'active')->paginate(30);
        }
        else
        {
            $products = Product::where('address_id', $city->id)->where('expire_at', '>=', Carbon::today()->toDateString())->where('cat_id', $filter)->where('status', 'active')->paginate(30);
        }

        return view('web.cat_products', compact('products'));
    }


    public function get_latest_products()
    {
        $city = Address::where('id', Session::get('city')->id)->first();

        $products = Product::where('address_id', $city->id)->where('expire_at', '>=', Carbon::today()->toDateString())->where('created_at', '>=', Carbon::now()->subMonth()->toDateString())->where('status', 'active')->paginate(30);
        return view('web.latest', compact('products'));
    }


    public function get_limited_products()
    {
        $city = Address::where('id', Session::get('city')->id)->first();

        $products = Product::where('address_id', $city->id)->where('expire_at', '!=', NULL)->where('expire_at', '>=', Carbon::today()->toDateString())->where('status', 'active')->paginate(30);
        return view('web.limited', compact('products'));
    }


    public function terms()
    {
        $lang = lang();
        $terms = Term::select($lang.'_text as text')->first();

        return view('web.terms', compact('terms'));
    }


    public function partners()
    {
        $lang = App::getLocale();
        $partners = Partner::select($lang.'_text as text')->first();

        return view('web.partners', compact('partners'));
    }


    public function terms_merchants()
    {
        $lang = App::getLocale();
        $terms = MerchantTerm::select($lang.'_text as text')->first();

        return view('web.terms_merchants', compact('terms'));
    }


    public function privacy()
    {
        $lang = App::getLocale();
        $privacy = Privacy::select($lang.'_text as text')->first();

        return view('web.privacy', compact('privacy'));
    }


    public function faqs()
    {
        $lang = App::getLocale();
        $faqs = Faq::select('id',$lang.'_question as question', $lang.'_answer as answer')->get();

        return view('web.faqs', compact('faqs'));
    }


    public function submit_deal()
    {
        $lang = App::getLocale();
        $submit = SubmitDeal::select($lang.'_text as text')->first();

        return view('web.submit', compact('submit'));
    }


    public function contact_us()
    {
        $lang = App::getLocale();
        $about = AboutUs::select('id',$lang.'_address as address', $lang.'_text as text','email','phone','lat','lng')->first();

        return view('web.contact', compact('about'));
    }


    public function suggest(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email',
                'message' => 'required'
            ]
        );

        Suggest::create
        (
            [
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message,
            ]
        );

        return back()->with('success', 'msg_sentً');
    }


    public function get_cities($parent)
    {
        $lang = App::getLocale();
        $cities = Address::where('parent_id', $parent)->select('id',$lang.'_name as name')->get();

        return response()->json($cities);
    }


    public function subscribe(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required|email',
            ]);

        MailList::updateOrcreate
        (
            [
                'email' => $request->email
            ]
        );

        return back()->with('success','subscribed_successfully');
    }


    public function logout()
    {
        Auth::guard('user')->logout();
        return redirect('/');
    }



    public function payment_view()
    {
        return view('web.payment_methods');
    }


    public function banner_click(Request $request)
    {
        $this->validate($request,
            [
                'banner_id' => 'required|exists:banners,id',
            ]);

        Banner::find($request->banner_id)->increment('clicks');

        return response()->json(['status' => 'success']);
    }
}

