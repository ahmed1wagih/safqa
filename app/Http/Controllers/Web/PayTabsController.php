<?php

namespace App\Http\Controllers\Web;

use App\Models\Address;
use App\Models\CurrencyExchange;
use App\Models\Order;
use App\Models\User;
use Damas\Paytabs\paytabs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PayTabsController extends Controller
{
    public function pay(Request $request)
    {
        $this->validate($request,
            [
                'amount' => 'required|exists:packs,amount',
                'first_name' => 'required',
                'last_name' => 'required',
            ]
        );

        $user = user();

        $order = new Order();
            $order->country_id = user()->country_id;
            $order->trans_id = str_random(50);
            $order->type = 'paytabs';
            $order->user_id = $user->id;
            $order->amount = $request->amount;
            $order->currency = 'sar';
        $order->save();

        $pt = paytabs::getInstance("paytaps@bestdeal.market", "jtOtMmWU0eFYTxlPuldakIDDxqYjJPx3ilx5ItZJIFIBc3qHoDRImumUVkdEaKIs6fyPIyyqdg2qjs7I6ZqsuNmp7A4hlMKr1jgK");

        $result = $pt->create_pay_page(array(
            "merchant_email" => "paytaps@bestdeal.market",
            'secret_key' => "jtOtMmWU0eFYTxlPuldakIDDxqYjJPx3ilx5ItZJIFIBc3qHoDRImumUVkdEaKIs6fyPIyyqdg2qjs7I6ZqsuNmp7A4hlMKr1jgK",
            'title' => 'BestDeal Charge',
            'cc_first_name' => $request->first_name,
            'cc_last_name' => $request->last_name,
            'email' => $user->email,
            'cc_phone_number' => $user->phone,
            'phone_number' => $user->phone,
            'billing_address' => 'address address',
            'city' => 'city  city',
            'state' => 'state state',
            'postal_code' => "97300",
            'country' => 'SAU',
            'address_shipping' => 'country country',
            'city_shipping' => 'city city',
            'state_shipping' => 'state state',
            'postal_code_shipping' => "97300",
            'country_shipping' => 'SAU',
            "products_per_title"=> "BestDeal Points Charge",
            'currency' => $order->currency,
            "unit_price"=> $request->amount,
            'quantity' => "1",
            'other_charges' => "0",
            'amount' => $request->amount,
            'discount'=>"0",
            "msg_lang" => "english",
            "reference_no" =>  $order->trans_id,
            "site_url" => "https://bestdeal.market",
            'return_url' => "https://bestdeal.market/payment/result",
            "cms_with_version" => "API USING PHP",
        ));


        if($result->response_code == 4012)
        {
            $order->ref_id = $result->p_id;
            $order->save();

            return redirect($result->payment_url);
        }
        else
        {
            return redirect('/charge_credit')->with('error','error_payment');
        }
    }


    public function fallback_result(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'payment_reference' => 'required|exists:orders,ref_id,status,awaiting'
            ]
        );

        if($validator->fails())
        {
            return redirect('/charge_credit')->with('error','error_payment');
        }


        $pt = paytabs::getInstance("paytaps@bestdeal.market", "jtOtMmWU0eFYTxlPuldakIDDxqYjJPx3ilx5ItZJIFIBc3qHoDRImumUVkdEaKIs6fyPIyyqdg2qjs7I6ZqsuNmp7A4hlMKr1jgK");
        $result = $pt->verify_payment($request->payment_reference);

        $order = Order::where('ref_id', $request->payment_reference)->where('trans_id',$result->reference_no)->first();

        if($result->response_code == 100)
        {
            $order->status = 'approved';
            $order->save();

            $country = Address::where('ar_currency', $order->currency)->first();
            $exchange = CurrencyExchange::where('country_id', $country->id)->first();

            $points = $order->amount * $exchange->point_per_currency;

            $user = User::where('id', $order->user_id)->first();
            $user->credit = $user->credit + $points;
            $user->save();

            return view('web.payment_success');
        }
        elseif($result->response_code == 0)
        {
            $order->status = 'canceled';
            $order->save();

            return view('web.payment_canceled');
        }
        else
        {
            $order->status = 'declined';
            $order->save();

            return view('web.payment_error');
        }


    }


}
