<?php

namespace App\Http\Controllers\Web;

use App\Models\Address;
use App\Models\BankAccount;
use App\Models\Cart;
use App\Models\CurrencyExchange;
use App\Models\Order;
use App\Models\Pack;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\ProductRate;
use App\Models\Transfer;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function profile()
    {
        return view('web.profile');
    }


    public function edit()
    {
        $lang = lang();
        $user = user();
        $countries = Address::where('parent_id', NULL)->select('id',$lang.'_name as name')->get();

        return view('web.profile_edit', compact('lang','user','countries'));
    }


    public function update(Request $request)
    {
        $user = user();

        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|exists:users,email,id,'.$user->id,
                'phone' => 'required|exists:users,phone,id,'.$user->id,
                'address_id' => 'required|exists:addresses,id,parent_id,'.$user->country_id,
                'age' => 'required|numeric',
                'gender' => 'required|in:male,female',
                'password' => 'sometimes|nullable|confirmed',
             ],
             [
                 'name.required' => 'name_required',
                 'email.required' => 'email_required',
                 'email.unique' => 'email_unique',
                 'phone.required' => 'phone_required',
                 'phone.unique' => 'phone_exists',
                 'password.confirmed' => 'password_not_confirmed',
             ]
        );

            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            if($request->password)  $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/profile')->with('success', 'updated_successfully');
    }


    public function my_deals()
    {
        $user = user();
        $deals = ProductInfo::where('user_id', $user->id)->get();

        return view('web.deals', compact('user','deals'));
    }


    public function my_transfers()
    {

        $transfers = Transfer::where('user_id', user()->id)->get();
        $onlines = Order::where('user_id',user()->id)->get();

        return view('web.transfers', compact('transfers','onlines'));
    }


    public function deal_info($id, Request $request)
    {
        $request->merge(['deal_id' => $id, 'user_id' => user()->id]);

        $this->validate($request,
            [
                'deal_id' => 'required|exists:product_infos,id,user_id,'.$request->user_id
            ]
        );

        $deal = ProductInfo::find($request->deal_id);

        return view('web.deal_info', compact('deal'));
    }


    public function wish_list()
    {
        $lang = lang();
        $ids = Wishlist::where('user_id', user()->id)->pluck('product_id');
        $wishes = Product::where('count','>',0)->where('status', 'active')->whereIn('id', $ids)->select('id','address_id',$lang.'_title as title','old_price','new_price','deal_price','expire_at')->get();

        return view('web.wish_list', compact('wishes'));
    }


    public function wish_store($id)
    {
        $check = Wishlist::where('user_id', user()->id)->where('product_id', $id)->first();

        if($check)
        {
            $check->delete();
            return back()->with('success', 'deleted_favorite');
        }
        else
        {
            Wishlist::create
            (
                [
                    'user_id' => user()->id,
                    'product_id' => $id
                ]
            );

            return back()->with('success', 'added_favorite');
        }
    }


    public function wish_delete($id)
    {
        Wishlist::where('user_id', user()->id)->where('product_id', $id)->delete();
        return back()->with('success', 'deleted_successfully');
    }


    public function rate(Request $request)
    {
        $this->validate($request,
            [
                'rate' => 'required|between:1,5',
                'text' => 'required'
            ],
            [
                'rate.required' => 'rate_required',
                'text.required' => 'text_required'
            ]
        );

        ProductRate::updateOrcreate
        (
            [
                'user_id' => user()->id,
                'product_id' => $request->product_id
            ],
            [
                'rate' => $request->rate,
                'text' => $request->text,
            ]
        );

        return back()->with('success', 'rated_successfully');
    }


    public function charge_credit()
    {
        $lang = lang();

        $banks = BankAccount::where('country_id', Session::get('country')->id)->select('id','country_id',$lang.'_name as name','iban','number')->get();

        $packs = Pack::where('country_id',user()->country_id)->select('id',$lang.'_name as name','price','points')->get();

        return view('web.credit', compact('banks','packs'));
    }
}
