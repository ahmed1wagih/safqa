<?php

namespace App\Http\Controllers\Web;

use App\Models\Address;
use App\Models\Merchant;
use App\Models\ProductInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class MerchantController extends Controller
{
//    public function merchant_check(Request $request)
//    {
//        $code = ProductInfo::where('serial_no', $request->code)->first();
//        return view('web.merchant_check', compact('code'));
//    }
//
//
//    public function merchant_report(Request $request)
//    {
//        $this->validate($request,
//            [
//                'serial_no' => 'required|exists:product_infos,serial_no'
//            ]
//        );
//
//        $code = ProductInfo::where('serial_no', $request->serial_no)->first();
//        $code->status = 'invalid';
//        $code->save();
//
//        return back()->with('success', 'reported_successfully');
//    }


    public function apply_view()
    {
        $countries = Address::where('status','active')->where('type','main')->select('id',lang().'_name as name')->get();
        return view('web.merchant_apply',compact('countries'));
    }


    public function apply(Request $request)
    {
        $this->validate($request,
            [
                'city_id' => 'required|exists:addresses,id,type,sub,status,active',
                'ar_name' => 'required|unique:merchants,ar_name',
                'en_name' => 'required|unique:merchants,en_name',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_address' => 'required',
                'en_address' => 'required',
                'owner' => 'required',
                'owner_phone' => 'required',
                'in_charge' => 'required',
                'in_charge_phone' => 'required',
                'type' => 'required|in:local,online',
                'link' => 'required_if:type,online',
                'lat' => 'required_if:type,local',
                'lng' => 'required_if:type,local',
                'email' => 'required|email|unique:merchants,email',
                'new_password' => 'required|confirmed|min:6',
                'policy' => 'required',
            ],
            [
                'ar_name.required' => 'field_required',
                'ar_name.unique' => 'field_exists',
                'en_name.required' => 'field_required',
                'en_name.unique' => 'field_exists',
                'ar_desc.required' => 'field_required',
                'en_desc.required' => 'field_required',
                'ar_address.required' => 'field_required',
                'en_address.required' => 'field_required',
                'owner.required' => 'field_required',
                'owner_phone.required' => 'field_required',
                'in_charge.required' => 'field_required',
                'in_charge_phone.required' => 'field_required',
                'email.required' => 'field_required',
                'email.email' => 'field_invalid',
                'email.unique' => 'field_exists',
                'new_password.required' => 'field_required',
                'new_password.confirmed' => 'field_invalid',
                'new_password.min' => 'field_min_6',
                'type.required' => 'field_required',
                'link.required' => 'field_required',
                'lat.required' => 'field_required',
                'lng.required' => 'field_required',
                'policy.required' => 'accept_terms_first',
            ]
        );

        $merchant = new Merchant();
            $merchant->country_id = Address::where('id',$request->city_id)->select('parent_id')->first()->parent_id;
            $merchant->city_id =  $request->city_id;
            $merchant->ar_name = $request->ar_name;
            $merchant->en_name = $request->en_name;
            $merchant->ar_desc = $request->ar_desc;
            $merchant->en_desc = $request->en_desc;
            $merchant->ar_address = $request->ar_address;
            $merchant->en_address = $request->en_address;
            $merchant->owner = $request->owner;
            $merchant->owner_phone = $request->owner_phone;
            $merchant->in_charge = $request->in_charge;
            $merchant->in_charge_phone = $request->in_charge_phone;
            $merchant->email = $request->email;
            $merchant->password = Hash::make($request->new_password);
            $merchant->type = $request->type;
            if($request->type == 'online') $merchant->link = $request->link;
            else
            {
                $merchant->lat = $request->lat;
                $merchant->lng = $request->lng;
            }
        $merchant->save();

        return back()->with('success', 'applied_successfully');
    }
}
