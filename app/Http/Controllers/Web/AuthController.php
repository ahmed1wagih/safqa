<?php

namespace App\Http\Controllers\Web;

use App\Models\Address;
use App\Models\Code;
use App\Models\MailList;
use App\Models\Setting;
use App\Models\Sms;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function register_view()
    {
        $countries = Address::has('active_cities')->where('status','active')->where('parent_id', NULL)->select('id',lang().'_name as name')->get();
        return view('web.register', compact('countries'));
    }


    public function register(Request $request)
    {
        $this->validate($request,
        [
                 'name' => 'required',
                 'email' => 'required|email|unique:users',
                 'phone' => 'required|unique:users',
                 'country_id' => 'required|exists:addresses,id,status,active,type,main',
                 'password' => 'required|min:6|confirmed',
                 'policy' => 'required'
            ],
            [
                'name.required' => 'name_required',
                'email.required' => 'email_required',
                'email.unique' => 'email_unique',
                'phone.required' => 'phone_required',
                'phone.unique' => 'phone_exists',
                'password.required' => 'password_required',
                'password.confirmed' => 'password_not_confirmed',
                'policy.required' => 'confirm_privacy_policy',
            ]
         );

        $user = User::create
        (
            [
                'country_id' => $request->country_id,
                'city_id' => Address::where('parent_id',$request->country_id)->where('status','active')->first()->id,
                'status' => 'suspended',
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
                'credit' => Setting::first()->credit_on_register,
                'expire_at' => Carbon::now()->addMonth()->toDateString()
            ]
        );

        $code = Code::create
        (
            [
                'type' => 'activation',
                'phone' => $user->phone,
                'code' => rand(1000,9999),
                'expire_at' => Carbon::now()->addHour()->toDateTimeString()
            ]
        );

        $data = [
            'name' => $user->name,
            'subject' => 'مرحباً بك في موقع Best Deal',
            'content' => 'بيست ديل ماركت هو افضل موقع وتطبيق الكتروني لحجز وشراء الصفقات الحصرية بخصومات وتخفيضات مميزة ولفترة محدودة , ياتلحق يا ماتلحق ,تسوق معنا واشتري افضل الصفقات مع حرية الدفع و الاستخدام في وقت اطول كل ماعليك تقديم كود الصفقة للتاجر لتحصل على الخصم و هذا هو كود التفعيل',
            'code' => $code->code,
            'more' => ''
        ];


        Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
            $message->from('info@bestdeal.app','Welcome@BestDeal')
                ->to($user->email)
                ->subject('بيست ديل | Best Deal');
        });



//        if(lang() == 'ar') $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
//        else $msg = 'Verification Code In Best Deal ' . $code->code . ',Thanks';
//
//
//        Sms::send($msg,$user->phone);

        return redirect('/activate')->with('success', 'registered_please_verify');
    }


    public function __construct()
    {
        $this->middleware('guest');
    }


    public function login_view()
    {
        return view('web.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'login' => 'required',
                'password' => 'required'
            ]
        );

        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $request->merge([$field => $request->input('login')]);

        $user = User::where($field, $request->login)->first();

        if($user && $user->blocked == 1) return back()->with('error', 'account_blocked_contact_admin');


        if($user && $user->status == 'suspended')
        {
            $code = Code::updateOrcreate
            (
                [
                    'type' => 'activation',
                    'phone' => $user->phone
                ],
                [
                    'code' => rand(1000, 9999),
                ]
            );


            if(lang() == 'ar') $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
            else $msg = 'Verification Code In Best Deal ' . $code->code . ',Thanks';

            $data = [
                'name' => $user->name,
                'subject' => 'موقع Best Deal',
                'content' => $msg,
                'code' => $code->code,
                'more' => ''
            ];


            Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
                $message->from('info@bestdeal.market','Activation@BestDeal')
                    ->to($user->email)
                    ->subject('بيست ديل | Best Deal');
            });

//            Sms::send($msg,$user->phone);

            return redirect('/activate')->with('error', 'account_suspended_please_verify');
        }
        elseif($user && $user->status == 'active')
        {
            if(Auth::guard('user')->attempt([$field => $request->$field, 'password' => $request->password]))
            {
                Session::put('country',user()->country);
                Session::put('city', Address::where('parent_id',country()->id)->where('status','active')->first());

                return redirect('/');
            }
            else return back()->with('error', 'invalid_data');
        }
        else return back()->with('error', 'user_not_found');
    }


    public function activate_view()
    {
        return view('web.activate');
    }


    public function activate(Request $request)
    {
        $this->validate($request,
            [
                'code' => 'required',
            ]
        );

        $code = Code::where('type','activation')->where('code',$request->code)->first();

        if($code)
        {
            $user = User::where('phone', $code->phone)->first();
//            dd($user,$user->country);

            if($user)
            {
                $user->status = 'active';
                $user->save();

                $code->delete();

                Session::put('country',$user->country);
                Auth::guard('user')->loginUsingId($user->id);
            }
            else
            {
                return back()->with('error', 'invalid_data');
            }


            return redirect('/')->with('success', 'activated_successfully');
        }
        else
        {
            return back()->with('error', 'invalid_data');
        }
    }


    public function password_reset()
    {
        return view('web.forget_password');
    }


    public function password_reset_send(Request $request)
    {
        $this->validate($request,
            [
                'email_send' => 'required|exists:users,email|email'
            ]
        );


        $user = User::where('email', $request->email_send)->first();

        $code = Code::updateOrcreate
        (
            [
                'type' => 'reset',
                'phone' => $user->phone
            ],
            [
                'code' => rand(1000,9999)
            ]
        );


        if(lang() == 'ar') $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
        else $msg = 'Verification Code In Best Deal ' . $code->code . ',Thanks';

        $data = [
            'name' => $user->name,
            'subject' => 'موقع Best Deal',
            'content' => $msg,
            'code' => $code->code,
            'more' => ''
        ];


        Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
            $message->from('info@bestdeal.market','Reset@BestDeal')
                ->to($user->email)
                ->subject('بيست ديل | Best Deal');
        });

//        Sms::send($msg,$user->phone);

        return back()->with('success', 'code_sent_please_verify');
    }


    public function password_reset_check(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required|exists:users,email',
                'code' => 'required'
            ],
            [
                'email.exists' => 'email_exists',
            ]
        );

        $user = User::where('email', $request->email)->first();
        $check = Code::where('phone', $user->phone)->where('type','reset')->where('code',$request->code)->select('code')->first();
        $code = $request->code;

        if($check)
        {
            return view('web.change_password', compact('code'));
        }
        else return back()->with('error', 'invalid_code');

    }


    public function password_reset_change(Request $request)
    {
        $this->validate($request,
            [
                'code' => 'required|exists:codes,code',
                'new_password' => 'required|min:6|confirmed',
            ],
            [
                'new_password.min' => 'password_min',
                'new_password.confirmed' => 'password_confirmed',
            ]
        );


        $code = Code::where('type','reset')->where('code', $request->code)->first();
        $user = User::where('phone', $code->phone)->first();

        if($user)
        {
            $user->password = Hash::make($request->new_password);
            $user->save();

            $code->delete();

            return redirect('/login')->with('success', 'password_changed_please_login');
        }
        else return redirect('/login')->with('error', 'invalid_data');
    }
}
