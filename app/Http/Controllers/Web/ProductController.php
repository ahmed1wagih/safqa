<?php

namespace App\Http\Controllers\Web;

use App\Models\FooterStates;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\ProductRate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function show($id, Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'required|exists:products,id'
            ]
        );

        $product = Product::find($id);
        if($product->status != 'active') return back()->with('error','deal_suspended');
        $related = Product::where('expire_at', '>=', Carbon::today()->toDateString())->where('status', 'active')->where('cat_id', $product->cat_id)->where('id','!=', $id)->inRandomOrder()->take(4)->get();
        $latest = Product::where('expire_at', '>=', Carbon::today()->toDateString())->where('status', 'active')->where('created_at', '>=', Carbon::now()->subMonth()->toDateString())->inRandomOrder()->take(3)->get();

        return view('web.product_show', compact('product', 'related','latest'));
    }
}
