<?php

namespace App\Http\Controllers\Web;

use App\Models\Cart;
use App\Models\Product;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function index()
    {
        $carts = Cart::where('user_id', user()->id)->join('products','carts.product_id','products.id')->select('carts.id','carts.product_id','products.address_id','cat_id','ar_title','en_title','old_price','new_price','deal_price')->get();

        $total = 0;

        foreach($carts as $cart)
        {
            $total = $cart->deal_price + $total;
        }

        return view('web.cart', compact('carts','total'));
    }


    public function store($id,Request $request)
    {
        $request->merge(['id' => $id]);

        $this->validate($request,
            [
                'id' => 'required|exists:products,id'
            ]
        );

        $product = Product::where('id', $request->id)->select('id','expire_at')->first();

        if($product->expire_at < Carbon::today()->toDateString())
        {
            Cart::where('user_id', user()->id)->where('product_id', $product->id)->delete();
            return back()->with('error','expired_deal_deleted');
        }

        Cart::create
        (
            [
                'user_id' => user()->id,
                'product_id' => $id,
            ]
        );

        return redirect('/cart')->with('success', 'added');
    }


    public function destroy($id)
    {
        Cart::where('user_id', user()->id)->where('id', $id)->delete();
        return back()->with('success', 'deleted');
    }

}
