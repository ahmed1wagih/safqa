<?php

namespace App\Http\Controllers\Merchant;

use App\Models\MerchantRep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RepController extends Controller
{
    public function index($status)
    {
        $reps = MerchantRep::where('merchant_id', merchant()->id)->where('status', $status)->paginate(50);
        return view('merchant.reps.index', compact('reps'));
    }


    public function create()
    {
        return view('merchant.reps.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|unique:merchant_reps,email',
                'phone' => 'required|unique:merchant_reps,phone',
                'password' => 'required|confirmed|min:6'
            ],
            [
                'name.required' => 'إسم الوكيل مطلوب',
                'email.required' => 'بريد الوكيل الوكيل مطلوب',
                'email.unique' => 'بريد الوكيل موجود من قبل',
                'phone.required' => 'هاتف الوكيل مطلوب',
                'phone.unique' => 'هاتف الوكيل موجود من قبل',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'تأكيد كلمة المرور غير متطابق',
            ]
        );

        $rep = new MerchantRep();
            $rep->merchant_id = merchant()->id;
            $rep->name = $request->name;
            $rep->email = $request->email;
            $rep->phone = $request->phone;
            $rep->password = Hash::make($request->password);
        $rep->save();

        return redirect('/merchant/reps/suspended')->with('success', 'تم إضافة الوكيل بنجاح,الرجاء التفعيل');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'exists:merchant_reps,id,merchant_id,'.merchant()->id
            ]
        );

        $rep = MerchantRep::find($id);

        return view('merchant.reps.single', compact('rep'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'rep_id' => 'required|exists:merchant_reps,id,merchant_id,'.merchant()->id,
                'name' => 'required',
                'email' => 'required|unique:merchant_reps,email,'.$request->rep_id,
                'phone' => 'required|unique:merchant_reps,phone,'.$request->rep_id,
                'password' => 'sometimes|nullable|confirmed|min:6'
            ],
            [
                'name.required' => 'إسم الوكيل مطلوب',
                'email.required' => 'بريد الوكيل الوكيل مطلوب',
                'email.unique' => 'بريد الوكيل موجود من قبل',
                'phone.required' => 'هاتف الوكيل مطلوب',
                'phone.unique' => 'هاتف الوكيل موجود من قبل',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'تأكيد كلمة المرور غير متطابق',
            ]
        );


        $rep = MerchantRep::find($request->rep_id);
            $rep->name = $request->name;
            $rep->email = $request->email;
            $rep->phone = $request->phone;
            if($request->password) $rep->password = Hash::make($request->password);
        $rep->save();

        return redirect('/merchant/reps/'.$rep->status)->with('success', 'تم تعديل بيانات الوكيل بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'rep_id' => 'required|exists:merchant_reps,id,merchant_id,'.merchant()->id
            ]
        );

        $rep = MerchantRep::find($request->rep_id);
            if($rep->status == 'active') $rep->status = 'suspended';
            else $rep->status = 'active';
        $rep->save();

        return back()->with('success', 'تم تغيير حالة الوكيل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'rep_id' => 'required|exists:merchant_reps,id,merchant_id,'.merchant()->id
            ]
        );

        MerchantRep::where('id', $request->rep_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
