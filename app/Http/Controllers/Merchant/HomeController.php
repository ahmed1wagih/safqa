<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Category;
use App\Models\Merchant;
use App\Models\MerchantRep;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\ProductRate;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    public function dashboard()
    {
        $deals = Product::where('merchant_id', merchant()->id)->select('id','status')->get();
        $all_codes = ProductInfo::whereIn('product_id', $deals->pluck('id'))->get();
        $all_codes_used = ProductInfo::whereIn('product_id', $deals->pluck('id'))->where('user_id','!=',NULL)->count();

        return view('merchant.dashboard', compact('deals','all_codes','all_codes_used'));
    }


    public function get_sub_cats($parent)
    {
        $cats = Category::where('parent_id', $parent)->get();
        return response()->json($cats);
    }


    public function merchant_deals()
    {
        $deals = Product::where('merchant_id',merchant()->id)->select('id','ar_title')->get();
        foreach($deals as $deal)
        {
            $deal['sold'] = ProductInfo::where('product_id',$deal->id)->where('user_id','!=',NULL)->count();
            $rate = ProductRate::where('product_id',$deal->id)->avg('rate');
            $deal['rate'] = $rate ? (integer)$rate : 0;
        }

        return response()->json($deals);
    }


    public function month_deals_graph()
    {
        $date = date('Y-m');
        $count = Carbon::now()->daysInMonth;
        $days = [];

        for($i = 1; $i < $count; $i++)
        {
            $days[] = $i;
        }

        $data = new Collection();
        $products = Product::where('merchant_id',merchant()->id)->pluck('id')->unique()->toArray();

        foreach($days as $day)
        {
            if($day < 10) $this_day = '0'.$day;
            else $this_day = $day;

            $trans_count = ProductInfo::whereIn('product_id',$products)->whereDate('purchased_at',$date.'-'.$this_day)->count();
            $data[] = collect(['x' => $date.'-'.$this_day, 'y' => $trans_count]);
        }

        return response()->json($data);
    }

}
