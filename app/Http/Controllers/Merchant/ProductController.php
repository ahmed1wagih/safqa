<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Address;
use App\Models\Category;
use App\Models\DelegatesMerchants;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductInfo;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function index($status)
    {
        $cities = Address::where('parent_id', merchant()->country_id)->pluck('id');
        $products = Product::where('merchant_id', merchant()->id)->whereIn('address_id', $cities)->where('status' , $status)->paginate(50);

        return view('merchant.products.index', compact('products'));
    }


    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.single', compact('product'));
    }


    public function create()
    {
        $cities = Address::where('parent_id', merchant()->country_id)->get();
        $cats = Category::where('parent_id', NULL)->get();

        return view('merchant.products.single', compact('cities','cats'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'address_id' => 'required|exists:addresses,id,parent_id,'.merchant()->country_id,
                'cat_id' => 'required|exists:categories,id',
                'ar_title' => 'required',
                'en_title' => 'required',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_conds' => 'required',
                'en_conds' => 'required',
                'count' => 'required|integer',
                'old_price' => 'required|integer',
                'new_price' => 'required|integer',
//                'deal_price' => 'required|integer',
                'expire_at' => 'required|date',
                'limit' => 'required|integer',
                'images' => 'required',
                'images.*' => 'image'
            ],
            [
                'address_id.required' => 'المدينة مطلوبة',
                'address_id.exists' => 'المدينة غير صحيحة',
                'cat_id.required' => 'القسم مطلوب',
                'cat_id.exists' => 'القسم غير صحيح',
                'ar_title.required' => 'عنوان المنتج بالعربية مطلوب',
                'en_title.required' => 'عنوان المنتج بالإنجليزية مطلوب',
                'ar_desc.required' => 'وصف المنتج بالعربية مطلوب',
                'en_desc.required' => 'وصف المنتج بالإنجليزية مطلوب',
                'ar_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'en_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'count.required' => 'الكمية مطلوبة',
                'old_price.required' => 'السعر قبل الخصم مطلوب',
                'old_price.integer' => 'السعر قبل الخصم لا بد أن يكون رقم',
                'new_price.required' => 'السعر بعد الخصم مطلوب',
                'new_price.integer' => 'السعر بعد الخصم لا بد أن يكون رقم',
//                'deal_price.required' => 'سعر الصفقة مطلوب',
//                'deal_price.integer' => 'سعر الصفقة لا بد أن يكون رقم',
                'expire_at.required' => 'تاريخ إنتهاء الصفقة مطلوب',
                'limit.required' => 'حد أكواد الخصم مطلوب',
                'limit.integer' => 'حد أكواد الخصم غير صحيح',
                'images.required' => 'صور المنتج مطلوبة',
                'images.*.image' => 'صور المنتج غير صحيحة',
            ]
        );

        $deal_price = ($request->old_price - $request->new_price) *  20 / 100;

        $product = new Product();
            $product->merchant_id = merchant()->id;
            $product->country_id = merchant()->country_id;
            $product->address_id = $request->address_id;
            $product->cat_id = $request->cat_id;
            $product->ar_title = $request->ar_title;
            $product->en_title = $request->en_title;
            $product->ar_desc = $request->ar_desc;
            $product->en_desc = $request->en_desc;
            $product->ar_conds = $request->ar_conds;
            $product->en_conds = $request->en_conds;
            $product->count = $request->count;
            $product->old_price = $request->old_price;
            $product->new_price = $request->new_price;
            $product->deal_price = $deal_price;
            $product->expire_at = $request->expire_at;
            $product->limit = $request->limit;
        $product->save();

        foreach($request->images as $image)
        {
            $name = unique_file($image->getClientOriginalName());
            $image->move(base_path().'/public/products/',$name);

            $image = Image::make(base_path().'/public/products/'.$name);
                $image->resize(750, 420);
            $image->save(base_path().'/public/products/'.$name);


            ProductImage::create
            (
                [
                    'product_id' => $product->id,
                    'image' => $name
                ]
            );

        }

        for($i = 0; $i < $product->count; $i ++)
        {
            $x = 0;

            $tmp = mt_rand(1,9);
            do
            {
                $tmp .= mt_rand(0, 9);
            }
            while(++$x < 9);

            ProductInfo::create
            (
                [
                    'status' => 'invalid',
                    'product_id' => $product->id,
                    'serial_no' => $tmp
                ]
            );
        }

        return redirect('/merchant/products/suspended')->with('success', 'تمت إضافة المنتج بنجاح,الرجاء الإنتظار لحين موافقة إدارة الموقع');
    }


    public function edit($id, Request $request)
    {
        $request->merge(['product_id' => $id]);

        $this->validate($request,
            [
                'product_id' => 'exists:products,id,merchant_id,'.merchant()->id.',by,merchant'
            ]
        );

        $product = Product::find($id);
        $cities = Address::where('parent_id', merchant()->country_id)->get();
        $cats = Category::get();

        return view('merchant.products.single', compact('product','cities','cats'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,by,merchant,merchant_id,'.merchant()->id,
                'address_id' => 'required|exists:addresses,id,parent_id,'.merchant()->country_id,
                'cat_id' => 'sometimes|exists:categories,id',
                'ar_title' => 'required',
                'en_title' => 'required',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_conds' => 'required',
                'en_conds' => 'required',
                'count' => 'required|integer',
                'old_price' => 'required|integer',
                'new_price' => 'required|integer',
                'deal_price' => 'required|integer',
                'expire_at' => 'required|date',
                'limit' => 'required|integer',
                'images' => 'sometimes',
                'images.*' => 'image'
            ],
            [
                'address_id.required' => 'المدينة مطلوبة',
                'address_id.exists' => 'المدينة غير صحيحة',
                'cat_id.exists' => 'القسم غير صحيح',
                'ar_title.required' => 'عنوان المنتج بالعربية مطلوب',
                'en_title.required' => 'عنوان المنتج بالإنجليزية مطلوب',
                'ar_desc.required' => 'وصف المنتج بالعربية مطلوب',
                'en_desc.required' => 'وصف المنتج بالإنجليزية مطلوب',
                'ar_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'en_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'count.required' => 'الكمية مطلوبة',
                'old_price.required' => 'السعر قبل الخصم مطلوب',
                'old_price.integer' => 'السعر قبل الخصم لا بد أن يكون رقم',
                'new_price.required' => 'السعر بعد الخصم مطلوب',
                'new_price.integer' => 'السعر بعد الخصم لا بد أن يكون رقم',
                'deal_price.required' => 'سعر الصفقة مطلوب',
                'deal_price.integer' => 'سعر الصفقة لا بد أن يكون رقم',
                'expire_at.required' => 'تاريخ إنتهاء الصفقة مطلوب',
                'limit.required' => 'حد أكواد الخصم مطلوب',
                'limit.integer' => 'حد أكواد الخصم غير صحيح',
                'images.required' => 'صور المنتج مطلوبة',
                'images.*.image' => 'صور المنتج غير صحيحة',
            ]
        );

        $deal_price = ($request->old_price - $request->new_price) *  merchant()->country->exchange->point_per_currency;

        $product = Product::find($request->product_id);
            $product->address_id = $request->address_id;
            if($request->cat_id) $product->cat_id = $request->cat_id;
            $product->ar_title = $request->ar_title;
            $product->en_title = $request->en_title;
            $product->ar_desc = $request->ar_desc;
            $product->en_desc = $request->en_desc;
            $product->ar_conds = $request->ar_conds;
            $product->en_conds = $request->en_conds;
            $product->count = $request->count;
            $product->old_price = $request->old_price;
            $product->new_price = $request->new_price;
            $product->deal_price = $deal_price;
            $product->expire_at = $request->expire_at;
            $product->limit = $request->limit;
        $product->save();

        if($request->deleted_id)
        {
            foreach($request->deleted_ids as $image)
            {
                $image = ProductImage::find($image);
                unlink(base_path().'/public/products/'.$image->image);
                $image->delete();
            }
        }

        if($request->images)
        {
            foreach($request->images as $image)
            {
                $name = unique_file($image->getClientOriginalName());
                $image->move(base_path().'/public/products/',$name);

                $image = Image::make(base_path().'/public/products/'.$name);
                    $image->resize(750, 420);
                $image->save(base_path().'/public/products/'.$name);

                ProductImage::create
                (
                    [
                        'product_id' => $product->id,
                        'image' => $name
                    ]
                );
            }
        }

        return redirect('/merchant/products/'.$product->status)->with('success','تم تعديل بيانات الصفقة بنجاح');
    }



    public function deals_export($product_id, Request $request)
    {
        $request->merge(['product_id' => $product_id]);

        $this->validate($request,
            [
                'product_id' => 'exists:products,id,merchant_id,'.merchant()->id
            ]
        );

        $codes = ProductInfo::where('product_id', $product_id)->select('status as Status','user_id as User','serial_no as Serial')->get();

        foreach($codes as $code)
        {
            if($code->User) $user = User::where('id', $code->User)->select('name')->first()->name;
            else $user = 'No User Yet';

            $code['User'] = $user;
        }

        $codes = $codes->toArray();
        $product = Product::where('id', $product_id)->select('en_title','merchant_id','created_at','expire_at')->first();
        $merchant = Merchant::where('id', $product->merchant_id)->select('en_name')->first();

        $merchant_name = str_replace(' ','-',$merchant->en_name);

        $filename = 'safqa_'.$merchant_name.'_'.$product->en_name.'_'.$product->created_at->toDateString().'_to_'.$product->expire_at.'_codes.xls';

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $heads = false;
        foreach($codes as $code)
        {
            if($heads == false)
            {
                echo implode("\t", array_keys($code)) . "\n";
                $heads = true;
            }
            {
                echo implode("\t", array_values($code)) . "\n";
            }
        }

        die();
    }
}
