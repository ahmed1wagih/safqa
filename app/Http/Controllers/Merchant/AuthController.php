<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Code;
use App\Models\Sms;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login_view()
    {
        if(merchant()) return redirect('/merchant/dashboard');
        else return view('merchant.auth.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );


        if(Auth::guard('merchant')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('/merchant/dashboard');
        }
        else
        {
            return back()->with('error','بيانات خاطئة');
        }
    }


    public function profile()
    {
        $merchant = merchant();
        return view('merchant.auth.profile', compact('merchant'));
    }


    public function update(Request $request)
    {
        $request->merge(['merchant_id' => merchant()->id]);
        $this->validate($request,
            [
                'ar_name' => 'required|unique:merchants,ar_name,'.$request->merchant_id,
                'en_name' => 'required|unique:merchants,en_name,'.$request->merchant_id,
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_address' => 'required',
                'en_address' => 'required',
                'link' => 'sometimes',
                'lat' => 'required',
                'lng' => 'required',
                'email' => 'required|email|unique:merchants,email,'.$request->merchant_id,
                'new_password' => 'sometimes|confirmed|nullable|min:6'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالإنجليزية موجود من قبل',
                'ar_desc.required' => 'الوصف بالعربية مطلوب',
                'en_desc.required' => 'الوصف بالإنجليزية مطلوب',
                'ar_address.required' => 'العنوان بالعربية مطلوب',
                'en_address.required' => 'العنوان بالإنجليزية مطلوب',
                'lat.required' => 'خطأ بموقع التاجر',
                'lng.required' => 'خطأ بموقع التاجر',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.email' => 'البريد الإلكتروني غير صحيح',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'new_password.confirmed' => 'تأكيد كلمة المرور الجديدة غير متطابق',
                'new_password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
            ]
        );


        $merchant = merchant();
            $merchant->ar_name = $request->ar_name;
            $merchant->en_name = $request->en_name;
            $merchant->ar_desc = $request->ar_desc;
            $merchant->en_desc = $request->en_desc;
            $merchant->ar_address = $request->ar_address;
            $merchant->en_address = $request->en_address;
            if($request->link) $merchant->link = $request->link; else $merchant->link = NULL;
            $merchant->lat = $request->lat;
            $merchant->lng = $request->lng;
            $merchant->email = $request->email;
            if($request->new_password) $merchant->password = Hash::make($request->new_password);
        $merchant->save();

        return back()->with('success', 'تم تعديل بياناتك بنجاح');
    }


    public function logout()
    {
        Auth::guard('merchant')->logout();
        return redirect('/merchant/login');
    }
}
