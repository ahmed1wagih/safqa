<?php

namespace App\Http\Controllers\Merchant;

use App\Models\Product;
use App\Models\ProductInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    public function index()
    {
        if(isset($_GET['serial_no'])) $check_deal = ProductInfo::where('serial_no',$_GET['serial_no'])->first();

        $product_ids = Product::where('merchant_id',merchant()->id)->pluck('id');
        $codes = ProductInfo::whereIn('product_id',$product_ids)->where('rep_id','!=',NULL)->paginate(50);

        return view('merchant.deals.index',get_defined_vars());
    }
}
