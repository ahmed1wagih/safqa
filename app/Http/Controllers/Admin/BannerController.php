<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::where('country_id', admin()->country_id)->paginate(50);
        return view('admin.banners.index', compact('banners'));
    }


    public function create()
    {
        return view('admin.banners.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'link' => 'required',
                'image' => 'required|image',
                'height' => 'required|integer',
                'width' => 'required|integer'
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'image.required' => 'الصورة مطلوبة',
                'image.file' => 'الصورة غير صحيحة',
                'height.required' => 'الإرتفاع مطلوب',
                'height.integer' => 'الإرتفاع غير صحيح',
                'width.required' => 'العض مطلوب',
                'width.integer' => 'العرض غير صحيح',
            ]
        );

        $banner = new Banner();
            $banner->country_id = admin()->country_id;
            $banner->link = $request->link;
            $banner->height = $request->height;
            $banner->width = $request->width;

            $name = unique_file($request->image->getClientOriginalName());
            $request->image->move(base_path().'/public/banners/', $name);
            $banner->image = $name;
        $banner->save();

        return redirect('/admin/banners/index')->with('success', 'تم إضافة البانر بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['id' => $id]);

        $this->validate($request,
            [
                'id' => 'required|exists:banners,id,country_id,'.admin()->country_id
            ]
        );

        $banner = Banner::find($request->id);
        return view('admin.banners.single', compact('banner'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'banner_id' => 'required|exists:banners,id,country_id,'.admin()->country_id,
                'link' => 'required',
                'image' => 'sometimes|image',
                'height' => 'required|integer',
                'width' => 'required|integer'
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'image.file' => 'الصورة غير صحيحة',
                'height.required' => 'الإرتفاع مطلوب',
                'height.integer' => 'الإرتفاع غير صحيح',
                'width.required' => 'العض مطلوب',
                'width.integer' => 'العرض غير صحيح',
            ]
        );

        $banner = Banner::find($request->banner_id);
            $banner->link = $request->link;
            $banner->height = $request->height;
            $banner->width = $request->width;

            if($request->image)
            {
                unlink(base_path().'/public/banners/'.$banner->image);

                $name = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/banners/', $name);
                $banner->image = $name;
            }
        $banner->save();

        return redirect('/admin/banners/index')->with('success', 'تم تعديل البانر بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'banner_id' => 'required|exists:banners,id,country_id,'.admin()->country_id,
            ]
        );

        $banner = Banner::where('id', $request->banner_id)->first();
        unlink(base_path().'/public/banners/'.$banner->image);
        $banner->delete();

        return back()->with('success', 'تم حذف البانر بنجاح');
    }
}
