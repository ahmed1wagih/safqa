<?php

namespace App\Http\Controllers\Admin;

use App\Models\Address;
use App\Models\Category;
use App\Models\Merchant;
use App\Models\MerchantRep;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductInfo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index($status)
    {
        $cities = Address::where('parent_id', admin()->country_id)->pluck('id');
        $products = Product::whereIn('address_id', $cities)->where('status' , $status)->paginate(50);

        return view('admin.products.index', compact('products'));
    }


    public function show($id,Request $request)
    {
        $request->merge(['product_id' => $id]);
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,country_id,'.admin()->country_id,
            ]
        );

        $product = Product::find($id);
        return view('admin.products.single', compact('product'));
    }


    public function specialize($id,Request $request)
    {
        $request->merge(['product_id' => $id]);
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,country_id,'.admin()->country_id,
            ]
        );

        Product::where('id', $id)->update(['special' => 1]);

        return back()->with('success', 'تم التمييز بنجاح');
    }


    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,country_id,'.admin()->country_id,
            ]
        );


        $product = Product::where('id', $request->product_id)->first();

        if($product->status == 'active')
        {
            $product->status = 'suspended';
        }
        else
        {
            $product->status = 'active';
        }

        $product->save();

        ProductInfo::where('product_id', $product->id)->where('user_id', NULL)->change_status($product->status);

        return back()->with('success', 'تمت العملية بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,country_id,'.admin()->country_id
            ]
        );


        Product::where('id', $request->product_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }


    public function deals_export($product_id, Request $request)
    {
        $request->merge(['product_id' => $product_id]);

        $this->validate($request,
            [
                'product_id' => 'exists:products,id,country_id,'.admin()->country_id
            ]
        );

        $codes = ProductInfo::where('product_id', $product_id)->select('status as Status','user_id as User','rep_id as Cashier','serial_no as Serial')->get();

        foreach($codes as $code)
        {
            if($code->User) $user = User::where('id', $code->User)->select('name')->first()->name;
            else $user = 'No User Yet';

            if($code->Cashier) $rep = MerchantRep::where('id', $code->Cashier)->select('name')->first()->name;
            else $rep = 'No Cashier Yet';

            $code['User'] = $user;
            $code['Cashier'] = $rep;
        }

        $codes = $codes->toArray();
        $product = Product::where('id', $product_id)->select('en_title','merchant_id','created_at','expire_at')->first();
//        $merchant = Merchant::where('id', $product->merchant_id)->select('en_name')->first();

        $filename = 'safqa_'.$product->en_title.'_'.$product->created_at->toDateString().'_to_'.$product->expire_at.'_codes.xls';

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $heads = false;
        foreach($codes as $code)
        {
            if($heads == false)
            {
                echo implode("\t", array_keys($code)) . "\n";
                $heads = true;
            }
            {
                echo implode("\t", array_values($code)) . "\n";
            }
        }

        die();
    }
}
