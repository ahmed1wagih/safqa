<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index($status)
    {
        $admins = Admin::where('id','!=',admin()->id)->where('country_id',admin()->country_id)->where('status' , $status)->paginate(20);
        return view('admin.admins.index', compact('admins'));
    }


    public function create()
    {
        return view('admin.admins.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:country_admins,email',
                'phone' => 'required|unique:country_admins,phone',
                'image' => 'sometimes|image',
                'password' => 'required|confirmed|min:6',
            ],
            [
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $admin = new admin();
            $admin->status = 'active';
            $admin->country_id = admin()->country_id;
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            $admin->password = Hash::make($request->password);
            if($admin->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $admin->image = $image;
            }
        $admin->save();

        return redirect('/admin/admins/active')->with('success', 'تم إضافة حساب الموظف بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['admin_id' => $id]);
        $this->validate($request,
        [
            'admin_id' => 'required|exists:country_admins,id,country_id,'.admin()->country_id
        ]);

        $admin = Admin::find($request->admin_id);

        return view('admin.admins.single', compact('admin'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id',
                'name' => 'required',
                'email' => 'required|email|unique:country_admins,email,'.$request->id,
                'phone' => 'required|unique:country_admins,phone,'.$request->id,
                'image' => 'sometimes|image',
                'password' => 'sometimes|nullable|confirmed|min:6',
            ],
            [
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $admin = Admin::find($request->id);
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            if($request->password) $admin->password = Hash::make($request->password);
            if($request->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $admin->image = $image;
            }
        $admin->save();

        return redirect('/admin/admins/'.$admin->status)->with('success', 'تم تعديل الحساب بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id',
                'status' => 'required|in:active,suspended'
            ]
        );

        $admin = Admin::find($request->id);
            $admin->status = $request->status;
        $admin->save();

        return back()->with('success', 'تم تغيير الحالة بنجاح');

    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:country_admins,id'
            ]
        );

        Admin::where('id', $request->id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
