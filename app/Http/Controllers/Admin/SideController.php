<?php

namespace App\Http\Controllers\Admin;

use App\Models\Side;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SideController extends Controller
{
    public function index()
    {
        $sides = Side::where('country_id', admin()->country_id)->paginate(50);
        return view('admin.sides.index', compact('sides'));
    }


    public function create()
    {
        return view('admin.sides.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'link' => 'required',
                'image' => 'required|image',
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'image.required' => 'الصورة مطلوبة',
                'image.file' => 'الصورة غير صحيحة',
            ]
        );

        $side = new Side();
            $side->country_id = admin()->country_id;
            $side->link = $request->link;
            $name = unique_file($request->image->getClientOriginalName());
            $request->image->move(base_path().'/public/sides/', $name);
            $side->image = $name;
        $side->save();

        return redirect('/admin/sides/index')->with('success', 'تم إضافة البانر الجانبي بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['id' => $id]);

        $this->validate($request,
            [
                'id' => 'required|exists:sides,id,country_id,'.admin()->country_id
            ]
        );

        $side = Side::find($request->id);
        return view('admin.sides.single', compact('side'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'side_id' => 'required|exists:sides,id,country_id,'.admin()->country_id,
                'link' => 'required',
                'image' => 'sometimes|image',
            ],
            [
                'link.required' => 'لينك التحويل مطلوب',
                'image.file' => 'الصورة غير صحيحة',
            ]
        );

        $side = Side::find($request->side_id);
            $side->link = $request->link;
            if($request->image)
            {
                unlink(base_path().'/public/sides/'.$side->image);

                $name = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/sides/', $name);
                $side->image = $name;
            }
        $side->save();

        return redirect('/admin/sides/index')->with('success', 'تم تعديل البانر الجانبي بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'side_id' => 'required|exists:sides,id,country_id,'.admin()->country_id,
            ]
        );

        $side = Side::where('id', $request->side_id)->first();
            unlink(base_path().'/public/sides/'.$side->image);
        $side->delete();

        return back()->with('success', 'تم حذف البانر الجانبي بنجاح');
    }
}
