<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackController extends Controller
{
    public function index($status)
    {
        $packs = Pack::where('country_id', admin()->country_id)->where('status', $status)->paginate(50);
        return view('admin.packs.index', compact('packs'));
    }


    public function create()
    {
        return view('admin.packs.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required',
                'en_name' => 'required',
                'months_count' => 'required|numeric',
                'points' => 'required|numeric',
                'price' => 'required|numeric',
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'months_count.required' => 'عدد الشهور مطلوب',
                'months_count.numeric' => 'عدد الشهور غير صحيح',
                'points.required' => 'عدد الناقط مطلوب',
                'points.numeric' => 'عدد النقاط غير صحيح',
                'price.required' => 'السعر مطلوب',
                'price.numeric' => 'السعر غير صحيح',
            ]
        );

        $pack = new Pack();
            $pack->status = 'active';
            $pack->country_id = admin()->country_id;
            $pack->ar_name = $request->ar_name;
            $pack->en_name = $request->en_name;
            $pack->months_count = $request->months_count;
            $pack->points = $request->points;
            $pack->price = $request->price;
        $pack->save();

        return redirect('/admin/packs/active')->with('success', 'تم إضافة الباقة بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'exists:packs,id,country_id,'.admin()->country_id
            ]
        );

        $pack = Pack::find($id);

        return view('admin.packs.single', compact('pack'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'pack_id' => 'required|exists:packs,id,country_id,'.admin()->country_id,
                'ar_name' => 'required',
                'en_name' => 'required',
                'months_count' => 'required|numeric',
                'points' => 'required|numeric',
                'price' => 'required|numeric',
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'months_count.required' => 'عدد الشهور مطلوب',
                'months_count.numeric' => 'عدد الشهور غير صحيح',
                'points.required' => 'عدد الناقط مطلوب',
                'points.numeric' => 'عدد النقاط غير صحيح',
                'price.required' => 'السعر مطلوب',
                'price.numeric' => 'السعر غير صحيح',
            ]
        );


        $pack = Pack::find($request->pack_id);
            $pack->ar_name = $request->ar_name;
            $pack->en_name = $request->en_name;
            $pack->months_count = $request->months_count;
            $pack->points = $request->points;
            $pack->price = $request->price;
        $pack->save();

        return redirect('/admin/packs/'.$pack->status)->with('success', 'تم تعديل بيانات الباقة بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'pack_id' => 'exists:packs,id,country_id,'.admin()->country_id
            ]
        );

        $pack = Pack::find($request->pack_id);
            if($pack->status == 'active') $pack->status = 'suspended';
            else $pack->status = 'active';
        $pack->save();

        return back()->with('success', 'تم تغيير حالة الباقة بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'pack_id' => 'exists:packs,id,country_id,'.admin()->country_id
            ]
        );

        Pack::where('id', $request->pack_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
