<?php

namespace App\Http\Controllers\Admin;

use App\Models\CurrencyExchange;
use App\Models\Order;
use App\Models\Pack;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TransferController extends Controller
{
    public function index($status)
    {
        $transfers = Transfer::where('status', $status)->where('country_id', admin()->country_id)->paginate(50);
        return view('admin.transfers.index', compact('transfers'));
    }


    public function show($id)
    {
        $transfer = Transfer::find($id);
        return view('admin.transfers.single', compact('transfer'));
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'transfer_id' => 'required|exists:transfers,id,country_id,'.admin()->country_id,
                'status' => 'required|in:approved,declined'
            ]
        );

        $transfer = Transfer::where('id', $request->transfer_id)->first();
            $transfer->status = $request->status;
        $transfer->save();

       if($request->status == 'approved')
       {
           $user = User::find($transfer->user_id);
               $pack = Pack::where('country_id', admin()->country_id)->where('id',$transfer->pack_id)->first();
               $user->credit += $pack->points;
               $user->expire_at = Carbon::parse($user->expire_at)->addMonths($pack->months_count);
           $user->save();

           if(lang() == 'ar') $msg = 'تم قبول و تفعيل إشتراكك في باقة ' . $pack->ar_name . 'بمبلغ '.$pack->price.' '.$pack->country->ar_currency;
           else $msg = 'Your subscription for ' . $pack->en_name . ' pack has been confirmed,with price of '.$pack->price.' '.$pack->country->en_currency;

           $data = [
               'name' => $user->name,
               'subject' => 'موقع Best Deal',
               'content' => $msg,
               'code' => '',
               'more' => ''
           ];


           Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
               $message->from('info@bestdeal.market','Subscriptions@BestDeal')
                   ->to($user->email)
                   ->subject('بيست ديل | Best Deal');
           });

           $pack->sold ++;
           $pack->save();
       }

        return redirect('/admin/transfers/awaiting')->with('success', 'تمت العملية بنجاح');
    }
}
