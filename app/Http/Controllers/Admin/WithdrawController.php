<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WithdrawController extends Controller
{
    public function index($status)
    {
        $withdraws = Withdraw::where('country_id', admin()->country_id)->where('status', $status)->latest()->paginate(50);
        return view('admin.withdraws.index', compact('withdraws'));
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'withdraw_id' => 'required|exists:withdraws,id,country_id,'.admin()->country_id,
                'status' => 'required|in:approved,declined'
            ]
        );

        $withdraw = Withdraw::find($request->withdraw_id);
            $withdraw->status = $request->status;
        $withdraw->save();

        if($request->status == 'approved')
        {
            $total = round($withdraw->amount_to_points($withdraw->amount),0);
            $delegate = User::where('id', $withdraw->delegate_id)->first();
            $delegate->credit = $delegate->credit - $total;
            $delegate->save();
        }


        return back()->with('success', 'تمت العملية بنجاح');
    }
}
