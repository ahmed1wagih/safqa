<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ad;
use App\Models\Admin;
use App\Models\Super;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login_view()
    {
        if(admin()) return redirect('/admin/dashboard');

        return view('admin.login.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );

        $check = Admin::where('email', $request->email)->first();
        if($check && $check->status == 'suspended') return back()->with('error','الحساب موقوف,الرجاء المتابعة مع الإدارة');

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('/admin/dashboard');
        }
        else
        {
            return back()->with('error','بيانات خاطئة');
        }
    }

    public function show()
    {
        return view('admin.auth.profile');
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:country_admins,email,'.super()->id,
                'phone' => 'required|numeric|unique:country_admins,phone,'.super()->id,
                'password' => 'nullable|confirmed|min:6'
            ],
            [
                'name.required' => 'name_required',
                'email.required' => 'email_required',
                'email.email' => 'email_email',
                'email.unique' => 'email_unique',
                'phone.required' => 'phone_required',
                'phone.numeric' => 'phone_numeric',
                'phone.unique' => 'phone_required',
                'password.min' => 'password_min',
                'password.confirmed' => 'password_confirmed',
            ]
        );

        $admin = Admin::find(super()->id);
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->phone = $request->phone;
            if($request->password) $admin->password = Hash::make($request->password);
        $admin->save();

        return back()->with('success', 'تم تحديث البيانات بنجاح');
    }


    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }
}
