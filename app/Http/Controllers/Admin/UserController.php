<?php

namespace App\Http\Controllers\Admin;

use App\Models\Address;
use App\Models\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index($status)
    {
        $users = User::where('country_id',admin()->country_id)->where('status' , $status)->paginate(20);
        return view('admin.users.index', compact('users'));
    }


    public function create()
    {
        $cities = Address::where('parent_id',admin()->country_id)->where('type','sub')->select('id','ar_name')->get();
        return view('admin.users.single',compact('cities'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'city_id' => 'required|exists:addresses,id,type,sub,parent_id,'.admin()->country_id,
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|unique:users,phone',
                'image' => 'sometimes|image',
                'password' => 'required|confirmed|min:6',
            ],
            [
                'city_id.required' => 'المدينة مطلوبة',
                'city_id.exists' => 'المدينة غير صحيحة',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $user = new User();
            $user->status = 'active';
            $user->country_id = admin()->country_id;
            $user->city_id = $request->city_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            if($user->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $user->image = $image;
            }
        $user->save();

        return redirect('/admin/users/active')->with('success', 'تم إضافة حساب العميل بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['user_id' => $id]);
        $this->validate($request,
        [
            'user_id' => 'required|exists:users,id,country_id,'.admin()->country_id
        ]);

        $cities = Address::where('parent_id',admin()->country_id)->where('type','sub')->select('id','ar_name')->get();
        $user = User::find($request->user_id);

        return view('admin.users.single', compact('user','cities'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:users,id,country_id,'.admin()->country_id,
                'city_id' => 'required|exists:addresses,id,type,sub,parent_id,'.admin()->country_id,
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->id,
                'phone' => 'required|unique:users,phone,'.$request->id,
                'image' => 'sometimes|image',
                'password' => 'sometimes|nullable|confirmed|min:6',
            ],
            [
                'city_id.required' => 'المدينة مطلوبة',
                'city_id.exists' => 'المدينة غير صحيحة',
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف موجود من قبل',
                'image.image' => 'صورة غير صحيحة',
                'password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
                'password.confirmed' => 'كلمة المرور غير متطابقة',
            ]
        );

        $user = User::find($request->id);
            $user->city_id = $request->city_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            if($request->password) $user->password = Hash::make($request->password);
            if($request->image)
            {
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $user->image = $image;
            }
        $user->save();

        return redirect('/admin/users/'.$user->status)->with('success', 'تم تعديل الحساب بنجاح');
    }


    public function change_status(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:users,id,country_id,'.admin()->country_id,
                'status' => 'required|in:active,suspended'
            ]
        );

        $user = User::find($request->id);
            $user->status = $request->status;
        $user->save();

        return back()->with('success', 'تم تغيير الحالة بنجاح');

    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'id' => 'required|exists:users,id,country_id,'.admin()->country_id
            ]
        );

        User::where('id', $request->id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
