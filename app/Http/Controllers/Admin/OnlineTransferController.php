<?php

namespace App\Http\Controllers\Admin;

use App\Models\CurrencyExchange;
use App\Models\Order;
use App\Models\Pack;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OnlineTransferController extends Controller
{
    public function index($status)
    {
        $online_transfers = Order::where('status', $status)->where('country_id', admin()->country_id)->paginate(50);

        return view('admin.online_transfers.index', compact('online_transfers'));
    }


    public function show($id,Request $request)
    {
        $request->merge(['id' => $id]);
        $this->validate($request,
            [
                'id' => 'required|exists:transfers,id,country_id,'.admin()->country_id,
            ]
        );

        $transfer = Transfer::find($id);
        return view('admin.online_transfers.single', compact('transfer'));
    }
}
