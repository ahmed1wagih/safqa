<?php

namespace App\Http\Controllers\Admin;

use App\Models\DelegatesMerchants;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MerchantController extends Controller
{
    public function index($status)
    {
        $merchants = Merchant::where('country_id', admin()->country_id)->where('status', $status)->paginate(50);

        if($status == 'active')
        {
            $status = 'التجار المفعلين';
        }
        else
        {
            $status = 'التجار الموقوفين';
        }

        return view('admin.merchants.index', compact('merchants','status'));
    }


    public function create()
    {
        return view('admin.merchants.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'ar_name' => 'required|unique:merchants,ar_name',
                'en_name' => 'required|unique:merchants,en_name',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_address' => 'required',
                'en_address' => 'required',
                'owner' => 'required',
                'owner_phone' => 'required',
                'in_charge' => 'required',
                'in_charge_phone' => 'required',
                'type' => 'required|in:local,online',
                'link' => 'required_if:type,online',
                'lat' => 'required_if:type,local',
                'lng' => 'required_if:type,local',
                'email' => 'required|email',
                'new_password' => 'required|confirmed|min:6'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالإنجليزية موجود من قبل',
                'ar_desc.required' => 'الوصف بالعربية مطلوب',
                'en_desc.required' => 'الوصف بالإنجليزية مطلوب',
                'ar_address.required' => 'العنوان بالعربية مطلوب',
                'en_address.required' => 'العنوان بالإنجليزية مطلوب',
                'owner.required' => 'إسم المالك مطلوب',
                'owner_phone.required' => 'هاتف المالك مطلوب',
                'in_charge.required' => 'إسم المسئول مطلوب',
                'in_charge_phone.required' => 'هاتف المسئول مطلوب',
                'type.required' => 'الموقع الإلكرتوني مطلوب',
                'link.required' => 'الموقع الإلكرتوني مطلوب',
                'lat.required' => 'خطأ بموقع التاجر',
                'lng.required' => 'خطأ بموقع التاجر',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.email' => 'البريد الإلكتروني غير صحيح',
                'new_password.confirmed' => 'تأكيد كلمة المرور الجديدة غير متطابق',
                'new_password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
            ]
        );

        $merchant = new Merchant();
            $merchant->country_id = user()->country_id;
            $merchant->ar_name = $request->ar_name;
            $merchant->en_name = $request->en_name;
            $merchant->ar_desc = $request->ar_desc;
            $merchant->en_desc = $request->en_desc;
            $merchant->ar_address = $request->ar_address;
            $merchant->en_address = $request->en_address;
            $merchant->owner = $request->owner;
            $merchant->owner_phone = $request->owner_phone;
            $merchant->in_charge = $request->in_charge;
            $merchant->in_charge_phone = $request->in_charge_phone;
            $merchant->type = $request->type;
            if($request->type == 'online') $merchant->link = $request->link;
            else
            {
                $merchant->lat = $request->lat;
                $merchant->lng = $request->lng;
            }
            $merchant->email = $request->email;
            $merchant->password = Hash::make($request->new_password);
        $merchant->save();

        return redirect('/admin/merchants/suspended')->with('success', 'تمت الإضافة بنجاح');
    }


    public function edit($id,Request $request)
    {
        $request->merge(['merchant_id' => $id]);
        $this->validate($request,
            [
                'merchant_id' => 'required|exists:merchants,id,country_id,'.admin()->country_id
            ]
        );

        $merchant = Merchant::where('country_id', admin()->country_id)->find($id);
        return view('admin.merchants.single', compact('merchant'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'merchant_id' => 'required|exists:merchants,id',
                'ar_name' => 'required|unique:merchants,ar_name,'.$request->merchant_id,
                'en_name' => 'required|unique:merchants,en_name,'.$request->merchant_id,
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_address' => 'required',
                'en_address' => 'required',
                'owner' => 'required',
                'owner_phone' => 'required',
                'in_charge' => 'required',
                'in_charge_phone' => 'required',
                'type.required' => 'الموقع الإلكرتوني مطلوب',
                'link.required' => 'الموقع الإلكرتوني مطلوب',
                'lat.required' => 'خطأ بموقع التاجر',
                'lng.required' => 'خطأ بموقع التاجر',
                'email' => 'required|email',
                'new_password' => 'sometimes|nullable|confirmed|min:6'
            ],
            [
                'ar_name.required' => 'الإسم بالعربية مطلوب',
                'ar_name.unique' => 'الإسم بالعربية موجود من قبل',
                'en_name.required' => 'الإسم بالإنجليزية مطلوب',
                'en_name.unique' => 'الإسم بالإنجليزية موجود من قبل',
                'ar_desc.required' => 'الوصف بالعربية مطلوب',
                'en_desc.required' => 'الوصف بالإنجليزية مطلوب',
                'ar_address.required' => 'العنوان بالعربية مطلوب',
                'en_address.required' => 'العنوان بالإنجليزية مطلوب',
                'owner.required' => 'إسم المالك مطلوب',
                'owner_phone.required' => 'هاتف المالك مطلوب',
                'in_charge.required' => 'إسم المسئول مطلوب',
                'in_charge_phone.required' => 'هاتف المسئول مطلوب',
                'type.required' => 'الموقع الإلكرتوني مطلوب',
                'link.required' => 'الموقع الإلكرتوني مطلوب',
                'lat.required' => 'خطأ بموقع التاجر',
                'lng.required' => 'خطأ بموقع التاجر',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.email' => 'البريد الإلكتروني غير صحيح',
                'new_password.confirmed' => 'تأكيد كلمة المرور الجديدة غير متطابق',
                'new_password.min' => 'كلمة المرور لا بد أن تكون 6 خانات علي الأقل',
            ]
        );

        $merchant = Merchant::find($request->merchant_id);
            $merchant->ar_name = $request->ar_name;
            $merchant->en_name = $request->en_name;
            $merchant->ar_desc = $request->ar_desc;
            $merchant->en_desc = $request->en_desc;
            $merchant->ar_address = $request->ar_address;
            $merchant->en_address = $request->en_address;
            $merchant->owner = $request->owner;
            $merchant->owner_phone = $request->owner_phone;
            $merchant->in_charge = $request->in_charge;
            $merchant->in_charge_phone = $request->in_charge_phone;
            $merchant->type = $request->type;
            if($request->type == 'online') $merchant->link = $request->link;
            else
            {
                $merchant->lat = $request->lat;
                $merchant->lng = $request->lng;
            }
            $merchant->email = $request->email;
            if($request->new_password) $merchant->password = Hash::make($request->new_password);
        $merchant->save();

        return redirect('/admin/merchants/'.$merchant->status)->with('success', 'تم التعديل بنجاح');
    }


    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'merchant_id' => 'required|exists:merchants,id,country_id,'.admin()->country_id
            ]
        );

        $merchant = Merchant::find($request->merchant_id);
            if($merchant->status == 'active')
            {
                $merchant->status = 'suspended';
            }
            else
            {
                $merchant->status = 'active';
            }
        $merchant->save();

        return back()->with('success', 'تمت العملية بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'merchant_id' => 'required|exists:merchants,id,country_id,'.admin()->country_id
            ]
        );

        Merchant::where('id', $request->merchant_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
