<?php

namespace App\Http\Controllers\Admin;


use App\Models\Category;
use App\Models\Merchant;
use App\Models\Order;
use App\Models\Pack;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Transfer;
use App\Models\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    public function dashboard()
    {
        $merchants = Merchant::where('country_id', admin()->country_id)->select('status')->get();
        $deals = Product::where('country_id', admin()->country->id)->select('id','status')->get();
        $all_codes = ProductInfo::whereIn('product_id', $deals->pluck('id'))->select('user_id')->get();
        $packs = Pack::where('country_id', admin()->country_id)->select('status','sold')->get();
        $users = User::where('country_id', admin()->country_id)->select('status')->get();

        $bank_transfers = Transfer::where('country_id', admin()->country_id)->where('status','approved')->select('price')->get();
        $online_transfers = Order::where('country_id', admin()->country_id)->where('status','approved')->select('amount')->get();

        return view('admin.dashboard', get_defined_vars());
    }


    public function merchant_deals()
    {
        $merchants = Merchant::where('country_id',admin()->country_id)->select('id','ar_name')->get();
        foreach($merchants as $merchant) $merchant['deals'] = Product::where('merchant_id',$merchant->id)->count();

        return response()->json($merchants);
    }


    public function category_deals()
    {
        $categories = Category::select('id','ar_name')->get();
        foreach($categories as $category) $category['deals'] = Product::where('country_id', admin()->country_id)->where('cat_id',$category->id)->count();

        return response()->json($categories);
    }


    public function month_deals_graph()
    {
        $date = date('Y-m');
        $count = Carbon::now()->daysInMonth;
        $days = [];

        for($i = 1; $i < $count; $i++)
        {
            $days[] = $i;
        }

        $data = new Collection();
        $products = Product::where('country_id',admin()->country_id)->pluck('id')->unique()->toArray();

        foreach($days as $day)
        {
            if($day < 10) $this_day = '0'.$day;
            else $this_day = $day;

            $trans_count = ProductInfo::whereIn('product_id',$products)->whereDate('purchased_at',$date.'-'.$this_day)->count();
            $data[] = collect(['x' => $date.'-'.$this_day, 'y' => $trans_count]);
        }

        return response()->json($data);
    }
}
