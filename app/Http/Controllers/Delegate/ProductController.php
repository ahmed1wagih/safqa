<?php

namespace App\Http\Controllers\Delegate;

use App\Models\Address;
use App\Models\Category;
use App\Models\DelegatesMerchants;
use App\Models\Merchant;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductInfo;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index($status)
    {
        $products = Product::where('status' , $status)->where('delegate_id', Auth::user()->id)->paginate(100);
        return view('delegate.products.index', compact('products'));
    }


    public function create()
    {
        $cities = Address::where('parent_id', Auth::user()->country_id)->get();
        $cats = Category::where('parent_id', NULL)->get();
        $merchants_ids = DelegatesMerchants::where('delegate_id', Auth::user()->id)->pluck('merchant_id');
        $merchants = Merchant::whereIn('id', $merchants_ids)->get();

        return view('delegate.products.single', compact('cities','cats','merchants'));
    }


    public function store(Request $request)
    {
        $request->merge(['country_id'=> Auth::user()->country_id, 'delegate_id' => Auth::user()->id]);
        $this->validate($request,
            [
                'country_id' => 'required|exists:addresses,id',
                'address_id' => 'required|exists:addresses,id,parent_id,'.Auth::user()->country_id,
                'cat_id' => 'required|exists:categories,id',
                'merchant_id' => 'required|exists:merchants,id|exists:delegates_merchants,merchant_id,delegate_id,'.Auth::user()->id,
                'ar_title' => 'required',
                'en_title' => 'required',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_conds' => 'required',
                'en_conds' => 'required',
                'count' => 'required|integer',
                'old_price' => 'required|integer',
                'new_price' => 'required|integer',
                'deal_price' => 'required|integer',
                'expire_at' => 'required|date',
                'limit' => 'required|integer',
                'images' => 'sometimes',
                'images.*' => 'image'
            ],
            [
                'address_id.required' => 'المدينة مطلوبة',
                'address_id.exists' => 'المدينة غير صحيحة',
                'cat_id.required' => 'القسم مطلوب',
                'cat_id.exists' => 'القسم غير صحيح',
                'merchant_id.required' => 'التاجر مطلوب',
                'merchant_id.exists' => 'التاجر غير صحيح',
                'ar_title.required' => 'عنوان المنتج بالعربية مطلوب',
                'en_title.required' => 'عنوان المنتج بالإنجليزية مطلوب',
                'ar_desc.required' => 'وصف المنتج بالعربية مطلوب',
                'en_desc.required' => 'وصف المنتج بالإنجليزية مطلوب',
                'ar_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'en_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'count.required' => 'الكمية مطلوبة',
                'old_price.required' => 'السعر قبل الخصم مطلوب',
                'old_price.integer' => 'السعر قبل الخصم لا بد أن يكون رقم',
                'new_price.required' => 'السعر بعد الخصم مطلوب',
                'new_price.integer' => 'السعر بعد الخصم لا بد أن يكون رقم',
                'deal_price.required' => 'سعر الصفقة مطلوب',
                'deal_price.integer' => 'سعر الصفقة لا بد أن يكون رقم',
                'expire_at.required' => 'تاريخ إنتهاء الصفقة مطلوب',
                'limit.required' => 'حد أكواد الخصم مطلوب',
                'limit.integer' => 'حد أكواد الخصم غير صحيح',
                'images.required' => 'صور المنتج مطلوبة',
                'images.*.image' => 'صور المنتج غير صحيحة',
            ]
        );

        $product = Product::create($request->except(    'images'));

        foreach($request->images as $image)
        {
            $name = unique_file($image->getClientOriginalName());
            $image->move(base_path().'/public/products/',$name);

            ProductImage::create
            (
                [
                    'product_id' => $product->id,
                    'image' => $name
                ]
            );
        }

        for($i = 0; $i < $product->count; $i ++)
        {
            $x = 0;

            $tmp = mt_rand(1,9);
            do
            {
                $tmp .= mt_rand(0, 9);
            }
            while(++$x < 9);

            ProductInfo::create
            (
                [
                    'status' => 'invalid',
                    'product_id' => $product->id,
                    'serial_no' => $tmp
                ]
            );
        }



        return redirect('/delegate/products/suspended')->with('success', 'تمت إضافة المنتج بنجاح,الرجاء الإنتظار لحين موافقة إدارة الموقع');
    }


    public function edit($id)
    {
        $product = Product::find($id);
        $cities = Address::where('parent_id', Auth::user()->country_id)->get();
        $cats = Category::get();
        $merchants_ids = DelegatesMerchants::where('delegate_id', Auth::user()->id)->pluck('merchant_id');
        $merchants = Merchant::whereIn('id', $merchants_ids)->get();

        return view('delegate.products.single', compact('product','cities','cats','merchants'));
    }


    public function update(Request $request)
    {
        $request->merge(['country_id', Auth::user()->country_id]);
        $this->validate($request,
            [
                'product_id' => 'required|exists:products,id,delegate_id,'.Auth::user()->id,
                'address_id' => 'required|exists:addresses,id,parent_id,'.Auth::user()->country_id,
                'cat_id' => 'required|exists:categories,id',
                'merchant_id' => 'required|exists:merchants,id|exists:delegates_merchants,merchant_id,delegate_id,'.Auth::user()->id,
                'ar_title' => 'required',
                'en_title' => 'required',
                'ar_desc' => 'required',
                'en_desc' => 'required',
                'ar_conds' => 'required',
                'en_conds' => 'required',
                'count' => 'required|integer',
                'old_price' => 'required',
                'new_price' => 'required',
                'deal_price' => 'required',
                'expire_at' => 'required|date',
                'limit' => 'required|integer',
                'images' => 'sometimes',
                'images.*' => 'image'
            ],
            [
                'address_id.required' => 'المدينة مطلوبة',
                'address_id.exists' => 'المدينة غير صحيحة',
                'cat_id.required' => 'القسم مطلوب',
                'cat_id.exists' => 'القسم غير صحيح',
                'merchant_id.required' => 'التاجر مطلوب',
                'merchant_id.exists' => 'التاجر غير صحيح',
                'ar_title.required' => 'عنوان المنتج بالعربية مطلوب',
                'en_title.required' => 'عنوان المنتج بالإنجليزية مطلوب',
                'ar_desc.required' => 'وصف المنتج بالعربية مطلوب',
                'en_desc.required' => 'وصف المنتج بالإنجليزية مطلوب',
                'ar_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'en_conds.required' => 'شروط المنتج بالعربية مطلوب',
                'count.required' => 'الكمية مطلوبة',
                'old_price.required' => 'السعر قبل الخصم مطلوب',
                'new_price.required' => 'السعر بعد الخصم مطلوب',
                'deal_price.required' => 'سعر الصفقة مطلوب',
                'expire_at.required' => 'تاريخ إنتهاء الصفقة مطلوب',
                'limit.required' => 'حد أكواد الخصم مطلوب',
                'limit.integer' => 'حد أكواد الخصم غير صحيح',
                'images.*.image' => 'صور المنتج غير صحيحة',
            ]
        );

        $product = Product::where('id', $request->product_id)->update($request->except('images'));

        foreach($request->deleted_ids as $image)
        {
            $image = ProductImage::find($image);
            unlink(base_path().'/public/products/'.$image->image);
            $image->delete();
        }

        foreach($request->images as $image)
        {
            $name = unique_file($image->getClientOriginalName());
            $image->move(base_path().'/public/products/',$name);

            ProductImage::create
            (
                [
                    'product_id' => $product->id,
                    'image' => $name
                ]
            );
        }

        if($product->status == 'active')
        {
            return redirect('/delegate/products/active')->with('success', 'تم التعديل بنجاح');
        }
        else
        {
            return redirect('/delegate/products/suspended')->with('success', 'تم التعديل بنجاح,الرجاء الإنتظار لحين موافقة إدارة الموقع');
        }

    }


    public function get_sub_cats($parent)
    {
        $cats = Category::where('parent_id', $parent)->get();
        return response()->json($cats);
    }

}
