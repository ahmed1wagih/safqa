<?php

namespace App\Http\Controllers\Delegate;

use App\Models\Withdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WithdrawController extends Controller
{
    public function index()
    {
        $withdraws = Withdraw::where('delegate_id', Auth::user()->id)->latest()->paginate(100);
        return view('delegate.withdraws.index', compact('withdraws'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'amount' => 'required|integer'
            ],
            [
                'amount.required' => 'الرجاء تحديد المبلغ',
                'amount.integer' => 'المبلغ غير صحيح'
            ]
        );

        Withdraw::create
        (
            [
                'delegate_id' => Auth::user()->id,
                'amount' => $request->amount,
                'country_id' => Auth::user()->country_id
            ]
        );

        return back()->with('success', 'تم إرسال الطلب بنجاح');
    }
}
