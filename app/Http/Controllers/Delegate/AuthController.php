<?php

namespace App\Http\Controllers\Delegate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User ;

class AuthController extends Controller
{
    public function login_view()
    {
        if(Auth::user()) return redirect('/super_admin/dashboard');
        return view('delegate.login.login');
    }


    public function login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ],
            [
                'email.required' => 'البريد الإلكتروني مطلوب',
                'password.required' => 'كلمة المرور مطلوبة'
            ]
        );

        $check = User::where('email', $request->email)->first();
        if($check->blocked == 1) return back()->with('error','الحساب موقوف,الرجاء المتابعة مع الإدارة');

       if(Auth::attempt(['email' => $request->email, 'password' => $request->password], false))
       {
           return redirect('/delegate/dashboard');
       }
       else
       {
           return back()->with('error','بيانات خاطئة');
       }
    }


    public function logout()
    {
        Auth::logout();
        return back();
    }
}
