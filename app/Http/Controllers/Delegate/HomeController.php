<?php

namespace App\Http\Controllers\Delegate;

use App\Models\Address;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function dashboard()
    {
        $deals = Product::where('delegate_id', Auth::user()->id)->get();
        $all_codes = ProductInfo::whereIn('product_id', $deals->pluck('id'))->get();
//        $products = $all_codes->pluck('product_id');

//        $fees = 0;
//        foreach($products as $product_id)
//        {
//            $this_fee = Product::where('id', $product_id)->select('deal_price')->first()->deal_price;
//            $fees = $this_fee + $fees;
//        }

        $delegate = Auth::user();
        return view('delegate.dashboard', compact('deals','all_codes','fees','delegate'));
    }


    public function profile()
    {
        return view('delegate.profile.show');
    }


    public function update_profile(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.Auth::user()->id,
                'phone' => 'required|unique:users,phone,'.Auth::user()->id,
            ],
            [
                'name.required' => 'الإسم مطلوب',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'phone.required' => 'الهاتف مطلوب',
                'phone.unique' => 'الهاتف موجود من قبل',
            ]
        );

        $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
        $user->save();

        return back()->with('success', 'تم التعديل بنجاح');
    }


    public function change_password(Request $request)
    {
        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id,type,delegate',
                'password' => 'required|min:6|confirmed',
            ]
        );

        $user = User::where('id', $request->user_id)->first();
            $user->password = Hash::make($request->password);
        $user->save();

        return back()->with('success', 'تم تغيير كلمة المرور بنجاح');
    }
}
