<?php

namespace App\Http\Controllers\Api;

use App\Models\AboutUs;
use App\Models\Address;
use App\Models\Banner;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Faq;
use App\Models\Merchant;
use App\Models\Privacy;
use App\Models\Product;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index($lang,$address_id,$user_id)
    {
        App::setLocale($lang);

        $city = Address::where('id', $address_id)->select('parent_id',$lang.'_name as name')->first();



        $banners = Banner::where('country_id', $city->parent_id)->inRandomOrder()->select('link','image','height','width')->get();

        $specials = Product::where('address_id', $address_id)->where('special', 1)->where('status', 'active')->where('count','>',0)->where('expire_at','>',Carbon::now())->inRandomOrder()->take(5)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->get();
        $latest = Product::where('address_id', $address_id)->where('special', 1)->where('status', 'active')->latest()->take(5)->inRandomOrder()
            ->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->get();
        $categories = Category::where('parent_id', NULL)->select('id', $lang.'_name as name')->get();

        foreach($specials as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        foreach($latest as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }


        foreach($categories as $category)
        {
           $category['sub_cats'] = $category->api_sub_cats($lang,$category->id);
           $category['products'] = $category->api_products($lang,$category->id,$address_id,$user_id);
        }

        foreach($banners as $banner)
        {
            $banner['image'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/banners/'.$banner->image;
        }

        return response()->json(['specials' => $specials, 'latest' => $latest, 'categories' => $categories, 'banners' => $banners]);
    }


    public function search($lang,$address_id,$user_id)
    {
        $search = Input::get('search');
        $products = Product::where('status', 'active')->where('address_id', $address_id)->where(function ($q) use($search)
            {
                $q->where('ar_title','like','%'.$search.'%');
                $q->orWhere('en_title','like','%'.$search.'%');
            }
        )->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->paginate(30);

        foreach($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return response()->json(['search' => $search, 'products' => $products]);
    }


    public function get_latest_products($lang,$address_id,$user_id)
    {
        $products = Product::where('address_id', $address_id)->where('status', 'active')->latest()->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->paginate(30);

        foreach($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return response()->json($products);
    }


    public function get_category_products($id,$lang,$address_id,$user_id)
    {
        $category = Category::where('id', $id)->select('id','parent_id')->first();

        if($category->parent_id == NULL)
        {
            $subs = Category::where('parent_id', $id)->pluck('id');
        }
        else
        {
            $subs = Category::where('id', $id)->pluck('id');
        }

        $products = $category->api_cat_products($lang,$subs,$address_id,$user_id);

        return response()->json($products);
    }


    public function get_addresses($lang)
    {
        $addresses = Address::where('parent_id', NULL)->select('id',$lang.'_name as name','image','ar_currency as currency')->get();
        foreach($addresses as $address)
        {
            $address['image'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/addresses/'.$address->image;
            $address['cities'] = $address->get_cities($lang,$address->id);
        }

        return response()->json($addresses);
    }


    public function merchants($lang)
    {
        $merchants = Merchant::where('status','active')->select('id',$lang.'_name as name','lat','lng')->get();
        return response()->json($merchants);
    }


    public function merchant_deals($lang,$merchant_id,$user_id)
    {
        $products = Product::where('merchant_id', $merchant_id)->where('status', 'active')->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->paginate(30);

        foreach($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return response()->json($products);
    }



    public function get_about($lang)
    {
        $about = AboutUs::select($lang.'_address as address', $lang.'_text as text','email','phone','lat','lng')->first();
        return response()->json($about);
    }


    public function get_terms($lang)
    {
        $text = Term::select($lang.'_text as text')->first();
        return response()->json($text);
    }


    public function get_privacy($lang)
    {
        $text = Privacy::select($lang.'_text as text')->first();
        return response()->json($text);
    }


    public function get_faqs($lang)
    {
        $text = Faq::select($lang.'_question as question',$lang.'_answer as answer')->get();
        return response()->json($text);
    }


    public function get_policy()
    {
        return view('policy');
    }
}
