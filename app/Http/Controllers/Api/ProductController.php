<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\ProductRate;
use App\Models\Wishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function show($id,$lang,$user_id)
    {
        $product = Product::where('id', $id)->select('id',$lang.'_title as title',$lang.'_desc as desc',$lang.'_conds as conds','address_id','old_price','new_price','deal_price','count','expire_at')->first();

        $discount = $product->old_price - $product->new_price;

        $product['city'] = $product->get_address($lang,$product->address_id);
        $product['currency'] = $product->get_currency($product->address_id);
        $product['images'] = $product->get_images($id);
        $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
        $product['rate'] = $product->get_rate($product->id);
        $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        $product['merchant'] = $product->api_get_merchant($lang,$product->id);
        $product['rates'] = $product->api_get_rates($lang,$product->id);
        $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
        $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());

        return response()->json($product);
    }


    public function add_rate(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'product_id' => 'required|exists:products,id',
                'rate' => 'required|between:0,5',
                'text' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        ProductRate::updateOrcreate
        (
            [
                'product_id' => $request->product_id,
                'user_id' => $request->user_id,
            ],
            [
                'rate' => $request->rate,
                'text' => $request->text
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'rate inserted successfully']);
    }


    public function favorites($lang,$user_id)
    {
        $ids = Wishlist::where('user_id', $user_id)->pluck('product_id');
        $products = Product::whereIn('id', $ids)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->paginate(20);
        foreach($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return response()->json($products);
    }


    public function add_favorite(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'product_id' => 'required|exists:products,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $favorite = Wishlist::where('user_id', $request->user_id)->where('product_id', $request->product_id)->first();

        if($favorite)
        {
            $favorite->delete();

            return response()->json(['status' => 'success', 'msg' => 'favorite removed successfully']);
        }
        else
        {
            Wishlist::create
            (
                [
                    'user_id' => $request->user_id,
                    'product_id' => $request->product_id,
                ]
            );

            return response()->json(['status' => 'success', 'msg' => 'favorite stored successfully']);
        }
    }
}
