<?php

namespace App\Http\Controllers\Api;

use App\Models\Cart;
use App\Models\FooterStates;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function show($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $carts = Cart::where('user_id', $request->user_id)->select('id as cart_id','product_id as id')->paginate(30);

        foreach($carts as $product)
        {
            $this_product = Product::where('id', $product->id)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->first();

            $discount = $this_product->old_price - $this_product->new_price;

            $product['id'] = $this_product->id;
            $product['title'] = $this_product->title;
            $product['address_id'] = $this_product->address_id;
            $product['old_price'] = $this_product->old_price;
            $product['new_price'] = $this_product->new_price;
            $product['deal_price'] = $this_product->deal_price;
            $product['expire_at'] = strtotime(Carbon::parse($this_product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $this_product->get_address($lang,$this_product->address_id);
            $product['currency'] = $this_product->get_currency($this_product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$this_product->get_cover($this_product->id);
            $product['discount'] = round($discount / $this_product->old_price * 100,2) . '%';
            $product['rate'] = $this_product->get_rate($this_product->id);
            $product['is_favorite'] = $this_product->api_is_like($product->id,$request->user_id);
        }

        $total = $carts->sum('deal_price');
        $credit = User::where('id', $request->user_id)->select('credit')->first()->credit;

        $after = $credit - $total;
        
        return response()->json(['products' => $carts, 'total' => $total, 'credit_points' => $credit, 'credit_after' => $after]);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'product_id' => 'required|exists:products,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $product = Product::where('id', $request->product_id)->select('id','expire_at')->first();

        if($product->expire_at < Carbon::now())
        {
            Cart::where('user_id', $request->user_id)->where('product_id', $product->id)->delete();
            return response()->json(['status' => 'failed', 'msg' => 'expired product']);
        }

        Cart::create
        (
            [
                'user_id' => $request->user_id,
                'product_id' => $request->product_id
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'added to cart successfully']);
    }


    public function remove(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'cart_id' => 'required|exists:carts,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        Cart::where('user_id', $request->user_id)->where('id', $request->cart_id)->delete();

        return response()->json(['status' => 'success', 'msg' => 'removed from cart successfully']);
    }


    public function checkout(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }


        $carts = Cart::where('user_id', $request->user_id)->pluck('product_id', 'id');

        foreach($carts as $id => $product_id)
        {
            $product = Product::where('id', $product_id)->select('id','by','count','deal_price','expire_at','country_id','old_price','new_price','limit')->first();

            if($product->expire_at < Carbon::now())
            {
                Cart::where('user_id', $request->user_id)->where('product_id', $product->id)->delete();
                return response()->json(['status' => 'failed', 'msg' => 'expired product']);
            }

            $user = User::where('id', $request->user_id)->select('id','credit','name','email')->first();

            if($product->count <= 0)
            {
                return response()->json(['status' => 'error', 'msg' => 'product out of stock']);
            }

            $count = ProductInfo::where('product_id',$product_id)->where('user_id', $user->id)->count();

            if($count >= $product->limit)
            {
                Cart::where('user_id', $user->id)->where('product_id', $product->id)->delete();
                return response()->json(['status' => 'error', 'msg' => 'product purchases exceeded limit']);
            }

            if($user->credit >= $product->deal_price)
            {
                $code = ProductInfo::where('product_id', $product_id)->where('user_id', NULL)->first();
                    $code->user_id = $request->user_id;
                    $code->purchased_at = Carbon::now()->toDateTimeString();
                $code->save();

                Cart::where('id', $id)->delete();

                $user->credit = $user->credit - $product->deal_price;
                $user->save();

                $product->count = $product->count - 1;
                $product->save();

                $footer_states = FooterStates::where('country_id', $product->country_id)->first();
                    $footer_states->total_sold = $footer_states->total_sold + 1;
                    $footer_states->total_saved = $product->old_price - $product->new_price;
                $footer_states->save();


//
//                Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
//                    $message->from('info@bestdeal.market','Sales@BestDeal')
//                        ->to($user->email)
//                        ->subject('بيانات صفقتك | Best Deal');
//                });
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'insufficient credit']);
            }
        }


        return response()->json(['status' => 'success', 'msg' => 'checked out successfully']);
    }
}
