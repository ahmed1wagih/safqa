<?php

namespace App\Http\Controllers\Api;

use App\Models\Address;
use App\Models\Code;
use App\Models\ContactUs;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\Setting;
use App\Models\Sms;
use App\Models\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'address_id' => 'required|exists:addresses,id',
                'gender' => 'required|in:male,female',
                'age' => 'required',
                'password' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $email_check = User::where('email', $request->email)->first();
        $phone_check = User::where('phone', $request->phone)->first();

        if($email_check) return response()->json(['status' => 'failed', 'msg' => 'email already exists']);
        if($phone_check) return response()->json(['status' => 'failed', 'msg' => 'phone already exists']);

        $country_id = Address::where('id', $request->address_id)->select('parent_id')->first()->parent_id;

        $user = User::create
        (
            [
                'type' => 'user',
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'country_id' => $country_id,
                'address_id' => $request->address_id,
                'gender' => $request->gender,
                'age' => $request->age,
                'password' => Hash::make($request->password),
                'credit' => Setting::first()->credit_on_register
            ]
        );

        $code = Code::updateOrCreate
        (
            [
                'phone' => $user->phone,
                'type' => 'activation'
            ],
            [
                'code' => rand(1000,9999),
                'expire_at' => Carbon::now()->addHour()->toDateTimeString()
            ]
        );

        $data = [
            'name' => $user->name,
            'subject' => 'كود التفعيل | BestDeal',
            'content' => 'هذا هو كود التفعيل في تطبيق BestDeal'.'<br/>'.$code->code.'<br/> و شكراً',
            'code' => $code->code
        ];


        Mail::send('admin.emails.email',$data, function ($message) use ($data,$user) {
            $message->from('info@bestdeal.market','Info@BestDeal')
                ->to($user->email)
                ->subject('كود التفعيل | Best Deal');
        });

//        $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
//        Sms::send($msg,$user->phone);

        return response()->json(['status' => 'success', 'msg' => 'registered', 'code' => $code->code]);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $user = User::where('email', $request->email)->select('id','status','password')->first();

        if($user)
        {
            if($user->blocked == 1) return response()->json(['status' => 'failed', 'msg' => 'invalid email']);

            if($user->status == 'suspended')
            {
                $code = Code::updateOrCreate
                (
                    [
                        'type' => 'activation',
                        'phone' => $user->phone
                    ],
                    [
                        'code' => rand(1000,9999),
                        'expire_at' => Carbon::now()->addHour()->toDateTimeString()
                    ]
                );

                $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
                Sms::send($msg,$user->phone);

                return response()->json(['status' => 'not_active', 'msg' => 'user is not active,code has been sent', 'code' => $code->code]);
            }
            else
            {
                $check = Hash::check($request->password,$user->password);

                if($check)
                {
                    return response()->json(['status' => 'success', 'msg' => 'logged in', 'user_id' => $user->id]);
                }
                else
                {
                    return response()->json(['status' => 'failed', 'msg' => 'invalid password']);
                }
            }
        }
        else
        {
            return response()->json(['status' => 'failed', 'msg' => 'invalid email']);
        }
    }


    public function code_send(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|exists:users,email',
                'type' => 'required|in:activation,reset'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $user = User::where('email',$request->email)->first();

        $code = Code::updateOrCreate
        (
            [
                'type' => $request->type,
                'phone' => $user->phone
            ],
            [
                'code' => rand(1000,9999),
                'expire_at' => Carbon::now()->addHour()->toDateTimeString()
            ]
        );

        if($request->type == 'activation') $msg = 'كود التفعيل في Best Deal ' . $code->code . ',وشكراً';
        else $msg = 'كود تعيين كلمة المرور في Best Deal ' . $code->code . ',وشكراً';

        Sms::send($msg,$user->phone);

        return response()->json(['status' => 'success', 'msg' => 'code send', 'code' => $code->code]);
    }


    public function code_check(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'code' => 'required',
                'type' => 'required|in:activation,reset'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $check = Code::where('type', $request->type)->where('code', $request->code)->first();

        if($check)
        {
            $user = User::Where('phone', $check->phone)->select('id','status')->first();

            if($check->type == 'activation')
            {
                $user->status = 'active';
                $user->save();

                $check->delete();

                return response()->json(['status' =>'success', 'msg' => 'activated', 'user_id' => $user->id]);
            }
            else
            {
                return response()->json(['status' => 'success', 'msg' => 'code matched', 'user_id' => $user->id]);
            }
        }
        else
        {
            return response()->json(['status' => 'failed', 'msg' => 'invalid code']);
        }
    }


    public function reset_password(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'password' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $user = User::where('id', $request->user_id)->select('id','password')->first();
            $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(['status' => 'success', 'msg' => 'password changed']);
    }


    public function profile_show($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $user = User::where('id', $request->user_id)->select('id','name','email','phone','address_id','gender','age')->first();
        $user['city'] = $user->get_address($lang,$user->address_id);

        return response()->json($user);
    }


    public function profile_update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'gender' => 'required',
                'age' => 'required',
                'address_id' => 'required|exists:addresses,id'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $email_check = User::where('email', $request->email)->where('id','!=',$request->user_id)->first();
        $phone_check = User::where('phone', $request->phone)->where('id','!=',$request->user_id)->first();

        if($email_check) return response()->json(['status' => 'failed', 'msg' => 'email already exists']);
        if($phone_check) return response()->json(['status' => 'failed', 'msg' => 'phone already exists']);

        $user = User::where('id', $request->user_id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->gender = $request->gender;
            $user->age = $request->age;
            $user->address_id = $request->address_id;
        $user->save();

        return response()->json(['status' => 'success', 'msg' => 'info updated']);
    }


    public function my_deals($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $products = ProductInfo::where('user_id', $request->user_id)->select('id as deal_id','product_id as id')->paginate(20);
        $deals_points = 0;
        $deals_saved = 0;

        foreach($products as $product)
        {
            $this_product = Product::where('id', $product->id)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->first();

            $discount = $this_product->old_price - $this_product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($this_product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['title'] = $this_product->title;
            $product['address_id'] = $this_product->address_id;
            $product['old_price'] = $this_product->old_price;
            $product['new_price'] = $this_product->new_price;
            $product['deal_price'] = $this_product->deal_price;
            $product['expire_at'] = $this_product->expire_at;
            $product['city'] = $this_product->get_address($lang,$this_product->address_id);
            $product['currency'] = $this_product->get_currency($this_product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$this_product->get_cover($this_product->id);
            $product['discount'] = (string) round($discount / $this_product->old_price * 100,2) . '%';
            $product['rate'] = $this_product->get_rate($this_product->id);
            $product['is_favorite'] = $this_product->api_is_like($this_product->id,$request->user_id);

            $deals_points = $deals_points + $product->deal_price;
            $saved = $product->old_price - $product->new_price;
            $deals_saved = $deals_saved + $saved;
        }

        $deals_count = $products->count();

        return response()->json(['deals' => $products, 'deals_count' => $deals_count, 'deals_points' => $deals_points, 'deals_saved' => $deals_saved]);
    }


    public function deal_view($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'deal_id' => 'required|exists:product_infos,id'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $product = ProductInfo::where('user_id', $request->user_id)->where('id', $request->deal_id)->select('id as deal_id','product_id as id','serial_no','status','purchased_at')->first();
        $this_product = Product::where('id', $product->id)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price')->first();

        $discount = $this_product->old_price - $this_product->new_price;

        $product['expire_at'] = strtotime(Carbon::parse($this_product->expire_at)->toDateTimeString());
        $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
        $product['title'] = $this_product->title;
        $product['address_id'] = $this_product->address_id;
        $product['old_price'] = $this_product->old_price;
        $product['new_price'] = $this_product->new_price;
        $product['deal_price'] = $this_product->deal_price;
        $product['city'] = $this_product->get_address($lang,$this_product->address_id);
        $product['currency'] = $this_product->get_currency($this_product->address_id);
        $product['images'] =  $this_product->get_images($this_product->id);
        $product['discount'] = (string) round($discount / $this_product->old_price * 100,2) . '%';
        $product['rate'] = $this_product->get_rate($this_product->id);
        $product['is_favorite'] = $this_product->api_is_like($this_product->id,$request->user_id);
        $product['status'] = $this_product->api_status($lang,$product->status);

        unset($product->deal_id);

        return response()->json($product);
    }


    public function my_credit(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $credit = User::where('id', $request->user_id)->select('credit')->first();

        return response()->json($credit);
    }


    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'title' => 'required',
                'desc' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        ContactUs::create
        (
            [
                'name' => $request->email,
                'email' => $request->email,
                'title' => $request->title,
                'desc' => $request->desc,
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'contacted successfully']);
    }
}
