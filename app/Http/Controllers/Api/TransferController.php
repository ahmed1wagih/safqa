<?php

namespace App\Http\Controllers\Api;

use App\Models\Address;
use App\Models\BankAccount;
use App\Models\Transfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TransferController extends Controller
{
    public function get_banks($lang,$address_id)
    {
//        $country_id = Address::where('id', $address_id)->select('parent_id')->first()->parent_id;
        $banks = BankAccount::where('country_id', $address_id)->select('id',$lang.'_name as name','number','iban')->get();

        return response()->json($banks);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'bank_id' => 'required|exists:bank_accounts,id',
                'name' => 'required',
                'amount' => 'required',
                'date' => 'required|date',
                'trans_no' => 'required',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $trans_check = Transfer::where('trans_no', $request->trans_no)->first();
        if($trans_check) return response()->json(['status' => 'failed', 'msg' => 'trans no already exists']);

        Transfer::create
        (
            [
                'user_id' => $request->user_id,
                'bank_id' => $request->bank_id,
                'country_id' => BankAccount::where('id', $request->bank_id)->select('country_id')->first()->country_id,
                'name' => $request->name,
                'amount' => $request->amount,
                'date' => $request->date,
                'trans_no' => $request->trans_no,
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'transfer stored successfully']);

    }


    public function my_transfers($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $transfers = Transfer::where('user_id', $request->user_id)->select('status','name','country_id as country','bank_id as bank','trans_no','amount','date')->get();

        foreach($transfers as $transfer)
        {
            $transfer['country'] = $transfer->get_country($lang,$transfer->country);
            $transfer['bank'] = $transfer->get_bank($lang,$transfer->bank);
        }

        return response()->json($transfers);
    }
}
