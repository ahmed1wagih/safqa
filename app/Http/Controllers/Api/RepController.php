<?php

namespace App\Http\Controllers\Api;

use App\Models\MerchantRep;
use App\Models\Product;
use App\Models\ProductInfo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RepController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required'
            ]
        );

        if($validator->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $cashier = MerchantRep::where('email', $request->email)->where('status', 'active')->select('id', 'merchant_id', 'name', 'email', 'phone','password')->first();

        if($cashier)
        {
            $check = Hash::check($request->password, $cashier->password);

            if($check)
            {
                unset($cashier->password);
                return response()->json(['status' => 'success', 'msg' => 'logged_id', 'cashier' => $cashier]);
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'invalid_password']);
            }
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid_data or blocked']);
        }

    }


    public function code_check($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'cashier_id' => 'required|exists:merchant_reps,id,merchant_id,'.$request->merchant_id,
                'merchant_id' => 'required|exists:merchants,id',
                'serial' => 'required'
            ]
        );


        if($validator->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }


        $products = Product::where('merchant_id', $request->merchant_id)->pluck('id');
        $code = ProductInfo::where('serial_no',$request->serial)->whereIn('product_id',$products)->first();

        if($code)
        {           
            if($code->status == 'valid')
            {
                $product = Product::where('id',$code->product_id)->select('id','address_id',$lang.'_title as title',$lang.'_desc as desc',$lang.'_conds as conds','old_price','new_price')->first();
                $product['currency'] = $product->get_currency($product->address_id);
                $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
                $product['serial'] = $code->serial_no;

                return response()->json(['status' => 'success','msg' => 'code found','product' => $product]);
            }
            else
            {
                return response()->json(['status' => 'error','msg' => 'code already used']);
            }
        }
        else
        {
            return response()->json(['status' => 'error','msg' => 'code not found']);
        }
    }


    public function code_checkout(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'cashier_id' => 'required|exists:merchant_reps,id,merchant_id,'.$request->merchant_id,
                'merchant_id' => 'required|exists:merchants,id',
                'serial' => 'required'
            ]
        );


        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }


        $products = Product::where('merchant_id', $request->merchant_id)->pluck('id');
        $code = ProductInfo::where('serial_no',$request->serial)->whereIn('product_id',$products)->where('status','valid')->first();

        if($code)
        {
            $code->status = 'invalid';
            $code->rep_id = $request->cashier_id;
            $code->purchased_at = Carbon::now();
            $code->save();

            return response()->json(['status' => 'success','msg' => 'code checked out successfully']);
        }
        else
        {
            return response()->json(['status' => 'error','msg' => 'invalid_code']);
        }
    }


    public function get_previous_scans($lang,Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'cashier_id' => 'required|exists:merchant_reps,id,merchant_id,'.$request->merchant_id,
                'merchant_id' => 'required|exists:merchants,id',
            ]
        );


        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }


        $codes = ProductInfo::where('rep_id', $request->cashier_id)->select('product_id','serial_no','purchased_at')->get();
       
        foreach($codes as $code)
        {
            $product = Product::where('id',$code->product_id)->select('id','address_id',$lang.'_title as title','old_price','new_price')->first();
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            
            $code['product'] = $product;
           
            unset($code->product_id,$product->id,$product->address_id);
        }

        return response()->json($codes);
    }

}
