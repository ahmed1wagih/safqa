<?php

namespace App\Http\Controllers\Api;

use App\Models\Address;
use App\Models\CurrencyExchange;
use App\Models\Order;
use App\Models\Pack;
use App\Models\User;
use Damas\Paytabs\paytabs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PayTabsController extends Controller
{
    public function get_currencies($lang)
    {
        $currencies = Address::where('parent_id', NULL)->select($lang.'_name as name','ar_currency as currency')->get();
        return response()->json($currencies);
    }


    public function get_packs()
    {
        $packs = Pack::select('amount')->get();
        $exchange = CurrencyExchange::where('country_id', 1)->first();

        foreach($packs as $pack)
        {
            $pack['points'] = $pack->amount * $exchange->point_per_currency;
        }

        return response()->json($packs);
    }



    public function pay(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'type' => 'required|in:paytabs,paypal',
                'amount' => 'required|exists:packs,amount',
                'currency' => 'required|exists:addresses,ar_currency',
                'first_name' => 'required',
                'last_name' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $user = User::where('id', $request->user_id)->first();

        $order = new Order();
            $order->trans_id = str_random(50);
            $order->user_id = $user->id;
            $order->amount = $request->amount;
            $order->currency = $request->currency;
        $order->save();

        $payload =
            [
                "merchant_email" => "paytaps@bestdeal.market",
                'secret_key' => "jtOtMmWU0eFYTxlPuldakIDDxqYjJPx3ilx5ItZJIFIBc3qHoDRImumUVkdEaKIs6fyPIyyqdg2qjs7I6ZqsuNmp7A4hlMKr1jgK",
                'title' => 'BestDeal Charge',
                'cc_first_name' => $request->first_name,
                'cc_last_name' => $request->last_name,
                'email' => $user->email,
                'cc_phone_number' => $user->phone,
                'phone_number' => $user->phone,
                'billing_address' => 'address address',
                'city' => 'city  city',
                'state' => 'state state',
                'postal_code' => "97300",
                'country' => 'SAU',
                'address_shipping' => 'country country',
                'city_shipping' => 'city city',
                'state_shipping' => 'state state',
                'postal_code_shipping' => "97300",
                'country_shipping' => 'SAU',
                "products_per_title"=> "BestDeal Points Charge",
                'currency' => $order->currency,
                "unit_price"=> $request->amount,
                'quantity' => "1",
                'other_charges' => "0",
                'amount' => $request->amount,
                'discount'=>"0",
                "msg_lang" => "english",
                "reference_no" =>  $order->trans_id,
                "site_url" => "https://bestdeal.market",
                'return_url' => "https://bestdeal.market/payment/result",
                "cms_with_version" => "API USING PHP",
            ];

         return response()->json($payload);
    }


    public function set_reference(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'p_id' => 'required',
                'reference_no' => 'required|exists:orders,trans_id,status,awaiting',
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'failed', 'msg' => $validator->messages()]);
        }

        $order = Order::where('trans_id', $request->reference_no)->first();
            $order->ref_id = $request->p_id;
        $order->save();

        return response()->json(['status' => 'success', 'msg' => 'order updated successfully']);
    }
}
