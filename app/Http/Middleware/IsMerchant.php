<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsMerchant
{


    public function handle($request, Closure $next)
    {
            if (Auth::guard('merchant')->user() && Auth::guard('merchant')->user()->status == 'active')
            {
                return $next($request);
            }
            elseif(Auth::guard('merchant')->user() && Auth::guard('merchant')->user()->status == 'suspended')
            {
                Auth::logout();
                return redirect('/merchant/login')->with('error','عفواً,الحساب موقوف من قبل إدارة الموقع');
            }
            else
            {
                Auth::guard('merchant')->logout();
                return redirect('/merchant/login')->with('error','الرجاء تسجيل الدخول');
            }
    }


}
