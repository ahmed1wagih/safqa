<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{


    public function handle($request, Closure $next)
    {
        if(Auth::guard('admin')->user() && Auth::guard('admin')->user()->status == 'active')
        {
            return $next($request);
        }
        elseif(Auth::guard('admin')->user() && Auth::guard('admin')->user()->status == 'suspended')
        {
            Auth::guard('admin')->logout();
            return redirect('/admin/login')->with('error','عفواً,الحساب موقوف من قبل إدارة الموقع');
        }
        else
        {
            return redirect('/admin/login')->with('error','الرجاء تسجيل الدخول');
        }
    }


}
