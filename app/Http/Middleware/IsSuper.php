<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsSuper
{


    public function handle($request, Closure $next)
    {

        if(Auth::guard('super')->user() && Auth::guard('super')->user()->status == 'active')
        {

            return $next($request);
        }
        elseif(Auth::guard('super')->user() && Auth::guard('super')->user()->status == 'suspended')
        {
            Auth::guard('super')->logout();
            return redirect('/super_admin/login')->with('error','عفواً,الحساب موقوف من قبل إدارة الموقع');
        }
        else
        {
            return redirect('/super_admin/login')->with('error','الرجاء تسجيل الدخول');
        }
    }


}
