<?php

namespace App\Http\Middleware;

use App\Models\Address;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IsUser
{


    public function handle($request, Closure $next)
    {
        if(user() && user()->status == 'active')
        {
            Session::put('country',user()->country);
            return $next($request);
        }
        elseif(user() && user()->status == 'suspended')
        {
            Session::put('country',user()->country);
            Auth::guard('user')->logout();
            return redirect('/login')->with('error','account_suspended_please_verify');
        }
        elseif(user() && user()->blocked == 1)
        {
            Session::put('country',user()->country);
            Auth::guard('user')->logout();
            return redirect('/login')->with('error','account_blocked');
        }
        else
        {
            return redirect('/login')->with('error','please_login');
        }
    }


}
