<?php

use App\Models\Address;
use \Illuminate\Support\Facades\Session;

function unique_file($fileName)
{
    $fileName = str_replace(' ', '-', $fileName);
    return time().uniqid().'-'.$fileName;
}

function super()
{
    return \Illuminate\Support\Facades\Auth::guard('super')->user();
}

function admin()
{
    return \Illuminate\Support\Facades\Auth::guard('admin')->user();
}


function merchant()
{
    return \Illuminate\Support\Facades\Auth::guard('merchant')->user();
}


function user()
{
    return \Illuminate\Support\Facades\Auth::guard('user')->user();
}


function lang()
{
    return App::getLocale();
}


function country()
{

     return Session::get('country');
}


function city()
{
    return Session::get('city');
}

