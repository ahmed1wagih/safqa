<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    protected $rememberTokenName = false;

    protected $fillable = [
        'type','status','country_id','city_id','name', 'email', 'phone','gender', 'age', 'password','credit','expire_at'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'delegate_id');
    }


    public function country()
    {
        return $this->belongsTo(Address::class, 'country_id');
    }


    public function city()
    {
        return $this->belongsTo(Address::class, 'city_id');
    }


    public function pack()
    {
        return $this->belongsTo(Pack::class, 'pack_id');
    }


    public function purchased($user_id)
    {
        $ids = ProductInfo::where('user_id', $user_id)->select('product_id')->get();
        $total = 0;

        foreach($ids as $id)
        {
            $product = Product::where('id', $id->product_id)->select('deal_price')->first();
            $total = $product->deal_price + $total;
        }

        return $total;
    }


    public function money_saved($user_id)
    {
        $ids = ProductInfo::where('user_id', $user_id)->select('product_id')->get();
        $total = 0;

        foreach($ids as $id)
        {
            $product = Product::where('id', $id->product_id)->select('old_price','new_price')->first();

            $saved = $product->old_price - $product->new_price;

            $total += $saved;
        }

        return $total;
    }


    public function sold_products($id)
    {
        $products = Product::where('delegate_id', $id)->pluck('id');
        $barcodes = ProductInfo::whereIn('product_id', $products)->where('user_id','!=',NULL)->count();

        return $barcodes;
    }


    public function get_address($lang,$id)
    {
        $address= Address::where('id', $id)->select($lang.'_name as name')->first()->name;
        return $address;
    }


    public function get_merchants($delegate_id)
    {
        $count = DelegatesMerchants::where('delegate_id', $delegate_id)->count();
        return $count;
    }
}
