<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable =
        [
            'user_id','status','type','trans_id','currency','amount'
        ];


    public function pack()
    {
        return $this->belongsTo(Pack::class,'pack_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
