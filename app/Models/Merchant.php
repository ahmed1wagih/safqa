<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Merchant extends Authenticatable
{
    protected $rememberTokenName = false;

    protected $fillable =
        [
            'status','ar_name','en_name','ar_desc','en_desc','ar_address','en_address','link','lat','lng','email'
        ];


    public function country()
    {
        return $this->belongsTo(Address::class, 'country_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'merchant_id');
    }


    public function get_rate($id)
    {
        $ids = Product::where('merchant_id', $id)->pluck('id');
        $rates = ProductRate::whereIn('product_id', $ids)->pluck('rate');

        $sum = $rates->sum();
        $count = $rates->count();

        if($count == 0)
        {
            return 0;
        }
        else
        {
            $average = $sum / $count ;
            return (integer) $average;
        }
    }


    public function get_rate_count($id)
    {
        $ids = Product::where('merchant_id', $id)->pluck('id');
        $rates = ProductRate::whereIn('product_id', $ids)->pluck('rate')->count();

        return $rates;
    }


    public function check_sub($delegate_id,$merchant_id)
    {
        $check = DelegatesMerchants::where('delegate_id', $delegate_id)->where('merchant_id', $merchant_id)->first();
        return boolval($check);
    }


    public function getLinkAttribute($value)
    {
        if(strpos($value,'http') === 0) return $value;
        else return 'http://'.$value;
    }
}
