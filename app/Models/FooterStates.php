<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FooterStates extends Model
{
    protected $fillable =
        [
            'country_id','total_sold','total_saved'
        ];
}
