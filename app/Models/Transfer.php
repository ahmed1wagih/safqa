<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $fillable =
        [
            'user_id','status','name','bank_id','country_id','trans_no','pack_id','price','date'
        ];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }


    public function order()
    {
        return $this->hasOne(Order::class, 'trans_id');
    }


    public function pack()
    {
        return $this->belongsTo(Pack::class,'pack_id');
    }


    public function product($id)
    {
        $product = Product::where('id', $id)->select('id','address_id','ar_title','deal_price')->first();
        $product['ar_currency'] = $product->city->parent->ar_currency;

        return $product;
    }


    public function bank()
    {
        return $this->belongsTo(BankAccount::class, 'bank_id');
    }


    public function get_country($lang,$country_id)
    {
        $name = Address::where('id', $country_id)->select($lang.'_name as name')->first()->name;
        return $name;
    }


    public function get_bank($lang,$bank_id)
    {
        $name = BankAccount::where('id', $bank_id)->select($lang.'_name as name')->first()->name;
        return $name;
    }



}
