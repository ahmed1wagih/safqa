<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailList extends Model
{
    protected $fillable =
        [
            'country_ids','title','users_count'
        ];


    public function getCountryIdsAttribute($value)
    {
        return json_decode($value);
    }
}
