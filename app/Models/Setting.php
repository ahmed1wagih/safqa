<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable =
        [
            'ar_name','en_name','footer_rights','credit_on_register','delegate_fee'
        ];
}
