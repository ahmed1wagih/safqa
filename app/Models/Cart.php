<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    protected $fillable =
        [
            'user_id','product_id'
        ];


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    public static function get_cart_total()
    {
        $carts = Cart::where('user_id', user()->id)
            ->join('products','carts.product_id','products.id')->select('carts.id','carts.product_id','products.address_id','cat_id','ar_title as title','old_price','new_price','deal_price')
            ->get();

        $total = 0;

        foreach($carts as $cart)
        {
            $total = $cart->deal_price + $total;
        }

        return $total;
    }
}
