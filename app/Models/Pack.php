<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $fillable =
        [
            'status','country_id','ar_name','en_name','points','months_count','price'
        ];


    public function country()
    {
        return $this->belongsTo(Address::class,'country_id');
    }
}
