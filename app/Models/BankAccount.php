<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable =
        [
            'country_id','ar_name','en_name','number','iban'
        ];


    public function country()
    {
        return $this->belongsTo(Address::class, 'country_id');
    }
}
