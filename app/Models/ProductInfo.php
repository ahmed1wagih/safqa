<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInfo extends Model
{
    protected $fillable =
        [
            'product_id','serial_no','status','purchased_at'
        ];


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function rep()
    {
        return $this->belongsTo(MerchantRep::class, 'rep_id');
    }


    public function scopeChange_status($query,$status)
    {
        if ($status == 'active')
        {
            $query->update(['status' => 'valid']);
        }
        else
        {
            $query->update(['status' => 'invalid']);
        }

        return $query;
    }
}
