<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Super extends Authenticatable
{
    protected $rememberTokenName = false;

    protected $table = 'super_admins';
}
