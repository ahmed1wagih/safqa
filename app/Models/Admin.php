<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $rememberTokenName = false;

    protected $table = 'country_admins';

    public function country()
    {
        return $this->belongsTo(Address::class,'country_id');
    }
}
