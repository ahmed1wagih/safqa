<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Address extends Model
{
    protected $fillable =
        [
            'parent_id','ar_name','en_name','code'
        ];


    public static function get_address($parent)
    {
        $name = Address::where('id', $parent)->first()->ar_name;
        return $name;
    }


    public function parent()
    {
        return $this->belongsTo(Address::class, 'parent_id');
    }


    public function cities()
    {
        return $this->hasMany(Address::class, 'parent_id');
    }


    public function active_cities()
    {
        return $this->hasMany(Address::class, 'parent_id')->where('status','active');
    }



    public function get_cities($lang,$id)
    {
        $cities = Address::where('parent_id', $id)->has('products')->select('id',$lang.'_name as name')->get();
        return $cities;
    }


    public function bank_accounts()
    {
        return $this->hasMany(BankAccount::class, 'country_id');
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'address_id');
    }


    public function active_products()
    {
        return $this->hasMany(Product::class, 'address_id')->where('status','active');
    }

    public function exchange()
    {
        return $this->hasOne(CurrencyExchange::class, 'country_id');
    }

}
