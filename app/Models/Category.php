<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Category extends Model
{
    protected $fillable =
        [
            'parent_id','ar_name','en_name'
        ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }


    public function sub_cats()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }


    public function api_sub_cats($lang,$id)
    {
        $subs = Category::where('parent_id', $id)->select('id',$lang.'_name as name')->get();
        return $subs;
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'cat_id');
    }


    public function api_products($lang,$id,$address_id,$user_id)
    {
        $sub_cats = Category::where('parent_id', $id)->pluck('id');
        $products = Product::whereIn('cat_id', $sub_cats)->where('status', 'active')->where('address_id', $address_id)->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->take(6)->get();

        foreach ($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return $products;
    }


    public function api_cat_products($lang,$subs,$address_id,$user_id)
    {
        $products = Product::whereIn('cat_id', $subs)->where('address_id', $address_id)->where('status', 'active')->select('id',$lang.'_title as title','address_id','old_price','new_price','deal_price','expire_at')->paginate(30);

        foreach ($products as $product)
        {
            $discount = $product->old_price - $product->new_price;

            $product['expire_at'] = strtotime(Carbon::parse($product->expire_at)->toDateTimeString());
            $product['now_timestamp'] = strtotime(Carbon::now()->toDateTimeString());
            $product['city'] = $product->get_address($lang,$product->address_id);
            $product['currency'] = $product->get_currency($product->address_id);
            $product['cover'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$product->get_cover($product->id);
            $product['discount'] = (string) round($discount / $product->old_price * 100,2) . '%';
            $product['rate'] = $product->get_rate($product->id);
            $product['is_favorite'] = $product->api_is_like($product->id,$user_id);
        }

        return $products;
    }


    public function all_products($parent)
    {
        $cats = Category::where('parent_id', $parent)->get();
        $n = 0;

        foreach($cats as $cat)
        {
            $n = $n + $cat->products->count();
        }

        return $n;
    }


    public function home_products($parent)
    {
        $country = Address::where('id', Session::get('country')->id)->first();
        $city = Address::where('id', Session::get('city')->id)->first();

        $sub_cats = Category::where('parent_id', $parent)->pluck('id');
        $products = Product::where('country_id', $country->id)->where('status', 'active')->where('address_id', $city->id)->whereIn('cat_id', $sub_cats)->where('expire_at', '>=', Carbon::today()->toDateString())->inRandomOrder()->take(3)->get();

        return $products;
    }


    public function header_products()
    {
        return $this->hasMany(Product::class, 'cat_id')->where('status', 'active')->where('expire_at', '>=', Carbon::today()->toDateString())->inRandomOrder()->take(3);
    }


    public static function get_cat($id)
    {
        $cat = Category::find($id);
        return $cat;
    }


    public function global_sub_cats($parent)
    {
        $sub_cats = Category::where('parent_id', $parent)->take(6)->get();
        return $sub_cats;
    }


    public static function getBanner()
    {
        return Banner::where('country_id',country()->id)->inRandomOrder()->select('id','image','height','link','width')->first();
    }
}
