<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DelegatesMerchants extends Model
{
    protected $table = 'delegates_merchants';
    protected $fillable =
        [
            'delegate_id','merchant_id'
        ];
}
