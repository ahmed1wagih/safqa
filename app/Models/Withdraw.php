<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Withdraw extends Model
{
    protected $fillable =
        [
            'delegate_id','amount','country_id'
        ];


    public function delegate()
    {
        return $this->belongsTo(User::class, 'delegate_id');
    }

    public function amount_to_points($amount)
    {
        $value = CurrencyExchange::where('country_id', user()->country_id)->select('point_per_currency')->first()->point_per_currency;
        $total = $amount / $value;

        return $total;
    }
}
