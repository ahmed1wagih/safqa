<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    protected $fillable =
        [
            'user_id', 'product_id', 'rate', 'text'
        ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    public function get_user($id)
    {
        $user = User::where('id', $id)->select('name')->first();
        return $user->name;
    }
}
