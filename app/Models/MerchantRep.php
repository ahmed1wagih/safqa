<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantRep extends Model
{
    public function codes()
    {
        return $this->hasMany(ProductInfo::class, 'rep_id');
    }
}
