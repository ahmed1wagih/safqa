<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    protected $fillable =
        [
            'status','special','count','cat_id','merchant_id','country_id','address_id','delegate_id','ar_title','en_title','ar_desc','en_desc','ar_conds','en_conds','old_price','new_price','deal_price','expire_at','limit'
        ];


    public function category()
    {
        return $this->belongsTo(Category::class, 'cat_id');
    }


    public function country()
    {
        return $this->belongsTo(Address::class, 'country_id');
    }


    public function city()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }


    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id');
    }


    public function delegate()
    {
        return $this->belongsTo(User::class, 'delegate_id');
    }


    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }


    public function rates()
    {
        return $this->hasMany(ProductRate::class, 'product_id');
    }


    public function cover()
    {
        return $this->hasOne(ProductImage::class, 'product_id');
    }


    public function get_discount($id)
    {
        $product = Product::find($id);
        $percentage = $product->new_price / $product->old_price * 100;
        $percentage = round(100 - $percentage);

        return $percentage;
    }


    public function get_rate($id)
    {
        $rates = ProductRate::where('product_id', $id)->pluck('rate');

        $sum = $rates->sum();
        $count = $rates->count();

        if($count == 0)
        {
            return 0;
        }
        else
        {
            $average = $sum / $count ;
            return (integer) $average;
        }
    }


    public function get_rate_count($id)
    {
        $rates = ProductRate::where('product_id', $id)->count();
        return $rates;
    }


    public function best_rated($cat_id)
    {

    }


    public function get_random_ad()
    {
        $ad = Ad::inRandomOrder()->first();
        return $ad->image;
    }


    public function get_cart_count($id)
    {
        return Cart::where('user_id', user()->id)->where('product_id', $id)->first()->count;
    }


    public function is_like($id)
    {
        return boolval(Wishlist::where('user_id', user()->id)->where('product_id', $id)->first());
    }


    public function api_is_like($id,$user_id)
    {
        return boolval(Wishlist::where('user_id', $user_id)->where('product_id', $id)->first());
    }


    public function codes()
    {
        return $this->hasMany(ProductInfo::class, 'product_id')->where('user_id','!=',NULL);
    }


    public function get_cover($id)
    {
        $image = ProductImage::where('product_id', $id)->select('image')->first();
        return $image->image;
    }


    public function get_images($id)
    {
        $images = ProductImage::where('product_id', $id)->select('image')->get();

        foreach($images as $image)
        {
           $image['image'] = 'https://'.$_SERVER['SERVER_NAME'].'/public/products/'.$image->image;
        }

        return $images;
    }


    public function api_get_merchant($lang,$id)
    {
        $merchant_id = Product::where('id', $id)->select('merchant_id')->first()->merchant_id;

        $merchant = Merchant::where('id', $merchant_id)->select($lang.'_name as name',$lang.'_desc as desc',$lang.'_address as address','lat','lng','link','map_link')->first();
        $merchant['link'] = isset($merchant->link) ? $merchant->link : '';
        $merchant['map_link'] = isset($merchant->map_link) ? $merchant->map_link : '';

        return $merchant;
    }


    public function api_get_rates($lang,$id)
    {
        Carbon::setLocale($lang);

        $rates = ProductRate::where('product_id', $id)->select('user_id','rate','text','created_at')->get();
        foreach($rates as $rate)
        {
            $rate['user'] = $rate->get_user($rate->user_id);
            $rate['date'] = $rate->created_at->diffForHumans();

            unset($rate->user_id,$rate->created_at);
        }

        return $rates;
    }


    public function get_address($lang,$id)
    {
        $name = Address::where('id', $id)->select($lang.'_name as name')->first()->name;
        return $name;
    }


    public function get_currency($id)
    {
        $country_id = Address::where('id', $id)->select('parent_id')->first()->parent_id;
        $currency = Address::where('id', $country_id)->select('ar_currency')->first()->ar_currency;

        return $currency;
    }


    public function api_status($lang,$status)
    {
        if($lang == 'ar')
        {
            $status == 'valid' ? $str = 'صالح' : $str = 'غير صالح';
        }
        else
        {
            $str = ucfirst($status);
        }

        return $str;
    }

}
