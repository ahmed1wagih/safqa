<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantTerm extends Model
{
    protected $table = 'merchant_terms';
}
